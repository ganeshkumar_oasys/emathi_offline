package com.oasys.emathi.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.emathi.Dto.ExpensesTypeDtoList;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.activity.NewDrawerScreen;

import java.util.ArrayList;

public class TransactionTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public TransactionTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EMathiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertTransSavingData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.SAVING_COUNT, shgDto.getSCount());
                values.put(TableConstants.LOGIN_SAV, "1");
                values.put(TableConstants.OFF_SAVING, (shgDto.getSavAmount() != null && shgDto.getSavAmount().length() > 0) ? shgDto.getSavAmount() : null);
                values.put(TableConstants.OFF_V_SAVING, (shgDto.getVSavAmount() != null && shgDto.getVSavAmount().length() > 0) ? shgDto.getVSavAmount() : null);
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.TLT_SAV_AMOUNT, (shgDto.getTotalSavAmount() != null && shgDto.getTotalSavAmount().length() > 0) ? shgDto.getTotalSavAmount() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : null);
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);

                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_NONE);

            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }
    public static void insertTransIncomeData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.TLT_INCOME_AMOUNT, (shgDto.getTotalIncomeAmount() != null && shgDto.getTotalIncomeAmount().length() > 0) ? shgDto.getTotalIncomeAmount() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                if (shgDto.getTxSubtype().equals(AppStrings.otherincome)) {
                    values.put(TableConstants.LOGIN_OTHERINCOME, "1");
                } else if (shgDto.getTxSubtype().equals(AppStrings.penalty)) {
                    values.put(TableConstants.LOGIN_PENALTY, "1");
                } else if (shgDto.getTxSubtype().equals(AppStrings.subscriptioncharges)) {
                    values.put(TableConstants.LOGIN_SUBSCRIPTION, "1");
                } else if (shgDto.getTxSubtype().equals(AppStrings.mSeedFund)) {
                    values.put(TableConstants.LOGIN_SEEDFUND, "1");
                } else {
                    values.put(TableConstants.LOGIN_DONATION, "1");
                }
                values.put(TableConstants.INCOME_COUNT, shgDto.getICount());
                values.put(TableConstants.OFF_INC_TYPE_ID, (shgDto.getIncomeTypeId() != null && shgDto.getIncomeTypeId().length() > 0) ? shgDto.getIncomeTypeId() : null);
                values.put(TableConstants.IC_EXP_AMOUNT, (shgDto.getIc_exp_Amount() != null && shgDto.getIc_exp_Amount().length() > 0) ? shgDto.getIc_exp_Amount() : null);
                values.put(TableConstants.OTHER_INCOME, (shgDto.getAmount() != null && shgDto.getAmount().length() > 0) ? shgDto.getAmount() : null);
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : null);
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                values.put(TableConstants.BANK_ID, (shgDto.getBankId() != null && shgDto.getBankId().length() > 0) ? shgDto.getBankId() : null);
                values.put(TableConstants.BANK_SB_AC_ID, (shgDto.getShgSavingsAccountId() != null && shgDto.getShgSavingsAccountId().length() > 0) ? shgDto.getShgSavingsAccountId() : null);
                values.put(TableConstants.BANKNAME, (shgDto.getSSelectedBank() != null && shgDto.getSSelectedBank().length() > 0) ? shgDto.getSSelectedBank() : null);

                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_NONE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransExpenseData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.OFF_EXP_TYPE_ID, (shgDto.getExpenseTypeId() != null && shgDto.getExpenseTypeId().length() > 0) ? shgDto.getExpenseTypeId() : null);
                values.put(TableConstants.EXPENSE_COUNT, shgDto.getECount());
                values.put(TableConstants.LOGIN_EXPENSE, "1");


                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.OFF_EXP_TYPE_NAME, (shgDto.getExpenseTypeName() != null && shgDto.getExpenseTypeName().length() > 0) ? shgDto.getExpenseTypeName() : null);
                values.put(TableConstants.TLT_EXP_AMOUNT, (shgDto.getTotalExpenseAmount() != null && shgDto.getTotalExpenseAmount().length() > 0) ? shgDto.getTotalExpenseAmount() : null);
                values.put(TableConstants.IC_EXP_AMOUNT, (shgDto.getIc_exp_Amount() != null && shgDto.getIc_exp_Amount().length() > 0) ? shgDto.getIc_exp_Amount() : null);
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransBTData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.BT_F_BANK_SBAC_ID, (shgDto.getBtFromSavingAcId() != null && shgDto.getBtFromSavingAcId().length() > 0) ? shgDto.getBtFromSavingAcId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.BT_COUNT, shgDto.getBtCount());
                values.put(TableConstants.BANK_SB_AC_ID, shgDto.getSavingsBankId());

                if (shgDto.getTxSubtype().equals(AppStrings.mAccountToAccountTransfer)) {
                    values.put(TableConstants.LOGIN_BT_ACTOAC, "1");
                } else if (shgDto.getTxSubtype().equals(AppStrings.fixedDeposit)) {
                    values.put(TableConstants.LOGIN_BT_FD, "1");
                } else if (shgDto.getTxSubtype().equals(AppStrings.recurringDeposit)) {
                    values.put(TableConstants.LOGIN_BT_RD, "1");
                } else if (shgDto.getTxSubtype().equals(AppStrings.bankTransaction)) {
                    values.put(TableConstants.LOGIN_BT_ENTRY, "1");
                }


                values.put(TableConstants.BT_T_BANK_SBAC_ID, (shgDto.getBtToSavingAcId() != null && shgDto.getBtToSavingAcId().length() > 0) ? shgDto.getBtToSavingAcId() : null);
             /*   values.put(TableConstants.OFF_F_ACCOUNT_NO, (shgDto.getFromAccountNumber() != null && shgDto.getFromAccountNumber().length() > 0) ? shgDto.getFromAccountNumber() : null);
                values.put(TableConstants.OFF_T_ACCOUNT_NO, (shgDto.getToAccountNumber() != null && shgDto.getToAccountNumber().length() > 0) ? shgDto.getToAccountNumber() : null);*/
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.BT_EXPENSE, (shgDto.getBtExpense() != null && shgDto.getBtExpense().length() > 0) ? shgDto.getBtExpense() : null);
                values.put(TableConstants.BT_WITHDRAW, (shgDto.getBtWithdraw() != null && shgDto.getBtWithdraw().length() > 0) ? shgDto.getBtWithdraw() : null);
                values.put(TableConstants.BT_CHARGE, (shgDto.getBtCharge() != null && shgDto.getBtCharge().length() > 0) ? shgDto.getBtCharge() : null);
                values.put(TableConstants.BT_F_NAME, (shgDto.getMFromBk() != null && shgDto.getMFromBk().length() > 0) ? shgDto.getMFromBk() : null);
                values.put(TableConstants.BT_T_NAME, (shgDto.getMToBk() != null && shgDto.getMToBk().length() > 0) ? shgDto.getMToBk() : null);
                values.put(TableConstants.OFF_LOAN_NAME, (shgDto.getMLoanName() != null && shgDto.getMLoanName().length() > 0) ? shgDto.getMLoanName() : null);
                values.put(TableConstants.BT_T_CHARGE, (shgDto.getTransferCharge() != null && shgDto.getTransferCharge().length() > 0) ? shgDto.getTransferCharge() : null);
                values.put(TableConstants.BT_T_Amount, (shgDto.getTransferAmount() != null && shgDto.getTransferAmount().length() > 0) ? shgDto.getTransferAmount() : null);
                values.put(TableConstants.BT_INT_SUB_RX, (shgDto.getBt_intSubventionRecieved() != null && shgDto.getBt_intSubventionRecieved().length() > 0) ? shgDto.getBt_intSubventionRecieved() : null);
                values.put(TableConstants.BT_INTEREST, (shgDto.getBtInterest() != null && shgDto.getBtInterest().length() > 0) ? shgDto.getBtInterest() : null);
                values.put(TableConstants.BT_DEPOSIT, (shgDto.getBtDeposit() != null && shgDto.getBtDeposit().length() > 0) ? shgDto.getBtDeposit() : null);
                values.put(TableConstants.BT_T_LOAN_ACID, (shgDto.getBtToLoanId() != null && shgDto.getBtToLoanId().length() > 0) ? shgDto.getBtToLoanId() : null);
                values.put(TableConstants.OFF_ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : null);
                values.put(TableConstants.OFF_LOAN_OS, (shgDto.getOutStanding() != null && shgDto.getOutStanding().length() > 0) ? shgDto.getOutStanding() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransMRData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.MR_COUNT, shgDto.getMrCount());
                values.put(TableConstants.LOGIN_MR_LOAN, "1");
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : null);
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : null);
                values.put(TableConstants.OFF_LOAN_ID, (shgDto.getLoanId() != null && shgDto.getLoanId().length() > 0) ? shgDto.getLoanId() : null);
                values.put(TableConstants.OFF_LOAN_NAME, (shgDto.getMLoanName() != null && shgDto.getMLoanName().length() > 0) ? shgDto.getMLoanName() : null);
                /*   values.put(TableConstants.OFF_LOAN_ACC_ID, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : null);*/
                values.put(TableConstants.OFF_ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : null);
                values.put(TableConstants.MEM_AMOUNT, (shgDto.getMem_amount() != null && shgDto.getMem_amount().length() > 0) ? shgDto.getMem_amount() : null);
                values.put(TableConstants.MEM_INT_CDUE, (shgDto.getMemCurrentDue() != null && shgDto.getMemCurrentDue().length() > 0) ? shgDto.getMemCurrentDue() : null);
                values.put(TableConstants.MEM_INTEREST, (shgDto.getMemInterest() != null && shgDto.getMemInterest().length() > 0) ? shgDto.getMemInterest() : null);
                values.put(TableConstants.OFF_MEM_OS, (shgDto.getOutStanding() != null && shgDto.getOutStanding().length() > 0) ? shgDto.getOutStanding() : null);
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void insertAttendanceData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.ATT_COUNT, shgDto.getAttCount());
                values.put(TableConstants.LOGIN_aATTD, "1");
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertMoMData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.MoM_COUNT, shgDto.getMomCount());
                values.put(TableConstants.LOGIN_MOM, "1");
                values.put(TableConstants.MOM_ID, (shgDto.getId() != null && shgDto.getId().length() > 0) ? shgDto.getId() : null);
                values.put(TableConstants.MoM_NAME, (shgDto.getMomName() != null && shgDto.getMomName().length() > 0) ? shgDto.getMomName() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransGRData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.LOGIN_GRP_LOAN, "1");
                values.put(TableConstants.GRP_COUNT, shgDto.getGrpCount());
                values.put(TableConstants.GRP_CHARGE, (shgDto.getGrp_charge() != null && shgDto.getGrp_charge().length() > 0) ? shgDto.getGrp_charge() : null);
                values.put(TableConstants.GRP_INT_SUB_RX, (shgDto.getGrp_intSubventionRecieved() != null && shgDto.getGrp_intSubventionRecieved().length() > 0) ? shgDto.getGrp_intSubventionRecieved() : null);
                values.put(TableConstants.GRP_REPAYMENT, (shgDto.getGrp_repayment() != null && shgDto.getGrp_repayment().length() > 0) ? shgDto.getGrp_repayment() : null);
                values.put(TableConstants.GRP_INTEREST, (shgDto.getGrp_interest() != null && shgDto.getGrp_interest().length() > 0) ? shgDto.getGrp_interest() : null);
                values.put(TableConstants.OFF_LOAN_OS, (shgDto.getOutStanding() != null && shgDto.getOutStanding().length() > 0) ? shgDto.getOutStanding() : null);
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.OFF_LOAN_ID, (shgDto.getLoanId() != null && shgDto.getLoanId().length() > 0) ? shgDto.getLoanId() : null);
                values.put(TableConstants.OFF_LOAN_NAME, (shgDto.getMLoanName() != null && shgDto.getMLoanName().length() > 0) ? shgDto.getMLoanName() : null);
                values.put(TableConstants.BT_T_NAME, (shgDto.getMToBk() != null && shgDto.getMToBk().length() > 0) ? shgDto.getMToBk() : null);
                values.put(TableConstants.BT_T_BANK_SBAC_ID, (shgDto.getShgSavingsAccountId() != null && shgDto.getShgSavingsAccountId().length() > 0) ? shgDto.getShgSavingsAccountId() : null);
                values.put(TableConstants.BT_CHARGE, (shgDto.getBankCharges() != null && shgDto.getBankCharges().length() > 0) ? shgDto.getBankCharges() : null);
                values.put(TableConstants.OFF_ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertUploadImageData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.UPLOADSCHEDULE_COUNT, shgDto.getUploadCount());
                values.put(TableConstants.LOGIN_UPLOADIMAGE, "1");
                values.put(TableConstants.UPLOAD_URL, shgDto.getImageurl());

                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_NONE);

            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }

    public static void insertAuditingData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.LOGIN_AUDIT, "1");
                values.put(TableConstants.AUDIT_COUNT, shgDto.getAuditcount());
                values.put(TableConstants.OFF_FROMDATE, (shgDto.getFromDate() != null && shgDto.getFromDate().length() > 0) ? shgDto.getFromDate() : null);
                values.put(TableConstants.OFF_AUDITDATE, (shgDto.getToDate() != null && shgDto.getToDate().length() > 0) ? shgDto.getToDate() : null);
                values.put(TableConstants.OFF_TODATE, (shgDto.getAuditingDate() != null && shgDto.getAuditingDate().length() > 0) ? shgDto.getAuditingDate() : null);
                values.put(TableConstants.OFF_AUDITTNAME, (shgDto.getAuditorName() != null && shgDto.getAuditorName().length() > 0) ? shgDto.getAuditorName() : null);

                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_NONE);

            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }

    public static void insertTrainingData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                values.put(TableConstants.LOGIN_TRAINING, "1");
                values.put(TableConstants.TRAINING_COUNT, shgDto.getTrainingcount());
                values.put(TableConstants.OFF_TRAININGDATE, (shgDto.getTrainingDate() != null && shgDto.getTrainingDate().length() > 0) ? shgDto.getTrainingDate() : null);
                values.put(TableConstants.OFF_TRAININGLISTID, (shgDto.getId() != null && shgDto.getId().length() > 0) ? shgDto.getId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_NONE);

            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }


    public static ArrayList<OfflineDto> getGrpRepayment(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.GROUP_LOAN_REPAYMENT + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.GRP_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setGrp_charge(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_CHARGE)));
                    bD.setGrp_intSubventionRecieved(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_INT_SUB_RX)));
                    bD.setGrp_repayment(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_REPAYMENT)));
                    bD.setGrp_interest(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_INTEREST)));
                    bD.setGrp_os(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));
                    bD.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_ACCOUNT_NO)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_BANK_SBAC_ID)));
                    bD.setBankCharges(cursor.getString(cursor.getColumnIndex(TableConstants.BT_CHARGE)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setModifiedDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODIFIEDDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }



    public static ArrayList<OfflineDto> getAudit(int count) {

        ArrayList<OfflineDto> auditdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setAuditcount(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_COUNT)));
                    bD.setFromDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_FROMDATE)));
                    bD.setToDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_TODATE)));
                    bD.setAuditingDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_AUDITDATE)));
                    bD.setAuditorName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_AUDITTNAME)));
                    auditdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return auditdetails;
    }


    public static ArrayList<OfflineDto> getTraining(int count) {

        ArrayList<OfflineDto> trainingdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setTrainingcount(cursor.getString(cursor.getColumnIndex(TableConstants.TRAINING_COUNT)));
                    bD.setTrainingDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_TRAININGDATE)));
                    bD.setId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_TRAININGLISTID)));
                    trainingdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return trainingdetails;
    }


    public static ArrayList<OfflineDto> getUploadImages(int count) {

        ArrayList<OfflineDto> uploadimages = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setUploadCount(cursor.getString(cursor.getColumnIndex(TableConstants.UPLOADSCHEDULE_COUNT)));
                    bD.setImageurl(cursor.getString(cursor.getColumnIndex(TableConstants.UPLOAD_URL)));
                    uploadimages.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return uploadimages;
    }

    public static void insertTransILData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, true);
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : null);
                values.put(TableConstants.SYNC_STATUS, "N");
                if (shgDto.getTxSubtype().equals(AppStrings.InternalLoan)) {
                    values.put(TableConstants.MR_IL_COUNT, shgDto.getMrCount());
                    values.put(TableConstants.LOGIN_MR_IL, "1");
                } else {
                    values.put(TableConstants.LD_COUNT, shgDto.getLdCount());
                    values.put(TableConstants.LOGIN_LD_IL, "1");
                }
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : null);
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : null);
                values.put(TableConstants.MEM_AMOUNT, (shgDto.getMem_amount() != null && shgDto.getMem_amount().length() > 0) ? shgDto.getMem_amount() : null);
                values.put(TableConstants.OFF_LOAN_AMOUNT, (shgDto.getInternalLoanAmount() != null && shgDto.getInternalLoanAmount().length() > 0) ? shgDto.getInternalLoanAmount() : null);
                values.put(TableConstants.MEM_INTEREST, (shgDto.getMemInterest() != null && shgDto.getMemInterest().length() > 0) ? shgDto.getMemInterest() : null);
                values.put(TableConstants.OFF_MEM_OS, (shgDto.getMem_os() != null && shgDto.getMem_os().length() > 0) ? shgDto.getMem_os() : null);
                values.put(TableConstants.OFF_TENURE, (shgDto.getTenure() != null && shgDto.getTenure().length() > 0) ? shgDto.getTenure() : null);
                values.put(TableConstants.OFF_POL, (shgDto.getPolName() != null && shgDto.getPolName().length() > 0) ? shgDto.getPolName() : null);
                values.put(TableConstants.OFF_POL_ID, (shgDto.getPloanTypeId() != null && shgDto.getPloanTypeId().length() > 0) ? shgDto.getPloanTypeId() : null);
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : null);
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : null);
                values.put(TableConstants.OFF_MOBILEDATE, System.currentTimeMillis() + "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : null);
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : null);
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : null);
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : null);
                values.put(TableConstants.OFF_MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : null);
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : null);
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static ArrayList<OfflineDto> getLD_IL_Transaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.LOAN_DISBURSEMENT + "'AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.LD_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setInternalLoanAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_AMOUNT)));
                    bD.setTenure(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_TENURE)));
                    bD.setPolName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_POL)));
                    bD.setPloanTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_POL_ID)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    //  bD.setLoanUuid(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));
                    bD.setMem_os(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MEM_OS)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static void updateLoginflag(String SHGiD) {
        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.LOGIN_SAV, "0");
            values.put(TableConstants.LOGIN_SUBSCRIPTION, "0");
            values.put(TableConstants.LOGIN_OTHERINCOME, "0");
            values.put(TableConstants.LOGIN_PENALTY, "0");
            values.put(TableConstants.LOGIN_SEEDFUND, "0");
            values.put(TableConstants.LOGIN_DONATION, "0");
            values.put(TableConstants.LOGIN_GRP_LOAN, "0");
            values.put(TableConstants.LOGIN_MR_LOAN, "0");
            values.put(TableConstants.LOGIN_MR_IL, "0");
            values.put(TableConstants.LOGIN_LD_IL, "0");
            values.put(TableConstants.LOGIN_EXPENSE, "0");
            values.put(TableConstants.LOGIN_aATTD, "0");
            values.put(TableConstants.LOGIN_MOM, "0");
            values.put(TableConstants.LOGIN_BT_ENTRY, "0");
            values.put(TableConstants.LOGIN_BT_FD, "0");
            values.put(TableConstants.LOGIN_BT_RD, "0");
            values.put(TableConstants.LOGIN_BT_ACTOAC, "0");
            database.update(TableName.TABLE_TRANSACTION, values, TableConstants.SHG_ID + " = '" + SHGiD + "'", null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
    }


    public static ArrayList<OfflineDto> getLoginFlag(String txSubtype) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_SUB_TYPE + " LIKE '" + txSubtype + "')";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));

                    if (txSubtype.equals(AppStrings.savings)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_SAV)));
                    } else if (txSubtype.equals(AppStrings.expenses)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_EXPENSE)));
                    } else if (txSubtype.equals(AppStrings.subscriptioncharges)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_SUBSCRIPTION)));
                    } else if (txSubtype.equals(AppStrings.otherincome)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_OTHERINCOME)));
                    } else if (txSubtype.equals(AppStrings.penalty)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_PENALTY)));
                    } else if (txSubtype.equals(AppStrings.mSeedFund)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_SEEDFUND)));
                    } else if (txSubtype.equals(AppStrings.donation)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_DONATION)));
                    } else if (txSubtype.equals(AppStrings.bankTransaction)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_BT_ENTRY)));
                    } else if (txSubtype.equals(AppStrings.mAccountToAccountTransfer)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_BT_ACTOAC)));
                    } else if (txSubtype.equals(AppStrings.fixedDeposit)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_BT_FD)));
                    } else if (txSubtype.equals(AppStrings.recurringDeposit)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_BT_RD)));
                    } else if (txSubtype.equals(AppStrings.MinutesofMeeting)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_MOM)));
                    } else if (txSubtype.equals(AppStrings.Attendance)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_aATTD)));
                    } else if (txSubtype.equals(AppStrings.memberloanrepayment)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_MR_LOAN)));
                    } else if (txSubtype.equals(AppStrings.InternalLoan)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_MR_IL)));
                    } else if (txSubtype.equals(AppStrings.InternalLoanDisbursement)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_LD_IL)));
                    } else if (txSubtype.equals(AppStrings.grouploanrepayment)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_GRP_LOAN)));
                    } else if (txSubtype.equals(AppStrings.uploadschedule)) {
                        bD.setLoginFlag(cursor.getString(cursor.getColumnIndex(TableConstants.LOGIN_UPLOADIMAGE)));
                    }

                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


    public static ArrayList<OfflineDto> getAttendanceData(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.ATTENDANCE + "'AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.ATT_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getMoMeData(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.MINUTES_OF_MEETINGS + "'AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.MoM_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setId(cursor.getString(cursor.getColumnIndex(TableConstants.MOM_ID)));
                    bD.setMomName(cursor.getString(cursor.getColumnIndex(TableConstants.MoM_NAME)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


    public static ArrayList<OfflineDto> getBankName(String shgId) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_BANK_DETAILS + " where " + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.ACCOUNT_NO)));

                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getSavingTransaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.SAVINGS + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.SAVING_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setVoluntarySavingsAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_V_SAVING)));
                    bD.setSavingsAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_SAVING)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTotalSavAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_SAV_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getIncomeTransaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.INCOME + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.INCOME_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setIncomeTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_INC_TYPE_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    if (cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)).equals(AppStrings.mSeedFund) || cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)).equals(AppStrings.donation)) {
                        bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    } else {
                        bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.IC_EXP_AMOUNT)));
                    }
                    bD.setTotalIncomeAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    bD.setOtherIncome(cursor.getString(cursor.getColumnIndex(TableConstants.OTHER_INCOME)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_SB_AC_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getExpenseTransaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.EXPENCE + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.EXPENSE_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setExpenseTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_ID)));
                    bD.setExpenseTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_NAME)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    //  bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.IC_EXP_AMOUNT)));
                    bD.setTotalExpenseAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_EXP_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getBankTransaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.BANK_TRANSACTION + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.BT_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    //bD.setSavingsBankDetailsId("");
                    bD.setSavingsBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_SB_AC_ID)));
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setFromSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setToSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_BANK_SBAC_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setTransferCharge(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_CHARGE)));
                    bD.setTransferAmount(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_Amount)));

                    bD.setSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setLoanBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_LOAN_ACID)));

                    bD.setBtExpense(cursor.getString(cursor.getColumnIndex(TableConstants.BT_EXPENSE)));
                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setBankInterest(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INTEREST)));
                    bD.setBankCharges(cursor.getString(cursor.getColumnIndex(TableConstants.BT_CHARGE)));
                    bD.setWithdrawal(cursor.getString(cursor.getColumnIndex(TableConstants.BT_WITHDRAW)));
                    bD.setCashDeposit(cursor.getString(cursor.getColumnIndex(TableConstants.BT_DEPOSIT)));
                    bD.setInterestSubventionReceived(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INT_SUB_RX)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getMem_LR_Transaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.MEMBER_LOAN_REPAYMENT + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.MR_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setRepaymentAmount(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_AMOUNT)));
                    bD.setInterest(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_INTEREST)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setLoanUuid(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));
                    bD.setMem_os(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MEM_OS)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getMem_LR_IL_Transaction(int count) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.TRANSACTION_TYPE + " LIKE '" + NewDrawerScreen.MEMBER_LOAN_REPAYMENT + "' AND " + TableConstants.SYNC_STATUS + " LIKE 'N'" + " AND " + TableConstants.MR_IL_COUNT + " LIKE '" + count + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setRepaymentAmount(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_AMOUNT)));
                    bD.setInterest(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_INTEREST)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setLoanUuid(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));
                    bD.setMem_os(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MEM_OS)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


    public static ArrayList<String> getOfflineREPORTbyTxDates(String shgId) {

        ArrayList<String> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT Distinct lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "') ORDER BY lastTransactionDateTime DESC";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    String bD = new String();

                    bD = cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getOfflineReportMenu(String shgId, String datetime) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            //  String selectQuery = "SELECT Distinct lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            String selectQuery = "SELECT Distinct TRANSACTION_TYPE,TRANSACTION_SUB_TYPE,lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "' AND " + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " LIKE " + datetime + ")";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();

                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));

                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getOfflineSubTx(String shgId, String txSubtype) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            //  String selectQuery = "SELECT Distinct lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "' AND " + TableConstants.TRANSACTION_SUB_TYPE + " LIKE '" + txSubtype + "')";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();

                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));

                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


    public static ArrayList<OfflineDto> getOfflineRecords(String shgId, String datetime, String txtype) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            //  String selectQuery = "SELECT Distinct lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            String selectQuery = "SELECT Distinct TRANSACTION_SUB_TYPE,TRANSACTION_TYPE,lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "' AND " + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " LIKE " + datetime + " AND " + TableConstants.TRANSACTION_TYPE + " LIKE '" + txtype + "')";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();

                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));


                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }

    public static ArrayList<OfflineDto> getOfflineReportSubMenu(String shgId, String datetime, String txtype) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            //  String selectQuery = "SELECT Distinct lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            String selectQuery = "SELECT Distinct TRANSACTION_SUB_TYPE,TRANSACTION_TYPE,loanName,accountNumber,loanId,lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "' AND " + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " LIKE " + datetime + " AND " + TableConstants.TRANSACTION_TYPE + " LIKE '" + txtype + "')";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setMLoanName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_NAME)));
                    bD.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_ACCOUNT_NO)));
                    bD.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));

                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


    public static ArrayList<MemberList> getOfflineFinalUniqueReport(String shgId, String dateStr, String txType) {

        ArrayList<MemberList> arrMemberDetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT Distinct memberId,memberName FROM TABLE_TRANSACTION WHERE shg_Id LIKE '" + shgId + "' AND lastTransactionDateTime LIKE '" + dateStr + "' AND TRANSACTION_TYPE LIKE '" + txType + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    MemberList bD = new MemberList();
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    arrMemberDetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrMemberDetails;

    }

    public static ArrayList<OfflineDto> getOfflineFinalBT_Report(String shgId, String dateStr, String txType, String subtype) {

        ArrayList<OfflineDto> arrMemberDetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM TABLE_TRANSACTION WHERE shg_Id LIKE '" + shgId + "' AND lastTransactionDateTime LIKE '" + dateStr + "' AND TRANSACTION_TYPE LIKE '" + txType + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setSSelectedBank(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));
                    bD.setBtFromSavingAcId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setBtToSavingAcId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_BANK_SBAC_ID)));
                    bD.setBtCharge(cursor.getString(cursor.getColumnIndex(TableConstants.BT_CHARGE)));
                    bD.setBtExpense(cursor.getString(cursor.getColumnIndex(TableConstants.BT_EXPENSE)));
                    bD.setBtWithdraw(cursor.getString(cursor.getColumnIndex(TableConstants.BT_WITHDRAW)));
                    bD.setBtDeposit(cursor.getString(cursor.getColumnIndex(TableConstants.BT_DEPOSIT)));
                    bD.setBtInterest(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INTEREST)));
                    bD.setMLoanName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_NAME)));
                    bD.setMFromBk(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_NAME)));
                    bD.setMToBk(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_NAME)));
                    bD.setBtToLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_LOAN_ACID)));
                    bD.setTransferAmount(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_Amount)));
                    bD.setTransferCharge(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_CHARGE)));
                    bD.setBt_intSubventionRecieved(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INT_SUB_RX)));
                    arrMemberDetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrMemberDetails;

    }


    public static ArrayList<OfflineDto> getOfflineFinalGrp_Report(String shgId, String dateStr, String txType, String subtype) {

        ArrayList<OfflineDto> arrMemberDetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM TABLE_TRANSACTION WHERE shg_Id LIKE '" + shgId + "' AND lastTransactionDateTime LIKE '" + dateStr + "' AND TRANSACTION_TYPE LIKE '" + txType + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setGrp_intSubventionRecieved(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_INT_SUB_RX)));
                    bD.setGrp_charge(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_CHARGE)));
                    bD.setMToBk(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_NAME)));
                    bD.setGrp_interest(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_INTEREST)));
                    bD.setGrp_repayment(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_REPAYMENT)));
                    bD.setBankCharges(cursor.getString(cursor.getColumnIndex(TableConstants.BT_CHARGE)));

                    arrMemberDetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrMemberDetails;

    }


    public static ArrayList<OfflineDto> getOfflineFinalSF_Report(String shgId, String dateStr, String txType, String subtype) {

        ArrayList<OfflineDto> arrMemberDetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM TABLE_TRANSACTION WHERE shg_Id LIKE '" + shgId + "' AND lastTransactionDateTime LIKE '" + dateStr + "' AND TRANSACTION_TYPE LIKE '" + txType + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setSSelectedBank(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));
                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_SB_AC_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setOtherIncome(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    arrMemberDetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrMemberDetails;

    }

    public static OfflineDto getOfflineFinalDON_Report(String shgId, String dateStr, String txType, String subtype) {

        OfflineDto bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT  SUM(totalIncomeAmount) as s FROM TABLE_TRANSACTION WHERE shg_Id LIKE '" + shgId + "' AND lastTransactionDateTime LIKE '" + dateStr + "' AND TRANSACTION_TYPE LIKE '" + txType + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    bD = new OfflineDto();
                    bD.setAmount(cursor.getString(cursor.getColumnIndex("s")));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }


    public static MemberList getOfflineFinalUniqueReport(String memId) {
        MemberList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT memberId,memberName, SUM(savings) as s, SUM(vSavings) as vs from TABLE_TRANSACTION where memberId LIKE '" + memId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new MemberList();
                do {
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setSavingsAmount(cursor.getString(cursor.getColumnIndex("s")));
                    bD.setVoluntarySavingsAmount(cursor.getString(cursor.getColumnIndex("vs")));
                } while (cursor.moveToNext());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }

    public static MemberList getOfflineLoanDisbursement(String memId) {
        MemberList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT memberId,memberName, SUM(amount) as s from TABLE_TRANSACTION where memberId LIKE '" + memId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new MemberList();
                do {
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAmount(cursor.getString(cursor.getColumnIndex("s")));
                } while (cursor.moveToNext());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }



    public static ExpensesTypeDtoList getOfflineFinalEXP_UniqueReport(String expId) {
        ExpensesTypeDtoList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT expenseTypeId,expenseTypeName, SUM(ic_exp_Amount) as s from TABLE_TRANSACTION where expenseTypeId LIKE '" + expId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new ExpensesTypeDtoList();
                do {
                    bD.setExpensesTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_ID)));
                    bD.setExpensesTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_NAME)));
                    bD.setAmount(cursor.getString(cursor.getColumnIndex("s")));
                } while (cursor.moveToNext());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }


    public static MemberList getOffFinalIncomeReport(String memId, String subtype) {
        MemberList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT memberId,memberName, SUM(ic_exp_Amount) as s from TABLE_TRANSACTION where memberId LIKE '" + memId + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new MemberList();
                do {

                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAmount(cursor.getString(cursor.getColumnIndex("s")));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }

    public static MemberList getOffFinalMRReport(String memId, String subtype, String acno) {
        MemberList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT memberId,memberName, SUM(mem_amount) as s, SUM(memInterest) as i from TABLE_TRANSACTION where memberId LIKE '" + memId + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "' AND  loanId LIKE '" + acno + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new MemberList();
                do {

                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setMemAmount(cursor.getString(cursor.getColumnIndex("s")));
                    bD.setMemInterest(cursor.getString(cursor.getColumnIndex("i")));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }

    public static MemberList getOffFinalILReport(String memId, String subtype) {
        MemberList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT memberId,memberName, SUM(mem_amount) as s, SUM(memInterest) as i from TABLE_TRANSACTION where memberId LIKE '" + memId + "' AND TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new MemberList();
                do {

                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setMemAmount(cursor.getString(cursor.getColumnIndex("s")));
                    bD.setMemInterest(cursor.getString(cursor.getColumnIndex("i")));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }


    public static MemberList getOffFinalIncomeOthersReport(String subtype) {
        MemberList bD = null;
        try {
            openDatabase();
            String selectQuery = "SELECT SUM(otherIncome) as s from TABLE_TRANSACTION where TRANSACTION_SUB_TYPE LIKE '" + subtype + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                bD = new MemberList();
                do {
                    bD.setAmount(cursor.getString(cursor.getColumnIndex("s")));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bD;

    }


    public static ArrayList<OfflineDto> getOfflineFinalReport(String shgId, String dateStr, String txType) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            //  String selectQuery = "SELECT Distinct lastTransactionDateTime FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "' AND " + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " LIKE '" + dateStr + "' AND " + TableConstants.TRANSACTION_TYPE + " LIKE '" + txType + "')";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setVoluntarySavingsAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_V_SAVING)));
                    bD.setSavingsAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_SAVING)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTotalSavAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_SAV_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setIncomeTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_INC_TYPE_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    if (cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)).equals(AppStrings.mSeedFund) || cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)).equals(AppStrings.donation)) {
                        bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    } else {
                        bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.IC_EXP_AMOUNT)));
                    }
                    bD.setTotalIncomeAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_SB_AC_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setFromSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setToSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_BANK_SBAC_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setTransferCharge(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_CHARGE)));
                    bD.setTransferAmount(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_Amount)));

                    bD.setSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setLoanBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_LOAN_ACID)));

                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setBankInterest(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INTEREST)));
                    bD.setBankCharges(cursor.getString(cursor.getColumnIndex(TableConstants.BT_CHARGE)));
                    bD.setWithdrawal(cursor.getString(cursor.getColumnIndex(TableConstants.BT_WITHDRAW)));
                    bD.setCashDeposit(cursor.getString(cursor.getColumnIndex(TableConstants.BT_DEPOSIT)));
                    bD.setInterestSubventionReceived(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INT_SUB_RX)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setExpenseTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_ID)));
                    bD.setExpenseTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_NAME)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    //  bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.IC_EXP_AMOUNT)));
                    bD.setOtherIncome(cursor.getString(cursor.getColumnIndex(TableConstants.OTHER_INCOME)));
                    bD.setTotalExpenseAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_EXP_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setRepaymentAmount(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_AMOUNT)));
                    bD.setInterest(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_INTEREST)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setLoanUuid(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));
                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }



   /* public static ArrayList<OfflineDto> getOfflinereports(String shgId, String subMenu) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_TRANSACTION + " where (" + TableConstants.SHG_ID + " LIKE '" + shgId + "' AND " + TableConstants.TRANSACTION_SUB_TYPE + " LIKE '" + subMenu + "')";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setVoluntarySavingsAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_V_SAVING)));
                    bD.setSavingsAmount(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_SAVING)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTotalSavAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_SAV_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setIncomeTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_INC_TYPE_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    if (cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)).equals(AppStrings.mSeedFund) || cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)).equals(AppStrings.donation)) {
                        bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    } else {
                        bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.IC_EXP_AMOUNT)));
                    }
                    bD.setTotalIncomeAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_INCOME_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_SB_AC_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setFromSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setToSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_BANK_SBAC_ID)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setTransferCharge(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_CHARGE)));
                    bD.setTransferAmount(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_Amount)));

                    bD.setSavingsBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setLoanBankDetailsId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_T_LOAN_ACID)));

                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BT_F_BANK_SBAC_ID)));
                    bD.setBankInterest(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INTEREST)));
                    bD.setBankCharges(cursor.getString(cursor.getColumnIndex(TableConstants.BT_CHARGE)));
                    bD.setWithdrawal(cursor.getString(cursor.getColumnIndex(TableConstants.BT_WITHDRAW)));
                    bD.setCashDeposit(cursor.getString(cursor.getColumnIndex(TableConstants.BT_DEPOSIT)));
                    bD.setInterestSubventionReceived(cursor.getString(cursor.getColumnIndex(TableConstants.BT_INT_SUB_RX)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setExpenseTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_ID)));
                    bD.setExpenseTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_EXP_TYPE_NAME)));
                    bD.setModeOCash(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MODE_OF_CASH)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    //  bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setAmount(cursor.getString(cursor.getColumnIndex(TableConstants.IC_EXP_AMOUNT)));
                    bD.setTotalExpenseAmount(cursor.getString(cursor.getColumnIndex(TableConstants.TLT_EXP_AMOUNT)));
                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    bD.setRepaymentAmount(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_AMOUNT)));
                    bD.setInterest(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_INTEREST)));
                    bD.setLastTransactionDateTime(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LASTTRANSACTIONDATE_TIME)));
                    bD.setTxType(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_TYPE)));
                    bD.setTxSubtype(cursor.getString(cursor.getColumnIndex(TableConstants.TRANSACTION_SUB_TYPE)));
                    bD.setLoanUuid(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));

                    bD.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    bD.setCashInhand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    bD.setAnimatorId(cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_ID)));
                    bD.setMobileDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MOBILEDATE)));

                    // bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));


                    arrBankdetails.add(bD);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }*/


    public static void deleteTransactionTable() {
        try {
            MySharedPreference.writeBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, false);
            openDatabase();
            database.delete(TableName.TABLE_TRANSACTION, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
