package com.oasys.emathi.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.oasys.emathi.Dto.BranchList;
import com.oasys.emathi.Dto.LoanBankDto;
import com.oasys.emathi.Dto.ResponseContent;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.EMathiApplication;

import java.util.ArrayList;
import java.util.List;

public class BankBranchTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;
    public static  List<String> bankname;
    public static  List<String> jandhanbankname;
    public static  List<String> bankname_id;
    public static  List<String> jandhanbankname_id;
    public static  List<String> branchnames_id;
    public static  List<String> jandhanbranchnames_id;
    public static  List<LoanBankDto> bankList;
    public static  List<LoanBankDto> jandhanbankList;
    public static  List<BranchList> branches;
    public static  List<BranchList> jandhanbranches;
    public BankBranchTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EMathiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void inserBranchBankData(ResponseDto responseDto) {

        if(responseDto != null)
        {
            try {

                openDatabase();
                ContentValues values = new ContentValues();
                ResponseContent s = responseDto.getResponseContent();

                for (int i = 0; i < s.getBankNamesList().size(); i++) {

                    values.put(TableConstants.MEM_BANK_ID, (s.getBankNamesList().get(i).getId() != null && s.getBankNamesList().get(i).getId().length() > 0) ? s.getBankNamesList().get(i).getId() : "");
                    values.put(TableConstants.MEM_BANK_NAME, (s.getBankNamesList().get(i).getName() != null && s.getBankNamesList().get(i).getName().length() > 0) ? s.getBankNamesList().get(i).getName() : "");
//                    values.put(TableConstants.MEM_BANK_ID, s.getBankNamesList().get(i).getId());
//                    values.put(TableConstants.MEM_BANK_NAME, s.getBankNamesList().get(i).getName());

                    for (int j = 0; j <s.getBankNamesList().get(i).getBranchList().size(); j++) {

                        values.put(TableConstants.MEM_BRANCHNAME_ID, (s.getBankNamesList().get(i).getBranchList().get(j).getBranchId() != null && s.getBankNamesList().get(i).getBranchList().get(j).getBranchId().length() > 0) ? s.getBankNamesList().get(i).getBranchList().get(j).getBranchId() : "");
                        values.put(TableConstants.MEM_BRANCHNAME, (s.getBankNamesList().get(i).getBranchList().get(j).getBranchName() != null && s.getBankNamesList().get(i).getBranchList().get(j).getBranchName().length() > 0) ? s.getBankNamesList().get(i).getBranchList().get(j).getBranchName() : "");
//                         values.put(TableConstants.MEM_BRANCHNAME_ID, s.getBankNamesList().get(i).getBranchList().get(j).getBranchId());
//                        values.put(TableConstants.MEM_BRANCHNAME, s.getBankNamesList().get(i).getBranchList().get(j).getBranchName());
                        database.insertWithOnConflict(TableName.TABLE_MEMBER_BANKFULLDETAILS, TableConstants.MEM_BANK_ID, values, SQLiteDatabase.CONFLICT_REPLACE);

                    }
                }
            }
            catch (Exception e)
            {
                Log.e("insertEnergyException", e.toString());
            }
            finally {
                closeDatabase();
            }

        }

    }

    public static void insertJandhanBranchBankData(ResponseDto responseDto) {

        if(responseDto != null)
        {
            try {

                openDatabase();
                ContentValues values = new ContentValues();
                ResponseContent s = responseDto.getResponseContent();

                for (int i = 0; i < s.getBankNamesList().size(); i++) {

                    values.put(TableConstants.JAN_BANK_ID, (s.getBankNamesList().get(i).getId() != null && s.getBankNamesList().get(i).getId().length() > 0) ? s.getBankNamesList().get(i).getId() : "");
                    values.put(TableConstants.JAN_BANK_NAME, (s.getBankNamesList().get(i).getName() != null && s.getBankNamesList().get(i).getName().length() > 0) ? s.getBankNamesList().get(i).getName() : "");

                    for (int j = 0; j <s.getBankNamesList().get(i).getBranchList().size(); j++) {

                        values.put(TableConstants.JAN_BRANCHNAME_ID, (s.getBankNamesList().get(i).getBranchList().get(j).getBranchId() != null && s.getBankNamesList().get(i).getBranchList().get(j).getBranchId().length() > 0) ? s.getBankNamesList().get(i).getBranchList().get(j).getBranchId() : "");
                        values.put(TableConstants.JAN_BRANCHNAME, (s.getBankNamesList().get(i).getBranchList().get(j).getBranchName() != null && s.getBankNamesList().get(i).getBranchList().get(j).getBranchName().length() > 0) ? s.getBankNamesList().get(i).getBranchList().get(j).getBranchName() : "");
                        database.insertWithOnConflict(TableName.TABLE_JANDHAN_BANKNAMEDETAILS, TableConstants.JAN_BANK_ID, values, SQLiteDatabase.CONFLICT_REPLACE);

                    }
                }
            }
            catch (Exception e)
            {
                Log.e("insertEnergyException", e.toString());
            }
            finally {
                closeDatabase();
            }

        }

    }


    public static List<LoanBankDto> getBankname()
    {
        bankList = new ArrayList<>();

        try {

            openDatabase();
//            String selectQuery = " SELECT " + TableConstants.MEM_BANK_ID + TableConstants.MEM_BANK_NAME + " FROM " + TableName.TABLE_MEMBER_BANKFULLDETAILS;
            String selectQuery = " SELECT * FROM "  + TableName.TABLE_MEMBER_BANKFULLDETAILS + " GROUP BY " + TableConstants.MEM_BANK_NAME;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    LoanBankDto loanBankDto = new LoanBankDto();
                    loanBankDto.setId(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_BANK_ID)));
                    loanBankDto.setName(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_BANK_NAME)));

                    if (loanBankDto.getId() != null && loanBankDto.getId().length() > 0)
                    {
                        bankList.add(loanBankDto);
                    }

                } while (cursor.moveToNext());

            }


            bankname =new ArrayList<>();
            bankname_id =new ArrayList<>();
            for (int j = 0; j <bankList.size() ; j++) {

                bankname.add(bankList.get(j).getName());
                bankname_id.add(bankList.get(j).getId());

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return bankList;
    }


    public static List<LoanBankDto> getJandhanBankname()
    {
        jandhanbankList = new ArrayList<>();

        try {

            openDatabase();
            String selectQuery = " SELECT * FROM "  + TableName.TABLE_JANDHAN_BANKNAMEDETAILS + " GROUP BY " + TableConstants.JAN_BANK_NAME;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    LoanBankDto loanBankDto = new LoanBankDto();
                    loanBankDto.setId(cursor.getString(cursor.getColumnIndex(TableConstants.JAN_BANK_ID)));
                    loanBankDto.setName(cursor.getString(cursor.getColumnIndex(TableConstants.JAN_BANK_NAME)));

                    if (loanBankDto.getId() != null && loanBankDto.getId().length() > 0)
                    {
                        jandhanbankList.add(loanBankDto);
                    }

                } while (cursor.moveToNext());

            }


            jandhanbankname =new ArrayList<>();
            jandhanbankname_id =new ArrayList<>();
            for (int j = 0; j <jandhanbankList.size() ; j++) {

                jandhanbankname.add(jandhanbankList.get(j).getName());
                jandhanbankname_id.add(jandhanbankList.get(j).getId());

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return jandhanbankList;
    }



    public static List<BranchList> getBranchList(String bank_id) {
        branches = new ArrayList<>();
        if (bank_id != null && bank_id.length() > 0) {
            try {

                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.TABLE_MEMBER_BANKFULLDETAILS + " where " + TableConstants.MEM_BANK_ID + " = '" + bank_id + "'";
                // String selectQuery = "SELECT  " + TableConstants.MEM_BRANCHNAME +","+ TableConstants.MEM_BRANCHNAME_ID  + " FROM " + TableName.TABLE_MEMBER_BANKFULLDETAILS + " where " + TableConstants.MEM_BANK_ID + " = '" + bank_id + "'";
                //  String selectQuery = " SELECT " + TableConstants.MEM_BRANCHNAME_ID  +  TableConstants.MEM_BRANCHNAME + " FROM " + TableName.TABLE_MEMBER_BANKFULLDETAILS + " where " + TableConstants.MEM_BANK_ID + " = '" + bank_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {


                    do {
                        BranchList branchList = new BranchList();

                        branchList.setBranchId(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_BRANCHNAME_ID)));
                        branchList.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_BRANCHNAME)));

                        if (branchList.getBranchId() != null && branchList.getBranchId().length() > 0)
                        {
                            branches.add(branchList);
                        }

                    } while (cursor.moveToNext());
                }


                branchnames_id = new ArrayList<>();
                for (int j = 0; j <branches.size() ; j++) {

                    branchnames_id.add(branches.get(j).getBranchId());

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return branches;
    }


    public static List<BranchList> getJandhanBranchList(String bank_id) {
        jandhanbranches = new ArrayList<>();
        if (bank_id != null && bank_id.length() > 0) {
            try {

                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.TABLE_JANDHAN_BANKNAMEDETAILS + " where " + TableConstants.JAN_BANK_ID + " = '" + bank_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {


                    do {
                        BranchList branchList = new BranchList();

                        branchList.setBranchId(cursor.getString(cursor.getColumnIndex(TableConstants.JAN_BRANCHNAME_ID)));
                        branchList.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.JAN_BRANCHNAME)));

                        if (branchList.getBranchId() != null && branchList.getBranchId().length() > 0)
                        {
                            jandhanbranches.add(branchList);
                        }

                    } while (cursor.moveToNext());
                }


                jandhanbranchnames_id = new ArrayList<>();
                for (int j = 0; j <jandhanbranches.size() ; j++) {

                    jandhanbranchnames_id.add(jandhanbranches.get(j).getBranchId());

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return jandhanbranches;
    }



}