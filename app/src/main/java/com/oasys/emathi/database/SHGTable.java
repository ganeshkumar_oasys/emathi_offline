package com.oasys.emathi.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.emathi.Dto.BankTransaction;
import com.oasys.emathi.Dto.CashOfGroup;
import com.oasys.emathi.Dto.Donation;
import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.FDBankTransaction;
import com.oasys.emathi.Dto.GroupLoanRepayment;
import com.oasys.emathi.Dto.IncomeDisbursement;
import com.oasys.emathi.Dto.InternalLoanDisbursement;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MeetingExpense;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.OtherExpense;
import com.oasys.emathi.Dto.OtherIncome;
import com.oasys.emathi.Dto.OtherIncomeGroup;
import com.oasys.emathi.Dto.Penalty;
import com.oasys.emathi.Dto.Savings;
import com.oasys.emathi.Dto.SavingsDisbursement;
import com.oasys.emathi.Dto.SeedFund;
import com.oasys.emathi.Dto.Subscription;
import com.oasys.emathi.Dto.SubscriptionCharges;
import com.oasys.emathi.Dto.SubscriptiontoFeds;
import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.Dto.VoluntarySavings;
import com.oasys.emathi.Dto.VoluntarySavingsDisbursement;
import com.oasys.emathi.Dto.internalLoanRepayment;
import com.oasys.emathi.Dto.memberLoanRepayment;
import com.oasys.emathi.EMathiApplication;

import java.util.ArrayList;

public class SHGTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;
    static boolean isSuccessFullyUpadted = false;

    public SHGTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EMathiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertSHGData(ListOfShg shgDto, String u_id) {
        if (shgDto != null) {
            try {
                String CHvalue = "" + "0", CBvalue = "" + "0";
                if (shgDto.getCashInHand().length() > 0 && shgDto.getCashAtBank().length() > 0) {
                    CHvalue = "" + (int) Double.parseDouble(shgDto.getCashInHand());
                    CBvalue = "" + (int) Double.parseDouble(shgDto.getCashAtBank());
                }

                openDatabase();

                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_CODE, (shgDto.getCode() != null && shgDto.getCode().length() > 0) ? shgDto.getCode() : "");
                values.put(TableConstants.SHG_ID, (shgDto.getId() != null && shgDto.getId().length() > 0) ? shgDto.getId() : "");
                values.put(TableConstants.USERID, (u_id != null && u_id.length() > 0) ? u_id : "");
                values.put(TableConstants.CASH_IN_HAND, ((CHvalue + "") != null && (CHvalue + "").length() > 0) ? (CHvalue + "") : "");
                values.put(TableConstants.CASH_AT_BANK, ((CBvalue + "") != null && (CBvalue + "").length() > 0) ? (CBvalue + "") : "");
                values.put(TableConstants.GRP_FORMATION_DATE, (shgDto.getGroupFormationDate() != null && shgDto.getGroupFormationDate().length() > 0) ? shgDto.getGroupFormationDate() : "");
                values.put(TableConstants.OPENINGDATE, (shgDto.getOpeningDate() != null && shgDto.getOpeningDate().length() > 0) ? shgDto.getOpeningDate() : "");
                values.put(TableConstants.NAME, (shgDto.getName() != null && shgDto.getName().length() > 0) ? shgDto.getName() : "");
                values.put(TableConstants.BLOCKNAME, (shgDto.getBlockName() != null && shgDto.getBlockName().length() > 0) ? shgDto.getBlockName() : "");
                values.put(TableConstants.VILLAGENAME, (shgDto.getVillageName() != null && shgDto.getVillageName().length() > 0) ? shgDto.getVillageName() : "");
                values.put(TableConstants.PANCHAYATNAME, (shgDto.getPanchayatName() != null && shgDto.getPanchayatName().length() > 0) ? shgDto.getPanchayatName() : "");
                values.put(TableConstants.LASTTRANSACTIONDATE, (shgDto.getLastTransactionDate() != null && shgDto.getLastTransactionDate().length() > 0) ? shgDto.getLastTransactionDate() : "");
                values.put(TableConstants.DISTRICT_ID, (shgDto.getDistrictId() != null && shgDto.getDistrictId().length() > 0) ? shgDto.getDistrictId() : "");
                //   values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
              /*  if (MySharedPreference.readString(EShaktiApplication.getInstance(), MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(EShaktiApplication.getInstance(), MySharedPreference.SHG_ID, "").equals(shgDto.getShgId().trim())) {
                    if (MySharedPreference.readBoolean(EShaktiApplication.getInstance(), MySharedPreference.LOGOUT, false))
                        values.put(TableConstants.FIRST_SFLAG, "1");
                    else if (MySharedPreference.readBoolean(EShaktiApplication.getInstance(), MySharedPreference.SINGIN_DIFF, false))
                        values.put(TableConstants.FIRST_SFLAG, "0");
                }*/


                values.put(TableConstants.ACTIVEFLAG, (shgDto.isVerified()) ? shgDto.getLastTransactionDate() : "false");
                values.put(TableConstants.PRESIDENT_NAME, (shgDto.getPresidentName() != null && shgDto.getPresidentName().length() > 0) ? shgDto.getPresidentName() : "NA");

                database.insertWithOnConflict(TableName.TABLE_GROUP_NAME_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void updateSHGGrouploanDetails(ExistingLoan existloan1, String loanId) {

        try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.OFF_LOAN_OS,existloan1.getLoanOutstanding());
            long flag = database.update(TableName.OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST, values, TableConstants.LOAN_ID + " = '" + loanId + "'", null);

            if (flag != -1) {
                isSuccessFullyUpadted = true;
            } else {
                isSuccessFullyUpadted = false;
            }
        } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
    }

    public static void updateSHGGrouploanDetails(OfflineDto offlineDto) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_LOAN_OS,offlineDto.getOutStanding());
            long flag = database.update(TableName.OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST, values, TableConstants.LOAN_ID + " = '" + offlineDto.getLoanId() + "'", null);

            if (flag != -1) {
                isSuccessFullyUpadted = true;
            } else {
                isSuccessFullyUpadted = false;
            }
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }




    public static void updateSHGDetails(CashOfGroup rDto, String shgId) {

        if (rDto != null) {
            try {
                String CHvalue = "" + "0", CBvalue = "" + "0";
                if (rDto.getCashInHand().length() > 0 && rDto.getCashAtBank().length() > 0) {
                    CHvalue = (int) Double.parseDouble(rDto.getCashInHand()) + "";
                    CBvalue = "" + (int) Double.parseDouble(rDto.getCashAtBank());
                }
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.CASH_IN_HAND, ((CHvalue + "") != null && (CHvalue + "").length() > 0) ? (CHvalue + "") : "");
                values.put(TableConstants.CASH_AT_BANK, ((CBvalue + "") != null && (CBvalue + "").length() > 0) ? (CBvalue + "") : "");
                values.put(TableConstants.LASTTRANSACTIONDATE, (rDto.getLastTransactionDate() != null && rDto.getLastTransactionDate().length() > 0) ? rDto.getLastTransactionDate() : "NA");
                values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
                database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void updateBankCurrentBalanceDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_CURRENT_BALANCE, off.getCurr_balance() + "");
            //database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + off.getShgSavingsAccountId() + "'", null);
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID+"=?" , new String[]{off.getShgSavingsAccountId()});
            Log.d("bankaccount","query parameter : "+off.getCurr_balance());
            Log.d("bankaccount","savings account id : "+off.getShgSavingsAccountId());
            Log.d("bankaccount","update query called");
        } catch (Exception e) {
            //Log.e("insertEnergyExceptionss", e.toString());
            Log.d("bankaccount","Exception : "+e.getMessage());
        } finally {
            closeDatabase();
        }

    }


    public static void updateBankrecurringCurrentBalanceDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.RD_CURRENTBALANCE, off.getCurrent_balance() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_RD_LIST, values, TableConstants.SHGSAVINGSACCOUNTID + " = '" + off.getRecurringbankBalanceId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }
    public static void updateRecurringCurrentBalanceDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.RD_CURRENTBALANCE, off.getCurrent_balance() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_RD_LIST, values, TableConstants.RD_ACCOUNTNO + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }


    public static void updateBankSelectedCurrentBalanceDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_CURRENT_BALANCE, off.getSelectedcurrentbalance() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }


//    public static void updateRecuuringData(OfflineDto off) {
    public static void insertrecuringData(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.SHG_ID, off.getShgId());
            values.put(TableConstants.USERID, off.getAnimatorId());
            values.put(TableConstants.SHGSAVINGSACCOUNTID, off.getShgSavingsAccountId());
            values.put(TableConstants.RD_BANKNAME, off.getBankName());
            values.put(TableConstants.RD_BRANCHNAME, off.getBranchName());
            values.put(TableConstants.RD_ACCOUNTNO, off.getAccountNumber());
            values.put(TableConstants.RD_BANKID, off.getBankId());
            values.put(TableConstants.RD_IFSC, off.getIfsc());
            values.put(TableConstants.RD_BRANCH_ID, off.getBranchid());
            values.put(TableConstants.RD_INSTALLAMOUNT, off.getInstallmentamount());
            values.put(TableConstants.RD_RATE_OF_INTEREST, off.getRateofinterest());
            values.put(TableConstants.RD_TENURE, off.getTenure());
            values.put(TableConstants.RD_STATUS, off.getStatus());

            long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_RD_LIST, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);

//            database.update(TableName.OFFLINE_ANIMATOR_SHG_RD_LIST, values, TableConstants.SHG_ID + " = '" + off.getShgId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }

   /* public static void updatemultipleBankCurrentBalanceDetails(ArrayList<OfflineDto> offList) {

        try {
            openDatabase();
            for(OfflineDto off :offList) {
                ContentValues values = new ContentValues();
                values.put(TableConstants.OFF_CURRENT_BALANCE, off.getCurr_balance() + "");
                database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_ID + " = '" + off.getShgId() + "' and " + TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
            }
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }*/

    public static void updatemultipleBankCurrentBalanceDetails1(ArrayList<TableData> offList) {

        try {
            openDatabase();
            for(TableData off :offList) {
                ContentValues values = new ContentValues();
                values.put(TableConstants.OFF_CURRENT_BALANCE, off.getCurrentBalance() + "");
                database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_ID + " = '" + off.getShgId() + "' and " + TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
            }
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }


    public static void updateToBankCurrentBalanceDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_CURRENT_BALANCE, off.getCurr_balance1() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }

    }



    public static void updateSelectedBankCurrentBalanceDetails(OfflineDto off,String shgAcId) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_CURRENT_BALANCE, off.getCurr_balance1() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + shgAcId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }


    public static void updateSelectedBankCurrentBalanceDetailsoffline(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_CURRENT_BALANCE, off.getCurr_balance1() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateLoanOSDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_LOAN_OS, off.getOutStanding() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST, values, TableConstants.LOAN_ACC_ID + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }

    }

    public static void updateFDDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_FD_VALUE, off.getFd_value() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateRDDetails(OfflineDto off) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_RD_VALUE, off.getRd_value() + "");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, values, TableConstants.SHG_SB_AC_ID + " = '" + off.getAccountNumber() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateIstransaction(OfflineDto off) {
        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.IS_TRANSACTION_TDY, off.getIs_transaction_tdy());
            database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + off.getShgId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }

    public static void updateAuditIstransaction(OfflineDto off) {
        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.IS_TRANS_AUDIT, off.getIs_trans_Audit());
            database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + off.getShgId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateIs_Transaction_tdy_SHGDetails(ArrayList<TableData> tableData) {

        if (!tableData.isEmpty()) {

            try {
                openDatabase();
                for (TableData occupation : tableData) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.IS_TRANSACTION_TDY, occupation.getIs_transaction_tdy());
                    database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" +occupation.getId() + "'", null);
                }
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }

    public static void updateTransactionSHGDetails(String shgId) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
            values.put(TableConstants.FIRST_SFLAG, "1");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }


    public static void updateTransactionVerified(String shgId) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.ACTIVEFLAG, "true");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateSIgnInDiffUserDetails(String shgId) {

        try {

            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
            values.put(TableConstants.FIRST_SFLAG, "0");
            database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateopeningDate(ListOfShg tabledata,String shgId) {

        try {

            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OPENINGDATE,tabledata.getOpeningDate());
            values.put(TableConstants.LASTTRANSACTIONDATE,tabledata.getLastTransactionDate());
            database.update(TableName.OFFLINE_ANIMATOR_SHG_LIST, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }

    public static void updateSsnDetails(ArrayList<TableData> offList) {

        try {
            openDatabase();

            for (TableData data:offList) {
                ContentValues values = new ContentValues();
                values.put(TableConstants.SSS_NUMBER, data.getSss_number());
                values.put(TableConstants.SSS_TYPE_ID, data.getSssTypeId());
                values.put(TableConstants.SHG_ID, data.getShgId());
                values.put(TableConstants.SSN_BANKNAME, data.getBankName());
                values.put(TableConstants.SSN_BRANCHNAME, data.getBranchName());
                values.put(TableConstants.IS_BEFORE_2019, data.getIs_before_2019());
                values.put(TableConstants.IS_SSS, data.getIs_sss());
                values.put(TableConstants.SSS_UPDATE, data.getSss_date());
                values.put(TableConstants.BANK_NAME_ID, data.getBankNameId());
                values.put(TableConstants.BRANCH_NAME_ID, data.getBranchNameId());
//                database.update(TableName.OFFLINE_ANIMATOR_SSN_LIST, values, TableConstants.SSS_TYPE_ID + " = '" + data.getSssTypeId() + "' and " + TableConstants.MEMBER_ID + " = '" + data.getMemberId() + "'", null);
                database.update(TableName.OFFLINE_ANIMATOR_SSN_LIST, values, TableConstants.SHG_ID + " = '" + data.getShgId() + "' and " + TableConstants.MEMBER_ID + " = '" + data.getMemberId() + "'", null);

            }
            } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }

    public static void updateSsnDetails1(TableData data) {

        try {
            openDatabase();

//            for (TableData data:offList) {
                ContentValues values = new ContentValues();
                values.put(TableConstants.SSS_NUMBER, data.getSss_number());
                values.put(TableConstants.SSS_TYPE_ID, data.getSssTypeId());
                values.put(TableConstants.SHG_ID, data.getShgId());
                values.put(TableConstants.SSN_BANKNAME, data.getBankName());
                values.put(TableConstants.SSN_BRANCHNAME, data.getBranchName());
                values.put(TableConstants.IS_BEFORE_2019, data.getIs_before_2019());
                values.put(TableConstants.IS_SSS, data.getIs_sss());
                values.put(TableConstants.SSS_UPDATE, data.getSss_date());
                values.put(TableConstants.BANK_NAME_ID, data.getBankNameId());
                values.put(TableConstants.BRANCH_NAME_ID, data.getBranchNameId());
                database.update(TableName.OFFLINE_ANIMATOR_SSN_LIST, values, TableConstants.SSS_TYPE_ID + " = '" + data.getSssTypeId() + "' and " + TableConstants.MEMBER_ID + " = '" + data.getMemberId() + "'", null);

//            }
        } catch (Exception e) {
            Log.e("insertEnergyExceptionss", e.toString());
        } finally {
            closeDatabase();
        }

    }


    public static ArrayList<ListOfShg> getSHGDetails() {
        ArrayList<ListOfShg> shgList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_LIST;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ListOfShg shg = new ListOfShg();
                    shg.setCode(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_CODE)));
                    shg.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    shg.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    shg.setCashInHand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    shg.setGroupFormationDate(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_FORMATION_DATE)));
                    shg.setLastTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.LASTTRANSACTIONDATE)));
                    shg.setOpeningDate(cursor.getString(cursor.getColumnIndex(TableConstants.OPENINGDATE)));
                    shg.setModifiedDate(cursor.getString(cursor.getColumnIndex(TableConstants.MODIFIEDDATE)));
                    shg.setFFlag(cursor.getString(cursor.getColumnIndex(TableConstants.FIRST_SFLAG)));
                    shg.setPanchayatName(cursor.getString(cursor.getColumnIndex(TableConstants.PANCHAYATNAME)));
                    shg.setBlockName(cursor.getString(cursor.getColumnIndex(TableConstants.BLOCKNAME)));
                    shg.setVillageName(cursor.getString(cursor.getColumnIndex(TableConstants.VILLAGENAME)));
                    shg.setDistrictId(cursor.getString(cursor.getColumnIndex(TableConstants.DISTRICT_ID)));
                    shg.setPresidentName(cursor.getString(cursor.getColumnIndex(TableConstants.PRESIDENT_NAME)));
                    shg.setIsTransAudit(cursor.getString(cursor.getColumnIndex(TableConstants.IS_TRANS_AUDIT)));
                    shgList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return shgList;

    }

    public static ArrayList<ListOfShg> getSHGDetailList(String userid) {
        ArrayList<ListOfShg> shgList = new ArrayList<>();

        try {
            openDatabase();
            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_LIST + " where " + TableConstants.USERID + " LIKE '" + userid + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ListOfShg shg = new ListOfShg();
                    shg.setCode(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_CODE)));
                    shg.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    shg.setIsTransAudit(cursor.getString(cursor.getColumnIndex(TableConstants.IS_TRANS_AUDIT)));
                    shg.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    shg.setCashInHand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    shg.setGroupFormationDate(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_FORMATION_DATE)));
                    shg.setLastTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.LASTTRANSACTIONDATE)));
                    shg.setOpeningDate(cursor.getString(cursor.getColumnIndex(TableConstants.OPENINGDATE)));
                    shg.setVerified((cursor.getString(cursor.getColumnIndex(TableConstants.ACTIVEFLAG)).equals("true")) ? true : false);
                    shg.setModifiedDate(cursor.getString(cursor.getColumnIndex(TableConstants.MODIFIEDDATE)));
                    shg.setFFlag(cursor.getString(cursor.getColumnIndex(TableConstants.FIRST_SFLAG)));
                    shg.setPanchayatName(cursor.getString(cursor.getColumnIndex(TableConstants.PANCHAYATNAME)));
                    shg.setBlockName(cursor.getString(cursor.getColumnIndex(TableConstants.BLOCKNAME)));
                    shg.setVillageName(cursor.getString(cursor.getColumnIndex(TableConstants.VILLAGENAME)));
                    shg.setDistrictId(cursor.getString(cursor.getColumnIndex(TableConstants.DISTRICT_ID)));
                    shg.setPresidentName(cursor.getString(cursor.getColumnIndex(TableConstants.PRESIDENT_NAME)));
                    shgList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return shgList;

    }


    public static ListOfShg getSHGDetails(String shg_id) {
        ListOfShg shg = new ListOfShg();
        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_LIST + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    shg.setCode(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_CODE)));
                    shg.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    shg.setIsTransAudit(cursor.getString(cursor.getColumnIndex(TableConstants.IS_TRANS_AUDIT)));
                    shg.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    shg.setCashInHand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    shg.setGroupFormationDate(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_FORMATION_DATE)));
                    shg.setOpeningDate(cursor.getString(cursor.getColumnIndex(TableConstants.OPENINGDATE)));
                    shg.setLastTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.LASTTRANSACTIONDATE)));
                    shg.setModifiedDate(cursor.getString(cursor.getColumnIndex(TableConstants.MODIFIEDDATE)));
                    shg.setFFlag(cursor.getString(cursor.getColumnIndex(TableConstants.FIRST_SFLAG)));
                    shg.setPanchayatName(cursor.getString(cursor.getColumnIndex(TableConstants.PANCHAYATNAME)));
                    shg.setBlockName(cursor.getString(cursor.getColumnIndex(TableConstants.BLOCKNAME)));
                    shg.setVillageName(cursor.getString(cursor.getColumnIndex(TableConstants.VILLAGENAME)));
                    shg.setDistrictId(cursor.getString(cursor.getColumnIndex(TableConstants.DISTRICT_ID)));
                    shg.setPresidentName(cursor.getString(cursor.getColumnIndex(TableConstants.PRESIDENT_NAME)));
                    shg.setIs_transaction_tdy(cursor.getString(cursor.getColumnIndex(TableConstants.IS_TRANSACTION_TDY)));
                    shg.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHGSAVINGSACCOUNTID)));

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return shg;

    }


    public static TableData getAnimatorDetails(String userid)
    {
        TableData tableData =new TableData();
        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_PROFILE + " where " + TableConstants.USERID + " LIKE '" + userid + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {

                    tableData.setPhoneNumber(cursor.getLong(cursor.getColumnIndex(TableConstants.PHONE_NUMBER)));
                    tableData.setDateOfAssigning(cursor.getLong(cursor.getColumnIndex(TableConstants.DATE_OF_ASSIGNING)));
                    tableData.setAnimatorName((cursor.getString(cursor.getColumnIndex(TableConstants.ANIMATOR_NAME))));
                    tableData.setAge((cursor.getString(cursor.getColumnIndex(TableConstants.AGE))));
                    tableData.setTotalGroup((cursor.getString(cursor.getColumnIndex(TableConstants.TOTAL_GROUP))));

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            closeDatabase();
        }
     return tableData;
    }

    public static TableData getGroupProfile(String shg_id)
    {
        TableData tableData =new TableData();
        try {
             openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_PROFILE + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do {

                    tableData.setShgType(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_TYPE)));
                    tableData.setCode(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_CODE)));
                    tableData.setName(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_NAME)));
                    tableData.setShgCreatedDate(cursor.getLong(cursor.getColumnIndex(TableConstants.SHG_CREATION_DATE)));
                    tableData.setShgVerifiedDate(cursor.getLong(cursor.getColumnIndex(TableConstants.SHG_VERIFIED_DATE)));
                    tableData.setMobileNumber(cursor.getLong(cursor.getColumnIndex(TableConstants.SHG_GROUPMOBILE_NUMBER)));
                    tableData.setTotalMembers(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_GROUPTOTAL_MEMBER)));
                    tableData.setPresidentName(cursor.getString(cursor.getColumnIndex(TableConstants.PRESIDENT_NAME)));
                    tableData.setSecretaryName(cursor.getString(cursor.getColumnIndex(TableConstants.SECRETARY_NAME)));
                    tableData.setTreasuereName(cursor.getString(cursor.getColumnIndex(TableConstants.TREASURER_NAME)));
                    tableData.setBlockName(cursor.getString(cursor.getColumnIndex(TableConstants.BLOCK_NAME)));
                    tableData.setPanchayatName(cursor.getString(cursor.getColumnIndex(TableConstants.PANCHAYATNAME)));
                    tableData.setVillageName(cursor.getString(cursor.getColumnIndex(TableConstants.VILLAGENAME)));

                }while (cursor.moveToNext());
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            closeDatabase();
        }
return tableData;
    }


    public static ArrayList<TableData> getShgTrainingData()
    {
        ArrayList<TableData> data= new ArrayList<>();

        try {
            openDatabase();

//            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_PROFILE + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_TRAINING;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do {
                    TableData tableData =new TableData();
                    tableData.setName(cursor.getString(cursor.getColumnIndex(TableConstants.TRAINING_NAME)));
                    tableData.setUuid(cursor.getString(cursor.getColumnIndex(TableConstants.TRAINING_UUID)));
                    tableData.setLanguage_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.TRAINING_LANGUAGE_UUID)));
                    data.add(tableData);

                }while (cursor.moveToNext());
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            closeDatabase();
        }
        return data;
    }


    public static ArrayList<TableData> getRecurringData()
    {
        ArrayList<TableData> data= new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_RD_LIST;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do {
                    TableData values =new TableData();

                    values.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    values.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHGSAVINGSACCOUNTID)));
                    values.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BANKNAME)));
                    values.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BRANCHNAME)));
                    values.setAccountNo(cursor.getString(cursor.getColumnIndex(TableConstants.RD_ACCOUNTNO)));
                    values.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BANKID)));
                    values.setIfsc(cursor.getString(cursor.getColumnIndex(TableConstants.RD_IFSC)));
                    values.setBranchId(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BRANCH_ID)));
                    values.setIsPrimary(cursor.getString(cursor.getColumnIndex(TableConstants.RD_ISPRIMARY)));
                    values.setInstallment_amount(cursor.getString(cursor.getColumnIndex(TableConstants.RD_INSTALLAMOUNT)));
                    values.setRate_of_interest(cursor.getString(cursor.getColumnIndex(TableConstants.RD_RATE_OF_INTEREST)));
                    values.setTenure(cursor.getString(cursor.getColumnIndex(TableConstants.RD_TENURE)));
                    values.setCurrent_balance(cursor.getString(cursor.getColumnIndex(TableConstants.RD_CURRENTBALANCE)));
                    values.setOpening_balance(cursor.getString(cursor.getColumnIndex(TableConstants.RD_OPENINGBALANCE)));
                    values.setStatus(cursor.getString(cursor.getColumnIndex(TableConstants.RD_STATUS)));
                    data.add(values);

                }while (cursor.moveToNext());
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            closeDatabase();
        }
        return data;
    }


    public static ArrayList<TableData> getRecurringBank(String shgId)
    {
        ArrayList<TableData> data= new ArrayList<>();

        try {
            openDatabase();

//            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_RD_LIST;
            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_RD_LIST + " where " + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
//            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do {
                    TableData values =new TableData();

                    values.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    values.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHGSAVINGSACCOUNTID)));
                    values.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BANKNAME)));
                    values.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BRANCHNAME)));
                    values.setAccountNo(cursor.getString(cursor.getColumnIndex(TableConstants.RD_ACCOUNTNO)));
                    values.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BANKID)));
                    values.setIfsc(cursor.getString(cursor.getColumnIndex(TableConstants.RD_IFSC)));
                    values.setBranchId(cursor.getString(cursor.getColumnIndex(TableConstants.RD_BRANCH_ID)));
                    values.setIsPrimary(cursor.getString(cursor.getColumnIndex(TableConstants.RD_ISPRIMARY)));
                    values.setInstallment_amount(cursor.getString(cursor.getColumnIndex(TableConstants.RD_INSTALLAMOUNT)));
                    values.setRate_of_interest(cursor.getString(cursor.getColumnIndex(TableConstants.RD_RATE_OF_INTEREST)));
                    values.setTenure(cursor.getString(cursor.getColumnIndex(TableConstants.RD_TENURE)));
                    values.setCurrent_balance(cursor.getString(cursor.getColumnIndex(TableConstants.RD_CURRENTBALANCE)));
                    values.setOpening_balance(cursor.getString(cursor.getColumnIndex(TableConstants.RD_OPENINGBALANCE)));
                    values.setStatus(cursor.getString(cursor.getColumnIndex(TableConstants.RD_STATUS)));
                    data.add(values);

                }while (cursor.moveToNext());
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            closeDatabase();
        }
        return data;
    }



    public static ArrayList<TableData> getSssTypeData()
    {
        ArrayList<TableData> data= new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SSN_MASTER;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do {
                    TableData values =new TableData();

                    values.setId(cursor.getString(cursor.getColumnIndex(TableConstants.ID)));
                    values.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));

                    data.add(values);

                }while (cursor.moveToNext());
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            closeDatabase();
        }
        return data;
    }

    public static ArrayList<TableData> getSSNData(String shg_id,String sssType_id)
    {
        ArrayList<TableData> dataArrayList  = new ArrayList<>();
            openDatabase();

            try {
                openDatabase();

                String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SSN_LIST + " where " + TableConstants.SHG_ID + " = '" + shg_id + "'" + " AND " + TableConstants.SSS_TYPE_ID + " = '" + sssType_id + "'" ;
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        TableData tableData =new TableData();
                        tableData.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        tableData.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.SSN_MEMBERNAME)));
                        tableData.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.SSN_BANKNAME)));
                        tableData.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.SSN_BRANCHNAME)));
                        tableData.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        tableData.setSssTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.SSS_TYPE_ID)));
                        tableData.setSss_number(cursor.getString(cursor.getColumnIndex(TableConstants.SSS_NUMBER)));
                        tableData.setIs_before_2019((cursor.getString(cursor.getColumnIndex(TableConstants.IS_BEFORE_2019))));
                        tableData.setSss_date((cursor.getLong(cursor.getColumnIndex(TableConstants.SSS_UPDATE))));


                        dataArrayList.add(tableData);

                    } while (cursor.moveToNext());
                }



        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            closeDatabase();
        }
        return dataArrayList;
    }

    public static void deleteSHG(String userId) {
        try {
            openDatabase();
            database.execSQL("DELETE FROM " + TableName.TABLE_GROUP_NAME_DETAILS);
            database.execSQL("DELETE FROM " + TableName.OFFLINE_ANIMATOR_SHG_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

    }

    public static void deleteSHGListdata(String userId) {
        try {
            openDatabase();
            database.execSQL("DELETE FROM " + TableName.OFFLINE_ANIMATOR_SHG_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

    }

    public static void deleteTrainingListdata() {
        try {
            openDatabase();
            database.execSQL("DELETE FROM " + TableName.OFFLINE_ANIMATOR_SHG_TRAINING);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

    }

    public static ArrayList<TableData> getCheckSSSTypeIdAvailable(String sssTypeId) {

       ArrayList<TableData> dataArrayList =new ArrayList<>();
        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SSN_MASTER + " where " + TableConstants.ID + " LIKE '" + sssTypeId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do {
                    TableData tableData =new TableData();
                    tableData.setId(cursor.getString(cursor.getColumnIndex(TableConstants.ID)));
                    dataArrayList.add(tableData);

                }while (cursor.moveToNext());
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            closeDatabase();
        }
        return dataArrayList;

    }

    public static ArrayList<TableData> getSSNAllData(String sssType_id,String shg_id )
    {
        ArrayList<TableData> dataArrayList  = new ArrayList<>();
        openDatabase();

        try {
            openDatabase();

//            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SSN_LIST + " where " + TableConstants.SSS_TYPE_ID + " LIKE '" + ssstypeid + "'";
            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SSN_LIST + " where " + TableConstants.SHG_ID + " = '" + shg_id + "'" + " AND " + TableConstants.SSS_TYPE_ID + " = '" + sssType_id + "'" ;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    TableData tableData =new TableData();
                    tableData.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    tableData.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.SSN_MEMBERNAME)));
                    tableData.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.SSN_BANKNAME)));
                    tableData.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.SSN_BRANCHNAME)));
                    tableData.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    tableData.setSssTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.SSS_TYPE_ID)));
                    tableData.setSss_number(cursor.getString(cursor.getColumnIndex(TableConstants.SSS_NUMBER)));
                    tableData.setIs_sss(cursor.getString(cursor.getColumnIndex(TableConstants.IS_SSS)));
                    tableData.setIs_before_2019((cursor.getString(cursor.getColumnIndex(TableConstants.IS_BEFORE_2019))));
                    tableData.setSss_date((cursor.getLong(cursor.getColumnIndex(TableConstants.SSS_UPDATE))));
                    dataArrayList.add(tableData);

                } while (cursor.moveToNext());
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            closeDatabase();
        }
        return dataArrayList;
    }

    // AUDIT DETAILS

    public static ArrayList<Savings> getsavingAuditDetails() {
        ArrayList<Savings> savingList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_SAVINGS;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Savings shg = new Savings();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));

                    savingList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return savingList;

    }

    public static ArrayList<VoluntarySavings> getVoluntarysavingAuditDetails() {
        ArrayList<VoluntarySavings> voluntarySavings = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_VOLUNTARYSAVINGS;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    VoluntarySavings shg = new VoluntarySavings();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));

                    voluntarySavings.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return voluntarySavings;

    }

    public static ArrayList<InternalLoanDisbursement> getInternalLoanDisbursementAuditDetails() {
        ArrayList<InternalLoanDisbursement> internalLoanDisbursementArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_INTERNALLOANDISBURSEMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    InternalLoanDisbursement shg = new InternalLoanDisbursement();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    internalLoanDisbursementArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return internalLoanDisbursementArrayList;

    }


    public static ArrayList<SavingsDisbursement> getSavingdisbursementAuditDetails() {
        ArrayList<SavingsDisbursement> voluntarySavings = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_SAVINGDISBURSEMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    SavingsDisbursement shg = new SavingsDisbursement();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));

                    voluntarySavings.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return voluntarySavings;

    }

    public static ArrayList<SeedFund> getSeedfundAuditDetails() {
        ArrayList<SeedFund> seedFundArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_SEEDFUND;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    SeedFund shg = new SeedFund();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    seedFundArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return seedFundArrayList;

    }

    public static ArrayList<FDBankTransaction> getFDFdBankTransactionsAuditDetails() {
        ArrayList<FDBankTransaction> fdBankTransactionArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_FDBANKTRANSACTION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    FDBankTransaction shg = new FDBankTransaction();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    fdBankTransactionArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return fdBankTransactionArrayList;

    }

    public static ArrayList<IncomeDisbursement> getIncomeDisbursementAuditDetails() {
        ArrayList<IncomeDisbursement> incomeDisbursementArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_INCOMEDISBURSEMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    IncomeDisbursement shg = new IncomeDisbursement();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    incomeDisbursementArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return incomeDisbursementArrayList;

    }

    public static ArrayList<OtherIncome> getOtherIncomeAuditDetails() {
        ArrayList<OtherIncome> otherIncomeArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_OTHERINCOME;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OtherIncome shg = new OtherIncome();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    otherIncomeArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return otherIncomeArrayList;

    }

    public static ArrayList<GroupLoanRepayment> getGroupLoanRepaymentAuditDetails() {
        ArrayList<GroupLoanRepayment> groupLoanRepaymentArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_GROUPLOANREPAYMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    GroupLoanRepayment shg = new GroupLoanRepayment();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    groupLoanRepaymentArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return groupLoanRepaymentArrayList;

    }

    public static ArrayList<Penalty> getPenaltyAuditDetails() {
        ArrayList<Penalty> penaltyArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_PENALTY;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Penalty shg = new Penalty();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    penaltyArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return penaltyArrayList;

    }

    public static ArrayList<BankTransaction> getBankTransactionAuditDetails() {
        ArrayList<BankTransaction> bankTransactionArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_BANKTRANSACTION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    BankTransaction shg = new BankTransaction();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    bankTransactionArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return bankTransactionArrayList;

    }

    public static ArrayList<SubscriptiontoFeds> getSubscriptionAuditDetails() {
        ArrayList<SubscriptiontoFeds> subscriptionArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_SUBSCRIPTIONFEDS;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    SubscriptiontoFeds shg = new SubscriptiontoFeds();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    subscriptionArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return subscriptionArrayList;

    }

    public static ArrayList<OtherIncomeGroup> getOtherIncomeGroupAuditDetails() {
        ArrayList<OtherIncomeGroup> otherIncomeGroupArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_OTHERINCOMEGROUP;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OtherIncomeGroup shg = new OtherIncomeGroup();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    otherIncomeGroupArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return otherIncomeGroupArrayList;

    }


    public static ArrayList<memberLoanRepayment> getMemberLoanRepaymentAuditDetails() {
        ArrayList<memberLoanRepayment> memberLoanRepaymentArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_MEMBERLOANREAPYMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    memberLoanRepayment shg = new memberLoanRepayment();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    memberLoanRepaymentArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return memberLoanRepaymentArrayList;

    }

    public static ArrayList<VoluntarySavingsDisbursement> getVoluntarySavingsDisbursementAuditDetails() {
        ArrayList<VoluntarySavingsDisbursement> voluntarySavingsDisbursementArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_VOLUNTARYSAVINGSDISBURSEMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    VoluntarySavingsDisbursement shg = new VoluntarySavingsDisbursement();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    voluntarySavingsDisbursementArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return voluntarySavingsDisbursementArrayList;

    }

    public static ArrayList<OtherExpense> getOtherExpenseAuditDetails() {
        ArrayList<OtherExpense> otherExpenseArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_OTHEREXPENSE;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OtherExpense shg = new OtherExpense();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    otherExpenseArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return otherExpenseArrayList;

    }

    public static ArrayList<MeetingExpense> getMeetingExpenseAuditDetails() {
        ArrayList<MeetingExpense> meetingExpenseArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_MEETINGEXPENSE;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    MeetingExpense shg = new MeetingExpense();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    meetingExpenseArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return meetingExpenseArrayList;

    }

    public static ArrayList<internalLoanRepayment> getInternalLoanRepaymentAuditDetails() {
        ArrayList<internalLoanRepayment> internalLoanRepaymentArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_INTERLOANREPAYMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    internalLoanRepayment shg = new internalLoanRepayment();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    internalLoanRepaymentArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return internalLoanRepaymentArrayList;

    }

    public static ArrayList<Donation> getDonationAuditDetails() {
        ArrayList<Donation> donationArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_DONATION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Donation shg = new Donation();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    donationArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return donationArrayList;

    }

    public static ArrayList<Subscription> getSubscriptionAuditDetailss() {
        ArrayList<Subscription> subscriptionArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_SUBSCRIPTION;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Subscription shg = new Subscription();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    subscriptionArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return subscriptionArrayList;

    }

    public static ArrayList<SubscriptionCharges> getSubscriptionChargesAuditDetails() {
        ArrayList<SubscriptionCharges> subscriptionChargesArrayList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.AUDIT_SUBSCRIPTIONCHARGES;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    SubscriptionCharges shg = new SubscriptionCharges();
                    shg.setAuditor_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_ID)));
                    shg.setShg_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_USERID)));
                    shg.setMember(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEMBERNAME)));
                    shg.setMember_uuid(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_MEBERID)));
                    shg.setTransaction_date(cursor.getString(cursor.getColumnIndex(TableConstants.AUDIT_TRANSDATE)));
                    shg.setOld_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.OLD_TRANS_AMOUNT)));
                    shg.setNew_transaction_amount(cursor.getString(cursor.getColumnIndex(TableConstants.NEW_TRANS_AMOUNT)));
                    subscriptionChargesArrayList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return subscriptionChargesArrayList;

    }


}
