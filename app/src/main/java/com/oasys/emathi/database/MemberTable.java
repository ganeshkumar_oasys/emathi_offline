package com.oasys.emathi.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.MemberMobileNumbersDTOList;
import com.oasys.emathi.EMathiApplication;

import java.util.ArrayList;
import java.util.List;

public class MemberTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public MemberTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EMathiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertMemberData(MemberList memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (memDto.getShgId() != null && memDto.getShgId().length() > 0) ? memDto.getShgId() : "");
                values.put(TableConstants.MEMBER_ID, (memDto.getMemberId() != null && memDto.getMemberId().length() > 0) ? memDto.getMemberId() : "");
                values.put(TableConstants.MEMBER_NAME, (memDto.getMemberName() != null && memDto.getMemberName().length() > 0) ? memDto.getMemberName() : "");
                values.put(TableConstants.PHONE_NO, (memDto.getPhoneNumber() != null && memDto.getPhoneNumber().length() > 0) ? memDto.getPhoneNumber() : "");
                values.put(TableConstants.MEMBER_USER_ID, (memDto.getMemberUserId() != null && memDto.getMemberUserId().length() > 0) ? memDto.getMemberUserId() : "");
                values.put(TableConstants.BANK_ID, (memDto.getBankId() != null && memDto.getBankId().length() > 0) ? memDto.getBankId() : "");
                values.put(TableConstants.BRANCHNAME_ID, (memDto.getBranchNameId() != null && memDto.getBranchNameId().length() > 0) ? memDto.getBranchNameId() : "");
                values.put(TableConstants.BANKNAME_ID, (memDto.getBankNameId() != null && memDto.getBankNameId().length() > 0) ? memDto.getBankNameId() : "");
                values.put(TableConstants.BANKNAME, (memDto.getBankName() != null && memDto.getBankName().length() > 0) ? memDto.getBankName() : "");
                values.put(TableConstants.BRANCHNAME, (memDto.getBranchName() != null && memDto.getBranchName().length() > 0) ? memDto.getBranchName() : "");
                values.put(TableConstants.ACCOUNT_NO, (memDto.getAccountNumber() != null && memDto.getAccountNumber().length() > 0) ? memDto.getAccountNumber() : "");
                database.insertWithOnConflict(TableName.TABLE_MEMBER_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static List<MemberList> getMemberList(String shg_id) {
        List<MemberList> memList = new ArrayList<>();
        if (shg_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_MEMBER_LIST + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {

                    do {
                        MemberList memberList = new MemberList();
                        memberList.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        memberList.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        memberList.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                        memberList.setPhoneNumber(cursor.getString(cursor.getColumnIndex(TableConstants.PHONE_NO)));
                        memberList.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));
                        memberList.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCHNAME)));
                        memberList.setBranchNameId(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCH_ID)));
                        memberList.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.ACCOUNT_NO)));
                        memberList.setBankNameId(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME_ID)));
                        memberList.setAadharNumber(cursor.getString(cursor.getColumnIndex(TableConstants.AADHAR_NUM)));
                        memberList.setPipNumber(cursor.getString(cursor.getColumnIndex(TableConstants.PIP_NUM)));
                        memberList.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                        memberList.setMemberUserId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_USER_ID)));
                        if (memberList.getMemberName() != null && memberList.getMemberName().length() > 0)
                            memList.add(memberList);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }

    public static void updateMemberMobileNo(MemberMobileNumbersDTOList member) {


        try {

            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.PHONE_NO,member.getMobileNumber());
            database.update(TableName.OFFLINE_ANIMATOR_MEMBER_LIST, values, TableConstants.MEMBER_ID + " = '" + member.getMemberId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }

    public static void updateMemberPipNo(MemberMobileNumbersDTOList member) {

        try {

            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.PIP_NUM,member.getPipNumber());
            database.update(TableName.OFFLINE_ANIMATOR_MEMBER_LIST, values, TableConstants.MEMBER_ID + " = '" + member.getMemberId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }


    public static MemberList getMember(String u_id) {
        MemberList memberList = new MemberList();
        if (u_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_MEMBER_LIST + " where " + TableConstants.USERID + " LIKE '" + u_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        memberList.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        memberList.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        memberList.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                        memberList.setPhoneNumber(cursor.getString(cursor.getColumnIndex(TableConstants.PHONE_NO)));
                        memberList.setMemberUserId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_USER_ID)));

                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }

        }
        return memberList;
    }






    public static void deleteMemberList(String userId) {
        try {
            openDatabase();
            database.execSQL("DELETE FROM " + TableName.TABLE_MEMBER_DETAILS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

    }



}
