package com.oasys.emathi.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.EMathiApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 16 Dec, 2018.
 */

public class LoanTable {
    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public LoanTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EMathiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertLoanDetails(ExistingLoan memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                if ((memDto.getLoanId() != null && memDto.getLoanId().length() > 0)) {
                    values.put(TableConstants.SHG_ID, (memDto.getShgId() != null && memDto.getShgId().length() > 0) ? memDto.getShgId() : "");
                    values.put(TableConstants.LOAN_ID, (memDto.getLoanId() != null && memDto.getLoanId().length() > 0) ? memDto.getLoanId() : "");
                    values.put(TableConstants.LOAN_TYPE_ID, (memDto.getLoanId() != null && memDto.getLoanId().length() > 0) ? memDto.getLoanId() : "");
                    values.put(TableConstants.LOAN_NAME, (memDto.getLoanTypeName() != null && memDto.getLoanTypeName().length() > 0) ? memDto.getLoanTypeName() : "");
                    values.put(TableConstants.LOAN_AMOUNT, (memDto.getLoanOutstanding() != null && memDto.getLoanOutstanding().length() > 0) ? memDto.getLoanOutstanding() : "");
                    values.put(TableConstants.LOAN_ACC_NO, (memDto.getAccountNumber() != null && memDto.getAccountNumber().length() > 0) ? memDto.getAccountNumber() : "");
                    values.put(TableConstants.LOAN_ACC_ID, (memDto.getLoanAccountId() != null && memDto.getLoanAccountId().length() > 0) ? memDto.getLoanAccountId() : "");
                    values.put(TableConstants.LOAN_AMOUNT, (memDto.getLoanOutstanding() != null && memDto.getLoanOutstanding().length() > 0) ? memDto.getLoanOutstanding() : "");
                    values.put(TableConstants.BANKNAME, (memDto.getBankName() != null && memDto.getBankName().length() > 0) ? memDto.getBankName() : "");
                    database.insertWithOnConflict(TableName.TABLE_LOAN_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                }
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static List<ExistingLoan> getInternalLoan(String shg_id) {
        List<ExistingLoan> memList = new ArrayList<>();
        if (shg_id.length() > 0) {
            try {
                openDatabase();

                String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST + " WHERE " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ExistingLoan loanDto = new ExistingLoan();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        loanDto.setLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));

                        memList.add(loanDto);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }

    public static List<ExistingLoan> getMemLoanDetails(String shg_id) {
        List<ExistingLoan> memList = new ArrayList<>();
        if (shg_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST + " WHERE " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ExistingLoan loanDto = new ExistingLoan();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ID)));
                        loanDto.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        loanDto.setMemberLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));

                        memList.add(loanDto);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }

    public static List<ExistingLoan> getMemDetails(String shg_id, String loanid) {
        List<ExistingLoan> memList = new ArrayList<>();

        if (shg_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST + " WHERE (" + TableConstants.SHG_ID + " LIKE '" + shg_id + "' AND " + TableConstants.LOAN_ID + " LIKE '" + loanid + "')";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ExistingLoan loanDto = new ExistingLoan();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ID)));
                        loanDto.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        loanDto.setMemberLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));

                        memList.add(loanDto);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }


    public static ExistingLoan getLD_MemDetails(String shg_id, OfflineDto ml) {
        ExistingLoan loanDto = null;

        if (shg_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST + " WHERE (" + TableConstants.SHG_ID + " LIKE '" + shg_id + "' AND " + TableConstants.LOAN_ID + " LIKE '" + ml.getLoanId() + "' AND "+TableConstants.MEMBER_ID + " LIKE '" + ml.getMemberId() + "')";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        loanDto = new ExistingLoan();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ID)));
                        loanDto.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        loanDto.setMemberLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));


                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return loanDto;
    }


    public static ArrayList<MemberList> getMemberloanData(String loanId) {

        ArrayList<MemberList> arrMemDeatails = new ArrayList<>();

        try {
            // loanId = "880fada5-930c-49d9-bba8-9188978a483c";
            openDatabase();

//            String Query = "SELECT OFFLINE_ANIMATOR_MEMBER_LIST.memberName,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanAcNo,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.bankId,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.sSelectedBank,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanId,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanAcId,OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.memberId,OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.shg_Id,OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.OutStanding FROM ((OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST LEFT JOIN OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST ON OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanId = OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.loanId) LEFT JOIN OFFLINE_ANIMATOR_MEMBER_LIST ON OFFLINE_ANIMATOR_MEMBER_LIST.memberId = OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.memberId) where OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanId ='" + loanId + "' ORDER BY OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.OutStanding";
            String Query = "SELECT OFFLINE_ANIMATOR_MEMBER_LIST.memberName,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanAcNo,\n" +
                    "OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.bankId,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.sSelectedBank,\n" +
                    "OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanId,OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanAcId,\n" +
                    "OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.memberId,OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.shg_Id,\n" +
                    "OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.OutStanding  \n" +
                    "FROM\n" +
                    "OFFLINE_ANIMATOR_MEMBER_LIST\n" +
                    "LEFT JOIN OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST ON OFFLINE_ANIMATOR_MEMBER_LIST.memberId = OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.memberId\n" +
                    " LEFT JOIN OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST  ON OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanId = OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST.loanId\n" +
                    " where OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST.loanId ='" + loanId + "'";
            Log.d("query", Query);

            Cursor cursor = database.rawQuery(Query, null);

            if (cursor.moveToFirst()) {

                do {
                    MemberList bD = new MemberList();
                    bD.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MEMBER_NAME)));
                    bD.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ACC_NO)));
                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_BANK_ID)));
                    bD.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_BANKNAME)));
                    bD.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ID)));
                    bD.setLoanAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_ACC_ID)));
                    bD.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_MEMBER_ID)));
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));
                    arrMemDeatails.add(bD);
                }
                while ((cursor.moveToNext()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return arrMemDeatails;
    }


    public static ArrayList<MemberList> getILDetails(String shg_id) {
        ArrayList<MemberList> memList = new ArrayList<>();

        if (shg_id.length() > 0) {
            try {
                openDatabase();
//                String selectQuery = "SELECT OFFLINE_ANIMATOR_MEMBER_LIST.memberId, OFFLINE_ANIMATOR_MEMBER_LIST.memberName ,OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.shg_Id,OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.OutStanding FROM(OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST LEFT JOIN OFFLINE_ANIMATOR_MEMBER_LIST ON OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.memberId = OFFLINE_ANIMATOR_MEMBER_LIST.memberId) where OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.shg_Id ='" + shg_id + "' ORDER BY OFFLINE_ANIMATOR_MEMBER_LIST.memberId";
                String selectQuery = "SELECT OFFLINE_ANIMATOR_MEMBER_LIST.memberId, OFFLINE_ANIMATOR_MEMBER_LIST.memberName ,OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.shg_Id,\n" +
                        "OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.OutStanding\n" +
                        " FROM\n" +
                        " OFFLINE_ANIMATOR_MEMBER_LIST\n" +
                        "  LEFT JOIN OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST ON OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.memberId = OFFLINE_ANIMATOR_MEMBER_LIST.memberId\n" +
                        " where OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST.shg_Id ='" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        MemberList loanDto = new MemberList();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        loanDto.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                        loanDto.setLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));
                        memList.add(loanDto);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }


    public static List<ExistingLoan> getPOS() {
        List<ExistingLoan> memList = new ArrayList<>();
        try {
            openDatabase();

            String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ExistingLoan loanDto = new ExistingLoan();
                    loanDto.setId(cursor.getString(cursor.getColumnIndex(TableConstants.ID)));
                    loanDto.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));

                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return memList;
    }


    public static void updateILOSDetails(OfflineDto dto) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_LOAN_OS, dto.getMem_os());
            database.update(TableName.OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST, values, TableConstants.MEMBER_ID + " = '" + dto.getMemberId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }


    public static void updateMROSDetails(OfflineDto dto) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.OFF_LOAN_OS, dto.getMem_os());
            database.update(TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST, values, TableConstants.LOAN_ID + " = '" + dto.getLoanId() + "' AND " + TableConstants.MEMBER_ID + " = '" + dto.getMemberId() + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }


    public static List<ExistingLoan> getGrpLoanDetails(String shg_id) {
        List<ExistingLoan> memList = new ArrayList<>();
        if (shg_id.length() > 0) {
            try {
                openDatabase();

                String selectQuery = "SELECT * FROM " + TableName.OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST + " WHERE " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ExistingLoan loanDto = new ExistingLoan();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ID)));
                        loanDto.setLoanTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.LOANTYPENAME)));
                        loanDto.setLoanAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ACC_ID)));
                        loanDto.setLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_LOAN_OS)));
                        loanDto.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ACC_NO)));
                        loanDto.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));
                        loanDto.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                        loanDto.setBranchId(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCH_ID)));
                        loanDto.setDisbursmentDate(cursor.getString(cursor.getColumnIndex(TableConstants.OFF_DISBURSEDATETIME)));


                        memList.add(loanDto);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }


}
