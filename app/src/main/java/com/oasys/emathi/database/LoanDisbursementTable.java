package com.oasys.emathi.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.emathi.Dto.LoanBankDto;
import com.oasys.emathi.Dto.LoanDisbursementDto;
import com.oasys.emathi.Dto.LoanDto;
import com.oasys.emathi.EMathiApplication;

import java.util.ArrayList;
import java.util.List;

public class LoanDisbursementTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public LoanDisbursementTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EMathiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertLD_POL(LoanDto memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.LOAN_TYPE_ID, (memDto.getLoanTypeId() != null && memDto.getLoanTypeId().length() > 0) ? memDto.getLoanTypeId() : "");
                values.put(TableConstants.LOANTYPENAME, (memDto.getLoanTypeName() != null && memDto.getLoanTypeName().length() > 0) ? memDto.getLoanTypeName() : "");
                database.insertWithOnConflict(TableName.TABLE_LOAN_PURPOSE, TableConstants.LOAN_TYPE_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void insertLD_INS_TYPE(LoanDisbursementDto memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.InstallmentTypeId, (memDto.getInstallmentTypeId() != null && memDto.getInstallmentTypeId().length() > 0) ? memDto.getInstallmentTypeId() : "");
                values.put(TableConstants.InstallmentTypeName, (memDto.getInstallmentTypeName() != null && memDto.getInstallmentTypeName().length() > 0) ? memDto.getInstallmentTypeName() : "");
                database.insertWithOnConflict(TableName.TABLE_LOAN_INSTALLMENT, TableConstants.InstallmentTypeId, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }


    public static void insertLD_BANK_DETAILS(LoanBankDto memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.ID, (memDto.getId() != null && memDto.getId().length() > 0) ? memDto.getId() : "");
                values.put(TableConstants.NAME, (memDto.getName() != null && memDto.getName().length() > 0) ? memDto.getName() : "");
//                values.put(TableConstants.IFSC, (memDto.getIfscCode() != null && memDto.getIfscCode().length() > 0) ? memDto.getIfscCode() : "");
                database.insertWithOnConflict(TableName.TABLE_LOAN_BANK_DETAILS, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void insertLD_LTYPE(LoanDto memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.LOAN_TYPE_ID, (memDto.getLoanTypeId() != null && memDto.getLoanTypeId().length() > 0) ? memDto.getLoanTypeId() : "");
                values.put(TableConstants.LOANTYPENAME, (memDto.getLoanTypeName() != null && memDto.getLoanTypeName().length() > 0) ? memDto.getLoanTypeName() : "");
                database.insertWithOnConflict(TableName.TABLE_LOAN_LTYPES, TableConstants.LOAN_TYPE_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertLD_LTYPE_SETTING(LoanDisbursementDto memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.LOAN_SETTING_ID, (memDto.getLoanSettingId() != null && memDto.getLoanSettingId().length() > 0) ? memDto.getLoanSettingId() : "");
                values.put(TableConstants.LOAN_SETTING_NAME, (memDto.getLoanSettingName() != null && memDto.getLoanSettingName().length() > 0) ? memDto.getLoanSettingName() : "");
                values.put(TableConstants.S_FLAG, (memDto.getSflag() != 0 && memDto.getSflag() > 0) ? memDto.getSflag() : 1);
                database.insertWithOnConflict(TableName.TABLE_LOAN_IL_TYPE_SETTING, TableConstants.LOAN_SETTING_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void insertLD_MFI_SETTING(LoanDisbursementDto memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.LOAN_SETTING_ID, (memDto.getId() != null && memDto.getId().length() > 0) ? memDto.getId() : "");
                values.put(TableConstants.LOAN_SETTING_NAME, (memDto.getName() != null && memDto.getName().length() > 0) ? memDto.getName() : "");
                values.put(TableConstants.S_FLAG, (memDto.getSflag() != 0 && memDto.getSflag() > 0) ? memDto.getSflag() : 0);//TODO: Sflag is empty
                database.insertWithOnConflict(TableName.TABLE_LOAN_MFI_SETTING, TableConstants.LOAN_SETTING_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void insertLD_FED_SETTING(LoanDisbursementDto memDto) {
        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.LOAN_SETTING_ID, (memDto.getLoanSettingId() != null && memDto.getLoanSettingId().length() > 0) ? memDto.getLoanSettingId() : "");
                values.put(TableConstants.LOAN_SETTING_NAME, (memDto.getLoanSettingName() != null && memDto.getLoanSettingName().length() > 0) ? memDto.getLoanSettingName() : "");
                values.put(TableConstants.S_FLAG, (memDto.getSflag() != 0 && memDto.getSflag() > 0) ? memDto.getSflag() : 1);
                database.insertWithOnConflict(TableName.TABLE_LOAN_FED_SETTING, TableConstants.LOAN_SETTING_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }
    }


    public static List<LoanBankDto> getBankList() {
        List<LoanBankDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_BANK_DETAILS;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanBankDto loanDto = new LoanBankDto();
                    loanDto.setId(cursor.getString(cursor.getColumnIndex(TableConstants.ID)));
                    loanDto.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }


    public static List<LoanDisbursementDto> getInstallmentType() {
        List<LoanDisbursementDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_INSTALLMENT;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanDisbursementDto loanDto = new LoanDisbursementDto();
                    loanDto.setInstallmentTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.InstallmentTypeId)));
                    loanDto.setInstallmentTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.InstallmentTypeName)));
                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }


    public static List<LoanDisbursementDto> getIL_LoanType() {
        List<LoanDisbursementDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_IL_TYPE_SETTING;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanDisbursementDto loanDto = new LoanDisbursementDto();
                    loanDto.setLoanSettingId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_SETTING_ID)));
                    loanDto.setLoanSettingName(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_SETTING_NAME)));
                    loanDto.setSflag(cursor.getInt(cursor.getColumnIndex(TableConstants.S_FLAG)));
                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }

    public static List<LoanDisbursementDto> getFedType() {
        List<LoanDisbursementDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_FED_SETTING;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanDisbursementDto loanDto = new LoanDisbursementDto();
                    loanDto.setLoanSettingId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_SETTING_ID)));
                    loanDto.setLoanSettingName(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_SETTING_NAME)));
                    loanDto.setSflag(cursor.getInt(cursor.getColumnIndex(TableConstants.S_FLAG)));
                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }

    public static List<LoanDisbursementDto> getMFIType() {
        List<LoanDisbursementDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_MFI_SETTING;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanDisbursementDto loanDto = new LoanDisbursementDto();
                    loanDto.setLoanSettingId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_SETTING_ID)));
                    loanDto.setLoanSettingName(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_SETTING_NAME)));
                    // loanDto.setSflag(cursor.getInt(cursor.getColumnIndex(TableConstants.S_FLAG)));  TODO:: Added changes sflag empty
                    if (loanDto.getLoanSettingId() != null && loanDto.getLoanSettingId().length() > 0)
                        memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }


    public static List<LoanDto> getPOLType() {
        List<LoanDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_PURPOSE;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanDto loanDto = new LoanDto();
                    loanDto.setLoanTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_TYPE_ID)));
                    loanDto.setLoanTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.LOANTYPENAME)));
                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }

    public static List<LoanDto> getLoanType() {
        List<LoanDto> memList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_LTYPES;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LoanDto loanDto = new LoanDto();
                    loanDto.setLoanTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_TYPE_ID)));
                    loanDto.setLoanTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.LOANTYPENAME)));
                    memList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return memList;
    }


}
