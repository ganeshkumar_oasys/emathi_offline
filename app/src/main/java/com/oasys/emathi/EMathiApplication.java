package com.oasys.emathi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.oasys.emathi.Dto.GroupfinancialDetails;
import com.oasys.emathi.Dto.ResponseContent;
import com.oasys.emathi.OasysUtils.LruBitmapCache;
import com.oasys.emathi.Service.NetworkSchedulerService;
import com.oasys.emathi.database.DbHelper;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

public class EMathiApplication extends Application  {

    public static final String TAG = EMathiApplication.class.getSimpleName();
    public static GroupfinancialDetails grpFinanceVerification;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static String getUser_RegLanguage() {
        return User_RegLanguage;
    }

    public static void setUser_RegLanguage(String user_RegLanguage) {
        User_RegLanguage = user_RegLanguage;
    }

    private static String User_RegLanguage;
    private static EMathiApplication mAapplicaiton;
    public static ResponseContent vertficationDto;
    private static Context mAppContext;
    private static Handler mApplicationHandler;
    public static boolean isSelectedgrouptask = false;
    private static String mUserType;

    public static String flag = "0";
    public static String getFlag() {
        return flag;
    }

    public static void setFlag(String flag) {
        EMathiApplication.flag = flag;
    }



    private static String mServiceGroupID;
    public static boolean isAgent = false;
    private static String mGetsinglegrpId;
    private static boolean isPLOS = false;
    private static String mSetTransValues = "";
    private static boolean isBankDeposit = false;
    private static boolean isOfflineTransDate = false;
    private static boolean isOfflineTrans = false;

    private static boolean isSubmenuclicked = false;

    private static String mLastTransId;

    private static boolean isOnSavedFragment = false;
    private static boolean isDefault = false;
    private static String mLoginFlag = null;
    private static String mInternalLoanType = null;
    private static String mBankName_InternalLoan = null;
    private static String mOfflineAadharCardUniqueId = null;
    private static boolean isOfflineAadharcardPhot = false;
    private static boolean isOfflineServiceRunning = false;
    private static int mStepwiseSavings = 0;
    private static int mStepwiseInternalloan = 0;
    private static String mFixedDepositeAmount;
    private static boolean mStepWiseFragment = false;
    private static boolean mOtherIncomeFragment = false;
    private static String mOtherAmountValue = null;
    private static String mIMEI_NO = null;
    private static String mLoanaccBankName = null;
    private static String mLoanaccBankNameSendtoServer = null;
    private static String mLoanaccLoanId = null;
    private static String mLoanaccLoanType = null;
    private static String mLoanaccFixedDeposit = null;
    private static String mAcctoaccSelectBank = null;
    private static String mAcctoaccSendtoserverBank = null;
    private static boolean mAcctoaccTransferBank = false;
    private static boolean mLoanAccToBank = false;
    private static String mSubsidyAmount = null;
    private static String mSubsidyReserveFund = null;
    private static boolean isVoluntarySavings = false;
    private static boolean isGroupListValues = false;
    private static boolean isEditOBTransDate = false;
    private static String mEditOBTransactionDate = null;
    private static boolean mCheckGroupListTextColor = false;
    private static String mEmathiContacts = null;
    private static String mAppVersionCode = null;
    private static String mAppVersionName = null;
    private static String mTempBalanceSheetDate = null;
    private static boolean isAudit_TrainingFragment = false;
    private static boolean isBankFD = false;
    private static String mSelectedMemberName = null;
    private static String mLoanId = null;
    private static String mLoanName = null;
    private static boolean mSavAccToLoanAccTransfer = false;
    private static String mLoanBankName = null;
    private static boolean IsSeedFund = false;
    private static String mLoanDisburseValues = null;
    private static String mLoanAccBalanceAmount = null;
    private static String mSBAccBalanceAmount = null;
    private static String mGroupId_GroupLastTransDate = null;
    private static String mGroupResponse = null;
    private static String mLastTransactionDate = null;
    private static boolean mIsChangeLanguage = false;
    private static boolean mIsLoanDisBurseRepaid = false;
    private static String mSelectedType = null;
    private static String mSelectedBankAmount = null;
    private static String mTransactionDate = null;
    private static boolean mIsNewLoanDisburseDate = false;
    private static String mMemberLoanRepaymentLoanBankName = null;
    private static String mMemberLoanRepaymentLoanAccNo = null;
    private static boolean mGroupLoanRepayBank = false;
    private static String mRecyclerPosition = null;
    private static String mLoanAcc_LoanDisbursementDate = null;
    private static boolean isFragmentMenuListView = false;
    private static String mLoanDisbursementDate = null;
    private static boolean isOfflineServiceCall = false;
    private static boolean isEditOpeningScreen = false;
    private static String mLastTransDate_GroupId = null;
    private static String mLastTransDate_DB = null;
    private static String mSystemEntryDate = null;
    private static String mTempLastTansDate = null;
    private static String mNextMonthLastDate = null;
    private static boolean isAuditFragment = false;
    private static int mGroupLoanTotalInterest = 0;
    private static int mGroupLoanTotalCharges = 0;
    private static int mGroupLoanTotalRepayment = 0;
    private static int mGroupLoanTotalInterestSubventionRecevied = 0;
    private static int mGroupLoanTotalBankcharges = 0;
    private static String mNextMonthFirstDate = null;
    private static String mEditFinancialGroupId = null;
    private static String mShg_selected_bankName = null;
    private static String mGroupLoanRepaymentLoanBankName = null;
    private static String mGroupLoanRepaymentAccNo = null;
    private static boolean isAccountNumberBankName = false;
    private static String mTransAudit_UpdateValue = null;
    private static boolean mCalendarDateVisibleFlag = false;
    private static String mNewLoanDisbursementMinDate = null;
    private static String mLastAuditDate = null;
    private static String mAuditFromDate = null;
    private static String mAuditToDate = null;
    private static String mCalendarDialog_MinDate = null;

    @Override
    public void onCreate() {
        super.onCreate();
        EMathiApplication.mAapplicaiton = this;
        mApplicationHandler = new Handler();
        this.setAppContext(getApplicationContext());
        initialize();
       scheduleJob();
    }

    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        @SuppressLint("WrongConstant") JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(myJob);
    }



    public static EMathiApplication getInstance() {
        return EMathiApplication.mAapplicaiton;
    }

    public static Handler getHandler() {
        return EMathiApplication.mApplicationHandler;
    }

    public static Context getAppContext() {
        return mAppContext;
    }

    public void setAppContext(Context mAppContext) {
        EMathiApplication.mAppContext = mAppContext;
    }

    private void initialize() {
        //mTransactionManager = TransactionManager.getInstance(this);
        DbHelper.getInstance(this).init();
    }


    /**
     * @return the isSelectedgrouptask
     */
    public static boolean isSelectedgrouptask() {
        return isSelectedgrouptask;
    }

    /**
     * @param isSelectedgrouptask the isSelectedgrouptask to set
     */
    public static void setSelectedgrouptask(boolean isSelectedgrouptask) {
        EMathiApplication.isSelectedgrouptask = isSelectedgrouptask;
    }

    public static String getServiceGroupID() {
        return mServiceGroupID;
    }

    public static void setServiceGroupID(String mServiceGroupID) {
        EMathiApplication.mServiceGroupID = mServiceGroupID;
    }

    public static boolean isAgent() {
        return isAgent;
    }

    public static void setAgent(boolean isAgent) {
        EMathiApplication.isAgent = isAgent;
    }

    public static String getGetsinglegrpId() {
        return mGetsinglegrpId;
    }

    public static void setGetsinglegrpId(String mGetsinglegrpId) {
        EMathiApplication.mGetsinglegrpId = mGetsinglegrpId;
    }

    public static boolean isPLOS() {
        return isPLOS;
    }

    public static void setPLOS(boolean isPLOS) {
        EMathiApplication.isPLOS = isPLOS;
    }

    public static String getSetTransValues() {
        return mSetTransValues;
    }

    public static void setSetTransValues(String mSetTransValues) {
        EMathiApplication.mSetTransValues = mSetTransValues;
    }

    public static boolean isBankDeposit() {
        return isBankDeposit;
    }

    public static void setBankDeposit(boolean isBankDeposit) {
        EMathiApplication.isBankDeposit = isBankDeposit;
    }

    public static boolean isOfflineTransDate() {
        return isOfflineTransDate;
    }

    public static void setOfflineTransDate(boolean isOfflineTransDate) {
        EMathiApplication.isOfflineTransDate = isOfflineTransDate;
    }

    public static boolean isOfflineTrans() {
        return isOfflineTrans;
    }

    public static void setOfflineTrans(boolean isOfflineTrans) {
        EMathiApplication.isOfflineTrans = isOfflineTrans;
    }

    public static String getLastTransId() {
        return mLastTransId;
    }

    public static void setLastTransId(String mLastTransId) {
        EMathiApplication.mLastTransId = mLastTransId;
    }

    public static boolean isSubmenuclicked() {
        return isSubmenuclicked;
    }

    public static void setSubmenuclicked(boolean isSubmenuclicked) {
        EMathiApplication.isSubmenuclicked = isSubmenuclicked;
    }

    public static boolean isOnSavedFragment() {
        return isOnSavedFragment;
    }

    public static void setOnSavedFragment(boolean isOnSavedFragment) {
        EMathiApplication.isOnSavedFragment = isOnSavedFragment;
    }

    public static String getmUserType() {
        return mUserType;
    }

    public static void setmUserType(String userType) {
        EMathiApplication.mUserType = userType;
    }

    public static boolean isDefault() {
        return isDefault;
    }

    public static void setDefault(boolean isDefault) {
        EMathiApplication.isDefault = isDefault;
    }

    public static String getLoginFlag() {
        return mLoginFlag;
    }

    public static void setLoginFlag(String mLoginFlag) {
        EMathiApplication.mLoginFlag = mLoginFlag;
    }

    public static String getInternalLoanType() {
        return mInternalLoanType;
    }

    public static void setInternalLoanType(String mInternalLoanType) {
        EMathiApplication.mInternalLoanType = mInternalLoanType;
    }

    public static String getBankName_InternalLoan() {
        return mBankName_InternalLoan;
    }

    public static void setBankName_InternalLoan(String mBankName_InternalLoan) {
        EMathiApplication.mBankName_InternalLoan = mBankName_InternalLoan;
    }

    public static String getOfflineAadharCardUniqueId() {
        return mOfflineAadharCardUniqueId;
    }

    public static void setOfflineAadharCardUniqueId(String mOfflineAadharCardUniqueId) {
        EMathiApplication.mOfflineAadharCardUniqueId = mOfflineAadharCardUniqueId;
    }

    public static boolean isOfflineAadharcardPhot() {
        return isOfflineAadharcardPhot;
    }

    public static void setOfflineAadharcardPhot(boolean isOfflineAadharcardPhot) {
        EMathiApplication.isOfflineAadharcardPhot = isOfflineAadharcardPhot;
    }

    public static boolean isOfflineServiceRunning() {
        return isOfflineServiceRunning;
    }

    public static void setOfflineServiceRunning(boolean isOfflineServiceRunning) {
        EMathiApplication.isOfflineServiceRunning = isOfflineServiceRunning;
    }

    public static int getStepwiseSavings() {
        return mStepwiseSavings;
    }

    public static void setStepwiseSavings(int mStepwiseSavings) {
        EMathiApplication.mStepwiseSavings = mStepwiseSavings;
    }

    public static int getStepwiseInternalloan() {
        return mStepwiseInternalloan;
    }

    public static void setStepwiseInternalloan(int mStepwiseInternalloan) {
        EMathiApplication.mStepwiseInternalloan = mStepwiseInternalloan;
    }

    public final static Handler sApplicationHandler = new Handler(Looper.getMainLooper());


    public static String getFixedDepositeAmount() {
        return mFixedDepositeAmount;
    }

    public static void setFixedDepositeAmount(String mFixedDepositeAmount) {
        EMathiApplication.mFixedDepositeAmount = mFixedDepositeAmount;
    }

    public static boolean isStepWiseFragment() {
        return mStepWiseFragment;
    }

    public static void setStepWiseFragment(boolean mStepWiseFragment) {
        EMathiApplication.mStepWiseFragment = mStepWiseFragment;
    }

    public static boolean isOtherIncomeFragment() {
        return mOtherIncomeFragment;
    }

    public static void setOtherIncomeFragment(boolean mOtherIncomeFragment) {
        EMathiApplication.mOtherIncomeFragment = mOtherIncomeFragment;
    }

    public static String getOtherAmountValue() {
        return mOtherAmountValue;
    }

    public static void setOtherAmountValue(String mOtherAmountValue) {
        EMathiApplication.mOtherAmountValue = mOtherAmountValue;
    }

    public static String getIMEI_NO() {
        return mIMEI_NO;
    }

    public static void setIMEI_NO(String mIMEI_NO) {
        EMathiApplication.mIMEI_NO = mIMEI_NO;
    }

    public static String getLoanaccBankName() {
        return mLoanaccBankName;
    }

    public static void setLoanaccBankName(String mLoanaccBankName) {
        EMathiApplication.mLoanaccBankName = mLoanaccBankName;
    }

    public static String getLoanaccBankNameSendtoServer() {
        return mLoanaccBankNameSendtoServer;
    }

    public static void setLoanaccBankNameSendtoServer(String mLoanaccBankNameSendtoServer) {
        EMathiApplication.mLoanaccBankNameSendtoServer = mLoanaccBankNameSendtoServer;
    }

    public static String getLoanaccLoanId() {
        return mLoanaccLoanId;
    }

    public static void setLoanaccLoanId(String mLoanaccLoanId) {
        EMathiApplication.mLoanaccLoanId = mLoanaccLoanId;
    }

    public static String getLoanaccLoanType() {
        return mLoanaccLoanType;
    }

    public static void setLoanaccLoanType(String mLoanaccLoanType) {
        EMathiApplication.mLoanaccLoanType = mLoanaccLoanType;
    }

    public static String getLoanaccFixedDeposit() {
        return mLoanaccFixedDeposit;
    }

    public static void setLoanaccFixedDeposit(String mLoanaccFixedDeposit) {
        EMathiApplication.mLoanaccFixedDeposit = mLoanaccFixedDeposit;
    }

    public static String getAcctoaccSelectBank() {
        return mAcctoaccSelectBank;
    }

    public static void setAcctoaccSelectBank(String mAcctoaccSelectBank) {
        EMathiApplication.mAcctoaccSelectBank = mAcctoaccSelectBank;
    }

    public static String getAcctoaccSendtoserverBank() {
        return mAcctoaccSendtoserverBank;
    }

    public static void setAcctoaccSendtoserverBank(String mAcctoaccSendtoserverBank) {
        EMathiApplication.mAcctoaccSendtoserverBank = mAcctoaccSendtoserverBank;
    }

    public static boolean isAcctoaccTransferBank() {
        return mAcctoaccTransferBank;
    }

    public static void setAcctoaccTransferBank(boolean mAcctoaccTransferBank) {
        EMathiApplication.mAcctoaccTransferBank = mAcctoaccTransferBank;
    }

    public static boolean isLoanAccToBank() {
        return mLoanAccToBank;
    }

    public static void setLoanAccToBank(boolean mLoanAccToBank) {
        EMathiApplication.mLoanAccToBank = mLoanAccToBank;
    }

    public static String getSubsidyAmount() {
        return mSubsidyAmount;
    }

    public static void setSubsidyAmount(String mSubsidyAmount) {
        EMathiApplication.mSubsidyAmount = mSubsidyAmount;
    }

    public static String getSubsidyReserveFund() {
        return mSubsidyReserveFund;
    }

    public static void setSubsidyReserveFund(String mSubsidyReserveFund) {
        EMathiApplication.mSubsidyReserveFund = mSubsidyReserveFund;
    }

    public static boolean isVoluntarySavings() {
        return isVoluntarySavings;
    }

    public static void setVoluntarySavings(boolean isVoluntarySavings) {
        EMathiApplication.isVoluntarySavings = isVoluntarySavings;
    }

    public static boolean isGroupListValues() {
        return isGroupListValues;
    }

    public static void setGroupListValues(boolean isGroupListValues) {
        EMathiApplication.isGroupListValues = isGroupListValues;
    }

    public static boolean isEditOBTransDate() {
        return isEditOBTransDate;
    }

    public static void setEditOBTransDate(boolean isEditOBTransDate) {
        EMathiApplication.isEditOBTransDate = isEditOBTransDate;
    }

    public static String getEditOBTransactionDate() {
        return mEditOBTransactionDate;
    }

    public static void setEditOBTransactionDate(String mEditOBTransactionDate) {
        EMathiApplication.mEditOBTransactionDate = mEditOBTransactionDate;
    }

    public static boolean isCheckGroupListTextColor() {
        return mCheckGroupListTextColor;
    }

    public static void setCheckGroupListTextColor(boolean mCheckGroupListTextColor) {
        EMathiApplication.mCheckGroupListTextColor = mCheckGroupListTextColor;
    }

    public static String getEmathiContacts() {
        return mEmathiContacts;
    }

    public static void setEmathiContacts(String mEmathiContacts) {
        EMathiApplication.mEmathiContacts = mEmathiContacts;
    }

    public static String getAppVersionCode() {
        return mAppVersionCode;
    }

    public static void setAppVersionCode(String mAppVersionCode) {
        EMathiApplication.mAppVersionCode = mAppVersionCode;
    }

    public static String getmAppVersionName() {
        return mAppVersionName;
    }

    public static void setmAppVersionName(String mAppVersionName) {
        EMathiApplication.mAppVersionName = mAppVersionName;
    }

    public static String getTempBalanceSheetDate() {
        return mTempBalanceSheetDate;
    }

    public static void setTempBalanceSheetDate(String mTempBalanceSheetDate) {
        EMathiApplication.mTempBalanceSheetDate = mTempBalanceSheetDate;
    }

    public static boolean isAudit_TrainingFragment() {
        return isAudit_TrainingFragment;
    }

    public static void setAudit_TrainingFragment(boolean isAudit_TrainingFragment) {
        EMathiApplication.isAudit_TrainingFragment = isAudit_TrainingFragment;
    }

    public static boolean isBankFD() {
        return isBankFD;
    }

    public static void setBankFD(boolean isBankFD) {
        EMathiApplication.isBankFD = isBankFD;
    }

    public static String getSelectedMemberName() {
        return mSelectedMemberName;
    }

    public static void setSelectedMemberName(String mSelectedMemberName) {
        EMathiApplication.mSelectedMemberName = mSelectedMemberName;
    }

    public static String getLoanId() {
        return mLoanId;
    }

    public static void setLoanId(String mLoanId) {
        EMathiApplication.mLoanId = mLoanId;
    }

    public static String getLoanName() {
        return mLoanName;
    }

    public static void setLoanName(String mLoanName) {
        EMathiApplication.mLoanName = mLoanName;
    }

    public static boolean isSavAccToLoanAccTransfer() {
        return mSavAccToLoanAccTransfer;
    }

    public static void setSavAccToLoanAccTransfer(boolean mSavAccToLoanAccTransfer) {
        EMathiApplication.mSavAccToLoanAccTransfer = mSavAccToLoanAccTransfer;
    }

    public static String getLoanBankName() {
        return mLoanBankName;
    }

    public static void setLoanBankName(String mLoanBankName) {
        EMathiApplication.mLoanBankName = mLoanBankName;
    }

    public static boolean isSeedFund() {
        return IsSeedFund;
    }

    public static void setIsSeedFund(boolean isSeedFund) {
        IsSeedFund = isSeedFund;
    }

    public static String getLoanDisburseValues() {
        return mLoanDisburseValues;
    }

    public static void setLoanDisburseValues(String mLoanDisburseValues) {
        EMathiApplication.mLoanDisburseValues = mLoanDisburseValues;
    }

    public static String getLoanAccBalanceAmount() {
        return mLoanAccBalanceAmount;
    }

    public static void setLoanAccBalanceAmount(String mLoanAccBalanceAmount) {
        EMathiApplication.mLoanAccBalanceAmount = mLoanAccBalanceAmount;
    }

    public static String getSBAccBalanceAmount() {
        return mSBAccBalanceAmount;
    }

    public static void setSBAccBalanceAmount(String mSBAccBalanceAmount) {
        EMathiApplication.mSBAccBalanceAmount = mSBAccBalanceAmount;
    }

    public static String getGroupId_GroupLastTransDate() {
        return mGroupId_GroupLastTransDate;
    }

    public static void setGroupId_GroupLastTransDate(String mGroupId_GroupLastTransDate) {
        EMathiApplication.mGroupId_GroupLastTransDate = mGroupId_GroupLastTransDate;
    }

    public static String getGroupResponse() {
        return mGroupResponse;
    }

    public static void setGroupResponse(String mGroupResponse) {
        EMathiApplication.mGroupResponse = mGroupResponse;
    }

    public static String getLastTransactionDate() {
        return mLastTransactionDate;
    }

    public static void setLastTransactionDate(String mLastTransactionDate) {
        EMathiApplication.mLastTransactionDate = mLastTransactionDate;
    }

    public static boolean isIsChangeLanguage() {
        return mIsChangeLanguage;
    }

    public static void setIsChangeLanguage(boolean mIsChangeLanguage) {
        EMathiApplication.mIsChangeLanguage = mIsChangeLanguage;
    }

    public static boolean isIsLoanDisBurseRepaid() {
        return mIsLoanDisBurseRepaid;
    }

    public static void setIsLoanDisBurseRepaid(boolean mIsLoanDisBurseRepaid) {
        EMathiApplication.mIsLoanDisBurseRepaid = mIsLoanDisBurseRepaid;
    }

    public static String getSelectedType() {
        return mSelectedType;
    }

    public static void setSelectedType(String mSelectedType) {
        EMathiApplication.mSelectedType = mSelectedType;
    }

    public static String getSelectedBankAmount() {
        return mSelectedBankAmount;
    }

    public static void setSelectedBankAmount(String mSelectedBankAmount) {
        EMathiApplication.mSelectedBankAmount = mSelectedBankAmount;
    }

    public static String getTransactionDate() {
        return mTransactionDate;
    }

    public static void setTransactionDate(String mTransactionDate) {
        EMathiApplication.mTransactionDate = mTransactionDate;
    }

    public static boolean IsNewLoanDisburseDate() {
        return mIsNewLoanDisburseDate;
    }

    public static void setIsNewLoanDisburseDate(boolean mIsNewLoanDisburseDate) {
        EMathiApplication.mIsNewLoanDisburseDate = mIsNewLoanDisburseDate;
    }

    public static String getMemberLoanRepaymentLoanBankName() {
        return mMemberLoanRepaymentLoanBankName;
    }

    public static void setMemberLoanRepaymentLoanBankName(String mMemberLoanRepaymentLoanBankName) {
        EMathiApplication.mMemberLoanRepaymentLoanBankName = mMemberLoanRepaymentLoanBankName;
    }

    public static String getMemberLoanRepaymentLoanAccNo() {
        return mMemberLoanRepaymentLoanAccNo;
    }

    public static void setMemberLoanRepaymentLoanAccNo(String mMemberLoanRepaymentLoanAccNo) {
        EMathiApplication.mMemberLoanRepaymentLoanAccNo = mMemberLoanRepaymentLoanAccNo;
    }

    public static boolean isGroupLoanRepayBank() {
        return mGroupLoanRepayBank;
    }

    public static void setGroupLoanRepayBank(boolean mGroupLoanRepayBank) {
        EMathiApplication.mGroupLoanRepayBank = mGroupLoanRepayBank;
    }

    public static String getRecyclerPosition() {
        return mRecyclerPosition;
    }

    public static void setRecyclerPosition(String mRecyclerPosition) {
        EMathiApplication.mRecyclerPosition = mRecyclerPosition;
    }

    public static String getLoanAcc_LoanDisbursementDate() {
        return mLoanAcc_LoanDisbursementDate;
    }

    public static void setLoanAcc_LoanDisbursementDate(String mLoanAcc_LoanDisbursementDate) {
        EMathiApplication.mLoanAcc_LoanDisbursementDate = mLoanAcc_LoanDisbursementDate;
    }

    public static boolean isFragmentMenuListView() {
        return isFragmentMenuListView;
    }

    public static void setFragmentMenuListView(boolean isFragmentMenuListView) {
        EMathiApplication.isFragmentMenuListView = isFragmentMenuListView;
    }

    public static String getLoanDisbursementDate() {
        return mLoanDisbursementDate;
    }

    public static void setLoanDisbursementDate(String mLoanDisbursementDate) {
        EMathiApplication.mLoanDisbursementDate = mLoanDisbursementDate;
    }

    public static boolean isOfflineServiceCall() {
        return isOfflineServiceCall;
    }

    public static void setOfflineServiceCall(boolean isOfflineServiceCall) {
        EMathiApplication.isOfflineServiceCall = isOfflineServiceCall;
    }

    public static boolean isEditOpeningScreen() {
        return isEditOpeningScreen;
    }

    public static void setEditOpeningScreen(boolean isEditOpeningScreen) {
        EMathiApplication.isEditOpeningScreen = isEditOpeningScreen;
    }

    public static String getLastTransDate_GroupId() {
        return mLastTransDate_GroupId;
    }

    public static void setLastTransDate_GroupId(String mLastTransDate_GroupId) {
        EMathiApplication.mLastTransDate_GroupId = mLastTransDate_GroupId;
    }

    public static String getLastTransDate_DB() {
        return mLastTransDate_DB;
    }

    public static void setLastTransDate_DB(String mLastTransDate_DB) {
        EMathiApplication.mLastTransDate_DB = mLastTransDate_DB;
    }

    public static String getSystemEntryDate() {
        return mSystemEntryDate;
    }

    public static void setSystemEntryDate(String mSystemEntryDate) {
        EMathiApplication.mSystemEntryDate = mSystemEntryDate;
    }

    public static String getTempLastTansDate() {
        return mTempLastTansDate;
    }

    public static void setTempLastTransDate(String mTempLastTansDate) {
        EMathiApplication.mTempLastTansDate = mTempLastTansDate;
    }

    public static String getNextMonthLastDate() {
        return mNextMonthLastDate;
    }

    public static void setNextMonthLastDate(String mNextMonthLastDate) {
        EMathiApplication.mNextMonthLastDate = mNextMonthLastDate;
    }

    public static boolean isAuditFragment() {
        return isAuditFragment;
    }

    public static void setAuditFragment(boolean isAuditFragment) {
        EMathiApplication.isAuditFragment = isAuditFragment;
    }

    public static int getGroupLoanTotalInterest() {
        return mGroupLoanTotalInterest;
    }

    public static void setGroupLoanTotalInterest(int mGroupLoanTotalInterest) {
        EMathiApplication.mGroupLoanTotalInterest = mGroupLoanTotalInterest;
    }

    public static int getGroupLoanTotalCharges() {
        return mGroupLoanTotalCharges;
    }

    public static void setGroupLoanTotalCharges(int mGroupLoanTotalCharges) {
        EMathiApplication.mGroupLoanTotalCharges = mGroupLoanTotalCharges;
    }

    public static int getGroupLoanTotalRepayment() {
        return mGroupLoanTotalRepayment;
    }

    public static void setGroupLoanTotalRepayment(int mGroupLoanTotalRepayment) {
        EMathiApplication.mGroupLoanTotalRepayment = mGroupLoanTotalRepayment;
    }

    public static int getGroupLoanTotalInterestSubventionRecevied() {
        return mGroupLoanTotalInterestSubventionRecevied;
    }

    public static void setGroupLoanTotalInterestSubventionRecevied(int mGroupLoanTotalInterestSubventionRecevied) {
        EMathiApplication.mGroupLoanTotalInterestSubventionRecevied = mGroupLoanTotalInterestSubventionRecevied;
    }

    public static int getGroupLoanTotalBankcharges() {
        return mGroupLoanTotalBankcharges;
    }

    public static void setGroupLoanTotalBankcharges(int mGroupLoanTotalBankcharges) {
        EMathiApplication.mGroupLoanTotalBankcharges = mGroupLoanTotalBankcharges;
    }

    public static String getNextMonthFirstDate() {
        return mNextMonthFirstDate;
    }

    public static void setNextMonthFirstDate(String mNextMonthFirstDate) {
        EMathiApplication.mNextMonthFirstDate = mNextMonthFirstDate;
    }

    public static String getEditFinancialGroupId() {
        return mEditFinancialGroupId;
    }

    public static void setEditFinancialGroupId(String mEditFinancialGroupId) {
        EMathiApplication.mEditFinancialGroupId = mEditFinancialGroupId;
    }

    public static String getShg_selected_bankName() {
        return mShg_selected_bankName;
    }

    public static void setShg_selected_bankName(String mShg_selected_bankName) {
        EMathiApplication.mShg_selected_bankName = mShg_selected_bankName;
    }

    public static String getGroupLoanRepaymentLoanBankName() {
        return mGroupLoanRepaymentLoanBankName;
    }

    public static void setGroupLoanRepaymentLoanBankName(String mGroupLoanRepaymentLoanBankName) {
        EMathiApplication.mGroupLoanRepaymentLoanBankName = mGroupLoanRepaymentLoanBankName;
    }

    public static String getGroupLoanRepaymentAccNo() {
        return mGroupLoanRepaymentAccNo;
    }

    public static void setGroupLoanRepaymentAccNo(String mGroupLoanRepaymentAccNo) {
        EMathiApplication.mGroupLoanRepaymentAccNo = mGroupLoanRepaymentAccNo;
    }

    public static boolean isAccountNumberBankName() {
        return isAccountNumberBankName;
    }

    public static void setAccountNumberBankName(boolean isAccountNumberBankName) {
        EMathiApplication.isAccountNumberBankName = isAccountNumberBankName;
    }

    public static String getTransAudit_UpdateValue() {
        return mTransAudit_UpdateValue;
    }

    public static void setTransAudit_UpdateValue(String mTransAudit_UpdateValue) {
        EMathiApplication.mTransAudit_UpdateValue = mTransAudit_UpdateValue;
    }

    public static boolean isCalendarDateVisibleFlag() {
        return mCalendarDateVisibleFlag;
    }

    public static void setCalendarDateVisibleFlag(boolean mCalendarDateVisibleFlag) {
        EMathiApplication.mCalendarDateVisibleFlag = mCalendarDateVisibleFlag;
    }

    public static String getNewLoanDisbursementMinDate() {
        return mNewLoanDisbursementMinDate;
    }

    public static void setNewLoanDisbursementMinDate(String mNewLoanDisbursementMinDate) {
        EMathiApplication.mNewLoanDisbursementMinDate = mNewLoanDisbursementMinDate;
    }

    public static String getLastAuditDate() {
        return mLastAuditDate;
    }

    public static void setLastAuditDate(String mLastAuditDate) {
        EMathiApplication.mLastAuditDate = mLastAuditDate;
    }

    public static String getAuditFromDate() {
        return mAuditFromDate;
    }

    public static void setAuditFromDate(String mAuditFromDate) {
        EMathiApplication.mAuditFromDate = mAuditFromDate;
    }

    public static String getAuditToDate() {
        return mAuditToDate;
    }

    public static void setAuditToDate(String mAuditToDate) {
        EMathiApplication.mAuditToDate = mAuditToDate;
    }

    public static String getCalendarDialog_MinDate() {
        return mCalendarDialog_MinDate;
    }

    public static void setCalendarDialog_MinDate(String mCalendarDialog_MinDate) {
        EMathiApplication.mCalendarDialog_MinDate = mCalendarDialog_MinDate;
    }

    private boolean isDebuggable(Context context) {
        boolean debuggable = false;

        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(context.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));
        } catch (PackageManager.NameNotFoundException e) {
            /* debuggable variable will remain false */
        }

        return debuggable;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

        public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}
