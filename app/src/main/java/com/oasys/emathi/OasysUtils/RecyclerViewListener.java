package com.oasys.emathi.OasysUtils;

import android.view.View;

import java.io.InterruptedIOException;

public interface RecyclerViewListener {

	public void recyclerViewListClicked(View view, int position)
			throws InterruptedIOException;

}
