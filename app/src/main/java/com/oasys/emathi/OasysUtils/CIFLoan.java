package com.oasys.emathi.OasysUtils;

import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.VLoanTypes;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.SHGTable;

import java.util.ArrayList;

public class CIFLoan {

    private static ArrayList<OfflineDto> offlineDBTermLoanData1 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData2 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData3 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData4 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData5 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBCifLoanData6 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBCifLoanData7 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBCifLoanData8 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBCifLoanData9 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBCifLoanData10 = new ArrayList<>();

    public static void cifLoan()
    {
        ArrayList<VLoanTypes> cifLoan =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF();

        if(cifLoan!=null && cifLoan.size()>0) {
            for (int j = 0; j < cifLoan.size(); j++) {
                OfflineDto offline1 = new OfflineDto();
                offline1.setLoanId(cifLoan.get(j).getLoanId());
                offline1.setMemberId(cifLoan.get(j).getMemeberId());
                int amount = (int) Double.parseDouble(cifLoan.get(j).getAmount());
                offline1.setOutStanding(amount + "");
                offline1.setMem_os(amount + "");
                offlineDBTermLoanData1.add(offline1);
            }
        }
    }


    public static void cifLoan1()
    {
        ArrayList<VLoanTypes> cifLoan1 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF1();

        if(cifLoan1!=null && cifLoan1.size()>0) {
            for (int k = 0; k < cifLoan1.size(); k++) {
                OfflineDto offline2 = new OfflineDto();
                offline2.setLoanId(cifLoan1.get(k).getLoanId());
                offline2.setMemberId(cifLoan1.get(k).getMemeberId());
                int amount1 = (int) Double.parseDouble(cifLoan1.get(k).getAmount());
                offline2.setOutStanding(amount1 + "");
                offline2.setMem_os(amount1 + "");
                offlineDBTermLoanData2.add(offline2);
            }
        }
    }

    public  static void cifLoan2()
    {

        ArrayList<VLoanTypes> cifLoan2 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF2();
        if(cifLoan2!=null && cifLoan2.size()>0) {
            for (int l = 0; l < cifLoan2.size(); l++) {
                OfflineDto offline3 = new OfflineDto();
                offline3.setLoanId(cifLoan2.get(l).getLoanId());
                offline3.setMemberId(cifLoan2.get(l).getMemeberId());
                int amount2 = (int) Double.parseDouble(cifLoan2.get(l).getAmount());
                offline3.setOutStanding(amount2 + "");
                offline3.setMem_os(amount2 + "");
                offlineDBTermLoanData3.add(offline3);
            }
        }
    }

    public  static void cifLoan3()
    {

        ArrayList<VLoanTypes> cifLoan3 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF3();

        if(cifLoan3!=null && cifLoan3.size()>0) {
            for (int m = 0; m < cifLoan3.size(); m++) {
                OfflineDto offline4 = new OfflineDto();
                offline4.setLoanId(cifLoan3.get(m).getLoanId());
                offline4.setMemberId(cifLoan3.get(m).getMemeberId());
                int amount3 = (int) Double.parseDouble(cifLoan3.get(m).getAmount());
                offline4.setOutStanding(amount3 + "");
                offline4.setMem_os(amount3 + "");
                offlineDBTermLoanData4.add(offline4);
            }
        }
    }

    public  static void cifLoan4()
    {

        ArrayList<VLoanTypes> cifLoan4 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF4();

        if(cifLoan4!=null && cifLoan4.size()>0) {

            for (int n = 0; n < cifLoan4.size(); n++) {
                OfflineDto offline5 = new OfflineDto();
                offline5.setLoanId(cifLoan4.get(n).getLoanId());
                offline5.setMemberId(cifLoan4.get(n).getMemeberId());
                int amount4 = (int) Double.parseDouble(cifLoan4.get(n).getAmount());
                offline5.setOutStanding(amount4 + "");
                offline5.setMem_os(amount4 + "");
                offlineDBTermLoanData5.add(offline5);
            }
        }
    }

    // GroupLoan Outstanding

    public static  void groupCifLoan() {

        ArrayList<VLoanTypes> groupCifLoan = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF();
        if (groupCifLoan != null && groupCifLoan.size() > 0) {
            OfflineDto offline1 = new OfflineDto();
            offline1.setLoanId(groupCifLoan.get(0).getLoanId());
            offline1.setOutStanding(groupCifLoan.get(0).getOutStandingAmount());
            offlineDBCifLoanData6.add(offline1);
        }
    }


    public static  void groupCifLoan1() {

        ArrayList<VLoanTypes> groupCifLoan1 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF1();
        if (groupCifLoan1 != null && groupCifLoan1.size() > 0) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(groupCifLoan1.get(0).getLoanId());
            offline2.setOutStanding(groupCifLoan1.get(0).getOutStandingAmount());
            offlineDBCifLoanData7.add(offline2);
        }
    }


    public static  void groupCifLoan2() {

        ArrayList<VLoanTypes> groupCifLoan2 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF2();
        if (groupCifLoan2 != null && groupCifLoan2.size() > 0) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(groupCifLoan2.get(0).getLoanId());
            offline3.setOutStanding(groupCifLoan2.get(0).getOutStandingAmount());
            offlineDBCifLoanData8.add(offline3);
        }
    }

    public static  void groupCifLoan3() {

        ArrayList<VLoanTypes> groupCifLoan3 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF3();
        if (groupCifLoan3 != null && groupCifLoan3.size() > 0) {
            OfflineDto offline4 = new OfflineDto();
            offline4.setLoanId(groupCifLoan3.get(0).getLoanId());
            offline4.setOutStanding(groupCifLoan3.get(0).getOutStandingAmount());
            offlineDBCifLoanData9.add(offline4);
        }
    }

    public static  void groupCifLoan4() {

        ArrayList<VLoanTypes> groupCifLoan4 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF4();
        if (groupCifLoan4 != null && groupCifLoan4.size() > 0) {
            OfflineDto offline5 = new OfflineDto();
            offline5.setLoanId(groupCifLoan4.get(0).getLoanId());
            offline5.setOutStanding(groupCifLoan4.get(0).getOutStandingAmount());
            offlineDBCifLoanData10.add(offline5);
        }
    }
    public static void updateMROSCIFLoan()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData1) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateMROSCIFLoan1()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData2) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSCIFLoan2()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData3) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }
    public static  void updateMROSCIFLoan3()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData4) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSCIFLoan4()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData5) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateCIFLOANEDIT() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBCifLoanData6.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBCifLoanData6.get(0).getLoanId());

    }

    public static void updateCIFLOANEDIT1() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBCifLoanData7.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBCifLoanData7.get(0).getLoanId());

    }
    public static void updateCIFLOANEDIT2() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBCifLoanData8.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBCifLoanData8.get(0).getLoanId());

    }

    public static void updateCIFLOANEDIT3() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBCifLoanData9.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBCifLoanData9.get(0).getLoanId());

    }

    public static void updateCIFLOANEDIT4() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBCifLoanData10.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBCifLoanData10.get(0).getLoanId());

    }


}
