package com.oasys.emathi.OasysUtils;


public class Constants {


    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static void setBaseUrl(String baseUrl) {
        BASE_URL = baseUrl;
    }

//       public static String BASE_URL = "http://192.168.2.203:9821"; // TODO ::RAO
//    public static String BASE_URL = "https://eshdevapi.oasys.co";// TODO:: DEV
       public static String BASE_URL = "https://eshtestapi.oasys.co";// TODO::TEST
       //public static String BASE_URL = "https://uateshserver.oasys.co";// TODO::TEST
//       public static String BASE_URL = "https://uateshserver.oasys.co";// TODO :: UAT
//    public static String BASE_URL = "http://192.168.1.239:9821";//Ruithraj URL
// public static final String BASE_URL = "http://devoss.oasys.co:8631";+
    // http://192.168.2.82:8631/animator/login
    //http://devoss.oasys.co:863145c8f633-22ac-43f4-8f5b-ae307594f1ef

//    public static String BASE_URL = "http://192.168.3.38:9821";// TODO :: RAO LAPTOP
    public static String EXCEPTION = "Network Error";
    //GET Method:
    public static final String SPINNER_BANK_ITEM_AMOUNT = "/shgsavingsaccount/balance/get/";
    public static final String GET_LANGUAGE = "/animator/getanimatorlanguage/";
    public static final String SHG_URL = "/animator/getshgbyanimator?id=";
//    public static final String PROFILE_ANIMATOR = "/profile/animatorprofile/";
    public static final String PROFILE_ANIMATOR = "/profile/animatorprofile/?animatorId=";
    public static final String GROUP_SAVINGS_SUMMARY = "/apkreports/getgroupsavingssummary/";
    public static final String YEAR_REPORT = "/apkreports/get/years/list/forshg/";
    public static final String MONTH_REPORT = "/apkreports/membermonthlyreport/";
    public static final String REPORT_BANKBALANCE = "/apkreports/getbalance/";
    public static final String REPORT_BANKTRANSACTION = "/apkreports/getbanktransactionsummary/";
    public static final String PROFILE_GETSHGACCOUNTNUMBERUPDATION = "/profile/getshgaccountnumbers/";
    public static final String PROFILE_SHGACCOUNTNUMBERANDUPLOADPASSBOOK = "/shg/accountPassbookupload/";
    public static final String Get_Member = "/animator/getmembersbyshg/";
    public static final String Get_LoanTypes = "/newloan/getloandetails/";
    public static final String INCOME = "/incomeledger/getallincometypes";
    public static final String GETGROUPLOANTYPES = "/newloan/shg/loans/get/";
    public static final String GETOUTSTANDINGAMOUNT = "/memberloanrepayment/getmemberloanrepaymentlist";
    public static final String GETINTERNALOUTSTANDING = "/newinternalloan/get/members/internal/loan/outstanding/";
    public static final String GET_EXPENSE = "/expenses/expensestype";
    public static final String LD_IL_LOANTYPE = "/loansetting/purposeofinternalLoan";
    public static final String LD_BK_LTYPE = "/loansetting/bankloantype";
    public static final String LD_BL_POL = "/loansetting/purposeofloan";
    public static final String LD_INST_TYPE = "/loansetting/Installmenttype";
    public static final String LD_BANK_LOAN = "/newloan/get/newloantypes";
    public static final String LD_FED_LTYPE = "/loansetting/federationloanType";
//    public static final String LD_MFI_LTYPE = "/bankname/get/financebasedbank";   // TODO:"/loansetting/mfiloanType"
    public static final String LD_MFI_LTYPE = "/bankname/get/financebasedtype";   // TODO:"/loansetting/mfiloanType"
    public static final String LD_BANK_DETAILS = "/bankname/district/get/";  //http://192.168.3.253:863161386163-3838-6538-2d65-6665342d3131
    public static final String LD_BRANCH_DETAILS = "/branch/district/get?districtId=";// TODO:  62353466-3837-3963-2d65-6665342d3131&bankId=62303766-6138-3162-2d66-3763652d3131  /api/usermanagement/branch/bank/get/
    public static final String GROUP_LOAN_REPAYMENT = "/newloan/shg/loans/get/";
    public static final String TRANSACTION_LOANBANKSPINNERITEM = "/shgsavingsaccount/getByshgId/";
    public static final String GROUP_LOAN_REPAYMENT_OUTSTANDING = "/newloan/getloandetails/";
    public static final String LD_IL_OTList = "/newinternalloan/get/members/internal/loan/outstanding/";
    public static final String GETMINUTES_OF_MEETINGS = "/shgminutes/get";
    public static final String GET_MOM_LOANTYPE = "/loansetting/purposeofinternalLoan";
    public static final String MEMBERREPORT_LOANSUMMARY_OTHERLOANS = "/apkreports/memberloansummary/member/loanid/";
    public static final String GROUPLOANSUMMARY_OTHERLOAN = "/apkreports/getgrouploanssummary/";
    public static final String MEM_LOAN_SUMMARY = "/apkreports/memberinternalloan/";
//    public static final String MEM_LOAN_SUMMARY = "/apkreports/memberinternalloanoutstandamtandhistory/";
    public static final String GROUPLOANSUMMARY_INTERNALLOAN = "/apkreports/groupinternalloan/";
    public static final String MEMBERREPORT_SAVINGSUMMARY = "/apkreports/get/member/savingsummary/";
    public static final String NAV_DETAILS = "/cashofgroup/getbygroupid/";
    public static final String EXT_LOAN_TYPES = "/settings/get/existingloantypes";
    public static final String LD_FROM_LOANAC = "/loandisbursement/get/disbursmentfromloanaccount/";
    public static final String LD_FROM_SBAC = "/loandisbursement/get/disbursmentfromsbaccount/";
    public static final String LD_INC_LIMIT = "/newloan/increaselimit/";
    public static final String LD_CC_WITHDRAWL = "/loandisbursement/ccloanwithdrawalamount/";
    public static final String VERIFICATION = "/shgVerification/get/";
    public static final String DOWNLOAD__PDF_URL = "https://com.yesteam.eshakti.nabard.org/Downloads/mobileapplication.pdf";
    public static final String BT_CB_FD_VALUE = "/shgsavingsaccount/balance/get/";
    public static final String BT_RD_CB_VALUE = "/recurringdeposit/get?shgId=";
    public static final String MEMEBER_ACCOUNT_NO_BRANCHNAME_UPDATION = "/branch/district/get?";
    public static final String GETTRAININGLIST = "/shgTraining/get/training?languageId=";
    public static final String GET_CHECKLIST = "/settings/get/checklist/";
    public static final String PROFILE_SS = "";
    public static final String MEETING_AUDITINGDATE = "/shgAuditing/getauditdate/";
    public static final String PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION = "/bankname/district/";
    public static final String UPLOAD_SCHEDULE = "/shg/urlupload/";
    public static final String UPLOAD_PASSBOOK = "/shg/passbookupload/";
    public static final String UPLOAD_PHOTO = "/members/urlupload/";
    public static final String GET_MEM_PHOTO = "/members/viewimage/";
    public static final String LOGOUT_TOKENDELETION = "/revoke";
    public static final String TRANSACTION_AUDIT = "/auditors/getAuditAnimatorDetails?shgId=";
    public static final String APPROVE_AUDIT = "/auditors/updateShgTransactionAudit";

    //POST method
    public static final String LOGIN_URL = "/login";
    public static final String PROFILE_CREDIT_LINKAGE = "/profile/creditlinkedinfo";
    public static final String PROFILE_GROUP_PROFILE = "/profile/groupprofile/";
    //    public static final String MONTH_REPORT = "/apkreports/getmembermonthlyreport/b337b0d1-9c79-458d-a996-ff5d281f401b";
    public static final String PROFILE_SHGACCOUNTNUMBERUPDATION = "/profile/doshgaccountnumbersupdate";
//    public static final String SETTINGS_CHANGEPASSWORD = "/api/usermanagement/v1/users/changepassword";
    public static final String SETTINGS_CHANGEPASSWORD = "/users/changepassword";
    public static final String PROFILE_GET_MOBILE_NUMBER = "/profile/get/member/mobile/numbers/1393b472-de9c-4362-be6d-fb21246c5a14";
    public static final String PROFILE_UPDATE_MOBILE_NUMBER = "/profile/domobilenumberupdation";
    public static final String PROFILE_UPDATE_AADHAR_NUMBER = "/profile/doAadharnumberupdation";
    public static final String PROFILE_UPDATE_SSN_NUMBER = "/profile/doSsnnumberupdation";
    public static final String PROFILE_UPDATE_PIP_NUMBER = "/profile/doPipnumberupdation";
    public static final String PROFILE_SSS_TYPE_ID = "/settings/getPFlagIdByLanguageId/26";
    public static final String RECURRINGFLAGVALUE = "https://eshtestapi.oasys.co/recurringdeposit/getRecurringDepositFlag/";

    // public static final String REPORT_BANKBALANCE = "/apkreports/getbalance/";
    public static final String SAVINGS = "/savings/post";
    public static final String SUBSCRIPTION = "/incomeledger/post";
    public static final String PENALTY = "/incomeledger/post";
    public static final String OTHERINCOME = "/incomeledger/post";
    public static final String SEEDFUND = "/incomeledger/post";
    public static final String DONATION = "/incomeledger/post";
    public static final String BT_ENTRY = "/banktransaction/addbanktransaction";
    public static final String BT_PLOAN = "/newloan/shg/loans/get/";
    public static final String BT_FIXED = "/fixedDeposit/post";
    public static final String BT_RD = "/recurringdeposit/post";
    public static final String MEMBERLOANREPAYMENT = "/memberloanrepayment/post";
    public static final String INTERNALLOANREPAYMENT = "/newinternalloan/repayment/post";
    public static final String SAVTOLOAN = "/banktransaction/savingstoloanaccount";
    public static final String SAVTOSAV = "/banktransaction/savingstosavingsaccount";
    public static final String EXPENSE = "/expenses/post";
    public static final String LD_IL_ENTRY = "/newinternalloan/disburesement/post";
    public static final String LD_IL_MFI_DISB = "/newloan/mfi/post";
    public static final String LD_IL_BANK_DISB = "/newloan/bank/post";
    public static final String LD_IL_FED_DISB = "/newloan/federation/post";
    public static final String GROUPLOANREPAYMENT_END = "/grouploanrepayment/post";   //MEETINGS
    public static final String LOANAPPLICATION = "/loanapplication/post";
    public static final String LD_EL_ILIM_DIS = "/newloan/increaselimit";
    public static final String LD_EL_LFLAC_DIS = "/loandisbursement/ldl/post";
    public static final String LD_EL_LFSBAC_DIS = "/loandisbursement/lds/post";
    public static final String LD_EL_CCLW_DIS = "/loandisbursement/ccloanwithdrawal";
    public static final String VERIFY_UPDATE = "/shgVerification/updateverificationdetails";
    public static final String SHGTRAININGPOST = "/shgTraining/post";
    public static final String ATTENDANCEPOST = "/shgmeetingattendance/post";
    public static final String SHGMEETINGSPOST = "/shgmeetings/post";
    public static final String NEW_RD_ACCOUNTCREATION = "/recurringdeposit/addsavingdetail";
    public static final String SHG_DEACTIVATE = "/apk/groupdeactivation/";
    public static final String MEETING_AUDITING_DETAILS = "/shgAuditing/post";
    public static final String MEMBER_ACCOUNT_NUMBER_UPDATION = "/profile/domemberaccountupdation";

  /*  public static final String GET_TABLE_NAME_URL="http://192.168.1.239:9821/offline/tablename/get?userId=87195a04-1187-4878-a9fd-3fc102334f70";
    public static final String GET_TABLE_DETAILS_URL="http://192.168.1.239:9821/offline/tabledetails/get?userId=87195a04-1187-4878-a9fd-3fc102334f70";*/

   /* public static final String GET_TABLE_NAME_URL = "http://192.168.1.239:9821/offline/tablename/get?userId=";
    public static final String GET_TABLE_DETAILS_URL = "http://192.168.1.239:9821/offline/tabledetails/get";
    public static final String OFFLINE_API = "http://192.168.1.239:9821/offline/transaction";
*/

    public static final String GET_TABLE_NAME_URL = "/offline/tablename/get?userId=";
    public static final String GET_TABLE_DETAILS_URL = "/offline/tabledetails/get";
    public static final String OFFLINE_API = "/offline/transaction";
    public static final String GETPREVIOUS_USERDATA_API = "/offline/transDateFlag/";
    public static final String GET_UPDATEBANKAMOUNT_API = "/savings/get/balance/";


    public static final String FIXED_DEPOSIT_REPORT = "/apkreports/getfixeddeposit/";
    public static final String RECURRING_DEPOSIT_REPORT = "/apkreports/getrecurringdeposit/";
    public static final String ACCOUNTTOACCOUNTTRANFERT_REPORT = "/apkreports/getaccounttoaccounttransferreport/";

  /*  public static String EXCEPTION = "Network Error";
    //GET Method:
    public static final String SPINNER_BANK_ITEM_AMOUNT = "/api/eshakti/shgsavingsaccount/balance/get/";
    public static final String GET_LANGUAGE = "/api/eshakti/animator/getanimatorlanguage/";
    public static final String SHG_URL = "/api/eshakti/animator/getshgbyanimator?id=";
    public static final String PROFILE_ANIMATOR = "/api/eshakti/profile/animatorprofile/";
    public static final String GROUP_SAVINGS_SUMMARY = "/api/eshakti/apkreports/getgroupsavingssummary/";
    public static final String YEAR_REPORT = "/api/eshakti/apkreports/get/years/list/forshg/";
    public static final String MONTH_REPORT = "/api/eshakti/apkreports/membermonthlyreport/";
    public static final String REPORT_BANKBALANCE = "/api/eshakti/apkreports/getbalance/";
    public static final String REPORT_BANKTRANSACTION = "/api/eshakti/apkreports/getbanktransactionsummary/";
    public static final String PROFILE_GETSHGACCOUNTNUMBERUPDATION = "/api/eshakti/profile/getshgaccountnumbers/";
    public static final String Get_Member = "/api/eshakti/animator/getmembersbyshg/";
    public static final String Get_LoanTypes = "/api/eshakti/newloan/getloandetails/";
    public static final String INCOME = "/api/eshakti/incomeledger/getallincometypes";
    public static final String GETGROUPLOANTYPES = "/api/eshakti/newloan/shg/loans/get/";
    public static final String GETOUTSTANDINGAMOUNT = "/api/eshakti/memberloanrepayment/getmemberloanrepaymentlist";
    public static final String GETINTERNALOUTSTANDING = "/api/eshakti/newinternalloan/get/members/internal/loan/outstanding/";
    public static final String GET_EXPENSE = "/api/eshakti/expenses/expensestype";
    public static final String LD_IL_LOANTYPE = "/api/eshakti/loansetting/purposeofinternalLoan";
    public static final String LD_BK_LTYPE = "/api/eshakti/loansetting/bankloantype";
    public static final String LD_BL_POL = "/api/eshakti/loansetting/purposeofloan";
    public static final String LD_INST_TYPE = "/api/eshakti/loansetting/Installmenttype";
    public static final String LD_BANK_LOAN = "/api/eshakti/newloan/get/newloantypes";
    public static final String LD_FED_LTYPE = "/api/eshakti/loansetting/federationloanType";
    public static final String LD_MFI_LTYPE = "/api/eshakti/bankname/get/financebasedbank";   // TODO:"/api/eshakti/loansetting/mfiloanType"
    public static final String LD_BANK_DETAILS = "/api/eshakti/bankname/district/get/";  //http://192.168.3.253:863161386163-3838-6538-2d65-6665342d3131
    public static final String LD_BRANCH_DETAILS = "/api/eshakti/branch/district/get?districtId=";// TODO:  62353466-3837-3963-2d65-6665342d3131&bankId=62303766-6138-3162-2d66-3763652d3131  /api/usermanagement/branch/bank/get/
    public static final String GROUP_LOAN_REPAYMENT = "/api/eshakti/newloan/shg/loans/get/";
    public static final String TRANSACTION_LOANBANKSPINNERITEM = "/api/eshakti/shgsavingsaccount/getByshgId/";
    public static final String GROUP_LOAN_REPAYMENT_OUTSTANDING = "/api/eshakti/newloan/getloandetails/";
    public static final String LD_IL_OTList = "/api/eshakti/newinternalloan/get/members/internal/loan/outstanding/";
    public static final String GETMINUTES_OF_MEETINGS = "/api/eshakti/shgminutes/get";
    public static final String GET_MOM_LOANTYPE = "/api/eshakti/loansetting/purposeofinternalLoan";
    public static final String MEMBERREPORT_LOANSUMMARY_OTHERLOANS = "/api/eshakti/apkreports/memberloansummary/member/loanid/";
    public static final String GROUPLOANSUMMARY_OTHERLOAN = "/api/eshakti/apkreports/getgrouploanssummary/";
    public static final String MEM_LOAN_SUMMARY = "/api/eshakti/apkreports/memberinternalloan/";
    public static final String GROUPLOANSUMMARY_INTERNALLOAN = "/api/eshakti/apkreports/groupinternalloan/";
    public static final String MEMBERREPORT_SAVINGSUMMARY = "/api/eshakti/apkreports/get/member/savingsummary/";
    public static final String NAV_DETAILS = "/api/eshakti/cashofgroup/getbygroupid/";
    public static final String EXT_LOAN_TYPES = "/api/eshakti/settings/get/existingloantypes";
    public static final String LD_FROM_LOANAC = "/api/eshakti/loandisbursement/get/disbursmentfromloanaccount/";
    public static final String LD_FROM_SBAC = "/api/eshakti/loandisbursement/get/disbursmentfromsbaccount/";
    public static final String LD_INC_LIMIT = "/api/eshakti/newloan/increaselimit/";
    public static final String LD_CC_WITHDRAWL = "/api/eshakti/loandisbursement/ccloanwithdrawalamount/";
    public static final String VERIFICATION = "/api/eshakti/shgVerification/get/";
    public static final String DOWNLOAD__PDF_URL = "https://com.yesteam.eshakti.nabard.org/Downloads/mobileapplication.pdf";
    public static final String BT_CB_FD_VALUE = "/api/eshakti/shgsavingsaccount/balance/get/";
    public static final String BT_RD_CB_VALUE = "/api/eshakti/recurringdeposit/get?shgId=";
    public static final String MEMEBER_ACCOUNT_NO_BRANCHNAME_UPDATION = "/api/eshakti/branch/district/get?";
    public static final String GETTRAININGLIST = "/api/eshakti/shgTraining/get/training";
    public static final String GET_CHECKLIST = "/api/eshakti/settings/get/checklist/";
    public static final String PROFILE_SS = "";
    public static final String MEETING_AUDITINGDATE = "/api/eshakti/shgAuditing/getauditdate/";
    public static final String PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION = "/api/eshakti/bankname/district/get/";




    //POST method
    public static final String LOGIN_URL = "/api/eshakti/login";user123

    public static final String PROFILE_CREDIT_LINKAGE = "/api/eshakti/profile/creditlinkedinfo";
    public static final String PROFILE_GROUP_PROFILE = "/api/eshakti/profile/groupprofile/";
    //    public static final String MONTH_REPORT = "/api/eshakti/apkreports/getmembermonthlyreport/b337b0d1-9c79-458d-a996-ff5d281f401b";
    public static final String PROFILE_SHGACCOUNTNUMBERUPDATION = "/api/eshakti/profile/doshgaccountnumbersupdate";
    public static final String SETTINGS_CHANGEPASSWORD = "/api/usermanagement/v1/users/changepassword";
    public static final String PROFILE_GET_MOBILE_NUMBER = "/profile/get/member/mobile/numbers/1393b472-de9c-4362-be6d-fb21246c5a14";
    public static final String PROFILE_UPDATE_MOBILE_NUMBER = "/api/eshakti/profile/domobilenumberupdation";

    // public static final String REPORT_BANKBALANCE = "/api/eshakti/apkreports/getbalance/";
    public static final String SAVINGS = "/api/eshakti/savings/post";
    public static final String SUBSCRIPTION = "/api/eshakti/incomeledger/post";
    public static final String PENALTY = "/api/eshakti/incomeledger/post";
    public static final String OTHERINCOME = "/api/eshakti/incomeledger/post";
    public static final String SEEDFUND = "/api/eshakti/incomeledger/post";
    public static final String DONATION = "/api/eshakti/incomeledger/post";
    public static final String BT_ENTRY = "/api/eshakti/banktransaction/addbanktransaction";
    public static final String BT_PLOAN = "/api/eshakti/newloan/shg/loans/get/";
    public static final String BT_FIXED = "/api/eshakti/fixedDeposit/post";
    public static final String BT_RD = "/api/eshakti/recurringdeposit/post";
    public static final String MEMBERLOANREPAYMENT = "/api/eshakti/memberloanrepayment/post";
    public static final String INTERNALLOANREPAYMENT = "/api/eshakti/newinternalloan/repayment/post";
    public static final String SAVTOLOAN = "/api/eshakti/banktransaction/savingstoloanaccount";
    public static final String SAVTOSAV = "/api/eshakti/banktransaction/savingstosavingsaccount";
    public static final String EXPENSE = "/api/eshakti/expenses/post";
    public static final String LD_IL_ENTRY = "/api/eshakti/newinternalloan/disburesement/post";
    public static final String LD_IL_MFI_DISB = "/api/eshakti/newloan/mfi/post";
    public static final String LD_IL_BANK_DISB = "/api/eshakti/newloan/bank/post";
    public static final String LD_IL_FED_DISB = "/api/eshakti/newloan/federation/post";
    public static final String GROUPLOANREPAYMENT_END = "/api/eshakti/grouploanrepayment/post";   //MEETINGS
    public static final String LOANAPPLICATION = "/api/eshakti/loanapplication/post";
    public static final String LD_EL_ILIM_DIS = "/api/eshakti/newloan/increaselimit";
    public static final String LD_EL_LFLAC_DIS = "/api/eshakti/loandisbursement/ldl/post";
    public static final String LD_EL_LFSBAC_DIS = "/api/eshakti/loandisbursement/lds/post";
    public static final String LD_EL_CCLW_DIS = "/api/eshakti/loandisbursement/ccloanwithdrawal";
    public static final String VERIFY_UPDATE = "/api/eshakti/shgVerification/updateverificationdetails";
    public static final String SHGTRAININGPOST = "/api/eshakti/shgTraining/post";
    public static final String ATTENDANCEPOST = "/api/eshakti/shgmeetingattendance/post";
    public static final String SHGMEETINGSPOST = "/api/eshakti/shgmeetings/post";
    public static final String SHG_DEACTIVATE = "/api/eshakti/apk/groupdeactivation/";
    public static final String MEETING_AUDITING_DETAILS= "/api/eshakti/shgAuditing/post";*/


}

