package com.oasys.emathi.OasysUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Administrator on 2/6/15.
 * Checking for network connection
 */
public class NetworkConnection {

    private static Context context = null; //Context for activity
    private static NetworkConnection networkConnection = null;


    public static NetworkConnection getNetworkConnection(Context context_) {
        if (networkConnection == null) {
            networkConnection = new NetworkConnection();
        }
        context = context_;
        return networkConnection;
    }

    /*public NetworkConnection(Context context) {
        this.context = context;
    }*/

    //If network available it returns true else false
    public boolean isNetworkAvailable() {
       /* ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();*/
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                || conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {



            // notify user you are online
            return true;

        } else if (conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED
                || conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
            return false;
            // notify user you are not online
        }
        return false;
    }
}
