package com.oasys.emathi.OasysUtils;


public class RegionalLanguage_tamil {

	public RegionalLanguage_tamil() {
	}

	public static void RegionalStrings() {

        AppStrings.S_B_N = "வங்கி பெயரைத் தேர்ந்தெடுக்கவும்";
		AppStrings.recurringDeposit = "தொடர் வைப்பு நிதி";
		AppStrings.upload_schedule="அட்டவணை பதிவேற்றவும் VI";
		AppStrings.AboutLicense = "உரிமம்";
		AppStrings.version="பதிப்பு";
        AppStrings.EMATHI = "EMATHI";
		/*
		 * AppStrings.aboutYesbooks =
		 * "    சுய உதவி குழுக்கள் தங்களது வரவு செலவு கணக்குகளை " +
		 * " பதிவு செய்வதற்கு கைபேசி வாயிலாக" +
		 * " புதிய மென்பொருள் YESTEAM நிறுவனத்தால் உருவாக்கப்பட்டுள்ளது." +
		 * " மகளிர்குழுக்கள்  தங்களுடைய குழுவின் வரவு செலவு கணக்குகளை" +
		 * " கைபேசி வாயிலாக எளிய தமிழில் பதிவு செய்து கொள்ளலாம்.";
		 */
		AppStrings.aboutYesbooks = "EMathi or Digitisation of SHGs is an initiative of Micro Credit and Innovations Department of NABARD in line with our Hon'ble PM statement,"
				+ "'we move with the dream of electronic digital India...'. Digital India is a Rs 1.13-lakh crore initiative of Government of India to integrate the "
				+ "government departments and the people of India and to ensure effective governance. " + "It is to "
				+ " transform India into digital empowered society and knowledge economy";
		AppStrings.afterDate = " தேதிக்கு பின் தேர்ந்தெடுக்கவும்";
		AppStrings.AgentLogin = "முகவர் நுழைவு";
		AppStrings.agentProfile = "முகவர் சுயவிவரம்";
		AppStrings.amount = "தொகை";
		AppStrings.April = "ஏப்ரல்";
		AppStrings.Attendance = "வருகை பதிவேடு";
		AppStrings.August = "ஆகஸ்ட்";
		AppStrings.balance = "வரவேண்டியவை";
		AppStrings.balanceAsOn = "அன்று இருப்பு : ";
		AppStrings.balanceSheet = "இருப்பு நிலைக்குறிப்பு";
		AppStrings.balanceSheetHeader = "இருப்பு நிலைக்குறிப்பு";
		AppStrings.bankBalance = "வங்கி இருப்பு";
		AppStrings.bankTransaction = "வங்கி பரிவர்த்தனை";
		AppStrings.bankDeposit = "பணம்செலுத்துதல்";
		AppStrings.bankExpenses = "வங்கி செலவுகள்";
		AppStrings.bankInterest = "வங்கி வட்டி";
		AppStrings.bankrepayment = "பணம் திரும்பி கட்டுதல்";
		AppStrings.bankTransactionSummary = "வங்கிநடவடிக்கைசுருக்கம்";
		AppStrings.BL_Disbursed = "பெற்றது";
		AppStrings.BL_Repaid = "செலுத்தியது";
		AppStrings.calAlert = " வேறு தேதியை தேர்ந்தெடுக்கவும்";
		AppStrings.calFromToDateAlert = "இரண்டு தேதிகளையும் தேர்வுசெய்யவும்";
		AppStrings.cashatBank = "வங்கியிருப்பு :  ₹ ";
		AppStrings.cashatBankAlert = "வங்கியிருப்பை சரிபார்க்கவும்";
		AppStrings.cashFlowStatement = "பணப்பாய்வறிக்கை";
		AppStrings.cashinhand = "கையிருப்பு :  ₹ ";
		AppStrings.cashinHandAlert = "கையிருப்பை சரிபார்க்கவும்";
		AppStrings.ConfirmPassword = "திரும்ப புதிய ரகசிய எண்";
		AppStrings.contactNo = "தொடர்பு எண்";
		AppStrings.contacts = "தொடர்புக்கு";
		AppStrings.credit = "வரவு";
		AppStrings.Dashboard = " முதன்மைப் பட்டியலுக்குச் செல்ல";
		AppStrings.date = "தேதி";
		AppStrings.debit = "செலவு";
		AppStrings.December = "டிசம்பர்";
		AppStrings.DepositnullAlert = "அனைத்து தொகைகளின் விவரங்களை வழங்கவும்";
		AppStrings.details = "விபரம்";
		AppStrings.dialogMsg = "நீங்கள் இதே தேதியில் தொடர விரும்புகிறீர்களா ? ";
		AppStrings.dialogNo = "இல்லை";
		AppStrings.dialogOk = "ஆம்";
		AppStrings.difference = " தொகை வித்தியாசம் : ";
		AppStrings.disconnectInternet = " ஆஃப்லைன் பரிவர்த்தனையை தொடர உங்கள் இணைய இணைப்பை துண்டிக்கவும்.";
		AppStrings.donation = " நன்கொடை";
		AppStrings.edit = "திருத்த";
		AppStrings.expenses = "செலவுகள்";
		AppStrings.February = "பிப்ரவரி";
		AppStrings.federationincome = "FEDS";//"கூட்டமைப்பு  வரவு";
		AppStrings.federationIncomePaid = "கூட்டமைப்புக்கு செலுத்தியது";
		AppStrings.fixedDeposit = "இட்டு வைப்பு";
		AppStrings.fromDate = "இருந்து";
		AppStrings.FromDate = "தேதியிலிருந்து";
		AppStrings.groupList = "குழு பட்டியல்";
		AppStrings.groupLoan = "குழு கடன்";
		AppStrings.grouploanrepayment = "குழு கடன் செலுத்துதல்";
		AppStrings.groupLoansummary = "குழு கடன் சுருக்கம்";
		AppStrings.GroupLogin = "குழு நுழைவு";
		AppStrings.groupProfile = "குழு சுயவிவரம்";
		AppStrings.groupRepaidAlert = "உங்கள் நிலுவை தொகையை சரிபார்க்கவும்";//"தொகையை சரிபார்க்கவும்";
		AppStrings.GroupReports = "குழு அறிக்கை";
		AppStrings.groupSavingssummary = "குழு சேமிப்பு சுருக்கம்";
		AppStrings.help = "உதவி";
		AppStrings.income = "வருமானம்";
		AppStrings.interest = "வட்டி";
		AppStrings.internetError = "உங்கள் இணைய இணைப்பை சரிபார்க்கவும்";
		AppStrings.IncomeExpenditure = "வரவு செலவு கணக்கு";
		AppStrings.January = "ஜனவரி";
		AppStrings.July = "ஜூலை";
		AppStrings.June = "ஜூன்";
		AppStrings.last5Transaction = "இறுதியான ஐந்து பரிவர்த்தனைகள்";
		AppStrings.lastMonthReport = "மாத அறிக்கை";
		AppStrings.lastTransactionDate = "கடைசி பரிவர்த்தனை தேதி";
		AppStrings.listofexpenses = "செலவுகளின் வரிசை";
		AppStrings.loanOutstandingBank = " வங்கி நிலுவை : ";
		AppStrings.loanOutstandingBook = " புத்தக நிலுவை : ";
		AppStrings.loanSummary = "கடன் சுருக்கம்";
		AppStrings.login = "   உள்நுழைவு   ";
		AppStrings.loginAlert = "பயனாளர் பெயர் அல்லது ரகசிய எண் தவறு";
		AppStrings.loginPwdAlert = "ரகசிய எண் விவரங்களை வழங்கவும்";
		AppStrings.loginUserAlert = "பயனாளர்பெயர் விவரங்களை வழங்கவும்";
		AppStrings.logOut = "வெளியேறு";
		AppStrings.LoginScreen = "புகுபதிவு திரை";
		AppStrings.March = "மார்ச்";
		AppStrings.memberloanrepayment = "உறுப்பினர் கடன் செலுத்துதல்";
		AppStrings.meeting = "கலந்தாய்வு";
		AppStrings.memberName = "உறுப்பினர் பெயர்";
		AppStrings.May = "மே";
		AppStrings.Memberreports = "உறுப்பினர் அறிக்கை";
		AppStrings.MinutesAlert = "மேற்கண்டவற்றுள் ஏதேனும் ஒன்றை தேர்வுசெய்யவும்";
		AppStrings.MinutesofMeeting = "தீர்மானம்";
		AppStrings.Name = "பெயர்  ";
		AppStrings.NewPassword = "புதிய ரகசிய எண்";
		AppStrings.November = "நவம்பர்";
		AppStrings.nullAlert = "தொகைகளின் விவரங்களை வழங்கவும்";
		AppStrings.October = "அக்டோபர்";
		AppStrings.OldPassword = "தற்போதைய ரகசிய எண்";
		AppStrings.OutsatndingAmt = "நிலுவை";
		AppStrings.OutstandingAmount = "செலுத்த வேண்டியவை";
		AppStrings.offlineTransaction = "பரிவர்த்தனைகள்";
		AppStrings.offlineTransactionCompleted = "உங்கள் ஆஃப்லைன் தகவல்கள் சேகரிக்கப்பட்டுள்ளது.";
		AppStrings.offlineTransactionreports = "இணைப்பின்றி பதிவு செய்யப்பட்ட பரிவர்த்தனை";
		AppStrings.otherExpenses = "இதர செலவுகள்";
		AppStrings.otherincome = "இதர வருமானம் ";
		AppStrings.outstanding = "இருப்பு தொகை";
		AppStrings.passWord = "ரகசிய எண்";
		AppStrings.passwordchange = "ரகசிய எண் மாற்றுதல்";
		AppStrings.payment = "பண வழங்கீடு";
		AppStrings.penalty = "அபராதம்";
		AppStrings.InternalLoanDisbursement = "கடன் வழங்குதல்";
		AppStrings.principleAmount = "அசல்";
		AppStrings.profile = "சுயவிவரம்";
		AppStrings.pwdChangeAlert = "உங்கள் ரகசிய எண் மாற்றப்பட்டது";
		AppStrings.pwdIncorrectAlert = "சரியான ரகசிய எண்ணை உள்ளிடவும்";
		AppStrings.Il_Disbursed = "தனி நபர் கடன் பெற்றது";
		AppStrings.IL_Loan = "தனி நபர் கடன்";
		AppStrings.Il_Repaid = "தனி நபர் கடன் செலுத்தியது";
		AppStrings.receipt = "பற்று சீட்டு";
		AppStrings.reports = "அறிக்கை";
		AppStrings.Repaymenybalance = "நிலுவை";
		AppStrings.savings = "சேமிப்பு";
		AppStrings.savingsBank = " வங்கி சேமிப்பு : ";
		AppStrings.savingsBook = " புத்தக சேமிப்பு : ";
		AppStrings.savingsSummary = "சேமிப்பு சுருக்கம்";
		AppStrings.selectedDate = "தேர்வுசெய்யப்பட்ட தேதி";
		AppStrings.settings = "அமைப்பு";
		AppStrings.snacks = "டீ";
		AppStrings.stationary = "எழுதுபொருள்";
		AppStrings.submit = "சமர்ப்பிக்க";
		AppStrings.subscriptioncharges = "சந்தா ";
		AppStrings.summary = "சுருக்கம்";
		AppStrings.September = "செப்டெம்பர்";
		AppStrings.telephone = "தொலைபேசி";
		AppStrings.toDate = "வரை";
		AppStrings.ToDate = "தேதி வரை";
		AppStrings.total = "மொத்தம்";
		AppStrings.totalSavings = "மொத்த சேமிப்பு";
		AppStrings.transaction = "நடவடிக்கை";
		AppStrings.transactionCompleted = "உங்கள் பரிவர்த்தனை முடிந்தது";
		AppStrings.transactionsummary = "வங்கிநடவடிக்கைசுருக்கம்";
		AppStrings.transport = "போக்குவரத்து";
		AppStrings.trialBalance = "விசாரணை சமநிலை";
		AppStrings.ToDate = "தேதி வரை";
		AppStrings.uploadPhoto = "புகைப்பட பதிவேற்றம்";
		AppStrings.userName = "மொபைல் எண்";
		AppStrings.values = new String[] { "இடம் (சொந்த ஊர்).", "உறுப்பினர் அனைவரும் கலந்து கொண்டனர்.",
				"சேமிப்பு தொகை வசூலிக்கப்பட்டது.", "சங்க கடன் கொடுப்பது பற்றி விவாதிக்கப்பட்டது.",
				"கடன் தொகை மற்றும் வட்டி வசூலிக்கப்பட்டது.", "தொழில் பயிற்சி எடுப்பது பற்றி விவாதிக்கப்பட்டது. ",
				"வங்கி கடன் பெறலாம் என்று தீர்மானிக்கபட்டது. ", "பொது கருத்துக்கள் பற்றி பேசப்பட்டது.",
				"சுகாதாரம் பற்றி விவாதிக்கப்பட்டது.", "கல்வியின் முக்கியத்துவம் பற்றி பேசப்பட்டது. " };
		AppStrings.withdrawl = "பணம்எடுத்தல்";
		AppStrings.yes = "சரி";
		AppStrings.auditing = "தணிக்கை";
		AppStrings.auditing_Date = "தணிக்கை தேதி";
		AppStrings.auditor_Name = "தணிக்கையாளர் பெயர்";
		AppStrings.auditing_Period = "தணிக்கை கால கணக்கு தேதியை தேர்வுசெய்யவும்";
		AppStrings.training = "பயிற்சி";
		AppStrings.training_Date = "பயிற்சி தேதி";
		AppStrings.training_types = new String[] { "சுகாதார விழிப்புணர்வு", "கணக்கு வைப்பவர்கள் பயிற்சி",
				"வாழ்வாதார பயிற்சி", "சமூக விழிப்புணர்வு", "கல்வி விழிப்புணர்வு" };
		AppStrings.changeLanguage = "மொழி மாற்றம்";
		AppStrings.interestRepayAmount = "நடப்பு";
		AppStrings.interestRate = "வட்டி விகிதம்";
		AppStrings.savingsAmount = "சேமிப்பு தொகை";
		AppStrings.noGroupLoan_Alert = "குழு கடன் வழங்கப்படவில்லை";
		AppStrings.confirmation = "உறுதி செய்க";
		AppStrings.fromDateAlert = "தணிக்கை கால தேதியை வழங்கவும்";
		AppStrings.toDateAlert = "தணிக்கை கால தேதியை வழங்கவும்";
		AppStrings.auditingDateAlert = "தணிக்கை தேதியை வழங்கவும்";
		AppStrings.auditorAlert = "தணிக்கையாளர் பெயரை வழங்கவும்";
		AppStrings.nullDetailsAlert = "சரியான விவரங்களை வழங்கவும்";
		AppStrings.trainingDateAlert = "பயிற்சி தேதியை வழங்கவும்";
		AppStrings.trainingTypeAlert = "குறைந்தது ஒரு பயிற்சி வகையை தேர்வு செய்யவும்";
		AppStrings.offline_ChangePwdAlert = "தாங்கள் இணைய இணைப்பு இல்லாமல் கடவுச்சொல்லை மாற்ற இயலாது";
		AppStrings.transactionFailAlert = "உங்கள் பரிவர்த்தனை முழுமையடையவில்லை";
		AppStrings.voluntarySavings = "தன்னார்வ சேமிப்பு";
		AppStrings.purposeOfLoan = "கடன் வகை";
		AppStrings.tenure = "கால அளவு";
		AppStrings.chooseLabel = "கடன் வகையை தேர்வுக";
		AppStrings.Tenure_POL_Alert = "கடன் வகை மற்றும் கால அளவை தேர்ந்தெடுக்கவும்";
		AppStrings.interestSubvention = "வட்டி மானியம்";// நிர்வாகியைத்
		AppStrings.adminAlert = "உங்கள் நிர்வாக பொருப்பாளரை தொடர்பு கொள்ளவும்";
		AppStrings.userNotExist = "உள்நுழைவு விவரங்கள் இல்லை. ஆன்லைன் உள்நுழைவு தொடரவும்.";
		AppStrings.tryLater = "பின்னர் முயற்சிக்கவும்";
		AppStrings.offlineDataAvailable = "ஆஃப்லைன் தகவல்கள் உள்ளன வெளியேறிவிட்டு மீண்டும் முயற்சிக்கவும்";
		AppStrings.choosePOLAlert = "கடன் வகையை தேர்வு செய்யவும். ";
		AppStrings.InternalLoan = "தனி நபர் கடன்";
		AppStrings.TrainingTypes = "பயிற்சி  வகைகள்";
		AppStrings.uploadInfo = "முகவர் விவரம் பதிவேற்றம்";
		AppStrings.uploadAadhar = "ஆதார் பதிவேற்றம்";
		AppStrings.month = "-- மாதத்தை தேர்வு செய்க --";
		AppStrings.year = "-- வருடத்தை தேர்வு செய்க  --";
		AppStrings.monthYear_Alert = "மாதம் மற்றும் வருடத்தை தேர்ந்தெடுக்கவும்";
		AppStrings.chooseLanguage = "மொழி தேர்வு";
		AppStrings.autoFill = "தன்னிரப்பி";
		AppStrings.viewAadhar = "ஆதார்";
		AppStrings.viewPhoto = "புகைப்படம்";
		AppStrings.uploadAlert = "நீங்கள் இணைய இணைப்பு இல்லாமல் ஆதார் அட்டைப் பதிவு மற்றும் புகைப்படத்தைக் காண முடியாது";// "நீங்கள்
																															// இணைய
																															// இணைப்பு
																															// இல்லாமல்
																															// ஆதார்
																															// மற்றும்
																															// புகைப்படங்களைப்
																															// பதிவேற்ற
																															// முடியாது";
		AppStrings.view = "பார்வைக்கு";
		AppStrings.upload = "பதிவேற்றம்";
		AppStrings.showAadharAlert = "உங்கள் ஆதார் அட்டையை காண்பிக்கவும்";
		AppStrings.deactivateAccount = "கணக்கு நீக்கம்";
		AppStrings.deactivateAccount_SuccessAlert = "உங்கள் கணக்கு நீக்கப்பட்டது";
		AppStrings.deactivateAccount_FailAlert = "உங்கள் கணக்கு நீக்கப்படவில்லை";
		AppStrings.deactivateNetworkAlert = "இணைய இணைப்பு இல்லாமல் உங்கள் கணக்கை செயலிழக்க முடியாது";
		AppStrings.offlineReports = "இணையமின்றி பதியப்பட்ட அறிக்கை";
		AppStrings.registrationSuccess = "உங்கள் விவரம் பதிவு செய்யப்பட்டது";
		AppStrings.reg_MobileNo = "பதிவு செய்யப்பட்ட கைபேசி எண்ணை நிரப்புங்கள்";
		AppStrings.reg_IMEINo = "அனுமதி மறுக்கப்பட்டது";
		AppStrings.reg_BothNo = "அனுமதி மறுக்கப்பட்டது";
		AppStrings.reg_IMEIUsed = "அனுமதி மறுக்கப்பட்டது. கைபேசி பொருத்தமின்மை";
		AppStrings.signInAsDiffUser = "வேறு பயனராக உள்நுழைய";
		AppStrings.incorrectUserNameAlert = "தவறான பயனர்பெயர்";

		AppStrings.noofflinedatas = "எந்த ஒரு இணையமின்றி பரிவர்த்தனை இல்லை";
		AppStrings.uploadprofilealert = "உங்களுடைய விவரங்களை பதிவேற்றம் செய்க";
		AppStrings.monthYear_Warning = "முறையான மாதம் மற்றும் ஆண்டு தேர்ந்தெடுக்கவும்";
		AppStrings.of = " -ன் ";
		AppStrings.previous_DateAlert = "முந்தைய தேதியை தேர்ந்தெடுக்கவும்";
		AppStrings.nobanktransactionreportdatas = "எந்த ஒரு வங்கிநடவடிக்கைசுருக்கமும் இல்லை";

		AppStrings.mDefault = "படிப்படியான";
		AppStrings.mTotalDisbursement = "மொத்த பட்டுவாடா";
		AppStrings.mInterloan = "தனி நபர் கடன்";
		AppStrings.mTotalSavings = "மொத்த வசூல்";
		AppStrings.mCommonNetworkErrorMsg = "உங்கள் இணைய இணைப்பை சரிபார்க்கவும்";
		AppStrings.mLoginAlert_ONOFF = "இணைய இணைப்பு பயன்படுத்தி மீண்டும் உள்நுழையவும்";
		AppStrings.changeLanguageNetworkException = "வெளியேறு,இணைய இணைப்பு பயன்படுத்தி மீண்டும் உள்நுழையவும்";
		AppStrings.photouploadmsg = "புகைப்படம் வெற்றிகரமாக பதிவேற்றப்பட்டது";
		AppStrings.loginAgainAlert = "BACKGROUND DATA IS STILL LOADING PLEASE WAIT FOR SOME TIME.";//"வெளியேறு,மீண்டும் உள்நுழையவும்";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "தனிநபர் கடன்";
		AppStrings.mInternalBankLoanMenu = "வங்கி கடன்";
		AppStrings.mInternalMFILoanMenu = "எம் எஃப் ஐ கடன்";
		AppStrings.mInternalFederationMenu = "கூட்டமைப்பு கடன்";

		AppStrings.bankName = "வங்கி பெயர்";
		AppStrings.mfiname = "எம் எஃப் ஐ பெயர்";
		AppStrings.loanType = "கடன் வகை";
		AppStrings.bankBranch = "வங்கிக்கிளை";
		AppStrings.installment_type = "தவணை வகை";
		AppStrings.NBFC_Name = "வங்கிசாரா நிதி பெயர்";
		AppStrings.Loan_Account_No = "கடன் கணக்கு எண்";
		AppStrings.Loan_Sanction_Amount = "கடன் ஒப்புதல் அளித்தல் தொகை";
		AppStrings.Loan_Disbursement_Amount = "கடன் வழங்கும் தொகை";
		AppStrings.Loan_Sanction_Date = "கடன் ஒப்புதல் அளித்தல் தேதி";
		AppStrings.Loan_Disbursement_Date = "கடன் வழங்கும் தேதி";
		AppStrings.Loan_Tenure = "கடனை அடைப்பதற்கான காலம்";
		AppStrings.Subsidy_Amount = "மானியம் தொகை";
		AppStrings.Subsidy_Reserve_Fund = "மானியம் காப்பு நிதியம்";
		AppStrings.Interest_Rate = "வட்டி விகிதம்";
		AppStrings.mMonthly = "மாதாந்திர";
		AppStrings.mQuarterly = "காலாண்டு";
		AppStrings.mHalf_Yearly = "அரையாண்டு";
		AppStrings.mYearly = "வருடாந்திர";
		AppStrings.mAgri = "வேளாண்";
		AppStrings.mMSME = "MSME";
		AppStrings.mOthers = "மற்றவை";

		AppStrings.mMFI_NAME_SPINNER = "MFI பெயர்";
		AppStrings.mMFINabfins = "NABFINS";
		AppStrings.mMFIOthers = "மற்றவைகள்";

		AppStrings.mInternalTypeLoan = "உள்க் கடன்";

		AppStrings.mPervious = "முந்தைய";
		AppStrings.mNext = "அடுத்து";
		AppStrings.mPasswordUpdate = "உங்கள் கடவுச்சொல் வெற்றிகரமாகப் சமர்ப்பிக்கப்பட்டது";

		AppStrings.mFixedDepositeAmount = "இட்டு வைப்புத் தொகை";
		AppStrings.mFedBankName = "கூட்டமைப்பு பெயர்";//"BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "கடன் வழங்கும் தொகையை சரிபார்க்கவும்";
		AppStrings.mStepWise_LoanDibursement = "உள் கடன் வழங்குதல்";

		AppStrings.mExpense_meeting = "செலவுகள்";//கூட்டு  இழப்பு";
		AppStrings.mSubmitbutton = "சரி";
		AppStrings.mNewUserSignup="பதிவு";
		AppStrings.mMobilenumber="மொபைல் எண்";
		AppStrings.mIsAadharAvailable = "ஆதார் தகவல் சமர்ப்பிக்கப்படவில்லை";
		AppStrings.mSavingsTransaction = "சேமிப்பு பரிவர்த்தனை";
		AppStrings.mBankCharges = "வங்கிக் கட்டணங்கள்";
		AppStrings.mCashDeposit = "பண வைப்பு";
		AppStrings.mLimit = "கடன்";

		AppStrings.mAccountToAccountTransfer = "கணக்கு பரிமாற்றம் செய்வதற்கான கணக்கு";
		AppStrings.mTransferFromBank = "வங்கியில் இருந்து";
		AppStrings.mTransferToBank = "வங்கிக்கு";
		AppStrings.mTransferAmount = "பரிமாற்று";
		AppStrings.mTransferCharges = "பரிமாற்ற கட்டணம்";
		AppStrings.mTransferSpinnerFloating = "வங்கிக்கு";
		AppStrings.mTransferNullToast = "அனைத்து விவரங்களையும் வழங்கவும்";
		AppStrings.mAccToAccTransferToast = "நீங்கள் ஒரே ஒரு வங்கி கணக்கு மட்டும் வைத்திருக்கிறீர்கள்";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "கடன் கணக்கில்";
		AppStrings.mLoanaccCash = "ரொக்கம்";
		AppStrings.mLoanaccBank = "வங்கி";
		AppStrings.mLoanaccWithdrawal = "தொகை திரும்பப் பெற";
		AppStrings.mLoanaccExapenses = "செலவுகள்";
		AppStrings.mLoanaccSpinnerFloating = "கடன் கணக்கு வங்கி";
		AppStrings.mLoanaccCash_BankToast = "ஒரு பண அல்லது வங்கி தேர்ந்தெடுக்கவும்";
		AppStrings.mLoanaccNullToast = "அனைத்து விவரங்களையும் வழங்கவும்";
		AppStrings.mLoanaccBankNullToast = "ஒரு வங்கியின் பெயர் தேர்ந்தெடுக்கவும்";

		AppStrings.mBankTransSavingsAccount = "சேமிப்பு கணக்கு";
		AppStrings.mBankTransLoanAccount = "கடன் கணக்கில்";

		AppStrings.mLoanAccType = "வகை";
		AppStrings.mLoanAccBankAmountAlert = "வங்கி இருப்பு சரிபார்க்கவும்";

		AppStrings.mCashAtBank = "வங்கியிருப்பு";
		AppStrings.mCashInHand = "கையிருப்பு";
		AppStrings.mShgSeedFund = "சுய உதவிக் குழு விதை நிதி";
		AppStrings.mFixedAssets = "நிலையான சொத்துக்கள்";
		AppStrings.mVerified = "சரிபார்க்கப்பட்டது";
		AppStrings.mConfirm = "உறுதி";
		AppStrings.mProceed = "தொடர";
		AppStrings.mDialogAlertText = "YOUR OPENING BALANCE DATAS ARE UPDATED SUCCESSFULLY, NOW YOU CAN PROCEED";
		AppStrings.mBalanceSheetDate = "இருப்புநிலை குறிப்பில் பதிவேற்றம் செய்யப்பட்டுள்ளது";
		AppStrings.mCheckMonthlyEntries = "NO ENTRIES FOR PREVIOUS MONTH, SO YOU SHOULD PUT DATAS FOR PREVIOUS MONTH";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "நிலுவையில் குழு கடன்";
		AppStrings.mMemberLoanOutstanding = "நிலுவையில் உறுப்பினருக்கு கடன்";
		AppStrings.mContinueWithDate = "நீங்கள் இந்த தேதியை மாற்ற வேண்டும் என விரும்புகிறீர்கள்";
		AppStrings.mCheckList = "பட்டியல் சரிபார்க்க";
		AppStrings.mMicro_Credit_Plan = "சிறுகடன் திட்டம்";
		AppStrings.mAadharCard_Invalid = "உங்கள் ஆதார் கார்டு தவறானது";
		AppStrings.mIsNegativeOpeningBalance = "எதிர்மறை இருப்பு திருத்தி";
		AppStrings.mLoginDetailsDelete = "முந்தைய உள்நுழைவு விவரங்கள் நீக்கப்படும். நீங்கள் தரவு நீக்க விரும்புகிறீர்களா?";
		AppStrings.mVerifyGroupAlert = "சரிபார்க்க நீங்கள் ஆன்லைனில் இருக்க வேண்டும்";
		AppStrings.next = "அடுத்து";
		AppStrings.mNone = "எதுவும் இல்லை";
		AppStrings.mSelect = "தேர்வு";
		AppStrings.mAnimatorName = "அனிமேட்டர் பெயர்";
		AppStrings.mSavingsAccount = "சேமிப்பு கணக்கு";
		AppStrings.mAccountNumber = "கணக்கு எண்";
		AppStrings.mLoanAccountAlert = "ஆஃப்லைன் பயன்முறையில் கடன் கணக்கில் மாற்ற முடியாது";
		AppStrings.mSeedFund = "விதை நிதி";
		AppStrings.mLoanTypeAlert = "கடன் கணக்கு வகை தேர்ந்தெடுக்க தயவு செய்து";
		AppStrings.mOutstandingAlert = "உங்கள் கடன் தொகையை சரிபார்க்கவும்";
		AppStrings.mLoanAccTransfer = "கடன் கணக்கில் பரிமாற்ற";
		AppStrings.mLoanName = "கடன் பெயர்";
		AppStrings.mLoanId = "கடன் ஐடி";
		AppStrings.mLoanDisbursementDate = "கடன் வழங்கும் தேதி";
		AppStrings.mAdd = "கூட்டு";
		AppStrings.mLess = "குறைவான";
		AppStrings.mRepaymentWithInterest = "கடனை திருப்பி செலுத்தும் (வட்டி)";
		AppStrings.mCharges = "செலவுகள்";
		AppStrings.mExistingLoan = "ஏற்கனவே கடன்";
		AppStrings.mNewLoan = "புதிய கடன்";
		AppStrings.mIncreaseLimit = "அதிகரிப்பு எல்லை";
		AppStrings.mAvailableLimit = "கிடைக்கும் எல்லை";
		AppStrings.mDisbursementDate = "அளிப்பதற்கான தேதி";
		AppStrings.mDisbursementAmount = "அளிப்பதற்கான அளவு";
		AppStrings.mSanctionAmount = "அங்கீகாரத்தையோ அளவு";
		AppStrings.mBalanceAmount = "மீதம் உள்ள தொகை";
		AppStrings.mMemberDisbursementAmount = "உறுப்பினர் அளிப்பதற்கான அளவு";
		AppStrings.mLoanDisbursementFromLoanAcc = "கடன் கணக்கில் இருந்து கடன் வழங்கும்";
		AppStrings.mLoanDisbursementFromSbAcc = "எஸ்.பி. கணக்கில் இருந்து கடன் வழங்கும்";
		AppStrings.mRepaid = " திருப்பி செலுத்து";
		AppStrings.mCheckBackLog = "Backlog சரிபார்க்கவும்";
		AppStrings.mTarget = "இலக்கு";
		AppStrings.mCompleted = "நிறைவு";
		AppStrings.mPending = "நிலுவையில்";
		AppStrings.mAskLogout = "வெளியேறி விரும்புகிறீர்கள் ?.";
		AppStrings.mCheckDisbursementAlert = "PLEASE CHECK YOUR DISBURSEMENT AMOUNT";
		AppStrings.mCheckbalanceAlert = "உங்கள் இருப்பு அளவு சரிபார்க்கவும்";
		AppStrings.mVideoManual = "வீடியோ கையேடு";
		AppStrings.mPdfManual = "பிடிஎஃப் கையேடு";
		AppStrings.mAmountDisbursingAlert = "DO YOU WANT TO CONTINUE WITHOUT DISBURSING TO MEMBERS?.";
		AppStrings.mOpeningDate = "தொடக்க தேதி";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "உங்கள் கடன் அனுமதி தேதி மற்றும் கடன் பெற்ற தேதியை சரிபார்க்கவும்.";
		AppStrings.mLoanDisbursementFromRepaid = "Cc கடன் திரும்பப் பெறுதல்";
		AppStrings.mExpense = "செலவு";
		AppStrings.mBeforeLoanPay = "கடன் ஊதியம் முன்";
		AppStrings.mLoans = "கடன்";
		AppStrings.mSurplus = "உபரி";
		AppStrings.mPOLAlert = "கடன் நோக்கம் உள்ளிடவும்";
		AppStrings.mCheckLoanAmounts = "உங்கள் கடன் ஒப்புதலுக்கான தொகை மற்றும் கடனளிப்பு தொகை ஆகியவற்றை சரிபார்க்கவும்";
		AppStrings.mCheckLoanSanctionAmount = "உங்கள் கடன் ஒப்புதலுக்கான தொகை உள்ளிடவும்";
		AppStrings.mCheckLoanDisbursementAmount = "உங்கள் கடன் தொகையை சரிபார்க்கவும்";
		AppStrings.mIsPasswordSame = "உங்கள் பழைய கடவுச்சொல் மற்றும் புதிய கடவுச்சொல்லிரண்டும் ஒன்றா என சரிபார்க்கவும்";
		AppStrings.mSavings_Banktransaction = "சேமிப்பு - வங்கி பரிவர்த்தனை";
		AppStrings.mCheckBackLogOffline = "CAN'T VIEW CHECK BACKLOG IN OFFLINE";
		AppStrings.mCredit = "கடன்";
		AppStrings.mDebit = "பற்று";
		AppStrings.mCheckFixedDepositAmount = "உங்கள் நிலையான வைப்பு அளவு சரிபார்க்கவும்.";
		AppStrings.mCheckDisbursementDate = "PLEASE CHECK YOUR LOAN DISBURSEMENT DATE";
		AppStrings.mSendEmail = "மின்னஞ்சல்";
		AppStrings.mTotalInterest = "மொத்த வட்டி";
		AppStrings.mTotalCharges = "மொத்த கட்டணம்";
		AppStrings.mTotalRepayment = "மொத்த திருப்பிச் செலுத்துதல்";
		AppStrings.mTotalInterestSubventionRecevied = "மொத்த வட்டி மானியம் பெற்றது";
		AppStrings.mTotalBankCharges = "மொத்த வங்கி கட்டணங்கள்";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "கட்டணம்";
		AppStrings.mCashWithdrawal = "பணம் எடுத்தல்";
		AppStrings.mFundTransfer = "நிதி பரிமாற்றம்";
		AppStrings.mDepositAmount = "வைப்பு தொகை";
		AppStrings.mWithdrawalAmount = "திரும்பப் பெறுதல் தொகை";
		AppStrings.mTransactionMode = "பரிவர்த்தனை முறை";
		AppStrings.mBankLoanDisbursement = "வங்கி கடனை ஒதுக்குதல்";
		AppStrings.mMemberToMember = "உறுப்பினர் இருந்து மற்றொரு உறுப்பினர்";
		AppStrings.mDebitAccount = "பற்று கணக்கு";
		AppStrings.mSHG_SavingsAccount = "சுய உதவிக்குழு சேமிப்பு கணக்கு";
		AppStrings.mDestinationMemberName = "இலக்கு உறுப்பினர் பெயர்";
		AppStrings.mSourceMemberName = "மூல உறுப்பினர் பெயர்";
		AppStrings.mMobileNoUpdation = "மொபைல் எண் புதுப்பித்தல்";
		AppStrings.mMobileNo = "மொபைல் எண்";
		AppStrings.mMobileNoUpdateSuccessAlert = "மொபைல் எண் வெற்றிகரமாக புதுப்பிக்கப்பட்டது.";
		AppStrings.mAadhaarNoUpdation = "ஆதார் எண் புதுப்பித்தல்";
		AppStrings.mSHGAccountNoUpdation = "SHG கணக்கு எண் புதுப்பித்தல்";
		AppStrings.mAccountNoUpdation = "உறுப்பினர் கணக்கு எண் புதுப்பித்தல்";
		AppStrings.mAadhaarNo = "ஆதார் எண்";
		AppStrings.mBranchName = "கிளை பெயர்";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "ஆதார் எண் வெற்றிகரமாக புதுப்பிக்கப்பட்டது";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "உறுப்பினர் கணக்கு எண் வெற்றிகரமாக புதுப்பிக்கப்பட்டது";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "SHG கணக்கு எண் வெற்றிகரமாக புதுப்பிக்கப்பட்டது";
		AppStrings.mCheckValidMobileNo = "சரியான மொபைல் எண்ணை உள்ளிடவும்";
		AppStrings.mInvalidAadhaarNo = "தவறான ஆதார் எண்";
		AppStrings.mCheckValidAadhaarNo = "தயவுசெய்து சரியான ஆதார் எண்ணை உள்ளிடவும்";
		AppStrings.mInvalidAccountNo = "தவறான கணக்கு எண்";
		AppStrings.mCheckAccountNumber = "தயவு செய்து உங்கள் கணக்கு எண்ணைச் சரிபார்க்கவும்";
		AppStrings.mInvalidMobileNo = "தவறான மொபைல் எண்";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "MOBILE NUMBERS REPEATED";
		AppStrings.mMobileNoUpdationNetworkCheck = "ஆஃப்லைன் பயன்முறையில் மொபைல் எண் புதுப்பிக்க முடியாது";
		AppStrings.mAadhaarNoNetworkCheck = "ஆஃப்லைன் பயன்முறையில் ஆதார் எண் புதுப்பிக்க முடியாது";
		AppStrings.mShgAccNoNetworkCheck = "ஆஃப்லைன் பயன்முறையில் SHG கணக்கு எண் புதுப்பிக்க முடியாது";
		AppStrings.mAccNoNetworkCheck = "ஆஃப்லைன் பயன்முறையில் உறுப்பினர் கணக்கு எண்ணை புதுப்பிக்க முடியாது";
		AppStrings.mAccountNoRepeat = "ACCOUNT NUMBER REPEATED";
		AppStrings.mCheckBankAndBranchNameSelected = "தயவுசெய்து வங்கி மற்றும் கிளை பெயரை சரிபார்க்கவும்";
		AppStrings.mCheckNewLoanSancDate = "கடன் ஒப்புதலுக்கான தேதியை தேர்ந்தெடுக்கவும்";
		AppStrings.mCreditLinkageInfo = "கடன் இணைப்பு தகவல்";
		AppStrings.mCreditLinkage = "கடன் இணைப்பு";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ஆஃப்லைன் முறையில் கடன் இணைப்பு தகவலை புதுப்பிக்க முடியாது";
		AppStrings.mDonation = "நன்கொடை";
		AppStrings.mloanaccountnumber="கடன் கணக்கு எண்";
		AppStrings.mcash="பணம்";

	}
}
