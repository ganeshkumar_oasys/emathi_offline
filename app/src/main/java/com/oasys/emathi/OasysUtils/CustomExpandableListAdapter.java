package com.oasys.emathi.OasysUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;

import java.util.List;
import java.util.Map;


/**
 * Created by User on 28/06/2018.
 */

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<String> listTitle;
    private Map<String, List<String>> listItem;

    public CustomExpandableListAdapter(Context mContext, List<String> listTitle, Map<String, List<String>> listItem) {
        this.mContext = mContext;
        this.listTitle = listTitle;
        this.listItem = listItem;
    }

    @Override
    public int getGroupCount() {
        return listTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listItem.get(listTitle.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listItem.get(listTitle.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String title = (String) getGroup(groupPosition);

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_group, null);

        }
        TextView txtTitle = (TextView) convertView.findViewById(R.id.listTitle);
        ImageView listImage = (ImageView) convertView.findViewById(R.id.listImage);
        ImageView listIcon = (ImageView) convertView.findViewById(R.id.listIcon);
        txtTitle.setText(title);
        txtTitle.setTypeface(LoginActivity.sTypeface);


        if (isExpanded) {
            listImage.setImageResource(R.drawable.down_arrow);
        } else {
            listImage.setImageResource(R.drawable.side);
        }
        if (groupPosition == 6 || groupPosition == 7 ) {
            listImage.setImageResource(R.drawable.hide);
        }


        if (groupPosition == 0) {
            listIcon.setImageResource(R.drawable.transactionn);
        } else if (groupPosition == 1) {
            listIcon.setImageResource(R.drawable.profilen);
        } else if (groupPosition == 2) {
            listIcon.setImageResource(R.drawable.reportsn);
        } else if (groupPosition == 3) {
            listIcon.setImageResource(R.drawable.meetingn);
        } else if (groupPosition == 4) {
            listIcon.setImageResource(R.drawable.settingsn);
        } else if (groupPosition == 5) {
            listIcon.setImageResource(R.drawable.helpn);
        } else if (groupPosition == 6) {
            listIcon.setImageResource(R.drawable.grouplistn);
        }else if (groupPosition == 7) {
            listIcon.setImageResource(R.drawable.logout);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String title = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_items, null);
        }
        TextView txtChild = (TextView) convertView.findViewById(R.id.expandableListItem);
        ImageView list_child_icons = (ImageView) convertView.findViewById(R.id.list_child_icons);
        txtChild.setText(title);
        txtChild.setTypeface(LoginActivity.sTypeface);

        switch (groupPosition) {
            case 0:
                switch (childPosition) {
                    case 0:
                        list_child_icons.setImageResource(R.drawable.savingsn);
                        break;
                    case 1:
                        list_child_icons.setImageResource(R.drawable.loanrepaymentn);
                        break;
                    case 2:
                        list_child_icons.setImageResource(R.drawable.expensesn);
                        break;
                    case 3:
                        list_child_icons.setImageResource(R.drawable.incomen);
                        break;
                    case 4:
                        list_child_icons.setImageResource(R.drawable.grouploanrepaymentn);
                        break;
                    case 5:
                        list_child_icons.setImageResource(R.drawable.banksavingsn);
                        break;
                    case 6:
                        list_child_icons.setImageResource(R.drawable.loandisbursementn);
                        break;
                    case 7:
                        list_child_icons.setImageResource(R.drawable.savingsn);
                        break;
                    case 8:
                        list_child_icons.setImageResource(R.drawable.savingsn);
                        break;

                }
                break;
            case 1:
                switch (childPosition) {
                    case 0:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 1:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 2:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 3:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 4:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 5:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 6:
                        list_child_icons.setImageResource(R.drawable.adhaarn);
                        break;
                    case 7:
                        list_child_icons.setImageResource(R.drawable.groupprofilen);
                        break;
                    case 8:
                        list_child_icons.setImageResource(R.drawable.agentprofilen);
                        break;
                }
                break;
            case 2:
                switch (childPosition) {
                    case 0:
                        list_child_icons.setImageResource(R.drawable.memberreportsn);
                        break;
                    case 1:
                        list_child_icons.setImageResource(R.drawable.groupreportsn);
                        break;
                    case 2:
                        list_child_icons.setImageResource(R.drawable.banktransactionn);
                        break;
                    case 3:
                        list_child_icons.setImageResource(R.drawable.bankbalancen);
                        break;
                    case 4:
                        list_child_icons.setImageResource(R.drawable.reportsn);
                        break;
                }
                break;

            case 3:
                switch (childPosition) {
                    case 0:
                        list_child_icons.setImageResource(R.drawable.attendancen);
                        break;
                    case 1:
                        list_child_icons.setImageResource(R.drawable.minutesn);
                        break;
                    case 2:
                        list_child_icons.setImageResource(R.drawable.auditn);
                        break;
                    case 3:
                        list_child_icons.setImageResource(R.drawable.trainingn);
                        break;
                    case 4:
                        list_child_icons.setImageResource(R.drawable.trainingn);
                        break;

                    case 5:
                        list_child_icons.setImageResource(R.drawable.trainingn);
                        break;
                }
                break;
            case 4:
                switch (childPosition) {
                    case 0:
                        list_child_icons.setImageResource(R.drawable.changepasswordn);
                        break;
                    case 1:
                        list_child_icons.setImageResource(R.drawable.changelanguaguen);
                        break;
                    case 2:
                        list_child_icons.setImageResource(R.drawable.trainingn);
                        break;
                    case 3:
                        list_child_icons.setImageResource(R.drawable.trainingn);
                        break;
                }
                break;
            case 5:
                switch (childPosition) {
                    case 0:
                        list_child_icons.setImageResource(R.drawable.contactn);
                        break;
                    case 1:
                        list_child_icons.setImageResource(R.drawable.contactn);
                        break;
                    case 2:
                        list_child_icons.setImageResource(R.drawable.contactn);
                        break;
                    case 3:
                        list_child_icons.setImageResource(R.drawable.contactn);
                        break;
                }
                break;
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
