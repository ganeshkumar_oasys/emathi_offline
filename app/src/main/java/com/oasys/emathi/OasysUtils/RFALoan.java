package com.oasys.emathi.OasysUtils;

import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.VLoanTypes;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.SHGTable;

import java.util.ArrayList;

public class RFALoan {

    private static ArrayList<OfflineDto> offlineDBTermLoanData1 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData2 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData3 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData4 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData5 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBRfaLoanData6 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBRfaLoanData7 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBRfaLoanData8 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBRfaLoanData9 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBRfaLoanData10 = new ArrayList<>();

    public static void rfaLoan()
    {
        ArrayList<VLoanTypes> rfaLoan =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA();

        if(rfaLoan!=null && rfaLoan.size()>0) {
            for (int j = 0; j < rfaLoan.size(); j++) {
                OfflineDto offline1 = new OfflineDto();
                offline1.setLoanId(rfaLoan.get(j).getLoanId());
                offline1.setMemberId(rfaLoan.get(j).getMemeberId());
                int amount = (int) Double.parseDouble(rfaLoan.get(j).getAmount());
                offline1.setOutStanding(amount + "");
                offline1.setMem_os(amount + "");
                offlineDBTermLoanData1.add(offline1);
            }
        }
    }

    public static void rfaLoan1()
    {
        ArrayList<VLoanTypes> rfaLoan1 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA1();

        if(rfaLoan1!=null && rfaLoan1.size()>0) {
            for (int k = 0; k < rfaLoan1.size(); k++) {
                OfflineDto offline2 = new OfflineDto();
                offline2.setLoanId(rfaLoan1.get(k).getLoanId());
                offline2.setMemberId(rfaLoan1.get(k).getMemeberId());
                int amount1 = (int) Double.parseDouble(rfaLoan1.get(k).getAmount());
                offline2.setOutStanding(amount1 + "");
                offline2.setMem_os(amount1 + "");
                offlineDBTermLoanData2.add(offline2);
            }
        }
    }

    public  static void rfaLoan2()
    {

        ArrayList<VLoanTypes> rfaLoan2 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA2();
        if(rfaLoan2!=null && rfaLoan2.size()>0) {
            for (int l = 0; l < rfaLoan2.size(); l++) {
                OfflineDto offline3 = new OfflineDto();
                offline3.setLoanId(rfaLoan2.get(l).getLoanId());
                offline3.setMemberId(rfaLoan2.get(l).getMemeberId());
                int amount2 = (int) Double.parseDouble(rfaLoan2.get(l).getAmount());
                offline3.setOutStanding(amount2 + "");
                offline3.setMem_os(amount2 + "");
                offlineDBTermLoanData3.add(offline3);
            }
        }
    }

    public  static void rfaLoan3()
    {

        ArrayList<VLoanTypes> rfaLoan3 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA3();

        if(rfaLoan3!=null && rfaLoan3.size()>0) {
            for (int m = 0; m < rfaLoan3.size(); m++) {
                OfflineDto offline4 = new OfflineDto();
                offline4.setLoanId(rfaLoan3.get(m).getLoanId());
                offline4.setMemberId(rfaLoan3.get(m).getMemeberId());
                int amount3 = (int) Double.parseDouble(rfaLoan3.get(m).getAmount());
                offline4.setOutStanding(amount3 + "");
                offline4.setMem_os(amount3 + "");
                offlineDBTermLoanData4.add(offline4);
            }
        }
    }

    public  static void rfaLoan4()
    {

        ArrayList<VLoanTypes> rfaLoan4 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA4();

        if(rfaLoan4!=null && rfaLoan4.size()>0) {

            for (int n = 0; n < rfaLoan4.size(); n++) {
                OfflineDto offline5 = new OfflineDto();
                offline5.setLoanId(rfaLoan4.get(n).getLoanId());
                offline5.setMemberId(rfaLoan4.get(n).getMemeberId());
                int amount4 = (int) Double.parseDouble(rfaLoan4.get(n).getAmount());
                offline5.setOutStanding(amount4 + "");
                offline5.setMem_os(amount4 + "");
                offlineDBTermLoanData5.add(offline5);
            }
        }
    }

    // GroupLoan Outstanding

    public static  void groupRFALoan() {

        ArrayList<VLoanTypes> groupRFALoan = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA();
        if (groupRFALoan != null && groupRFALoan.size() > 0) {
            OfflineDto offline1 = new OfflineDto();
            offline1.setLoanId(groupRFALoan.get(0).getLoanId());
            offline1.setOutStanding(groupRFALoan.get(0).getOutStandingAmount());
            offlineDBRfaLoanData6.add(offline1);
        }
    }

    public static  void groupRFALoan1() {

        ArrayList<VLoanTypes> groupRFALoan1 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA1();
        if (groupRFALoan1 != null && groupRFALoan1.size() > 0) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(groupRFALoan1.get(0).getLoanId());
            offline2.setOutStanding(groupRFALoan1.get(0).getOutStandingAmount());
            offlineDBRfaLoanData7.add(offline2);
        }
    }

    public static  void groupRFALoan2() {

        ArrayList<VLoanTypes> groupRFALoan2 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA2();
        if (groupRFALoan2 != null && groupRFALoan2.size() > 0) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(groupRFALoan2.get(0).getLoanId());
            offline3.setOutStanding(groupRFALoan2.get(0).getOutStandingAmount());
            offlineDBRfaLoanData8.add(offline3);
        }
    }

    public static  void groupRFALoan3() {

        ArrayList<VLoanTypes> groupRFALoan3 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA3();
        if (groupRFALoan3 != null && groupRFALoan3.size() > 0) {
            OfflineDto offline4 = new OfflineDto();
            offline4.setLoanId(groupRFALoan3.get(0).getLoanId());
            offline4.setOutStanding(groupRFALoan3.get(0).getOutStandingAmount());
            offlineDBRfaLoanData9.add(offline4);
        }
    }

    public static  void groupRFALoan4() {

        ArrayList<VLoanTypes> groupRFALoan4 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA4();
        if (groupRFALoan4 != null && groupRFALoan4.size() > 0) {
            OfflineDto offline5 = new OfflineDto();
            offline5.setLoanId(groupRFALoan4.get(0).getLoanId());
            offline5.setOutStanding(groupRFALoan4.get(0).getOutStandingAmount());
            offlineDBRfaLoanData10.add(offline5);
        }
    }


    public static void updateMROSRFALoan()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData1) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateMROSRFALoan1()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData2) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSRFALoan2()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData3) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }
    public static  void updateMROSRFALoan3()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData4) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSRFALoan4()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData5) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateRFALOANEDIT() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBRfaLoanData6.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBRfaLoanData6.get(0).getLoanId());

    }

    public static void updateRFALOANEDIT1() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBRfaLoanData7.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBRfaLoanData7.get(0).getLoanId());

    }

    public static void updateRFALOANEDIT2() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBRfaLoanData8.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBRfaLoanData8.get(0).getLoanId());

    }

    public static void updateRFALOANEDIT3() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBRfaLoanData9.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBRfaLoanData9.get(0).getLoanId());

    }

    public static void updateRFALOANEDIT4() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBRfaLoanData10.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBRfaLoanData10.get(0).getLoanId());

    }
}
