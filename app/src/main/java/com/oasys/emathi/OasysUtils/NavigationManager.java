package com.oasys.emathi.OasysUtils;

public interface NavigationManager {
    void showFragment(String title);
}
