package com.oasys.emathi.OasysUtils;




import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.VLoanTypes;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.SHGTable;


import java.util.ArrayList;

public class  Cashcredit{

    private static ArrayList<OfflineDto> offlineDBData1 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBData2 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBData3 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBData4 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBData5 = new ArrayList<>();

//    GroupLoan Outstanding Updation
    public static ArrayList<OfflineDto> offlineDBData6 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBData7 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBData8 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBData9 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBData10 = new ArrayList<>();



    public static void cashCredit()
{
     ArrayList<VLoanTypes> cashCredit =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit();

     if(cashCredit!=null && cashCredit.size()>0) {
         for (int j = 0; j < cashCredit.size(); j++) {
             OfflineDto offline1 = new OfflineDto();
             offline1.setLoanId(cashCredit.get(j).getLoanId());
             offline1.setMemberId(cashCredit.get(j).getMemeberId());
             int amount = (int) Double.parseDouble(cashCredit.get(j).getAmount());
             offline1.setOutStanding(amount + "");
             offline1.setMem_os(amount + "");
             offlineDBData1.add(offline1);
         }
     }
}



public static void cashCredit1()
{
    ArrayList<VLoanTypes> cashCredit1 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit1();

    if(cashCredit1!=null && cashCredit1.size()>0) {
        for (int k = 0; k < cashCredit1.size(); k++) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(cashCredit1.get(k).getLoanId());
            offline2.setMemberId(cashCredit1.get(k).getMemeberId());
            int amount1 = (int) Double.parseDouble(cashCredit1.get(k).getAmount());
            offline2.setOutStanding(amount1 + "");
            offline2.setMem_os(amount1 + "");
            offlineDBData2.add(offline2);
        }
    }
}


public  static void cashCredit2()
{

    ArrayList<VLoanTypes> cashCredit2 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit2();
    if(cashCredit2!=null && cashCredit2.size()>0) {
        for (int l = 0; l < cashCredit2.size(); l++) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(cashCredit2.get(l).getLoanId());
            offline3.setMemberId(cashCredit2.get(l).getMemeberId());
            int amount2 = (int) Double.parseDouble(cashCredit2.get(l).getAmount());
            offline3.setOutStanding(amount2 + "");
            offline3.setMem_os(amount2 + "");
            offlineDBData3.add(offline3);
        }
    }
}

    public  static void cashCredit3()
    {

        ArrayList<VLoanTypes> cashCredit3 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit3();

        if(cashCredit3!=null && cashCredit3.size()>0) {
            for (int m = 0; m < cashCredit3.size(); m++) {
                OfflineDto offline4 = new OfflineDto();
                offline4.setLoanId(cashCredit3.get(m).getLoanId());
                offline4.setMemberId(cashCredit3.get(m).getMemeberId());
                int amount3 = (int) Double.parseDouble(cashCredit3.get(m).getAmount());
                offline4.setOutStanding(amount3 + "");
                offline4.setMem_os(amount3 + "");
                offlineDBData4.add(offline4);
            }
        }
    }

    public  static void cashCredit4()
    {

        ArrayList<VLoanTypes> cashCredit4 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit4();

        if(cashCredit4!=null && cashCredit4.size()>0) {

            for (int n = 0; n < cashCredit4.size(); n++) {
                OfflineDto offline5 = new OfflineDto();
                offline5.setLoanId(cashCredit4.get(n).getLoanId());
                offline5.setMemberId(cashCredit4.get(n).getMemeberId());
                int amount4 = (int) Double.parseDouble(cashCredit4.get(n).getAmount());
                offline5.setOutStanding(amount4 + "");
                offline5.setMem_os(amount4 + "");
                offlineDBData5.add(offline5);
            }
        }
    }



    // GroupLoan Outstanding

    public static  void groupCashcredit() {

        ArrayList<VLoanTypes> groupcashCredit = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit();
        if (groupcashCredit != null && groupcashCredit.size() > 0) {
            OfflineDto offline1 = new OfflineDto();
            offline1.setLoanId(groupcashCredit.get(0).getLoanId());
            offline1.setOutStanding(groupcashCredit.get(0).getOutStandingAmount());
            offlineDBData6.add(offline1);
        }
    }


    public static  void groupCashcredit1() {

        ArrayList<VLoanTypes> groupcashCredit1 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit1();
        if (groupcashCredit1 != null && groupcashCredit1.size() > 0) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(groupcashCredit1.get(0).getLoanId());
            offline2.setOutStanding(groupcashCredit1.get(0).getOutStandingAmount());
            offlineDBData7.add(offline2);
        }
    }

    public static  void groupCashcredit2() {

        ArrayList<VLoanTypes> groupcashCredit2 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit2();

        if (groupcashCredit2 != null && groupcashCredit2.size() > 0) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(groupcashCredit2.get(0).getLoanId());
            offline3.setOutStanding(groupcashCredit2.get(0).getOutStandingAmount());
            offlineDBData8.add(offline3);
        }
    }

    public static  void groupCashcredit3() {

        ArrayList<VLoanTypes> groupcashCredit3 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit3();

        if (groupcashCredit3 != null && groupcashCredit3.size() > 0) {
            OfflineDto offline4 = new OfflineDto();
            offline4.setLoanId(groupcashCredit3.get(0).getLoanId());
            offline4.setOutStanding(groupcashCredit3.get(0).getOutStandingAmount());
            offlineDBData9.add(offline4);
        }
    }
    public static  void groupCashcredit4() {

        ArrayList<VLoanTypes> groupcashCredit4 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit4();

        if (groupcashCredit4 != null && groupcashCredit4.size() > 0) {
            OfflineDto offline5 = new OfflineDto();
            offline5.setLoanId(groupcashCredit4.get(0).getLoanId());
            offline5.setOutStanding(groupcashCredit4.get(0).getOutStandingAmount());
            offlineDBData10.add(offline5);
        }
    }


    public static void updateMROS()
    {
        for (OfflineDto ofdto : offlineDBData1) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateMROS1()
    {
        for (OfflineDto ofdto : offlineDBData2) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROS2()
    {
        for (OfflineDto ofdto : offlineDBData3) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROS3()
    {
        for (OfflineDto ofdto : offlineDBData4) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROS4()
    {
        for (OfflineDto ofdto : offlineDBData5) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }


    public static void updateGRPLOSEDIT() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBData6.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBData6.get(0).getLoanId());

    }

    public static void updateGRPLOSEDIT1() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBData7.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBData7.get(0).getLoanId());

    }

    public static void updateGRPLOSEDIT2() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBData8.get(0).getGrouploan_Outstanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBData8.get(0).getLoanId());

    }

    public static void updateGRPLOSEDIT3() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBData9.get(0).getGrouploan_Outstanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBData9.get(0).getLoanId());

    }

    public static void updateGRPLOSEDIT4() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBData10.get(0).getGrouploan_Outstanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBData10.get(0).getLoanId());

    }

}
