package com.oasys.emathi.OasysUtils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;


public class GetTypeface {


	public static void changeLanguage(Context context, String languageCode) {

		Log.e("lanuageCode",""+languageCode);
		Resources res = context.getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		android.content.res.Configuration conf = res.getConfiguration();
		conf.locale = new Locale(languageCode);
		res.updateConfiguration(conf, dm);


	}


	public static Typeface getTypeface(Context context, String string) {

		Typeface typeface = null;

		if ((string == null) || (string.equals("English"))) {
			System.out.println("Fixed the default font");
			RegionalLanguage_english.RegionalStrings();

			// RegionalFont
			typeface = Typeface.createFromAsset(context.getAssets(), "font/Exo-Medium.ttf");

		} else if (string.equals("Tamil")) {
			RegionalLanguage_tamil.RegionalStrings();

			// RegionalFont
			typeface = Typeface.createFromAsset(context.getAssets(), "font/bamini.ttf");
			// typeface = Typeface.createFromAsset(context.getAssets(),
			// "font/SamyakTamil.ttf");

		} else if (string.equals("Hindi")) {
			RegionalLanguage_hindi.RegionalStrings();

			// RegionalFont

			typeface = Typeface.createFromAsset(context.getAssets(), "font/MANGAL.TTF");

		} else if (string.equals("Marathi")) {
			RegionalLanguage_marathi.RegionalStrings();

			// RegionalFont

			typeface = Typeface.createFromAsset(context.getAssets(), "font/MANGALHindiMarathi.TTF");
		} else if (string.equals("Kannada")) {
			RegionalLanguage_Kannada.RegionalStrings();

			// RegionalFont
			typeface = Typeface.createFromAsset(context.getAssets(), "font/tungaKannada.ttf");

		} else if (string.equals("Bengali")) {
			RegionalLanguage_bengali.RegionalStrings();

			// RegionalFont

			typeface = Typeface.createFromAsset(context.getAssets(), "font/kalpurushBengali.ttf");
		} else if (string.equals("Gujarathi")) {
			RegionalLanguage_gujarathi.RegionalStrings();

			// RegionalFont

			typeface = Typeface.createFromAsset(context.getAssets(), "font/shrutiGujarathi.ttf");
		} else if (string.equals("Malayalam")) {
			RegionalLanguage_malayalam.RegionalStrings();

			// RegionalFont

			typeface = Typeface.createFromAsset(context.getAssets(), "font/MLKR0nttMalayalam.ttf");

		} else if (string.equals("Punjabi")) {
			RegionalLanguage_panjabi.RegionalStrings();

			// RegionalFont
			typeface = Typeface.createFromAsset(context.getAssets(), "font/mangal-1361510185.ttf");

		} else if (string.equals("Assamese")) {
			RegionalLanguage_assam.RegionalStrings();

			// RegionalFont
			typeface = Typeface.createFromAsset(context.getAssets(), "font/KirtanUni_Assamese.ttf");

		} else {

			System.out.println("Fixed the default font");
			RegionalLanguage_english.RegionalStrings();

			// RegionalFont
			typeface = Typeface.createFromAsset(context.getAssets(), "font/Exo-Medium.ttf");

		}

		return typeface;
	}

}
