package com.oasys.emathi.OasysUtils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.views.ButtonFlat;

public class AppialogUtilslogout {

    public static void showConfirmation_LogoutDialog(final Activity activity) {

        try {
            final Dialog confirmationDialog = new Dialog(activity);

            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View customView = li.inflate(R.layout.dialog_new_logout, null, false);
            final TextView mHeaderView, mMessageView;

            mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
            mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);


            final ButtonFlat mConYesButton, mConNoButton;

            mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
            mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);
            mConNoButton.setVisibility(View.VISIBLE);
            mConYesButton.setText(AppStrings.dialogOk);
            mConYesButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setText(AppStrings.dialogNo);

            mHeaderView.setText(AppStrings.logOut);
            mHeaderView.setTypeface(LoginActivity.sTypeface);
            mHeaderView.setVisibility(View.INVISIBLE);

            if (MySharedPreference.readBoolean(activity, MySharedPreference.UNAUTH, false)) {
                mMessageView.setText(AppStrings.mUnAuth);
            } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) > 0 && !MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 2) {
                    mMessageView.setText(AppStrings.mon_mode);
                } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 1) {
                    mMessageView.setText(AppStrings.moff_mode);
                }
            } else if (MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                mMessageView.setText(AppStrings.mAskLogout);
            }

            mMessageView.setTypeface(LoginActivity.sTypeface);
            mConYesButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();

                    try {
                        if (MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "").length() > 0) {
                            ListOfShg shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                            if (shgDto != null && shgDto.getFFlag() != null && (shgDto.getFFlag().equals("1") || shgDto.getFFlag().equals("0"))) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }
                            TransactionTable.updateLoginflag(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                        }


                        if (mMessageView.getText().equals(AppStrings.mAskLogout))
                            MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                        MySharedPreference.writeBoolean(activity, MySharedPreference.SINGIN_DIFF, false);
                        MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_NAME, "");
                        //   MySharedPreference.writeString(context,MySharedPreference.USERNAME,"");
                        // MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_ID, "");
                        //MySharedPreference.writeString(activity, MySharedPreference.SHG_ID, "");
                        MySharedPreference.writeString(activity, MySharedPreference.CASHINHAND, "");
                        MySharedPreference.writeString(activity, MySharedPreference.CASHATBANK, "");
                        MySharedPreference.writeString(activity, MySharedPreference.LAST_TRANSACTION, "");

                        activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
                        activity.finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            mConNoButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();
                    MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, false);
                }
            });

            confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confirmationDialog.setCanceledOnTouchOutside(false);
            confirmationDialog.setContentView(customView);
            confirmationDialog.setCancelable(false);
            confirmationDialog.show();

        } catch (
                Exception E)

        {
            E.printStackTrace();
        }

    }
}
