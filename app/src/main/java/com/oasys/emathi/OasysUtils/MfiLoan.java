package com.oasys.emathi.OasysUtils;

import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.VLoanTypes;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.SHGTable;

import java.util.ArrayList;

public class MfiLoan {

    private static ArrayList<OfflineDto> offlineDBTermLoanData1 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData2 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData3 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData4 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData5 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBmfiLoanData6 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBmfiLoanData7 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBmfiLoanData8 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBmfiLoanData9 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBmfiLoanData10 = new ArrayList<>();


    public static void mfiLoan()
    {
        ArrayList<VLoanTypes> mfiLoan =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan();

        if(mfiLoan!=null && mfiLoan.size()>0) {
            for (int j = 0; j < mfiLoan.size(); j++) {
                OfflineDto offline1 = new OfflineDto();
                offline1.setLoanId(mfiLoan.get(j).getLoanId());
                offline1.setMemberId(mfiLoan.get(j).getMemeberId());
                int amount = (int) Double.parseDouble(mfiLoan.get(j).getAmount());
                offline1.setOutStanding(amount + "");
                offline1.setMem_os(amount + "");
                offlineDBTermLoanData1.add(offline1);
            }
        }
    }


    public static void mfiLoan1()
    {
        ArrayList<VLoanTypes> mfiLoan1 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan1();

        if(mfiLoan1!=null && mfiLoan1.size()>0) {
            for (int k = 0; k < mfiLoan1.size(); k++) {
                OfflineDto offline2 = new OfflineDto();
                offline2.setLoanId(mfiLoan1.get(k).getLoanId());
                offline2.setMemberId(mfiLoan1.get(k).getMemeberId());
                int amount1 = (int) Double.parseDouble(mfiLoan1.get(k).getAmount());
                offline2.setOutStanding(amount1 + "");
                offline2.setMem_os(amount1 + "");
                offlineDBTermLoanData2.add(offline2);
            }
        }
    }

    public  static void mfiLoan2()
    {

        ArrayList<VLoanTypes> mfiLoan2 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan2();
        if(mfiLoan2!=null && mfiLoan2.size()>0) {
            for (int l = 0; l < mfiLoan2.size(); l++) {
                OfflineDto offline3 = new OfflineDto();
                offline3.setLoanId(mfiLoan2.get(l).getLoanId());
                offline3.setMemberId(mfiLoan2.get(l).getMemeberId());
                int amount2 = (int) Double.parseDouble(mfiLoan2.get(l).getAmount());
                offline3.setOutStanding(amount2 + "");
                offline3.setMem_os(amount2 + "");
                offlineDBTermLoanData3.add(offline3);
            }
        }
    }

    public  static void mfiLoan3()
    {

        ArrayList<VLoanTypes> mfiLoan3 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan3();

        if(mfiLoan3!=null && mfiLoan3.size()>0) {
            for (int m = 0; m < mfiLoan3.size(); m++) {
                OfflineDto offline4 = new OfflineDto();
                offline4.setLoanId(mfiLoan3.get(m).getLoanId());
                offline4.setMemberId(mfiLoan3.get(m).getMemeberId());
                int amount3 = (int) Double.parseDouble(mfiLoan3.get(m).getAmount());
                offline4.setOutStanding(amount3 + "");
                offline4.setMem_os(amount3 + "");
                offlineDBTermLoanData4.add(offline4);
            }
        }
    }

    public  static void mfiLoan4()
    {

        ArrayList<VLoanTypes> mfiLoan4 =  EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan4();

        if(mfiLoan4!=null && mfiLoan4.size()>0) {

            for (int n = 0; n < mfiLoan4.size(); n++) {
                OfflineDto offline5 = new OfflineDto();
                offline5.setLoanId(mfiLoan4.get(n).getLoanId());
                offline5.setMemberId(mfiLoan4.get(n).getMemeberId());
                int amount4 = (int) Double.parseDouble(mfiLoan4.get(n).getAmount());
                offline5.setOutStanding(amount4 + "");
                offline5.setMem_os(amount4 + "");
                offlineDBTermLoanData5.add(offline5);
            }
        }
    }




    // GroupLoan Outstanding

    public static  void groupMfiLoan() {

        ArrayList<VLoanTypes> groupMfiLoan = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan();
        if (groupMfiLoan != null && groupMfiLoan.size() > 0) {
            OfflineDto offline1 = new OfflineDto();
            offline1.setLoanId(groupMfiLoan.get(0).getLoanId());
            offline1.setOutStanding(groupMfiLoan.get(0).getOutStandingAmount());
            offlineDBmfiLoanData6.add(offline1);
        }
    }

    public static  void groupMfiLoan1() {

        ArrayList<VLoanTypes> groupMfiLoan1 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan1();
        if (groupMfiLoan1 != null && groupMfiLoan1.size() > 0) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(groupMfiLoan1.get(0).getLoanId());
            offline2.setOutStanding(groupMfiLoan1.get(0).getOutStandingAmount());
            offlineDBmfiLoanData7.add(offline2);
        }
    }

    public static  void groupMfiLoan2() {

        ArrayList<VLoanTypes> groupMfiLoan2 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan2();
        if (groupMfiLoan2 != null && groupMfiLoan2.size() > 0) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(groupMfiLoan2.get(0).getLoanId());
            offline3.setOutStanding(groupMfiLoan2.get(0).getOutStandingAmount());
            offlineDBmfiLoanData8.add(offline3);
        }
    }

    public static  void groupMfiLoan3() {

        ArrayList<VLoanTypes> groupMfiLoan3 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan3();
        if (groupMfiLoan3 != null && groupMfiLoan3.size() > 0) {
            OfflineDto offline4 = new OfflineDto();
            offline4.setLoanId(groupMfiLoan3.get(0).getLoanId());
            offline4.setOutStanding(groupMfiLoan3.get(0).getOutStandingAmount());
            offlineDBmfiLoanData9.add(offline4);
        }
    }

    public static  void groupMfiLoan4() {

        ArrayList<VLoanTypes> groupMfiLoan4 = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan4();
        if (groupMfiLoan4 != null && groupMfiLoan4.size() > 0) {
            OfflineDto offline5 = new OfflineDto();
            offline5.setLoanId(groupMfiLoan4.get(0).getLoanId());
            offline5.setOutStanding(groupMfiLoan4.get(0).getOutStandingAmount());
            offlineDBmfiLoanData10.add(offline5);
        }
    }




    public static void updateMROSMFILoan()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData1) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateMROSMFILoan1()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData2) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSMFILoan2()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData3) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }
    public static  void updateMROSMFILoan3()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData4) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSMFILoan4()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData5) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }


    public static void updateMFILOANEDIT() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBmfiLoanData6.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBmfiLoanData6.get(0).getLoanId());

    }

    public static void updateMFILOANEDIT1() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBmfiLoanData7.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBmfiLoanData6.get(0).getLoanId());

    }

    public static void updateMFILOANEDIT2() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBmfiLoanData8.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBmfiLoanData8.get(0).getLoanId());

    }

    public static void updateMFILOANEDIT3() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBmfiLoanData9.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBmfiLoanData9.get(0).getLoanId());

    }

    public static void updateMFILOANEDIT4() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBmfiLoanData10.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBmfiLoanData10.get(0).getLoanId());

    }


}
