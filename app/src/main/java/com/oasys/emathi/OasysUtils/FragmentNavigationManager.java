package com.oasys.emathi.OasysUtils;


import androidx.fragment.app.FragmentManager;

import com.oasys.emathi.activity.NewDrawerScreen;

public class FragmentNavigationManager implements NavigationManager {
    private static FragmentNavigationManager mInstance;
    private static FragmentManager mFragmentManager;
    private NewDrawerScreen newDrawerScreen;


    public static FragmentNavigationManager getInstance(NewDrawerScreen newDrawerScreen) {
        if (mInstance == null)
            mInstance = new FragmentNavigationManager();
        mInstance.configure(newDrawerScreen);
        return mInstance;

    }

    private void configure(NewDrawerScreen newDrawerScreen) {
        newDrawerScreen = newDrawerScreen;
        mFragmentManager = newDrawerScreen.getSupportFragmentManager();
    }

    @Override
    public void showFragment(String title) {

    }
}
