package com.oasys.emathi.model;

/**
 * Created by MuthukumarPandi on 12/17/2018.
 */

public class Loantype {

    private String title;
    private int imageId;

    public Loantype(String title, int imageId) {
        this.title = title;
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
