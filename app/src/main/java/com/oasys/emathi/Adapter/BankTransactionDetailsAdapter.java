package com.oasys.emathi.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.emathi.Dto.GroupBankTransactionSummaryList;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by MuthukumarPandi on 12/10/2018.
 */

public class BankTransactionDetailsAdapter extends RecyclerView.Adapter<BankTransactionDetailsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryLists;

    public BankTransactionDetailsAdapter(Context context, ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryLists) {
        this.context = context;
        this.groupBankTransactionSummaryLists = groupBankTransactionSummaryLists;
    }

    @Override
    public BankTransactionDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_transaction_view, parent, false);
        return new BankTransactionDetailsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BankTransactionDetailsAdapter.ViewHolder holder, int position) {
        try {
            String amount = "";
            amount = groupBankTransactionSummaryLists.get(position).getAmount();
            int oam = (int) Double.parseDouble(amount);
            holder.mamount.setText(oam+"");

            //holder.mamount.setText(groupBankTransactionSummaryLists.get(position).getAmount());
            holder.mdetails.setText(groupBankTransactionSummaryLists.get(position).getDetails());
            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
            Date d = new Date(Long.parseLong(groupBankTransactionSummaryLists.get(position).getTransactiondate()));
            String dateStr = simple.format(d);
            holder.mTransactionDate.setText(dateStr);
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return groupBankTransactionSummaryLists.size();
    }

   public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mamount;
        TextView mdetails;
        TextView mTransactionDate;

        public ViewHolder(View itemView) {
            super(itemView);
            mamount = (TextView) itemView.findViewById(R.id.bts_amount);
            mamount.setTypeface(LoginActivity.sTypeface);
            mdetails = (TextView) itemView.findViewById(R.id.bts_details);
            mdetails.setTypeface(LoginActivity.sTypeface);
            mTransactionDate = (TextView) itemView.findViewById(R.id.bts_date);
            mTransactionDate.setTypeface(LoginActivity.sTypeface);
        }
    }
}
