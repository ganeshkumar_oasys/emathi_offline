package com.oasys.emathi.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.oasys.emathi.Dto.MemberInternalLoan;
import com.oasys.emathi.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class GroupReportInternalLoanAdapter extends RecyclerView.Adapter<GroupReportInternalLoanAdapter.MyViewholder> {

    private Context context;
    private ArrayList<MemberInternalLoan > reportinternalloanSummaries ;
    private  String dateStr;

    public GroupReportInternalLoanAdapter(Context context, ArrayList<MemberInternalLoan > reportinternalloanSummaries) {
        this.context = context;
        this.reportinternalloanSummaries = reportinternalloanSummaries;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {

        Calendar calender = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(calender.getTime());

        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");


            Date d = new Date(Long.parseLong(reportinternalloanSummaries.get(position).getTransactiondate()));
            dateStr = simple.format(d);

        holder.groupsavings_name.setText(dateStr);
        holder.groupsavings_amount.setText(reportinternalloanSummaries.get(position).getAmount());
        holder.groupsavings_voluntarysaving.setText(reportinternalloanSummaries.get(position).getInterest());

    }

    @Override
    public int getItemCount() {
        return reportinternalloanSummaries.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name,groupsavings_amount,groupsavings_voluntarysaving;



        public MyViewholder(View itemView) {
            super(itemView);


            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
           // groupsavings_amount.setTypeface(LoginActivity.sTypeface);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);

        }
    }
}
