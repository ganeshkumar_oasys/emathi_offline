package com.oasys.emathi.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.emathi.Dto.BranchList;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.LoanBankDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.database.BankBranchTable;
import com.oasys.emathi.database.SHGTable;

import java.util.ArrayList;
import java.util.List;

import static com.oasys.emathi.fragment.MemberAcountNumberUpdationFragment.selected_bank_branches;
//import static com.oasys.eshakti.fragment.MemberAcountNumberUpdationFragment.selected_bank_branches;

public class Dialog_AccountNoUpdationFragment extends DialogFragment  {

	Context mContext;
	Activity mContext_;
	List<String> listArr = new ArrayList<String>();
	List<LoanBankDto> listArr1 = new ArrayList<LoanBankDto>();;
	private EditText mSearchEditText;
	private ListView mListView;
	private ArrayAdapter<String> adapter = null;
	int mRowPos, mColPos;
	TableLayout mTableLayout;
	private TextView mDialogHeader;
	String mFlag;
	ResponseDto mResponseDto;
	private ListOfShg shgDto;
	public static  String s;
	public  static  List<BranchList> branchLists;
	public static  List<String> branchname;


	@SuppressLint("ValidFragment")
	public Dialog_AccountNoUpdationFragment(Context context, List<String> mBankNameWithoutDupList, int rowPos, int colPos,
											TableLayout tableLayout) {
		// TODO Auto-generated constructor stub
		mContext = context;
		listArr = mBankNameWithoutDupList;
		mRowPos = rowPos;
		mColPos = colPos;
		mTableLayout = tableLayout;
	}
//	public Dialog_AccountNoUpdationFragment(Activity context, List<String> mBankNameWithoutDupList, int rowPos, int colPos,
//											TableLayout tableLayout, String mFlag, ResponseDto responseDto) {
//		// TODO Auto-generated constructor stub
//		mContext_ = context;
//		listArr = mBankNameWithoutDupList;
//		mRowPos = rowPos;
//		mColPos = colPos;
//		mTableLayout = tableLayout;
//		this.mFlag = mFlag;
//		mResponseDto = responseDto;
//	}

//	public Dialog_AccountNoUpdationFragment(FragmentActivity activity, List<LoanBankDto> mBankNameWithoutDupList, int rowPos, int colPos, TableLayout mRightContentTable) {
//
//		mContext = activity;
//		listArr1 = mBankNameWithoutDupList;
//		mRowPos = rowPos;
//		mColPos = colPos;
//		mTableLayout = mRightContentTable;
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.dialog_search, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MaterialDialog);
		shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

		try {
			mDialogHeader = (TextView) rootView.findViewById(R.id.dialog_header);
			mDialogHeader.setText("CHOOSE AN ITEM");
			mDialogHeader.setTypeface(LoginActivity.sTypeface);

			mSearchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
			mSearchEditText.setTypeface(LoginActivity.sTypeface);

			mSearchEditText.addTextChangedListener(filterTextWatcher);
			mListView = (ListView) rootView.findViewById(R.id.dialogListView);

			adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, listArr) {
				@SuppressWarnings("deprecation")
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					View view = super.getView(position, convertView, parent);

					TextView textview = (TextView) view.findViewById(android.R.id.text1);

					textview.setTextSize(14);
					textview.setTypeface(LoginActivity.sTypeface);
					textview.setTextColor(getResources().getColor(R.color.black));

					return view;

				}
			};
			mListView.setAdapter(adapter);

			mListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> a, View v, int position, long id) {
					Log.d("Dialog_ListviewFragment", "Selected Item is = " + mListView.getItemAtPosition(position));
					dismiss();
					try {

						View view = mTableLayout.getChildAt(mRowPos);
						TableRow r = (TableRow) view;
						TextView getTextview = (TextView) r.getChildAt(mColPos);
						String contentValue = getTextview.getText().toString();
						s = BankBranchTable.bankname.get(position);
						getTextview.setText(s);
						branchLists = BankBranchTable.getBranchList(BankBranchTable.bankList.get(position).getId());
						selected_bank_branches.put(mRowPos, BankBranchTable.getBranchList(BankBranchTable.bankList.get(position).getId()));

						 /*branchname=new ArrayList<>();
						for (int i = 0; i < branchLists.size() ; i++) {

							branchname.add(branchLists.get(i).getBranchName());
							}*/
						getTextview.setTextSize(14);
						getTextview.setTypeface(LoginActivity.sTypeface);
						getTextview.setTextColor(getResources().getColor(R.color.black));

						/*if (EShaktiApplication.isAccountNumberBankName()) {

							EShaktiApplication.setShg_selected_bankName(getTextview.getText().toString());
							Log.e("Selected Bank name  =  ", EShaktiApplication.getShg_selected_bankName()+"");

							TextView textView = (TextView) r.getChildAt(1);
							String contentValue1 = textView.getText().toString();
							textView.setText("SELECT A BRANCH");
							textView.setTextSize(14);
							textView.setTypeface(LoginActivity.sTypeface);
							textView.setTextColor(getResources().getColor(R.color.black));

						}*/


					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private TextWatcher filterTextWatcher = new TextWatcher() {

		public void afterTextChanged(Editable s) {
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s);
		}
	};

}
