package com.oasys.emathi.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.oasys.emathi.Dto.ShgBankDTOList;
import com.oasys.emathi.R;

import java.util.ArrayList;

public class ShgAccountNumberAdapter extends RecyclerView.Adapter<ShgAccountNumberAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ShgBankDTOList> shgBankDTOListArrayList;

    public ShgAccountNumberAdapter(Context context, ArrayList<ShgBankDTOList> shgBankDTOListArrayList) {
        this.context = context;
        this.shgBankDTOListArrayList = shgBankDTOListArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shgaccountnumberlayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.mshgau_bankname.setText(shgBankDTOListArrayList.get(position).getBankName());
        holder.mshgau_branchname.setText(shgBankDTOListArrayList.get(position).getBranchName());
        holder.mshgau_accountnumber.setText(shgBankDTOListArrayList.get(position).getAccountNumber());

        if(shgBankDTOListArrayList.get(position).isPrimary() == true)
        {
            holder.mShgaccount_bank.setText("Primary Account");
        }
        else
        {
            holder.mShgaccount_bank.setText("Other Account(s)");
        }
    }

    @Override
    public int getItemCount() {
        return shgBankDTOListArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mshgau_bankname,mshgau_branchname,mShgaccount_bank;
        EditText mshgau_accountnumber;


        public ViewHolder(View itemView) {
            super(itemView);
            mshgau_bankname = (TextView) itemView.findViewById(R.id.shgau_bankname);
            mshgau_branchname = (TextView) itemView.findViewById(R.id.shgau_branchname);
            mshgau_accountnumber = (EditText) itemView.findViewById(R.id.shgau_accountnumber);
            mShgaccount_bank = (TextView) itemView.findViewById(R.id.shgaccount_bank);
        }
    }
}
