package com.oasys.emathi.Adapter;



import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.R;


import java.util.List;

public class RecurringAdapter extends BaseAdapter {

    Context context;
    List<TableData> rowItems;

    public RecurringAdapter(Context context, List<TableData> items) {
        this.context = context;
        this.rowItems = items;
    }

    /* private view holder class */
    private class ViewHolder {
        TextView txtTitle,accNo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder = null;

        try {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null || !(convertView.getTag() instanceof ViewHolder)) {
                convertView = mInflater.inflate(R.layout.rdlayout, null);
                holder = new ViewHolder();

                holder.txtTitle = (TextView) convertView.findViewById(R.id.row_spn_tv);
                holder.accNo = (TextView) convertView.findViewById(R.id.row_spn_tv1);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            TableData rowItem = (TableData) getItem(position);

            holder.txtTitle.setText(rowItem.getBankName());
            holder.txtTitle.setGravity(Gravity.CENTER_VERTICAL);

            holder.accNo.setText(rowItem.getAccountNo());
            holder.accNo.setGravity(Gravity.CENTER_VERTICAL);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return rowItems.indexOf(getItem(position));
    }
}
