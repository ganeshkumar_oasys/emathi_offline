package com.oasys.emathi.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.emathi.Dto.MemberLoanSummary;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Report_MemberReport_LoanSummary_otherLoanAdapter extends RecyclerView.Adapter<Report_MemberReport_LoanSummary_otherLoanAdapter.MyViewholder> {

    private Context context;
    private ArrayList<MemberLoanSummary> memberLoanSummaries;
    private String dateStr;

    public Report_MemberReport_LoanSummary_otherLoanAdapter(Context context, ArrayList<MemberLoanSummary> memberLoanSummaries) {
        this.context = context;
        this.memberLoanSummaries = memberLoanSummaries;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {

        Calendar calender = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(calender.getTime());

//        DateFormat simple = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");

        if (memberLoanSummaries.get(position).getTransactiondate() != null && memberLoanSummaries.get(position).getTransactiondate().length() > 0) {
            Date d = new Date(Long.parseLong(memberLoanSummaries.get(position).getTransactiondate()));
            dateStr = simple.format(d);
        } else {
            dateStr = "NA";
        }

        holder.groupsavings_name.setText(dateStr);
        holder.groupsavings_amount.setText(memberLoanSummaries.get(position).getAmount());
        holder.groupsavings_voluntarysaving.setText(memberLoanSummaries.get(position).getInterest());

    }

    @Override
    public int getItemCount() {
        return memberLoanSummaries.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name, groupsavings_amount, groupsavings_voluntarysaving;


        public MyViewholder(View itemView) {
            super(itemView);


            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
          //  groupsavings_name.setTypeface(LoginActivity.sTypeface);
            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
            groupsavings_amount.setTypeface(LoginActivity.sTypeface);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);
            groupsavings_voluntarysaving.setTypeface(LoginActivity.sTypeface);

        }
    }
}
