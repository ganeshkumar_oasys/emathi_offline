package com.oasys.emathi.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;

import java.util.ArrayList;

public class BankTransactionReportAdapter extends RecyclerView.Adapter<BankTransactionReportAdapter.ViewHolder> {

    private Context context;
    private ArrayList<String> banktransactionlist;

    public BankTransactionReportAdapter(Context context,ArrayList<String> banktransactionlist) {
        this.context = context;
        this.banktransactionlist = banktransactionlist;
    }


    @Override
    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.memberreportitem, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder( ViewHolder viewHolder, int i) {

        viewHolder.mGroupreportName.setText(banktransactionlist.get(i));
        viewHolder.mGroupreportName.setTypeface(LoginActivity.sTypeface);
    }

    @Override
    public int getItemCount() {
        return banktransactionlist.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mGroupreportName;

        public ViewHolder(View itemView) {
            super(itemView);
            mGroupreportName = (TextView) itemView.findViewById(R.id.mMemberreportName);

        }
    }
}
