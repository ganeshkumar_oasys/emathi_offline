package com.oasys.emathi.Adapter;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.oasys.emathi.Dto.Memberinternallaon;
import com.oasys.emathi.R;

import java.util.ArrayList;


public class LoanreportAdapter extends RecyclerView.Adapter<LoanreportAdapter.ViewHolder> {

private Context context;
private ArrayList<Memberinternallaon> memberreportloansummaries;

    public LoanreportAdapter(Context context, ArrayList<Memberinternallaon> memberreportloansummaries) {
        this.context = context;
        this.memberreportloansummaries = memberreportloansummaries;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.monthlyloansummary, viewGroup, false);
        return new LoanreportAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
viewHolder.mloantype.setText(memberreportloansummaries.get(i).getLoan());
viewHolder.mlimit.setText(memberreportloansummaries.get(i).getAmount());
viewHolder.mloanoutstanding.setText(memberreportloansummaries.get(i).getOutstanding());
    }

    @Override
    public int getItemCount() {
        return memberreportloansummaries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mloantype,mlimit,mloanoutstanding;


        public ViewHolder(View itemView) {
            super(itemView);
            mlimit =(TextView)itemView.findViewById(R.id.limit);
            mloantype = (TextView) itemView.findViewById(R.id.typename);
            mloanoutstanding = (TextView) itemView.findViewById(R.id.amount);
        }
    }
}
