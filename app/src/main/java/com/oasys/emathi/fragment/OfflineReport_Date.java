package com.oasys.emathi.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.oasys.emathi.Adapter.CustomListAdapter;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.model.ListItem;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class OfflineReport_Date extends Fragment implements AdapterView.OnItemClickListener {

    private View view;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;

    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, mAutoFilllabel, mMemberName;
    private ArrayList<ListItem> listItems;
    private ListView mListView;
    private int listImage;
    String[] listItem;
    private int size;
    private CustomListAdapter mAdapter;
    public static String selectedItem;
    DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.offline_new_transactionlist, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        init();
    }

    private void init() {
        try {
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(com.oasys.emathi.activity.LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(com.oasys.emathi.activity.LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(com.oasys.emathi.activity.LoginActivity.sTypeface);
            /** UI Mapping **/

            mHeader = (TextView) view.findViewById(R.id.offline_headertext);
            mHeader.setText(AppStrings.offlineReports);
            mHeader.setTypeface(LoginActivity.sTypeface);
          /*  mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(AppStrings.offlineReports);
            mGroupName.setTypeface(LoginActivity.sTypeface);*/


            listItems = new ArrayList<ListItem>();
            mListView = (ListView) view.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;

            ArrayList<String> lastdateList = TransactionTable.getOfflineREPORTbyTxDates(shgDto.getShgId());


            Set<String> set = new LinkedHashSet<String>(lastdateList);
            listItem = new String[set.size()];
            set.toArray(listItem);

            for (int j = 0; j < listItem.length; j++) {
                System.out
                        .println("----------TRANSACTION LIST ITEMS---------"
                                + listItem[j].toString()
                                + " i pos " + j);
            }

            size = listItem.length;


            for (int i = 0; i < size; i++) {
                ListItem rowItem = new ListItem();
                rowItem.setTitle(simple.format(new Date(Long.parseLong(listItem[i]))));
                rowItem.setImageId(listImage);
                listItems.add(rowItem);
            }
            System.out.println("ROW ITEM " + listItems.size());

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);


        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            System.out.println("Error " + e.getMessage());
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        TextView textColor_Change = (TextView) view
                .findViewById(R.id.dynamicText);
        textColor_Change.setText(simple.format(new Date(Long.parseLong(listItem[position]))));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

        selectedItem = listItem[position];
        System.out.println("SELECTED ITEM  :" + selectedItem);

        OfflineReport_Menu fragment = new OfflineReport_Menu();
        Bundle bundle = new Bundle();
        bundle.putString("date", selectedItem);
        fragment.setArguments(bundle);
        NewDrawerScreen.showFragment(fragment);


    }
}