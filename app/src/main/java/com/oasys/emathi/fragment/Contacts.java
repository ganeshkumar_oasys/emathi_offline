package com.oasys.emathi.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.RegionalConversion;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.database.SHGTable;


public class Contacts extends Fragment {

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
    private TableLayout mTableLayout;
    private ListOfShg shgDto;
    private static final int READ_PHONE = 123;
    private static final int CALL_PHONE = 456;
    private String imei;

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // Camera permission has not been granted.
                requestPhoneStatePermission();

            } else {
                //   simState();
                try {
                    imei = getUniqueIMEIId(getActivity());
                } catch (Exception e) {
                    Log.e("onReqPermissionResult ", e.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestPhoneStatePermission() {
        // Camera permission has not been granted yet. Request it directly.
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE);
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE);
    }

    public static String getUniqueIMEIId(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "";
            }
            String imei = telephonyManager.getDeviceId();
            Log.e("imei", "=" + imei);
            if (imei != null && !imei.isEmpty()) {
                return imei;
            } else {
                return android.os.Build.SERIAL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "not_found";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case READ_PHONE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        imei = getUniqueIMEIId(getActivity());
                    } catch (Exception e) {
                        Log.e("onReqPermissionResult ", e.toString());
                    }

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE);
                }
                break;
            case CALL_PHONE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                  /*  try {
                        imei = getUniqueIMEIId(LoginActivity.this);
                    } catch (Exception e) {
                        Log.e("onReqPermissionResult ", e.toString());
                    }*/

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE);
                }
                break;

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_new_help_contacts, container, false);

        //MainFragment_Dashboard.isBackpressed = false;

        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mHeadertext = (TextView) rootView.findViewById(R.id.contacts_titleHead);
            mHeadertext.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.contacts)));
            mHeadertext.setTypeface(LoginActivity.sTypeface);

            mTableLayout = (TableLayout) rootView.findViewById(R.id.contactTableLayout);
            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
            params.setMargins(10, 5, 10, 5);

            // final String contact_No = "04342 - 261337";

            // final String contact_No = "+919994250580";

            String[] mContacts = new String[]{"8085311512", "9907117007"};

           /* if (PrefUtils.getOfflineContacts() != null) {
                if (EShaktiApplication.getEshaktiContacts() == null) {
                    EShaktiApplication.setEshaktiContacts(PrefUtils.getOfflineContacts());
                }
            }
            mContacts = EShaktiApplication.getEshaktiContacts().split("%");*/
            for (int i = 0; i < mContacts.length; i++) {
                String mContactSplit_temp = mContacts[i];
                // mContactSplit = mContactSplit_temp.split("~");

                // final String contact_No = mContactSplit[0] + " " +
                // mContactSplit[1];
                final String contact_No = mContactSplit_temp;
                TableRow row = new TableRow(getActivity());

                TextView contactText = new TextView(getActivity());
                contactText.setText(
                        RegionalConversion.getRegionalConversion(AppStrings.contactNo + " :  " + contact_No));
                contactText.setTypeface(LoginActivity.sTypeface);
                contactText.setTextColor(R.color.black);
                contactText.setPadding(5, 10, 5, 5);

                row.addView(contactText);

                TextView emptyTextView = new TextView(getActivity());
                emptyTextView.setText("   ");
                emptyTextView.setLayoutParams(params);
                row.addView(emptyTextView);

                ImageView imageView = new ImageView(getActivity());
                imageView.setBackgroundResource(android.R.drawable.ic_menu_call);
                imageView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + contact_No));
                        startActivity(callIntent);

                    }
                });
                row.addView(imageView);

                mTableLayout.addView(row);

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return rootView;
    }


}
