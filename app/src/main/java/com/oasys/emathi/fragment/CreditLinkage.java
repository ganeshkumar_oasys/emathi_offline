package com.oasys.emathi.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Dto.CreditlinkageDto;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.Service.NewTaskListener;

import org.json.JSONObject;

public class CreditLinkage extends Fragment implements AdapterView.OnItemSelectedListener, NewTaskListener {
    private Spinner creditlinkagespinner;
    private String creditlink_str = null;
    private Button credit_submit;
    private TextView mGroupName;
    private TextView creditline,creditlinkagetitle;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private ListOfShg shgDto;
    String[] values =
            {AppStrings.credit_linkage, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9","10"};
    private View rootView;
    OfflineDto  offline =new OfflineDto();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.fragment_new_creditlinkage);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_creditlinkage, container, false);
        return rootView;
    }

    public void init() {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setTypeface(LoginActivity.sTypeface);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        creditlinkagespinner = (Spinner) rootView.findViewById(R.id.creditlinkage_spinnerid);
        credit_submit = (Button) rootView.findViewById(R.id.submit_creditlinkage);
        credit_submit.setText(AppStrings.submit);
        credit_submit.setTypeface(LoginActivity.sTypeface);
        creditline = (TextView) rootView.findViewById(R.id.creditline);
        creditline.setText(AppStrings.subheader);
        creditline.setTypeface(LoginActivity.sTypeface);

        creditlinkagetitle = (TextView) rootView.findViewById(R.id.creditlinkagetitle);
        creditlinkagetitle.setText(AppStrings.credit_linkageinfo);
        creditlinkagetitle.setTypeface(LoginActivity.sTypeface);



    }

    private void insertTransaction_tdy()
    {

            offline.setIs_transaction_tdy("1.0");
            offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        creditlinkagespinner.setAdapter(adapter);
        creditlinkagespinner.setOnItemSelectedListener(this);
        credit_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    offline.setIs_transaction_tdy("1.0");
                    if (creditlink_str.toString() != null) {
                        if (creditlink_str.toString().contains("Credit Linkage")) {
                            Utils.showToast(getActivity(), "Select Credit Linkage");
                        } else {
                            CreditlinkageDto dto = new CreditlinkageDto();
                            dto.setCreditLinkageCount(creditlink_str);
                            dto.setShgId(shgDto.getShgId().toString());

                            String creditlinkReqJson = new Gson().toJson(dto);
                            // Log.d("LoginRequest", " ::::::::: " + loginReqJson);

                            RestClient.getRestClient(CreditLinkage.this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_CREDIT_LINKAGE, creditlinkReqJson, getActivity(), ServiceType.CREDITLINK);
                        }
                    } else {
                        Utils.showToast(getActivity(), "Select Credit Linkage");
                    }
                } catch (Exception e) {
                    Log.d("Credit", "::: " + e);

                }
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        creditlink_str = creditlinkagespinner.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case CREDITLINK:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    JSONObject jsonObject = new JSONObject(result);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        insertTransaction_tdy();
                        Utils.showToast(getActivity(), message);
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Profile",MainFragment.Flag_Profile);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        NewDrawerScreen.showFragment(new MainFragment());

                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;
        }


    }
}
