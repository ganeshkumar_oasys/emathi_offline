package com.oasys.emathi.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.core.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.RequestDto.ImageDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.PrefUtils;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadScedule extends Fragment implements View.OnClickListener, NewTaskListener {
    private TextView mGroupName, mCashInHand, mCashAtBank;

    ImageView mImageView;
    String mCurrentPhotoPath;
    RaisedButton mTakePhoto, mSubmitPhoto,mGallary_photo;

    static final int REQUEST_TAKE_PHOTO = 1;
    File photoFile = null;
    private File actualImage;
    private File compressedImage;
    private Dialog mProgressDilaog;
    Bitmap bitmap_ = null;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private String finalImageStr;
    private String shgId;
    OfflineDto offline =new OfflineDto();


    public UploadScedule() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_upload_scedule, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());


        try {

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mTakePhoto = (RaisedButton) rootView.findViewById(R.id.take_photo);
            mTakePhoto.setOnClickListener(this);
            mTakePhoto.setTypeface(LoginActivity.sTypeface);
            mTakePhoto.setText("TAKE PHOTO");

            mSubmitPhoto = (RaisedButton) rootView.findViewById(R.id.submit_photo);
            mSubmitPhoto.setOnClickListener(this);
            mSubmitPhoto.setTypeface(LoginActivity.sTypeface);
            mSubmitPhoto.setText(AppStrings.submit);
            mSubmitPhoto.setVisibility(View.INVISIBLE);

            mGallary_photo = (RaisedButton) rootView.findViewById(R.id.gallary_photo);
            mGallary_photo.setText("GALLARY");
            mGallary_photo.setVisibility(View.GONE);
//            mGallary_photo.setOnClickListener(this);


            mImageView = (ImageView) rootView.findViewById(R.id.imageview);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return rootView;
    }
    private void insertTransaction_tdy()
    {

            offline.setIs_transaction_tdy("1.0");
            offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.take_photo:
                takePhoto();
                break;

            case R.id.submit_photo:
                offline.setIs_transaction_tdy("1.0");
                ImageDto imageDto = new ImageDto();
                imageDto.setUrl(finalImageStr);
                String sreqString = new Gson().toJson(imageDto);
                mSubmitPhoto.setClickable(false);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.UPLOAD_SCHEDULE + shgId, sreqString, getActivity(), ServiceType.UPLOAD_SCHEDULE_IMAGE);
                } else {

                    if (TransactionTable.getLoginFlag(AppStrings.uploadschedule).size() <= 0 || ( !TransactionTable.getLoginFlag(AppStrings.uploadschedule).get(TransactionTable.getLoginFlag(AppStrings.uploadschedule).size()-1).getLoginFlag().equals("1"))) {
                        insertUploadschedule();
                        insertTransaction_tdy();
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Meeting", MainFragment.Flag_Meeting);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
                        TastyToast.makeText(getActivity(), "IMAGE UPLOADED SUCCESSFULLY",
                                TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                    }
                    else
                        TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                }
                break;
        }
    }

    private void insertUploadschedule() {

        int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.UPLOADSCHEDULE_COUNT, 0) + 1);
        if (value > 0)
            MySharedPreference.writeInteger(getActivity(), MySharedPreference.UPLOADSCHEDULE_COUNT, value);
            offline.setShgId(shgDto.getShgId());
            offline.setUploadCount(value + "");
            offline.setImageurl(finalImageStr);
        TransactionTable.insertUploadImageData(offline);
    }


    private void takePhoto() {

        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i("photo path = ", mCurrentPhotoPath);
        return image;
    }

/*
    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor << 1;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        try {

            actualImage = FileUtil.from(getActivity(), Uri.fromFile(new File(mCurrentPhotoPath)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            String storageDir = Environment.getExternalStorageDirectory() + "/Files/Compressed";

            compressedImage = new Compressor(getActivity()).setMaxWidth(640).setMaxHeight(480).setQuality(35)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)

                    .setDestinationDirectoryPath(storageDir).compressToFile(actualImage);

        } catch (IOException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }

        Matrix mtx = new Matrix();
        // mtx.postRotate(90);
        // Rotating Bitmap
        Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

        if (rotatedBMP != bitmap)
            bitmap.recycle();

        mImageView.setImageBitmap(rotatedBMP);
        finalImageStr = BitMapToString(rotatedBMP);

        File f = new File(compressedImage.getPath());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        try {
            mTakePhoto.setEnabled(false);
            mSubmitPhoto.setVisibility(View.VISIBLE);
            bitmap_ = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
*/

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

         //   setPic();

            String filePath = mCurrentPhotoPath;
            if (filePath != null) {
                Bitmap selectedImage = decodeFile(new File(filePath));
                //   Bitmap bitmap = (Bitmap) data.getExtras().get("data"); //CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
                mImageView.setImageBitmap(selectedImage);
                finalImageStr = bitmapToString(selectedImage);
            }

            try {
                mTakePhoto.setEnabled(false);
                mSubmitPhoto.setVisibility(View.VISIBLE);
             //   bitmap_ = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    String bitmapToString(Bitmap bm) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    private Bitmap decodeFile(File f) {

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Find the correct scale value. It should be the power of 2.
        final int REQUIRED_SIZE = 200;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void showError(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void setFragment(Fragment fragment) {
        // TODO Auto-generated method stub

        if (EMathiApplication.isDefault()) {
            PrefUtils.setStepWiseScreenCount("2");

        }

        getActivity().getSupportFragmentManager().beginTransaction()

                .replace(R.id.frame, fragment)
                .setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
                .addToBackStack(fragment.getClass().getName()).commit();

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case UPLOAD_SCHEDULE_IMAGE:
                try {
                    if (result != null) {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            insertTransaction_tdy();
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Meeting", MainFragment.Flag_Meeting);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
                            TastyToast.makeText(getActivity(), "IMAGE UPLOADED SUCCESSFULLY",
                                    TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);

                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);
                        }
                    }
                } catch (Exception e) {

                }
                break;

        }

    }
}
