package com.oasys.emathi.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Adapter.CustomListAdapter;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.RecyclerViewListener;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.model.ListItem;
import com.tutorialsee.lib.TastyToast;

import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Recurring_Deposit extends Fragment implements RecyclerViewListener, AdapterView.OnItemClickListener, NewTaskListener {

    private View rootView;
    String recurring_deposit_menu[];
    private List<ListItem> listItems;
    private ListView mListView;
    private CustomListAdapter mAdapter;
    private TextView mGroupName, mCashinHand, mCashatBank,mHeader;
    private ListOfShg shgDto;
    List<String> recurringMenu;
    private NetworkConnection networkConnection;
    int listImage;
    int flagvalue;
    private Dialog mProgressDilaog;
    private ArrayList<TableData> tableDataArrayList;
    private  String status="0.0";




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_menulist, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recurring_deposit_menu = new String[]{AppStrings.recurring_new_rd_account,AppStrings.recurring_deposit,AppStrings.recurring_withdrawl};
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        tableDataArrayList = SHGTable.getRecurringData();

//        flagValue();
        init();
    }

//    private void flagValue() {
//
//        if (networkConnection.isNetworkAvailable()) {
//            onTaskStarted();
//            RestClient.getRestClient(this).callWebServiceForGetMethod1( Constants.BASE_URL + Constants.RECURRINGFLAGVALUE + shgDto.getShgId(), getActivity(), ServiceType.RECURRING_DEPOSIT_FLAGVALUE);
//
//        }
//    }
    private void init() {

        try
        {

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
            mHeader.setVisibility(View.VISIBLE);
            mHeader.setText("" + AppStrings.mRd);
            mHeader.setTypeface(LoginActivity.sTypeface);

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;
            recurringMenu = Arrays.asList(recurring_deposit_menu);

            for (String it : recurringMenu) {
                ListItem rowItem = new ListItem();
                rowItem.setImageId(listImage);
                rowItem.setTitle(it);
                listItems.add(rowItem);
            }
            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);

            for (int i = 0; i <tableDataArrayList.size() ; i++) {

                if(shgDto.getShgId().equals(tableDataArrayList.get(i).getShgId()))
                {
                    status = "1.0";
                }
            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void recyclerViewListClicked(View view, int position) throws InterruptedIOException {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
        textColor_Change.setText(String.valueOf(recurringMenu.get(position)));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

        if ((position == 0)) {
            New_Rd_Accounnt_Fragment fragment = new New_Rd_Accounnt_Fragment();
            NewDrawerScreen.showFragment(fragment);
        } else if ((position == 1)) {
                if (status.equals("1.0")) {
                    RD_Deposit_Fragment fragment = new RD_Deposit_Fragment();
                    NewDrawerScreen.showFragment(fragment);
                }
            else {
                TastyToast.makeText(getActivity(),"Please Create RD Account",TastyToast.LENGTH_LONG,TastyToast.ERROR);
            }
        } else if (position == 2) {

                if (status.equals("1.0")) {
                    BT_RD_Entry fragment = new BT_RD_Entry();
                    NewDrawerScreen.showFragment(fragment);

            }else {
            TastyToast.makeText(getActivity(),"Please Create RD Account",TastyToast.LENGTH_LONG,TastyToast.ERROR);
        }
        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }


    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;

                switch (serviceType) {
                    case RECURRING_DEPOSIT_FLAGVALUE:

                        try {
                            ResponseDto cdto = new Gson().fromJson(result, ResponseDto.class);

                            if (cdto != null) {
                                String message = cdto.getMessage();
                                int statusCode = cdto.getStatusCode();
                                if (statusCode == Utils.Success_Code) {
                                    TastyToast.makeText(getActivity(), message, TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                                    flagvalue = Integer.parseInt(cdto.getResponseContent().getRdFlagDetails().getFlag());

                                } else {

                                    if (statusCode == 401) {

                                        Log.e("Group Logout", "Logout Sucessfully");
                                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                            mProgressDilaog.dismiss();
                                            mProgressDilaog = null;
                                        }
                                    }
                                    Utils.showToast(getActivity(), message);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            }

        }
    }

}
