package com.oasys.emathi.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.emathi.Adapter.FixedRecurringAccountoaccountAdapter;
import com.oasys.emathi.Dto.GroupBankTransactionSummaryList;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;
import com.tutorialsee.lib.TastyToast;

import java.util.ArrayList;


public class FixedDepositReport extends Fragment implements NewTaskListener {

    private View view;
    private TextView mGroupname, mCashinHand, mCashatBank, mHeader;
    private ListOfShg shgDto;
    private RecyclerView mFixeddeposit_recyclerview;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryDTOLists;

    private String url;
    private ResponseDto fixeddepositreportdto;
    private FixedRecurringAccountoaccountAdapter fixedRecurringAccountoaccountAdapter;

    public FixedDepositReport() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fixed_deposit_report, container, false);
        inIt(view);
        return view;
    }

    private void inIt(View view) {

        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        groupBankTransactionSummaryDTOLists = new ArrayList<>();

        mHeader = (TextView) view.findViewById(R.id.fragHeader);

        mGroupname = (TextView) view.findViewById(R.id.groupname);
        mGroupname.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupname.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);


        mFixeddeposit_recyclerview = (RecyclerView) view.findViewById(R.id.fd_recyclerview);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mFixeddeposit_recyclerview.setLayoutManager(linearLayoutManager);
        mFixeddeposit_recyclerview.setHasFixedSize(true);
        mFixeddeposit_recyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        Bundle bundle = getArguments();
        if (bundle != null) {
            String s = bundle.getString("FixedDeposit");
            if (s.equalsIgnoreCase("FIXEDDEPOSITREPORTS")) {

                mHeader.setText("FIXED DEPOSIT REPORT");
                mHeader.setTypeface(LoginActivity.sTypeface);
                fixedDeposit();
            } else if (s.equalsIgnoreCase("RECURRINGDEPOSITREPORTS")) {

                mHeader.setText("RECURRING DEPOSIT REPORTS");
                mHeader.setTypeface(LoginActivity.sTypeface);
                recurringDeposit();
            } else {
                mHeader.setText("ACCOUNT TO ACCOUNT REPORTS");
                mHeader.setTypeface(LoginActivity.sTypeface);
                accounttoaccounttransferDeposit();

            }
        } else {
            Toast.makeText(getActivity(), "The BundleReport is null", Toast.LENGTH_LONG).show();
        }


    }

    private void accounttoaccounttransferDeposit() {
        url = Constants.BASE_URL + Constants.ACCOUNTTOACCOUNTTRANFERT_REPORT + shgDto.getShgId();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(FixedDepositReport.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.FIXED_DEPOSIT_REPORT);
        } else {
            NewDrawerScreen.showFragment(new MainFragment());
            Utils.showToast(getActivity(), "No network Available");
        }
    }


    private void fixedDeposit() {

        url = Constants.BASE_URL + Constants.FIXED_DEPOSIT_REPORT + shgDto.getShgId();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(FixedDepositReport.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.FIXED_DEPOSIT_REPORT);
        } else {
            NewDrawerScreen.showFragment(new MainFragment());
            Utils.showToast(getActivity(), "No network Available");
        }
    }

    private void recurringDeposit() {

        url = Constants.BASE_URL + Constants.RECURRING_DEPOSIT_REPORT + shgDto.getShgId();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(FixedDepositReport.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.FIXED_DEPOSIT_REPORT);
        } else {
            NewDrawerScreen.showFragment(new MainFragment());
            Utils.showToast(getActivity(), "No network Available");
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {
            switch (serviceType) {
                case FIXED_DEPOSIT_REPORT:
                    fixeddepositreportdto = new Gson().fromJson(result.toString(), ResponseDto.class);

                    if (fixeddepositreportdto.getStatusCode() == Utils.Success_Code) {

                        Utils.showToast(getActivity(), fixeddepositreportdto.getMessage());
                        if ((fixeddepositreportdto.getResponseContent().getGroupBankTransactionSummaryList() != null)) {
                            fixedRecurringAccountoaccountAdapter = new FixedRecurringAccountoaccountAdapter(getActivity(),
                                    fixeddepositreportdto.getResponseContent().getGroupBankTransactionSummaryList());
                            mFixeddeposit_recyclerview.setAdapter(fixedRecurringAccountoaccountAdapter);
                        } else {
                            Utils.showToast(getActivity(), "Null Value");
                        }

                    } else if (fixeddepositreportdto.getStatusCode() == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    } else {
                        TastyToast.makeText(getActivity(), fixeddepositreportdto.getMessage(),
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }

                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
