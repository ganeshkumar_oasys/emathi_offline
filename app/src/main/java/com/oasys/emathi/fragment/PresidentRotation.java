package com.oasys.emathi.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.emathi.Adapter.CustomItemAdapter;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.PresidentRotationDto;
import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.model.RowItem;
import com.oasys.emathi.views.MaterialSpinner;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PresidentRotation#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PresidentRotation extends Fragment implements NewTaskListener {

    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private ListOfShg shgDto;
    private String shgId;
    private View rootView;
    private TableData tableData;
    private LinearLayout datePicker;
    private String shgformationMillis;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private Date openingDate,systemDate;
    private MaterialSpinner presidentNames_Spinner;
    static Context mContext;
    String defaultdatepick="";
    String date="";
    int datepick=0;
    private PresidentRotationDto presidentRotationDto;
    private TextView selected_Date;
    CustomItemAdapter presidentNameAdapter;
    private List<MemberList> memList;
    private List<RowItem> presidentNamesList;
    RaisedButton submit_button;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PresidentRotation(Context mContext) {
        // Required empty public constructor
        this.mContext = mContext;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PresidentRotation.
     */
    // TODO: Rename and change types and number of parameters
    /*public static PresidentRotation newInstance(String param1, String param2) {
        PresidentRotation fragment = new PresidentRotation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        tableData = SHGTable.getGroupProfile(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        rootView = inflater.inflate(R.layout.fragment_president_rotation,container,false);
        // Inflate the layout for this fragment
        return rootView;
    }

    public void init(){
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        datePicker = (LinearLayout) rootView.findViewById(R.id.datePicker);

        presidentNames_Spinner = (MaterialSpinner) rootView.findViewById(R.id.presidentNames_Spinner);
        presidentNames_Spinner.setBaseColor(R.color.grey_400);
        presidentNames_Spinner.setFloatingLabelText(AppStrings.mSelectedPresidentName);
        presidentNames_Spinner.setPaddingSafe(10, 0, 10, 0);

        selected_Date = (TextView) rootView.findViewById(R.id.selected_Date);

        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        presidentNamesList = new ArrayList<>();
        presidentNamesList.add(new RowItem("Select President Name"));
        for (int i =0; i<memList.size(); i++){
            presidentNamesList.add(new RowItem(memList.get(i).getMemberName()));
        }
        presidentNameAdapter = new CustomItemAdapter(mContext,presidentNamesList);
        presidentNames_Spinner.setAdapter(presidentNameAdapter);

        submit_button = (RaisedButton) rootView.findViewById(R.id.submit_button);
        submit_button.setText(AppStrings.submit);
        submit_button.setTypeface(LoginActivity.sTypeface);

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "Submit", Toast.LENGTH_SHORT).show();
                if (selected_Date.getText().toString().length()==0){
                    Toast.makeText(mContext, "Please Select Date", Toast.LENGTH_SHORT).show();
                }else {
                    if (presidentNames_Spinner.getSelectedItemPosition()==0){
                        Toast.makeText(mContext, "Please Select President Name", Toast.LENGTH_SHORT).show();
                    }else {
                        //Toast.makeText(mContext, ""+presidentNamesList.get(presidentNames_Spinner.getSelectedItemPosition()).getTitle(), Toast.LENGTH_SHORT).show();
                        presidentRotationDto.setPresidentName(presidentNamesList.get(presidentNames_Spinner.getSelectedItemPosition()).getTitle());
                        Toast.makeText(mContext, "Date : "+presidentRotationDto.getPresidentRotationDate()+"\nName : "+presidentRotationDto.getPresidentName(), Toast.LENGTH_SHORT).show();
                        networkConnection = NetworkConnection.getNetworkConnection(mContext);
                        if (networkConnection.isNetworkAvailable()){
                            //RestClient.getRestClient(mContext).callRestWebService();
                            String jsonRequest = new Gson().toJson(presidentRotationDto);
                            Toast.makeText(mContext, ""+jsonRequest, Toast.LENGTH_SHORT).show();
                            Log.i("print",jsonRequest);
                            performNetworkRequest(jsonRequest);
                        }else {

                        }
                    }
                }
            }
        });
    }

    public void performNetworkRequest(String jsonRequest){
        onTaskStarted();
        RestClient.getRestClient(this).callRestWebServiceForPutMethod("",jsonRequest,mContext,ServiceType.PRESIDENT_ROTATION);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        presidentRotationDto = new PresidentRotationDto();
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(requireContext(), "Clicked", Toast.LENGTH_SHORT).show();
                try {
                    if (shgDto != null)
                        shgformationMillis = shgDto.getOpeningDate();
                    Log.i("print",shgformationMillis);
                    Date opDate = new Date(Long.parseLong(shgformationMillis));
                    String formattedDate1 = df.format(opDate.getTime());
                    Log.i("print",formattedDate1);
                    openingDate = df.parse(formattedDate1);
                    Log.i("print",openingDate.toString());
                    Calendar calender = Calendar.getInstance();

                    String formattedDate = df.format(calender.getTime());
                    Log.i("print", formattedDate);
                    systemDate = df.parse(formattedDate);

                    Log.i("print",systemDate.toString());

                    if (openingDate.compareTo(systemDate) < 0 || openingDate.compareTo(systemDate) == 0){
                        Log.i("print","Function Called");
                        Log.i("print",String.valueOf(openingDate.compareTo(systemDate)));
                        calendarDialogShow(v, mContext);
                        //showDialog(mContext);
                    }else {
                        Log.i("print","else");
                        TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showDialog(Context context){
        new AlertDialog.Builder(requireContext())
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void calendarDialogShow(final View view, final Context mContext){
        //Context context = mContext;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(requireContext());

        LayoutInflater inflater = LayoutInflater.from(requireContext());
        View customView = inflater.inflate(R.layout.datepickerlayout_custom_ly, null);
        dialogBuilder.setView(customView);

        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);
        android.widget.DatePicker dpStartDate = (android.widget.DatePicker) customView.findViewById(R.id.dpStartDate);

        final TextView title = (TextView) customView.findViewById(R.id.title);
        final TextView sub_tit = (TextView) customView.findViewById(R.id.sub_tit);

        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        /*Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);*/

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (fday == cday && fmonth == cmonth && fyear == cyear) {
            min_Cal.set(cyear, cmonth, cday);
            lastDate.set(cyear, cmonth, cday);
        }
        else if (fyear <= cyear) {

            //Toast.makeText(mContext, "fyear less than cyear", Toast.LENGTH_SHORT).show();
            if (cmonth == fmonth && cyear == fyear) {

                min_Cal.set(fyear, fmonth, fday);
                lastDate.set(cyear, cmonth, cday);

            }
            else if (fmonth <= cmonth && cyear > fyear) {
                Toast.makeText(mContext, ""+fmonth, Toast.LENGTH_SHORT).show();
                min_Cal.set(fyear, fmonth, fday);

                Calendar c = Calendar.getInstance();
                c.setTime(systemDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                //lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                lastDate.set(cyear,cmonth,cday);

            } else if (cmonth > fmonth && cyear == fyear)
            {
                min_Cal.set(fyear, fmonth, fday);
                if ((cmonth - fmonth) == 1) {
//                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - fmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(systemDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }

            } else if (cmonth > fmonth && cyear > fyear) {
                min_Cal.set(fyear, fmonth, fday);
                Calendar c = Calendar.getInstance();
                c.setTime(systemDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(fyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }
        else
        {
            min_Cal.set(fyear, fmonth, fday);
            Calendar c = Calendar.getInstance();
            c.setTime(systemDate);
            c.add(Calendar.MONTH, 1);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
        }

      /*  datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());*/
        dpStartDate.setCalendarViewShown(false);
        if(lastDate.getTimeInMillis()>=min_Cal.getTimeInMillis()) {
            dpStartDate.setMinDate(min_Cal.getTimeInMillis());
            dpStartDate.setMaxDate(lastDate.getTimeInMillis());
            Log.d("lastdate",""+lastDate.getTimeInMillis());
            defaultdatepick= String.valueOf(lastDate.getTimeInMillis());
        }

        Calendar c = Calendar.getInstance();
        c.setTime(openingDate);
        DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
        String dateStr = simple1.format(c.getTime());

        title.setText(dateStr + " PICK A DATE AFTER THIS DATE");
        sub_tit.setText("Selected Date:");

        dpStartDate.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Toast.makeText(mContext, "Changed", Toast.LENGTH_SHORT).show();
                Log.e("onDateSet() call", dayOfMonth + "-" + month + "-" + year);
                date = dayOfMonth + "-" + (month + 1) + "-" + year;
                sub_tit.setText("Selected Date:"+date);
                datepick=1;
                try {

                    Calendar c = Calendar.getInstance();
                    c.set(year, month, dayOfMonth);
                    String formattedDate = df.format(c.getTime());
                    systemDate = df.parse(formattedDate);
                    MySharedPreference.writeString(mContext, MySharedPreference.LAST_TRANSACTION, systemDate.getTime() + "");


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int month = dpStartDate.getMonth()+1;
                date = dpStartDate.getDayOfMonth() + "-" + month + "-" + dpStartDate.getYear();
                presidentRotationDto.setPresidentRotationDate(date);
                Toast.makeText(mContext, ""+presidentRotationDto.getPresidentRotationDate(), Toast.LENGTH_SHORT).show();
                selected_Date.setText(presidentRotationDto.getPresidentRotationDate());
            }
        });

        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

    }

}