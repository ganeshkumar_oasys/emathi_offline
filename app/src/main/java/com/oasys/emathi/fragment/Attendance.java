    package com.oasys.emathi.fragment;

    import android.app.Dialog;
    import android.graphics.Color;
    import android.graphics.drawable.ColorDrawable;
    import android.os.Bundle;
    import androidx.annotation.NonNull;
    import androidx.annotation.Nullable;
    import androidx.fragment.app.Fragment;
    import androidx.fragment.app.FragmentManager;

    import android.util.Log;
    import android.view.LayoutInflater;
    import android.view.Menu;
    import android.view.View;
    import android.view.ViewGroup;
    import android.view.Window;
    import android.widget.Button;
    import android.widget.CheckBox;
    import android.widget.LinearLayout;
    import android.widget.TableLayout;
    import android.widget.TableRow;
    import android.widget.TextView;

    import com.google.gson.Gson;
    import com.oasys.emathi.Dialogue.Dialog_New_TransactionDate;
    import com.oasys.emathi.Dto.CashOfGroup;
    import com.oasys.emathi.Dto.ListOfShg;
    import com.oasys.emathi.Dto.MemberList;
    import com.oasys.emathi.Dto.OfflineDto;
    import com.oasys.emathi.Dto.RequestDto.AttendanceRequestDto;
    import com.oasys.emathi.Dto.RequestDto.Members;
    import com.oasys.emathi.Dto.ResponseDto;
    import com.oasys.emathi.EMathiApplication;
    import com.oasys.emathi.OasysUtils.AppDialogUtils;
    import com.oasys.emathi.OasysUtils.AppStrings;
    import com.oasys.emathi.OasysUtils.Constants;
    import com.oasys.emathi.OasysUtils.MySharedPreference;
    import com.oasys.emathi.OasysUtils.NetworkConnection;
    import com.oasys.emathi.OasysUtils.ServiceType;
    import com.oasys.emathi.OasysUtils.Utils;
    import com.oasys.emathi.R;
    import com.oasys.emathi.Service.NewTaskListener;
    import com.oasys.emathi.Service.RestClient;
    import com.oasys.emathi.activity.LoginActivity;
    import com.oasys.emathi.activity.NewDrawerScreen;
    import com.oasys.emathi.database.MemberTable;
    import com.oasys.emathi.database.SHGTable;
    import com.oasys.emathi.database.TransactionTable;
    import com.tutorialsee.lib.TastyToast;


    import java.text.DateFormat;
    import java.text.SimpleDateFormat;
    import java.util.ArrayList;
    import java.util.Calendar;
    import java.util.Date;
    import java.util.List;


    public class Attendance extends Fragment implements View.OnClickListener, NewTaskListener {
        private View view;
        private ListOfShg shgDto;
        private TextView mGroupName;
        private TextView mCashinHand;
        private TextView mCashatBank;
        private Button mRaised_Submit_Button;
        private Button mEdit_RaisedButton, mOk_RaisedButton;
        Button mPerviousButton, mNextButton;
        private NetworkConnection networkConnection;
        public static String members;
        private List<MemberList> memList;
        private int mSize;
        private LinearLayout mLayout;
        public static CheckBox sCheckBox[];
        String memberName, memberid;
        Dialog confirmationDialog;
        private ArrayList<Members> arrmemid;
        AttendanceRequestDto attendanceRequestDto = new AttendanceRequestDto();
        ResponseDto attendanceresponse;
//        public static String flag = "0";
        private TextView attendance_headertext, confirmationHeader;
        private ArrayList<OfflineDto> offlineDBData = new ArrayList<>();
        boolean resume_value=false;
        int statusCode;
        String message;
        String channel;


        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
            memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            view = inflater.inflate(R.layout.fragment_attendance, container, false);
            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);


            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);


            attendance_headertext = (TextView) view.findViewById(R.id.attendance_headertext);
            attendance_headertext.setTypeface(LoginActivity.sTypeface);
            attendance_headertext.setText(AppStrings.Attendance);


            mRaised_Submit_Button = (Button) view.findViewById(R.id.attendance_Submitbutton);
            mRaised_Submit_Button.setText(AppStrings.submit);
            mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
            mRaised_Submit_Button.setOnClickListener(this);

            mPerviousButton = (Button) view.findViewById(R.id.attendance_Previousbutton);
            //  mPerviousButton.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.mPervious));
            //  mPerviousButton.setTypeface(LoginActivity.sTypeface);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) view.findViewById(R.id.attendance_Nextbutton);
            //mNextButton.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.mNext));
            // mNextButton.setTypeface(LoginActivity.sTypeface);
            mNextButton.setOnClickListener(this);

//            if (EShaktiApplication.getFlag()!=null && EShaktiApplication.getFlag().trim().equals("1") ) {
//                NewDrawerScreen.mToolbarDashboard.setVisibility(View.INVISIBLE);
//            }
           /* Log.d("flagvalue",EShaktiApplication.flag);

            if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                NewDrawerScreen.item1.setVisible(false);
                NewDrawerScreen.item2.setVisible(false);
                NewDrawerScreen.item.setVisible(false);
                NewDrawerScreen.logOutItem.setVisible(false);
                NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
            }
            else
            {
                NewDrawerScreen.item1.setVisible(true);
                NewDrawerScreen.item2.setVisible(true);
                NewDrawerScreen.item.setVisible(true);
                NewDrawerScreen.logOutItem.setVisible(true);
                NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
            }
*/

            return view;
        }

        @Override
        public void onPrepareOptionsMenu(Menu menu) {
            if (EMathiApplication.flag!=null && EMathiApplication.flag.trim().equals("1") ) {
                menu.findItem(R.id.action_home).setVisible(false);
                menu.findItem(R.id.action_logout).setVisible(false);
                menu.findItem(R.id.menu_logout).setVisible(false);
                menu.findItem(R.id.action_grouplist).setVisible(false);
                NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
            }
            else
            {
                menu.findItem(R.id.action_home).setVisible(true);
                menu.findItem(R.id.action_logout).setVisible(true);
                menu.findItem(R.id.menu_logout).setVisible(true);
                menu.findItem(R.id.action_grouplist).setVisible(true);
                NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
            }
            super.onPrepareOptionsMenu(menu);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            try {
                if (MySharedPreference.readInteger(getActivity(), MySharedPreference.ATTND_COUNT, 0) <= 0)
                    MySharedPreference.writeInteger(getActivity(), MySharedPreference.ATTND_COUNT, 0);
                mLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
                sCheckBox = new CheckBox[mSize];
                for (int i = 0; i < mSize; i++) {

                    LinearLayout linearLayout = new LinearLayout(getActivity());
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                    sCheckBox[i] = new CheckBox(getActivity());
                    sCheckBox[i].setChecked(true);

                    sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);

                    linearLayout.addView(sCheckBox[i]);

                    TextView memberName = new TextView(getActivity());
                    memberName.setText(memList.get(i).getMemberName());
                    memberName.setTypeface(LoginActivity.sTypeface);
                    memberName.setPadding(20, 10, 0, 10);
                    linearLayout.addView(memberName);

                    mLayout.addView(linearLayout);
                }
            } catch (Exception e) {

            }


        }
        private void insertTransaction_tdy()
        {
            for (OfflineDto ofdto : offlineDBData) {
                ofdto.setIs_transaction_tdy("1.0");
                ofdto.setShgId(shgDto.getShgId());
                SHGTable.updateIstransaction(ofdto);
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.attendance_Submitbutton:
                    try {
                        arrmemid = new ArrayList<>();
                        String Yes_No = "0";
                        memberName = "";
                        memberid = "";

                        for (int i = 0; i < mSize; i++) {

                            if (sCheckBox[i].isChecked()) {
                                Yes_No = "1";
                                memberName = memberName + memList.get(i).getMemberName() + "~";
                                memberid = memberid + memList.get(i).getMemberId() + "~";
                            } else {
                                Yes_No = "0";
                            }

                        }


                        String memberNameArr[] = memberName.split("~");
                        String memberidArr[] = memberid.split("~");

                        if (!memberName.equals("")) {

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
                            dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText(AppStrings.confirmation);

                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                            Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                            String dateStr = simple.format(d);
                            TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                            transactdate.setText(dateStr);

                            try {

                                CheckBox checkBox[] = new CheckBox[memberNameArr.length];

                                for (int j = 0; j < memberNameArr.length; j++) {
                                    TableRow indv_memberNameRow = new TableRow(getActivity());

                                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                            TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                                    contentParams.setMargins(10, 5, 10, 5);

                                    checkBox[j] = new CheckBox(getActivity());
                                    checkBox[j].setText(memberNameArr[j]);
                                    checkBox[j].setChecked(true);
                                    checkBox[j].setClickable(false);
                                    checkBox[j].setTextColor(R.color.black);
                                    indv_memberNameRow.addView(checkBox[j]);

                                    Members memid = new Members();
                                    memid.setMemberId(memberidArr[j]);
                                    arrmemid.add(memid);

                                    try {
                                        OfflineDto offline = new OfflineDto();
                                        offline.setMemberId(memberidArr[j]);
                                        offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                                        offline.setShgId(shgDto.getShgId());
                                        offline.setCashInhand(shgDto.getCashInHand());
                                        offline.setCashAtBank(shgDto.getCashAtBank());
                                        offline.setTxType(NewDrawerScreen.ATTENDANCE);
                                        offline.setTxSubtype(AppStrings.Attendance);
//                                        offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                                        offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                                        offline.setModifiedDateTime(System.currentTimeMillis() + "");
                                        offline.setIs_transaction_tdy("1.0");
                                        offlineDBData.add(offline);


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    confirmationTable.addView(indv_memberNameRow,
                                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                }

                            } catch (Exception e) {
                                // TODO: handle exception
                                System.out.println("checkbox layout error:" + e.toString());
                            }

                            confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);

                            mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.MinutesAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                            memberName = "";
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    break;
                case R.id.fragment_Edit_button:

                    memberName = "";
                    mRaised_Submit_Button.setClickable(true);
                    confirmationDialog.dismiss();
                    break;
                case R.id.fragment_Ok_button:
                    try {
                        if (confirmationDialog.isShowing())
                            confirmationDialog.dismiss();

                        if (networkConnection.isNetworkAvailable()) {
                            if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                return;
                            }
                            // NOTHING TO DO::
                        } else {

                            if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                return;
                            }
                        }



                        String shgId = shgDto.getShgId();
                        Calendar calender = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String formattedDate = df.format(calender.getTime());
                        attendanceRequestDto.setShgId(shgId);
//                        attendanceRequestDto.setTransactionDate(shgDto.getLastTransactionDate());
                        attendanceRequestDto.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                        attendanceRequestDto.setMembers(arrmemid);
                        String sreqString = new Gson().toJson(attendanceRequestDto);

//                        if (EShaktiApplication.getFlag()!=null && EShaktiApplication.getFlag().trim().equals("1") ) {
                        if (EMathiApplication.flag!=null && EMathiApplication.flag.trim().equals("1") ) {
//                        if (channel!=null && channel.trim().equals("STEPWISE") ) {
                           /* confirmationDialog.dismiss();
                            Savings savings = new Savings();
                            Bundle bundle = new Bundle();
                            bundle.putString("stepwise", flag);
                            savings.setArguments(bundle);
                            insertAttendance();
                            NewDrawerScreen.showFragment(savings);*/

                            if (networkConnection.isNetworkAvailable()) {
                                RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.ATTENDANCEPOST, sreqString, getActivity(), ServiceType.ATTENDANCEPOST);
                            } else {
                                if (TransactionTable.getLoginFlag(AppStrings.Attendance).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.Attendance).get(TransactionTable.getLoginFlag(AppStrings.Attendance).size() - 1).getLoginFlag().equals("1"))) {
                                    confirmationDialog.dismiss();
                                    Savings savings = new Savings();
                                   /* Bundle bundle = new Bundle();
                                    bundle.putString("stepwise", EShaktiApplication.flag);
                                    savings.setArguments(bundle);*/
//                                    SharedPreferences preferences = getActivity().getSharedPreferences("stepwise" , Context.MODE_PRIVATE);
//                                    SharedPreferences.Editor editor = preferences.edit();
//                                    editor.putString("key", channel);
//                                    editor.commit();
                                    insertAttendance();
                                    NewDrawerScreen.showFragment(savings);

                                } else
                                    TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                            TastyToast.WARNING);
                            }
                        }
                        else {
                            if (networkConnection.isNetworkAvailable()) {
                                RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.ATTENDANCEPOST, sreqString, getActivity(), ServiceType.ATTENDANCEPOST);
                            } else {
                                if (TransactionTable.getLoginFlag(AppStrings.Attendance).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.Attendance).get(TransactionTable.getLoginFlag(AppStrings.Attendance).size() - 1).getLoginFlag().equals("1"))) {
                                    insertAttendance();
                                } else
                                    TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                            TastyToast.WARNING);
                            }
                       }
                    } catch (Exception e) {
                        Log.e("", e.toString());
                    }
                    break;
            }
        }

        private void insertAttendance() {

            try {
                int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.ATTND_COUNT, 0) + 1);
                if (value > 0)
                    MySharedPreference.writeInteger(getActivity(), MySharedPreference.ATTND_COUNT, value);

                for (OfflineDto ofdto : offlineDBData) {
                    ofdto.setAttCount(value + "");
                    TransactionTable.insertAttendanceData(ofdto);
                }   //TODO::   CIH,CAB & LT / Dont credit twice offline on same loan / Credit the same loan after logout

                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                }
                //TODO:::: CIH & CAB
                String cihstr = "", cabstr = "", lastTranstr = "";
                cihstr = shgDto.getCashInHand();
                cabstr = shgDto.getCashAtBank();
                lastTranstr = offlineDBData.get(0).getLastTransactionDateTime();

                CashOfGroup csg = new CashOfGroup();
                csg.setCashInHand(cihstr);
                csg.setCashAtBank(cabstr);
                csg.setLastTransactionDate(lastTranstr);
                SHGTable.updateSHGDetails(csg, shgDto.getId());

                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                NewDrawerScreen.showFragment(new MainFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onResume() {
            super.onResume();
      Log.d("resumevalue","velladhurai");
            if(resume_value==true)
            {

                    if (attendanceresponse.getStatusCode() == Utils.Success_Code) {
                        resume_value=true;
                        if (confirmationDialog.isShowing()) {
                            confirmationDialog.dismiss();
                        }

//                        if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {
                        if (EMathiApplication.flag!=null && EMathiApplication.flag.trim().equals("1") ) {
//                        if (channel!=null && channel.trim().equals("STEPWISE") ) {
                            confirmationDialog.dismiss();
                            Savings savings = new Savings();
//                            Bundle bundle = new Bundle();
//                            bundle.putString("stepwise", EShaktiApplication.flag);
//                            savings.setArguments(bundle);
                            /*SharedPreferences preferences = getActivity().getSharedPreferences("stepwise" , Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("key", channel);
                            editor.commit();*/
                            TastyToast.makeText(getActivity(), "Transaction Completed", TastyToast.LENGTH_SHORT,
                                    TastyToast.SUCCESS);
                            NewDrawerScreen.showFragment(savings);
                        }
                        else {

                            Utils.showToast(getActivity(), attendanceresponse.getMessage());
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Meeting", MainFragment.Flag_Meeting);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
                            //                        NewDrawerScreen.showFragment(new MainFragment());
                        }
                    } else {
                        insertAttendance();
                        if (attendanceresponse.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        confirmationDialog.dismiss();
                        Utils.showToast(getActivity(), attendanceresponse.getMessage());
                    }

            }
        }

        @Override
        public void onTaskStarted() {

        }

        @Override
        public void onTaskFinished(String result, ServiceType serviceType) {
            switch (serviceType) {
                case ATTENDANCEPOST:
                    try {
                        if (result != null) {
                            attendanceresponse = new Gson().fromJson(result, ResponseDto.class);

                              if (result != null) {
                                if (attendanceresponse.getStatusCode() == Utils.Success_Code) {

                                     if (confirmationDialog.isShowing()) {
                                        confirmationDialog.dismiss();
                                    }
                                    insertTransaction_tdy();
//                                    if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {
                                    if (EMathiApplication.flag!=null && EMathiApplication.flag.trim().equals("1") ) {
//                                    if (channel!=null && channel.trim().equals("STEPWISE") ) {
                                        resume_value=true;
                                        confirmationDialog.dismiss();
                                        Savings savings = new Savings();
//                                        Bundle bundle = new Bundle();
//                                        bundle.putString("stepwise", EShaktiApplication.flag);
//                                        savings.setArguments(bundle);
//                                        SharedPreferences preferences = getActivity().getSharedPreferences("stepwise" , Context.MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = preferences.edit();
//                                        editor.putString("key", channel);
//                                        editor.commit();
                                        TastyToast.makeText(getActivity(), "Transaction Completed", TastyToast.LENGTH_SHORT,
                                                TastyToast.SUCCESS);
                                        NewDrawerScreen.showFragment(savings);
                                    }
                                    else {

                                        Utils.showToast(getActivity(), attendanceresponse.getMessage());
                                        FragmentManager fm = getFragmentManager();
                                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        MainFragment mainFragment = new MainFragment();
                                        Bundle bundles = new Bundle();
                                        bundles.putString("Meeting", MainFragment.Flag_Meeting);
                                        mainFragment.setArguments(bundles);
                                        NewDrawerScreen.showFragment(mainFragment);
                                        //                        NewDrawerScreen.showFragment(new MainFragment());
                                    }
                                } else {
//                                    insertAttendance();
                                    if (attendanceresponse.getStatusCode() == 401) {

                                        Log.e("Group Logout", "Logout Sucessfully");
                                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                                    }
                                    confirmationDialog.dismiss();
                                    Utils.showToast(getActivity(), attendanceresponse.getMessage());
                                }
                            } else {
                                insertAttendance();
                            }
                        } else {
                            insertAttendance();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        insertAttendance();
                    }

                    break;
            }

        }
    }
