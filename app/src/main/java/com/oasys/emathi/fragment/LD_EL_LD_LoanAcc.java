package com.oasys.emathi.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.emathi.Adapter.CustomItemAdapter;
import com.oasys.emathi.Dialogue.Dialog_New_TransactionDate;
import com.oasys.emathi.Dto.InternalLoanEntry;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.ShgBankDetails;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.BankTable;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.model.RowItem;
import com.oasys.emathi.views.Get_EdiText_Filter;
import com.oasys.emathi.views.MaterialSpinner;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_EL_LD_LoanAcc extends Fragment implements View.OnClickListener, NewTaskListener, DatePickerDialog.OnDateSetListener {

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private TextView mSanctionAmountText, mDisbursementAmountText, mBalanceAmountText, mSanctionAmount_value,
            mDisbursementAmount_value, mBalanceAmount_value, mDis_AmountText;
    private TextView mDis_DateText;
    private EditText mDis_Amount_editText;
    RadioButton mCashRadio, mBankRadio;
    private RaisedButton mSubmitButton;
    View rootview;
//    public static String disbursementDate = "";
    public  String disbursementDate = "";
    public static String dashDate = "";
//    public static String selectedType, selectedItemBank;
    public  String selectedType, selectedItemBank;
    public static String disbursementAmount;
    public static String disbursementDate_check = "";
    private String mLanguageLocale = "";
    Locale locale;
    MaterialSpinner materialSpinner_Bank;
    CustomItemAdapter bankNameAdapter;
    private List<RowItem> bankNameItems;
    public static String mBankNameValue = null;
    LinearLayout mSpinnerLayout;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
    Dialog confirmationDialog;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    Date date_dashboard, date_loanDisb;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;
    private Dialog mProgressDilaog;
    Date disburseDate, openinigDate, currentDate;
    public static InternalLoanEntry lEntry = new InternalLoanEntry();


    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private Context globalContext = null;
    private Context context;
    public static ShgBankDetails mSelectedBank;
    OfflineDto offline = new OfflineDto();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragment_new_loanacc_disburse, container, false);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

        return rootview;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        if (networkConnection.isNetworkAvailable()) {

            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_FROM_LOANAC + LD_ExternalLoan.sExistingLoagSelection.getLoanId(), getActivity(), ServiceType.LD_FROM_LOANAC);

        }
    }

    private void init() {
        try {
            mGroupName = (TextView) rootview.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashInHand = (TextView) rootview.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);
            mCashAtBank = (TextView) rootview.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);
            mHeader = (TextView) rootview.findViewById(R.id.loanDisbursementheader);
            mSanctionAmountText = (TextView) rootview.findViewById(R.id.loan_dis_sanctionAmountTextView);
            mSanctionAmount_value = (TextView) rootview.findViewById(R.id.loan_dis_sanctionAmount_values);
            mDisbursementAmountText = (TextView) rootview.findViewById(R.id.loan_dis_disbursementAmountTextView);
            mDisbursementAmount_value = (TextView) rootview.findViewById(R.id.loan_dis_disbursementAmount_values);
            mBalanceAmountText = (TextView) rootview.findViewById(R.id.loan_dis_balanceTextView);
            mBalanceAmount_value = (TextView) rootview.findViewById(R.id.loan_dis_balance_values);
            mDis_AmountText = (TextView) rootview.findViewById(R.id.loanDisb_disbursementAmountTextView);
            mDis_DateText = (TextView) rootview.findViewById(R.id.loan_dis_disDateTV);
            mDis_DateText.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.mDisbursementDate));
            mDis_DateText.setText("");
            mCashRadio = (RadioButton) rootview.findViewById(R.id.radioDisbursementLimitCash);
            mBankRadio = (RadioButton) rootview.findViewById(R.id.radioDisbursementLimitBank);


            mHeader.setText((AppStrings.mLoanDisbursementFromLoanAcc));
            mSanctionAmountText.setText((AppStrings.mSanctionAmount));
            mDisbursementAmountText.setText((AppStrings.mDisbursementAmount));
            mBalanceAmountText.setText((AppStrings.mBalanceAmount));
            mDis_AmountText.setText((AppStrings.mDisbursementAmount));
            mCashRadio.setText((AppStrings.mLoanaccCash));
            mBankRadio.setText((AppStrings.mLoanaccBank));
            mSanctionAmount_value.setText("0");
            mDisbursementAmount_value.setText("0");
            mBalanceAmount_value.setText("0");


            mHeader.setTypeface(LoginActivity.sTypeface);
            mSanctionAmountText.setTypeface(LoginActivity.sTypeface);
            mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
            mBalanceAmountText.setTypeface(LoginActivity.sTypeface);
            mDis_AmountText.setTypeface(LoginActivity.sTypeface);
            mCashRadio.setTypeface(LoginActivity.sTypeface);
            mBankRadio.setTypeface(LoginActivity.sTypeface);
            mSanctionAmount_value.setTypeface(LoginActivity.sTypeface);
            mDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);
            mBalanceAmount_value.setTypeface(LoginActivity.sTypeface);

            mSpinnerLayout = (LinearLayout) rootview.findViewById(R.id.loan_dis_bankSpinnerlayout);
            mSpinnerLayout.setVisibility(View.GONE);


            //  mDis_DateText.setText("");
            mDis_DateText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    disbursementDate_check = "1";
                    try {
                        //  Date opDate = new Date(Long.parseLong(shgformationMillis));
                        Log.i("bankaccount","disbursement date : "+LD_ExternalLoan.sExistingLoagSelection.getDisbursmentDate());
                        Log.i("bankaccount","pref : "+MySharedPreference.readString(getActivity(), MySharedPreference.LAST_TRANSACTION, ""));
                        Date opDate = new Date(Long.parseLong(LD_ExternalLoan.sExistingLoagSelection.getDisbursmentDate()));
                        String formattedDate1 = df.format(opDate.getTime());

                        Date tDate = new Date(Long.parseLong(MySharedPreference.readString(getActivity(), MySharedPreference.LAST_TRANSACTION, "")));
                        String formattedDate2 = df.format(tDate.getTime());
                        // dashDate = formattedDate2;
                        openinigDate = df.parse(formattedDate1);
                        dashDate = formattedDate1;
                        Calendar now = Calendar.getInstance();
                        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                Calendar selectionCal = Calendar.getInstance();
                                selectionCal.set(year, month, day);
                                String formattedDate = df.format(selectionCal.getTime());
                                try {
                                    disburseDate = df.parse(formattedDate);
                                    disbursementDate = formattedDate + "";
                                    mDis_DateText.setText(formattedDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
                        Calendar min_Cal = Calendar.getInstance();
                        Calendar lastDate = Calendar.getInstance();

                        Calendar fcal = Calendar.getInstance();
                        fcal.setTimeInMillis(openinigDate.getTime());
                        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
                        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
                        int fday = fcal.get(Calendar.DATE);

//                        if (shgDto.getLastTransactionDate() != null && shgDto.getLastTransactionDate().length() > 0) {
                        if (Dialog_New_TransactionDate.cg.getLastTransactionDate() != null && Dialog_New_TransactionDate.cg.getLastTransactionDate().length() > 0) {
                            Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                            DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                            String dateStr = simple.format(d);
                            Date lastransactiondate = simple.parse(dateStr);
                            Calendar lcal = Calendar.getInstance();
                            lcal.setTimeInMillis(lastransactiondate.getTime());
                            int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                            int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                            int lday = lcal.get(Calendar.DATE);
                            lastDate.set(lyear, lmonth, lday);


                        }

//                        Date cDate = new Date(System.currentTimeMillis());
//                        String formattedDate = df.format(cDate.getTime());
//                        currentDate = df.parse(formattedDate);
//                        Calendar ccal1 = Calendar.getInstance();
//                        ccal1.setTimeInMillis(currentDate.getTime());

                        min_Cal.set(fyear, fmonth, fday);
                        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
                        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
                        datePickerDialog.show();












                       /* try {
                            Calendar now = Calendar.getInstance();
                            Calendar min_Cal = Calendar.getInstance();
                            Calendar lastDate = Calendar.getInstance();
                         //   min_Cal.add(Calendar.DAY_OF_YEAR, 30);//DummyValue
                            String formattedDate2 = df.format(min_Cal.getTime());  //DummyValue

                            //Date opDate = new Date(Long.parseLong(LD_ExternalLoan.sExistingLoagSelection.getDisbursmentDate()));
                            Date opDate = new Date(formattedDate2);
                            String formattedDate1 = df.format(opDate.getTime());
                            openinigDate = df.parse(formattedDate1);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity().getApplicationContext(), LD_EL_LD_LoanAcc.this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
                            datePickerDialog.getWindow().setType(WindowManager.LayoutParams.
                                    TYPE_SYSTEM_ALERT);
                            //TODO::


                            Calendar fcal = Calendar.getInstance();
                            fcal.setTimeInMillis(openinigDate.getTime());
                            int fyear = fcal.get(Calendar.YEAR); // this is deprecated
                            int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
                            int fday = fcal.get(Calendar.DATE);

                            String formattedDate = df.format(now.getTime());
                            dashDate = formattedDate;
                            currentDate = df.parse(formattedDate);
                            Calendar ccal1 = Calendar.getInstance();
                            ccal1.setTimeInMillis(currentDate.getTime());
                            int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                            int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                            int cday = ccal1.get(Calendar.DATE);
                            lastDate.set(cyear, cmonth, cday);

                            min_Cal.set(fyear, fmonth, fday + 1);
                            datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
                            datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
                            datePickerDialog.show();

                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });


            mDis_Amount_editText = (EditText) rootview.findViewById(R.id.loanDisb_disbursementAmount_value);
            mDis_Amount_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            mDis_Amount_editText.setPadding(5, 5, 5, 5);
            mDis_Amount_editText.setFilters(Get_EdiText_Filter.editText_filter());
            mDis_Amount_editText.setBackgroundResource(R.drawable.edittext_background);

            mSubmitButton = (RaisedButton) rootview.findViewById(R.id.loanDisbursement_submit);
            mSubmitButton.setText((AppStrings.next));
            mSubmitButton.setOnClickListener(this);


            materialSpinner_Bank = (MaterialSpinner) rootview.findViewById(R.id.loan_dis_bankspinner);

            RadioGroup radioGroup = (RadioGroup) rootview.findViewById(R.id.radioDisbursementLimit);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId) {
                        case R.id.radioDisbursementLimitCash:
                            selectedType = "Cash";
                            mSpinnerLayout.setVisibility(View.GONE);
                            break;

                        case R.id.radioDisbursementLimitBank:
                            selectedType = "Bank";
                            mSpinnerLayout.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });

            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName().toString());
                mBanknamesId_Array.add(String.valueOf(i));
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName().toString());
                mEngSendtoServerBankId_Array.add(bankdetails.get(i).getBankId());
            }

            materialSpinner_Bank.setBaseColor(R.color.grey_400);

            materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

            materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

            final String[] bankNames = new String[bankdetails.size() + 1];

            final String[] bankNames_BankID = new String[bankdetails.size() + 1];

            bankNames[0] = String.valueOf((AppStrings.bankName));
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames[i + 1] = bankdetails.get(i).getBankName().toString();
            }

            bankNames_BankID[0] = String.valueOf((AppStrings.bankName));
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames_BankID[i + 1] = bankdetails.get(i).getBankId().toString();
            }

            int size = bankNames.length;

            bankNameItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);//sBankNames.elementAt(i).toString());
                bankNameItems.add(rowItem);
            }
            bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
            materialSpinner_Bank.setAdapter(bankNameAdapter);

            materialSpinner_Bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_BankID[0];
                        mBankNameValue = "0";

                    } else {
                        selectedItemBank = bankNames_BankID[position];
                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        mBankNameValue = selectedItemBank;
                        mSelectedBank = bankdetails.get(position - 1);
                        String mBankname = null;
                        for (int i = 0; i < bankdetails.size(); i++) {
                            if (selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                                mBankname = mEngSendtoServerBank_Array.get(i);
                            }
                        }
                        ShgBankDetails details=BankTable.getBankTransaction(mSelectedBank.getShgSavingsAccountId());
                        mSelectedBank.setCurrentBalance(details.getCurrentBalance());

                        mBankNameValue = mBankname;

                    }
                    //  Log.e("Selected value", mBankNameValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });


        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    private void CallDisbursementDate() throws ParseException {

     /*   if (shgDto != null)
            shgformationMillis = shgDto.getGroupFormationDate();
        */


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.loanDisbursement_submit:
                try {
                    offline.setIs_transaction_tdy("1.0");
                    disbursementAmount = "0";
                    disbursementAmount = mDis_Amount_editText.getText().toString();
                    String balanceAmount = mBalanceAmount_value.getText().toString();
                    if (disbursementAmount.equals("") || disbursementAmount == null) {
                        disbursementAmount = "0";
                    }

                    if (!disbursementAmount.equals("0") && selectedType!=null && !disbursementDate.equals("")) {

                        // String dashBoardDate = DatePickerDialog.sDashboardDate;
                        String dashBoardDate1 = dashDate;

                        String loanDisbArr[] = disbursementDate.split("/");
                        String loanDisbDate = loanDisbArr[0] + "-" + loanDisbArr[1] + "-" + loanDisbArr[2];

                        String dashBoardArr1[] = dashBoardDate1.split("/");
                        String dashBoardDate = dashBoardArr1[0] + "-" + dashBoardArr1[1] + "-" + dashBoardArr1[2];

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                        try {
                            // onShowConfirmationDialog();
                            date_dashboard = sdf.parse(dashBoardDate);
                            date_loanDisb = sdf.parse(loanDisbDate);

                        } catch (ParseException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }

//TODO ::  LOGICAL CHanges

                        if (date_dashboard.compareTo(date_loanDisb) <= 0) {
                            if (((int) Double.parseDouble(balanceAmount)) != 0) {

                                if (((int) Double.parseDouble(disbursementAmount)) <= ((int) Double.parseDouble(balanceAmount))) {


                                    if (mBankRadio.isChecked()) {
                                        if (!mBankNameValue.equals("0") && mBankNameValue != null) {

                                            onShowConfirmationDialog();

                                        } else {
                                            TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                                    TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                        }

                                    } else {
                                        onShowConfirmationDialog();
                                    }

                                } else {
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementAlert,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }

                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                            }

                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:

                disbursementAmount = "0";
                mSubmitButton.setClickable(true);
                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:
                confirmationDialog.dismiss();

                lEntry.setMobileDate(System.currentTimeMillis() + "");
                lEntry.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                lEntry.setShgId(shgDto.getShgId());
                lEntry.setDisbursmentDate(new Date(disbursementDate).getTime() + "");
                lEntry.setDisbursementAmount(disbursementAmount);
                lEntry.setLoanId(LD_ExternalLoan.sExistingLoagSelection.getLoanId());

                if (selectedType.equals("Bank")) {
                    lEntry.setModeOfCash("1");
                    //  lEntry.setSbAccountId(selectedItemBank);
                    lEntry.setSbAccountId(mSelectedBank.getShgSavingsAccountId());
                } else {
                    lEntry.setModeOfCash("2");
                }


                LD_EL_Mem_Disburse loan_SB_disbursementFragment = new LD_EL_Mem_Disburse();
                NewDrawerScreen.showFragment(loan_SB_disbursementFragment);

                break;

        }

    }


    private void onShowConfirmationDialog() {
        // TODO Auto-generated method stub
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText((AppStrings.confirmation));
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);
        TextView transactiondate = (TextView) dialogView.findViewById(R.id.transactdate);
        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
        String dateStr = simple.format(d);
        transactiondate.setText(dateStr);

        TableRow increaseLimitRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 5, 10, 5);

        TextView increaseLimitText = new TextView(getActivity());
        increaseLimitText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
        increaseLimitText.setTypeface(LoginActivity.sTypeface);
        increaseLimitText.setTextColor(R.color.white);
        increaseLimitText.setPadding(5, 5, 5, 5);
        increaseLimitText.setLayoutParams(contentParams);
        increaseLimitRow.addView(increaseLimitText);

        TextView increaseLimit_values = new TextView(getActivity());
        increaseLimit_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedType)));
        increaseLimit_values.setTypeface(LoginActivity.sTypeface);
        increaseLimit_values.setTextColor(R.color.white);
        increaseLimit_values.setPadding(5, 5, 5, 5);
        increaseLimit_values.setGravity(Gravity.RIGHT);
        increaseLimit_values.setLayoutParams(contentParams);
        increaseLimitRow.addView(increaseLimit_values);

        confirmationTable.addView(increaseLimitRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        if (selectedType.equals("Bank")) {

            TableRow increaseLimitRow1 = new TableRow(getActivity());

            TextView increaseLimitText1 = new TextView(getActivity());
            increaseLimitText1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
            increaseLimitText1.setTypeface(LoginActivity.sTypeface);
            increaseLimitText1.setTextColor(R.color.white);
            increaseLimitText1.setPadding(5, 5, 5, 5);
            increaseLimitText1.setLayoutParams(contentParams);
            increaseLimitRow1.addView(increaseLimitText1);

            TextView increaseLimit_values1 = new TextView(getActivity());
            increaseLimit_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
            increaseLimit_values1.setTextColor(R.color.white);
            increaseLimit_values1.setPadding(5, 5, 5, 5);
            increaseLimit_values1.setGravity(Gravity.RIGHT);
            increaseLimit_values1.setLayoutParams(contentParams);
            increaseLimitRow1.addView(increaseLimit_values1);

            confirmationTable.addView(increaseLimitRow1,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        TableRow bankChargeRow = new TableRow(getActivity());

        TextView bankChargeText = new TextView(getActivity());
        bankChargeText
                .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mDisbursementAmount)));
        bankChargeText.setTypeface(LoginActivity.sTypeface);
        bankChargeText.setTextColor(R.color.white);
        bankChargeText.setPadding(5, 5, 5, 5);
        bankChargeText.setLayoutParams(contentParams);
        bankChargeRow.addView(bankChargeText);

        TextView bankCharge_values = new TextView(getActivity());
        bankCharge_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(disbursementAmount)));
        bankCharge_values.setTextColor(R.color.white);
        bankCharge_values.setPadding(5, 5, 5, 5);
        bankCharge_values.setGravity(Gravity.RIGHT);
        bankCharge_values.setLayoutParams(contentParams);
        bankChargeRow.addView(bankCharge_values);

        confirmationTable.addView(bankChargeRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TableRow tenureRow = new TableRow(getActivity());

        TextView tenureText = new TextView(getActivity());
        tenureText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mDisbursementDate)));
        tenureText.setTypeface(LoginActivity.sTypeface);
        tenureText.setTextColor(R.color.white);
        tenureText.setPadding(5, 5, 5, 5);
        tenureText.setLayoutParams(contentParams);
        tenureRow.addView(tenureText);

        TextView tenure_values = new TextView(getActivity());
        tenure_values.setText(GetSpanText.getSpanString(getActivity(),
                String.valueOf(disbursementDate)));
        tenure_values.setTextColor(R.color.white);
        tenure_values.setPadding(5, 5, 5, 5);
        tenure_values.setGravity(Gravity.RIGHT);
        tenure_values.setLayoutParams(contentParams);
        tenureRow.addView(tenure_values);

        confirmationTable.addView(tenureRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText((AppStrings.edit));
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText((AppStrings.yes));
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
    }

    private void insertTransaction_tdy()
    {

            offline.setIs_transaction_tdy("1.0");
        offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }


        switch (serviceType) {
            case LD_FROM_LOANAC:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        String message = mrDto.getMessage();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);
                            //init();
                            Utils.showToast(getActivity(), message);


                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            Utils.showToast(getActivity(), message);
                        } else if (statusCode == Utils.Success_Code) {
                            //init();
                            Utils.showToast(getActivity(), message);
                            //  init();
                            insertTransaction_tdy();
                            String samnt = mrDto.getResponseContent().getSansactionAmount();
                            String dAmnt = mrDto.getResponseContent().getDisbursementAmount();
                            String bAmnt = mrDto.getResponseContent().getBalanceAmount();

                            int samnt_value = (int) Double.parseDouble(samnt);
                            int sdAmnt_value = (int) Double.parseDouble(dAmnt);
                            int sbAmnt_value = (int) Double.parseDouble(bAmnt);

                            String sSanctionAmount = String.valueOf(samnt_value);
                            String sDisbursementAmount = String.valueOf(sdAmnt_value);
                            String sBalanceAmount = String.valueOf(sbAmnt_value);

                            mSanctionAmount_value.setText((sSanctionAmount != null && sSanctionAmount.length() > 0) ? sSanctionAmount : "0");
                            mDisbursementAmount_value.setText((sDisbursementAmount != null && sDisbursementAmount.length() > 0) ? sDisbursementAmount : "0");
                            if (mDisbursementAmount_value.getText().toString() != null && mDisbursementAmount_value.getText().toString().length() > 0)
                                MySharedPreference.writeString(getActivity(), MySharedPreference.DB_AMT, mDisbursementAmount_value.getText().toString());
                            else
                                MySharedPreference.writeString(getActivity(), MySharedPreference.DB_AMT, "0");
                            mBalanceAmount_value.setText((sBalanceAmount != null && sBalanceAmount.length() > 0) ? sBalanceAmount : "0");

                        }
                    /*    mSanctionAmount_value.setText("1000");
                        mDisbursementAmount_value.setText("800");
                        mBalanceAmount_value.setText("200");*/

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        Calendar selectionCal = Calendar.getInstance();
        selectionCal.set(year, month, day);
        String formattedDate = df.format(selectionCal.getTime());
        try {
            disburseDate = df.parse(formattedDate);
            disbursementDate = formattedDate + "";
            mDis_DateText.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
