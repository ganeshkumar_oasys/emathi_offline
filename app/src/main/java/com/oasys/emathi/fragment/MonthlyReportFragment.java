package com.oasys.emathi.fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;
import com.tutorialsee.lib.TastyToast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MonthlyReportFragment extends Fragment implements AdapterView.OnItemSelectedListener, NewTaskListener, View.OnClickListener {
    private Spinner yearspinner, monthspinner;
    private Button submit;
    private View view;
    ResponseDto monthlyreportdto;
    private String year_str;
    private  int month_str = 0;
    String month_data[] = {"--Choose Month--", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
    private static FragmentManager fm;
    private Context context;
    private String mMonth, mYear;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView Name;
    private ListOfShg shgDto;
    private String memberid;
    private int sel_year;


    public MonthlyReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_monthly_report, container, false);
        fm = getActivity().getSupportFragmentManager();        // responseContent =new ResponseContent();
        Bundle bundle2 = getArguments();
        memberid = bundle2.getString("Memberid");
        Log.d("Mem3", memberid);
        return view;

    }

    public void init() {
        try {

            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinhand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatbank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            Name = (TextView) view.findViewById(R.id.Name);
            Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));
          //  Name.setTypeface(LoginActivity.sTypeface);

         /*   mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.Memberreports);
            mHeader.setTypeface(LoginActivity.sTypeface);*/
            monthspinner = (Spinner) view.findViewById(R.id.spinner_label_month);
            yearspinner = (Spinner) view.findViewById(R.id.spinner_label_year);
            submit = (Button) view.findViewById(R.id.submit_month_year);
            submit.setTypeface(LoginActivity.sTypeface);
            monthspinner.setOnItemSelectedListener(this);
            submit.setOnClickListener(this);
            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, month_data);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            monthspinner.setAdapter(aa);
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        yearspinner.setOnItemSelectedListener(this);

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
//            RestClient.getRestClient(GroupSavingSummaryFragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUP_SAVINGS_SUMMARY, GroupSavingSummaryFragment.this, ServiceType.GROUP_SUMMARY_SAVINGS, "");

            RestClient.getRestClient(MonthlyReportFragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.YEAR_REPORT + shgDto.getShgId(), getActivity(), ServiceType.REPORT_MONTH);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if (adapterView.getId() == R.id.spinner_label_month) {
            //do this
            month_str = monthspinner.getSelectedItemPosition();
        } else if (adapterView.getId() == R.id.spinner_label_year) {
            //do this
            year_str = yearspinner.getSelectedItem().toString();


        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {


        switch (serviceType) {

            case REPORT_MONTH:
                if (result != null && result.length() > 0) {
                    Log.d("getDetails", " " + result.toString());
                    monthlyreportdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (monthlyreportdto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), monthlyreportdto.getMessage());

                        ArrayList<String> year_list = new ArrayList<>();
                        year_list.add("--Choose Year--");

                        for (int i = 0; i < monthlyreportdto.getResponseContent().getYearList().length; i++) {
                            year_list.add(monthlyreportdto.getResponseContent().getYearList()[i]);
                        }

                        Log.e("values",""+year_list);

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,year_list);
                        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        yearspinner.setAdapter(adapter);

                    }else{
                        if (monthlyreportdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }
                }
                break;


        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_month_year:
                submit();

        }


    }

    private void submit() {

//        if(month_str  null && month_str)
//        {
//            Utils.showToast(context,"PLEASE SELECT THE MONTH ");
//        }
//        else if(year_str !=null && year_str.isEmpty())
//        {
//            Utils.showToast(context,"PLEASE SELECT THE YEAR ");
//        }
//        else
//        {
//            mSubmitLoginDetails(mMonth,mYear);
//            Bundle bundle =new Bundle();
//            bundle.putString("month_key",month_str);
//            bundle.putString("year_key",year_str);
//            MonthlyReportFragment.showFragment(new MonthlyReportDetail());
        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        int cyear = cal.get(Calendar.YEAR); // this is deprecated
        int cmonth = cal.get(Calendar.MONTH); // this is deprecated
        int cday = cal.get(Calendar.DATE);
        try {
             sel_year = Integer.parseInt(year_str);
        }catch (Exception e)
        {
            e.printStackTrace();
        }


           if(month_str == 0)
           {
//               Utils.showToast(context,"PLEASE SELECT THE MONTH ");
               Toast.makeText(getActivity(),"PLEASE SELECT THE MONTH ",Toast.LENGTH_LONG).show();
           }
           else if(year_str !=null && year_str.equals("--Choose Year--"))
           {
//               Utils.showToast(context,"PLEASE SELECT THE YEAR ");
               Toast.makeText(getActivity(),"PLEASE SELECT THE YEAR",Toast.LENGTH_LONG).show();
           }
           else if(cyear==sel_year && month_str>cmonth)
           {
               TastyToast.makeText(getActivity(), "PLEASE SELECT PROPER YEAR AND MONTH",
                       TastyToast.LENGTH_SHORT, TastyToast.WARNING);           }
       else {
//               FragmentTransaction transection = getFragmentManager().beginTransaction();
               MonthlyReportDetail monthlyReportDetail = new MonthlyReportDetail();
               Bundle bundle = new Bundle();
               bundle.putInt("month_key", month_str);
               bundle.putString("year_key", year_str);
               bundle.putString("member_id", memberid);
               monthlyReportDetail.setArguments(bundle); //data being send to SecondFragment
               NewDrawerScreen.showFragment(monthlyReportDetail);

           }
           //        }

    }

//    private void mSubmitLoginDetails(String mMonth,String mYear) {

//        MonthYearDto monthYearDto =new MonthYearDto();
//        monthYearDto.setMonth(mMonth);
//        monthYearDto.setYear(mYear);
//        String nMonthYearSubmit = new Gson().toJson(monthYearDto);
//
//        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
//
//            RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL1 + Constants.MONTH_REPORT, nMonthYearSubmit, getActivity(), ServiceType.MONTH_YEAR_REPORT);
//        } else {
//            Utils.showToast(getActivity(), "Network Not Available");
//        }

//    }

    public static void showFragment(Fragment fragment) {

        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.static_frame, fragment);
        trans.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out);
        trans.show(fragment).commit();
    }
}