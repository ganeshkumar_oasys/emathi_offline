package com.oasys.emathi.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Adapter.CustomItemAdapter;
import com.oasys.emathi.Adapter.RecurringAdapter;
import com.oasys.emathi.Dialogue.Dialog_New_TransactionDate;
import com.oasys.emathi.Dto.AddBTDto;
import com.oasys.emathi.Dto.CashOfGroup;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.ShgBankDetails;
import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.BankTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.model.RowItem;
import com.oasys.emathi.views.MaterialSpinner;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RD_Deposit_Fragment extends Fragment implements View.OnClickListener, NewTaskListener {
    private View view;
    private TextView mAcctoaccFromBankEdittext;
    private EditText mDepositamount,mRateofinterest;
    private MaterialSpinner mLoanAccTransactionspinner;
    private  int size,size1;
    private ArrayList<ShgBankDetails> bankdetails;
    private List<RowItem> bankNameItems;
    RecurringAdapter bankNameAdapter;
    CustomItemAdapter customItemAdapter;
    public static String selectedItemBank;
    public static String mBankNameValue = null;
    private TableData selectedSavinAc;
    String shgacc_val,selectedbankid;
    String bal_val="";
    String bank_val="";
    String branch_val="";
    private Button mAcctoacc_submit;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    Dialog confirmationDialog;
    public static String[] dialogdiaplayitems,dialogdiaplayitemsvalues;
    private NetworkConnection networkConnection;
    private TextView mGroupName, mCashinHand, mCashatBank,mHeader,mAccountnumbervalue,mRecurringamount1;
    private  LinearLayout mRdLayout;
    private ListOfShg shgDto;
    private ShgBankDetails bankDetailsaccountnumbr;
    private ArrayList<TableData> tableDataArrayList;
    private ArrayList<TableData> tableDataArrayList1;
    private ArrayList<String> tableDataArrayListspinner = new ArrayList<>();
    int depositAmount,rateofinterest,depsoitinterest;
    OfflineDto offlineDBData = new OfflineDto();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_r_d__deposit_, container, false);
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        tableDataArrayList = SHGTable.getRecurringData();
        tableDataArrayList1 = SHGTable.getRecurringBank(shgDto.getShgId());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inIt();
    }

    private void inIt() {

        mRdLayout = (LinearLayout) view.findViewById(R.id.RdLayout);
        mRdLayout.setVisibility(View.INVISIBLE);

        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        mHeader = (TextView) view.findViewById(R.id.submenuHeaderTextview);
        mHeader.setVisibility(View.VISIBLE);
        mHeader.setText("" + AppStrings.mNewRdDeposit);
        mHeader.setTypeface(LoginActivity.sTypeface);


        mAccountnumbervalue = (TextView) view.findViewById(R.id.accountnumbervalue);
        mRecurringamount1 = (TextView) view.findViewById(R.id.recurringamount1);


        mAcctoaccFromBankEdittext = (TextView) view.findViewById(R.id.acctoaccfrombank1);
        mAcctoaccFromBankEdittext.setTypeface(LoginActivity.sTypeface);
        mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName());

        mDepositamount = (EditText)view.findViewById(R.id.depositamount);
        mRateofinterest = (EditText)view.findViewById(R.id.installmentamount);

        mLoanAccTransactionspinner = (MaterialSpinner)view.findViewById(R.id.loanAccTransactionspinner);

        mAcctoacc_submit = (Button)view.findViewById(R.id.acctoacc_submit);
        mAcctoacc_submit.setOnClickListener(this);

        final String[] bankNames = new String[tableDataArrayList1.size() + 1];
        //bankNames[0] = String.valueOf(AppStrings.S_B_N);
        bankNames[0] = String.valueOf(AppStrings.selectRDAccount);
        for (int i = 0; i < tableDataArrayList1.size(); i++) {
            bankNames[i + 1] = tableDataArrayList1.get(i).getBankName()+" - "+
                    tableDataArrayList1.get(i).getAccountNo();
            Log.i("print",tableDataArrayList1.get(i).getAccountNo());
            System.out.println("-----------------------" + bankNames[i + 1]);
        }


        final String[] bankNames_Id = new String[tableDataArrayList1.size() + 1];
        //bankNames_Id[0] = String.valueOf(AppStrings.S_B_N);
        bankNames_Id[0] = String.valueOf(AppStrings.selectRDAccount);
        for (int i = 0; i < tableDataArrayList1.size(); i++) {
                bankNames_Id[i + 1] = tableDataArrayList1.get(i).getBankId();
                System.out.println("-----------------------" + bankNames_Id[i + 1]);
            }

        size1 = bankNames.length;
        bankNameItems = new ArrayList<RowItem>();
        for (int i = 0; i < size1; i++) {
            RowItem rowItem = new RowItem(bankNames[i]);
            bankNameItems.add(rowItem);
        }

        customItemAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
        mLoanAccTransactionspinner.setAdapter(customItemAdapter);




        mLoanAccTransactionspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 0) {
                    selectedItemBank = bankNames[0];
                    shgacc_val=null;
                    mRdLayout.setVisibility(View.INVISIBLE);

                }else
                {
                    selectedItemBank = bankNames[position];
                    selectedSavinAc = tableDataArrayList1.get(position - 1);
                    shgacc_val = selectedSavinAc.getAccountNo();
                    selectedbankid = selectedSavinAc.getShgSavingsAccountId();
                    bal_val=selectedSavinAc.getCurrent_balance();
                    bank_val=selectedSavinAc.getBankName();
                    bank_val=selectedSavinAc.getBranchName();
                    System.out.println("SELECTED BANK NAME : " + selectedItemBank);


                    mRdLayout.setVisibility(View.VISIBLE);

                    if(bal_val!=null)
                    {
                        mRecurringamount1.setText(bal_val);
                    }
                    else{
                        mRecurringamount1.setText("0.0");
                    }

                    mRecurringamount1.setTypeface(LoginActivity.sTypeface);

                    mAccountnumbervalue.setText(selectedSavinAc.getAccountNo());
                    mAccountnumbervalue.setTypeface(LoginActivity.sTypeface);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.acctoacc_submit:
                int bankBalance=(int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance());

                if(mDepositamount.getText().toString().isEmpty() || mRateofinterest.getText().toString().isEmpty()) {

                    TastyToast.makeText(getActivity(), "Please provide the Cash Details", TastyToast.LENGTH_LONG, TastyToast.WARNING);
                }else {
                    depositAmount = Integer.parseInt(mDepositamount.getText().toString());
                    rateofinterest = Integer.parseInt(mRateofinterest.getText().toString());
                    depsoitinterest = depositAmount+rateofinterest;
                    if (bankBalance > depsoitinterest) {
                        confirmationDialog();
                    } else {
                        TastyToast.makeText(getActivity(), "Check your Bank Balance", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                    }
                }

                break;
            case R.id.fragment_Edit:
                confirmationDialog.dismiss();
                break;
            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                requestdataFetching();
                break;
        }
    }


    private void requestdataFetching() {

        try {
            int cab = 0, cb = 0, recurringbalance = 0;
            int cih = 0;
            cb = (((int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance())) - depositAmount);
            cab = (((int) Double.parseDouble(shgDto.getCashAtBank())) - depositAmount);
            cih = (int) Double.parseDouble(shgDto.getCashInHand());
            if(bal_val==null)
            {
                bal_val="0.0";
            }
            recurringbalance = (int) Double.parseDouble(bal_val) + depsoitinterest;
            OfflineDto offline = new OfflineDto();
            offline.setCashInhand(cih + "");
            offline.setCashAtBank(cab + "");
            offline.setCurr_balance(String.valueOf(cb));
            offline.setShgSavingsAccountId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
            offline.setSavingsBankId(selectedbankid);
            offline.setCurrent_balance(String.valueOf(recurringbalance));
            offline.setRecurringbankBalanceId(selectedbankid);
            offline.setAccountNumber(shgacc_val);
            offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
            offline.setIs_transaction_tdy("1.0");
            offline.setTxType(AppStrings.bankTransaction);
            offline.setTxSubtype(AppStrings.recurringDeposit);
            offline.setShgId(shgDto.getShgId());
            offline.setBtDeposit(String.valueOf(depositAmount));
            offline.setBtInterest(String.valueOf(rateofinterest));
            //offline.setFromSavingsBankDetailsId(bankDetailsaccountnumbr.getShgSavingsAccountId());
            //offline.setFromSavingsBankDetailsId("12345");
            offline.setBtFromSavingAcId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
            //offline.setSavingsBankDetailsId("");
            //offline.setFromSavingsBankDetailsId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
            offlineDBData = offline;

            AddBTDto bts = new AddBTDto();
            bts.setBankInterest(mRateofinterest.getText().toString());
            bts.setCashDeposit(mDepositamount.getText().toString());
            bts.setMobileDate(System.currentTimeMillis() + "");
            bts.setSavingsBankId(selectedbankid);
            bts.setFromBankId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
            bts.setShgId(shgDto.getShgId());
            bts.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
            bts.setTransactionFlowType(0);

            String sreqString = new Gson().toJson(bts);
            if (networkConnection.isNetworkAvailable()) {

                RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.BT_RD, sreqString, getActivity(), ServiceType.BT_RD);
            } else {
                if (TransactionTable.getLoginFlag(AppStrings.recurringDeposit).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.recurringDeposit).get(TransactionTable.getLoginFlag(AppStrings.recurringDeposit).size() - 1).getLoginFlag().equals("1")))
                    insertBT();
                else
                    TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);

            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void updateBKCB() {
        if (offlineDBData.getCurr_balance() != null || offlineDBData.getCurr_balance().length() > 0) {
            SHGTable.updateBankCurrentBalanceDetails(offlineDBData);
        }
    }

    private void updaterecurring_bal() {
        if (offlineDBData.getCurrent_balance() != null || offlineDBData.getCurrent_balance().length() > 0) {
            SHGTable.updateBankrecurringCurrentBalanceDetails(offlineDBData);
        }

        }

    private void insertTransaction_tdy() {
        offlineDBData.setIs_transaction_tdy("1.0");
        offlineDBData.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offlineDBData);
    }

    private void updateCIH() {
        String cihstr = "", cabstr = "", lastTranstr = "";
        cihstr = offlineDBData.getCashInhand();
        cabstr = offlineDBData.getCashAtBank();
        lastTranstr = offlineDBData.getLastTransactionDateTime();
        CashOfGroup csg = new CashOfGroup();
        csg.setCashInHand(cihstr);
        csg.setCashAtBank(cabstr);
        csg.setLastTransactionDate(lastTranstr);
        SHGTable.updateSHGDetails(csg, shgDto.getId());
    }

    private void insertBT() {
        try {

            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.BT_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.BT_COUNT, value);

            offlineDBData.setBtCount(value + "");
            TransactionTable.insertTransBTData(offlineDBData);


            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }
            updateBKCB();
            updaterecurring_bal();
            updateCIH();
            insertTransaction_tdy();

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void confirmationDialog() {
        confirmationDialog = new Dialog(getActivity());
        dialogdiaplayitems = new String[]{AppStrings.Accountnumber,AppStrings.depositamount,AppStrings.Rateofinterest};
        dialogdiaplayitemsvalues = new String[]{shgacc_val,mDepositamount.getText().toString(),mRateofinterest.getText().toString()};
        size = dialogdiaplayitems.length;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);
        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
        String dateStr = simple.format(d);
        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
        transactdate.setText(dateStr);

        for (int i = 0; i < size; i++) {

            TableRow indv_DepositEntryRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(
                    GetSpanText.getSpanString(getActivity(), String.valueOf(dialogdiaplayitems[i])));
            memberName_Text.setTypeface(LoginActivity.sTypeface);
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_DepositEntryRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(
                    GetSpanText.getSpanString(getActivity(), String.valueOf(dialogdiaplayitemsvalues[i])));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_DepositEntryRow.addView(confirm_values);

            confirmationTable.addView(indv_DepositEntryRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();


    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType)
        {
            case BT_RD:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        confirmationDialog.dismiss();
                        Utils.showToast(getActivity(), message);
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        updateBKCB();
                        updaterecurring_bal();
                        updateCIH();
                        insertTransaction_tdy();
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Transaction", MainFragment.Flag_Transaction);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);
                    }
                } catch (Exception e) {

                }
                break;
        }
    }
}