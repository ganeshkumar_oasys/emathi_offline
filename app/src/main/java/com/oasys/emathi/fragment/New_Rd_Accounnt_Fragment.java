package com.oasys.emathi.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Dialogue.Dialog_New_TransactionDate;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.RequestDto.New_Rd_Accountdto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.AlphaNumericInputFilter;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static com.oasys.emathi.OasysUtils.Constants.NEW_RD_ACCOUNTCREATION;
import static com.oasys.emathi.database.SHGTable.insertrecuringData;
//import static com.oasys.eshakti.database.SHGTable.updateRecuuringData;
import static com.oasys.emathi.fragment.BankTransaction.sSelectedBankid;
import static com.oasys.emathi.fragment.BankTransaction.sSelectedBranchid;
import static com.oasys.emathi.fragment.BankTransaction.sSelectedBranchifsc;
import static com.oasys.emathi.fragment.BankTransaction.sSelectedBranchname;


public class New_Rd_Accounnt_Fragment extends Fragment implements View.OnClickListener, NewTaskListener {

    private TextView mAcctoaccFromBankEdittext,mBranchname;
    private EditText mAccountnumber,mInstallmentamount,mRateofinterest,mTenure;
    private Button mAcctoacc_submit;
    private View view;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    Dialog confirmationDialog;
    public static String[] sNewrdaccountItem;
    public static String[] sNewrdaccountItemvalues;
    int size;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    private TextView mGroupName, mCashinHand, mCashatBank,mHeader;
    OfflineDto offline;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_new__rd__accounnt_, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        sNewrdaccountItem = new String[]{AppStrings.bankaccountnumber,AppStrings.installmentamount,AppStrings.rateofinterest,AppStrings.tenure};
        inIt();
    }

    private void inIt() {

        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        mHeader = (TextView) view.findViewById(R.id.submenuHeaderTextview);
        mHeader.setVisibility(View.VISIBLE);
        mHeader.setText("" + AppStrings.mNewRdAccount_Creation);
        mHeader.setTypeface(LoginActivity.sTypeface);

        mAcctoaccFromBankEdittext = (TextView) view.findViewById(R.id.acctoaccfrombank);
        mAcctoaccFromBankEdittext.setTypeface(LoginActivity.sTypeface);
        mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName());

        mBranchname = (TextView) view.findViewById(R.id.branchname);
        mBranchname.setTypeface(LoginActivity.sTypeface);
        mBranchname.setText(sSelectedBranchname);

        mAccountnumber = (EditText)view.findViewById(R.id.accountnumber);
        mInstallmentamount = (EditText)view.findViewById(R.id.installmentamount);
        mRateofinterest = (EditText)view.findViewById(R.id.rateofinterest);
        mTenure = (EditText)view.findViewById(R.id.tenure);

        mAcctoacc_submit = (Button)view.findViewById(R.id.acctoacc_submit);
        mAcctoacc_submit.setOnClickListener(this);

        mAccountnumber.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        ArrayList<InputFilter> curInputFilters = new ArrayList<InputFilter>(Arrays.asList(mAccountnumber.getFilters()));
        curInputFilters.add(0, new AlphaNumericInputFilter());
        curInputFilters.add(1, new InputFilter.LengthFilter(20));
        InputFilter[] newInputFilters = curInputFilters.toArray(new InputFilter[curInputFilters.size()]);
        mAccountnumber.setFilters(newInputFilters);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.acctoacc_submit:
                confirmationDialog();
                break;
            case R.id.fragment_Edit:
                confirmationDialog.dismiss();
                break;
            case R.id.frag_Ok:
                requestdataFetching();
                break;
        }

    }

    private void confirmationDialog() {
        confirmationDialog = new Dialog(getActivity());
        sNewrdaccountItemvalues = new String[]{mAccountnumber.getText().toString(),mInstallmentamount.getText().toString(),mRateofinterest.getText().toString(),mTenure.getText().toString()};
        size = sNewrdaccountItem.length;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);
        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
        String dateStr = simple.format(d);
        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
        transactdate.setText(dateStr);

        for (int i = 0; i < size; i++) {

            TableRow indv_DepositEntryRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(
                    GetSpanText.getSpanString(getActivity(), String.valueOf(sNewrdaccountItem[i])));
            memberName_Text.setTypeface(LoginActivity.sTypeface);
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_DepositEntryRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(
                    GetSpanText.getSpanString(getActivity(), String.valueOf(sNewrdaccountItemvalues[i])));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_DepositEntryRow.addView(confirm_values);

            confirmationTable.addView(indv_DepositEntryRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();


    }

    private void requestdataFetching() {

        /*offline =new OfflineDto();
        offline.setShgId(shgDto.getShgId());
        offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
        offline.setShgSavingsAccountId(sSelectedBank.getShgSavingsAccountId());
        offline.setBankName(BankTransaction.sSelectedBank.getBankName());
        offline.setBranchName(sSelectedBranchname);
        offline.setAccountNumber(mAccountnumber.getText().toString());
        offline.setBankId(sSelectedBankid);
        offline.setIfsc(sSelectedBranchifsc);
        offline.setBranchid(sSelectedBranchid);
        offline.setInstallmentamount(mInstallmentamount.getText().toString());
        offline.setRateofinterest(mRateofinterest.getText().toString());
        offline.setTenure(mTenure.getText().toString());
        offline.setStatus("1.0");
*/
        New_Rd_Accountdto new_rd_accountdto =new New_Rd_Accountdto();
        new_rd_accountdto.setInterestRate(mRateofinterest.getText().toString());
        new_rd_accountdto.setInstallmentAmount(mInstallmentamount.getText().toString());
        new_rd_accountdto.setLoanAccountNumber(mAccountnumber.getText().toString());
        new_rd_accountdto.setShgId(shgDto.getShgId());
        new_rd_accountdto.setBranchName(sSelectedBranchid);
        new_rd_accountdto.setBankName(sSelectedBankid);
        new_rd_accountdto.setIfsc(sSelectedBranchifsc);
        new_rd_accountdto.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
        new_rd_accountdto.setTenure(mTenure.getText().toString());

        String sreqString = new Gson().toJson(new_rd_accountdto);

        if (networkConnection.isNetworkAvailable()) {
            RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + NEW_RD_ACCOUNTCREATION, sreqString, getActivity(), ServiceType.NEW_RD_ACCOUNTCREATION);
        }

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case NEW_RD_ACCOUNTCREATION:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        confirmationDialog.dismiss();

                        offline =new OfflineDto();
                        offline.setShgId(cdto.getResponseContent().getShgId().getId());
                        offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                        offline.setShgSavingsAccountId(cdto.getResponseContent().getSavingsbankId().getId());
                        offline.setBankName(BankTransaction.sSelectedBank.getBankName());
                        offline.setBranchName(sSelectedBranchname);
                        offline.setAccountNumber(cdto.getResponseContent().getSavingsbankId().getBankId().getAccountNumber());
                        offline.setBankId(cdto.getResponseContent().getSavingsbankId().getBankId().getBankNameId());
                        offline.setIfsc(cdto.getResponseContent().getSavingsbankId().getBankId().getIfsc());
                        offline.setBranchid(cdto.getResponseContent().getSavingsbankId().getBankId().getBranchNameId());
                        offline.setInstallmentamount(cdto.getResponseContent().getInstallmentAmount());
                        offline.setRateofinterest(cdto.getResponseContent().getInterest());
                        offline.setTenure(cdto.getResponseContent().getTenure());

                        insertrecuringData(offline);
                        MainFragment fragment = new MainFragment();
                        NewDrawerScreen.showFragment(fragment);

                    } else {


                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;
        }
    }
}