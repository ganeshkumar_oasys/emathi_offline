package com.oasys.emathi.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.emathi.Adapter.CustomItemAdapter;
import com.oasys.emathi.Dialogue.Dialog_New_TransactionDate;
import com.oasys.emathi.Dto.AddBTDto;
import com.oasys.emathi.Dto.CashOfGroup;
import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.ShgBankDetails;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.BankTable;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.model.RowItem;
import com.oasys.emathi.views.MaterialSpinner;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.OasysUtils.AppDialogUtils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dell on 15 Dec, 2018.
 */

public class BTActoAcTransfer extends Fragment implements NewTaskListener, View.OnClickListener {


    private TextView mGroupName, mCashInHand, mCashAtBank;
    View rootView;
    private Dialog mProgressDilaog;
    TextView mAcctoaccheader, mAcctoaccFrombank, mAcctoaccFromBankEdittext, mAcctoaccTransferAmountText,
            mAcctoaccTransferChargesText;
    EditText mAcctoacctransferamount, mAcctoacctransfercharges;
    RaisedButton mSubmitButton;
    MaterialSpinner mSpinner_tobank, mSpinner_loanAcc;
    CustomItemAdapter bankNameAdapter, loanAccAdapter;

    private List<RowItem> bankNameItems, loanAccItems;
    public static String selectedItemBank, selectedLoanAcc, selectedLoanId;
    public static String mBankNameValue = null, mBankName = "", mTempBankId = "", mLoanAccValue = null,
            mLoanAccIdValue = null;
    public static String mAcctransferAmount, mAcctransferCharge;
    private Dialog confirmationDialog;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();

    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();

    String mLastTrDate = null, mLastTr_ID = null;

    public static int mFromBankAmount = 0, mToBankAmount = 0;
    boolean isGetTrid = false;
    private LinearLayout mSavingsAccLayout, mLoanAccLayout;
    private RadioButton mSavingsAccRadio, mLoanAccRadio;
    private RadioGroup mRadioGroup;
    private TextView mLoanOutstandingTextView, mBankNameTextView, mLoanAccNoTextView, mAccNoTextView;
    private TextView mLoanOutstanding_value_TextView, mBankName_value_TextView, mLoanAccNo_value_TextView,
            mAccNo_ValeTextView;
    private String[] mLoanTypeArray, mLoanIdArray;
    boolean isLoanAccount = false;
    boolean isSavingAccount = false;
    boolean isOfflineLoanAcc = false;
    boolean isLoanAccDetails = false;
    String selectedRadio = "";
    String ToLoanAccNo = "", outstandingAmt = "";
    ArrayList<String> mLoanBankName = new ArrayList<String>();
    ArrayList<String> mLoanName = new ArrayList<String>();
    ArrayList<String> mLoanId = new ArrayList<String>();
    ArrayList<String> mLoanOutstanding = new ArrayList<String>();
    ArrayList<String> mLoanAccNo = new ArrayList<String>();
    boolean isServiceCall = false;
    boolean isOfflineEntry = false;

    int mCount = 0;

    String mSqliteDBStoredValues_acc_toacc_Values = null;
    Date date_dashboard, date_loanDisb;
    OfflineDto offlineDBData = new OfflineDto();

    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private ArrayList<ShgBankDetails> bankdetails, bankSortedList;
    private List<ExistingLoan> loanDetails;
    private ExistingLoan selectedLoantype = null;
    private ExistingLoan selectedLoan;
    private ShgBankDetails selectedSavinAc;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_acc_to_acc_transfer, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        loanDetails = LoanTable.getGrpLoanDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
    /*    if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(BTActoAcTransfer.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_PLOAN + shgDto.getShgId(), getActivity(), ServiceType.LOANTYPE);
        }*/
        init();
    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);
            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);
            mAcctoaccheader = (TextView) rootView.findViewById(R.id.acctoaccheader);
            mAcctoaccheader.setTypeface(LoginActivity.sTypeface);
            mAcctoaccFrombank = (TextView) rootView.findViewById(R.id.acctoaccfrombanktext);
            mAcctoaccFrombank.setTypeface(LoginActivity.sTypeface);
            mAcctoaccFromBankEdittext = (TextView) rootView.findViewById(R.id.acctoaccfrombank);
            mAcctoaccFromBankEdittext.setTypeface(LoginActivity.sTypeface);
            mAcctoacctransferamount = (EditText) rootView.findViewById(R.id.acctoacctransferamount);
            mAcctoacctransfercharges = (EditText) rootView.findViewById(R.id.acctoacctransfercharges);
            mSubmitButton = (RaisedButton) rootView.findViewById(R.id.acctoacc_submit);
            mSpinner_tobank = (MaterialSpinner) rootView.findViewById(R.id.acctoacctobankspinner);
            mAcctoaccTransferAmountText = (TextView) rootView.findViewById(R.id.acctoacctransferamountTextView);
            mAcctoaccTransferAmountText.setTypeface(LoginActivity.sTypeface);
            mAcctoaccTransferChargesText = (TextView) rootView.findViewById(R.id.acctoacctransferchargesTextView);
            mAcctoaccTransferChargesText.setTypeface(LoginActivity.sTypeface);

            mAccNoTextView = (TextView) rootView.findViewById(R.id.accNotext);
            mAccNoTextView.setText(AppStrings.mAccountNumber);
            mAccNoTextView.setTypeface(LoginActivity.sTypeface);

            mAccNo_ValeTextView = (TextView) rootView.findViewById(R.id.accNo_value);
            mAccNo_ValeTextView.setText(BankTransaction.sSelectedBank.getAccountNumber());
            mAccNo_ValeTextView.setTypeface(LoginActivity.sTypeface);
            // if (ConnectionUtils.isNetworkAvailable(getActivity())) {

            // }

            mSavingsAccLayout = (LinearLayout) rootView.findViewById(R.id.acctoacctobankspinnerlayout);
            mLoanAccLayout = (LinearLayout) rootView.findViewById(R.id.loanAccTransactionspinnerLayout);

            mRadioGroup = (RadioGroup) rootView.findViewById(R.id.radio_loanAccTransaction);
            mSavingsAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_SavingsAcc);
            mLoanAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_LoanAcc);


            mLoanOutstandingTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstandingTextView);
            mLoanOutstandingTextView.setTypeface(LoginActivity.sTypeface);
            mLoanOutstanding_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstanding_value);
            mLoanOutstanding_value_TextView.setTypeface(LoginActivity.sTypeface);

            mBankNameTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankNameTextView);
            mBankNameTextView.setTypeface(LoginActivity.sTypeface);
            mBankName_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankName_value);
            mBankName_value_TextView.setTypeface(LoginActivity.sTypeface);

            mLoanAccNoTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNoTextView);
            mLoanAccNoTextView.setTypeface(LoginActivity.sTypeface);
            mLoanAccNo_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNo_value);
            mLoanAccNo_value_TextView.setTypeface(LoginActivity.sTypeface);
            mSubmitButton.setText(AppStrings.submit);
            mSubmitButton.setTypeface(LoginActivity.sTypeface);
            mAcctoaccheader.setText(AppStrings.mAccountToAccountTransfer);
            mAcctoaccFrombank.setText(AppStrings.mTransferFromBank);
            // mAcctoacctransferamount.setHint(AppStrings.mTransferAmount);
            // mAcctoacctransfercharges.setHint(AppStrings.mTransferCharges);

            // mAcctoaccFromBankEdittext.setEnabled(false);
            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName() + "  :  "
                    + String.valueOf(BankTransaction.sSelectedBank.getCurrentBalance()));

            mAcctoaccTransferAmountText.setText(AppStrings.mTransferAmount);
            mAcctoaccTransferChargesText.setText(AppStrings.mTransferCharges);

            mSavingsAccRadio.setText(AppStrings.mSavingsAccount);
            mSavingsAccRadio.setTypeface(LoginActivity.sTypeface);

            mLoanAccRadio.setText(AppStrings.mLoanaccHeader);
            mLoanAccRadio.setTypeface(LoginActivity.sTypeface);

            mLoanOutstandingTextView.setText(AppStrings.outstanding);
            mBankNameTextView.setText(AppStrings.bankName);
            mLoanAccNoTextView.setText(AppStrings.mAccountNumber);

            mSpinner_loanAcc = (MaterialSpinner) rootView.findViewById(R.id.loanAccTransactionspinner);
            mSpinner_loanAcc.setBaseColor(R.color.grey_400);
            mSpinner_loanAcc.setFloatingLabelText(AppStrings.mLoanAccType);
            mSpinner_loanAcc.setPaddingSafe(10, 0, 10, 0);


            mSpinner_tobank.setFloatingLabelText(AppStrings.mTransferSpinnerFloating);

            mSpinner_tobank.setPaddingSafe(10, 0, 10, 0);

            bankSortedList = new ArrayList<>();
            for (ShgBankDetails details : bankdetails) {
                if (details.getShgSavingsAccountId() != null && !(details.getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId())))
                    bankSortedList.add(details);
            }


            for (int i = 0; i < bankSortedList.size(); i++) {
                mBankName = mBankName + bankSortedList.get(i).getBankName().toString() + "~";

            }

            for (int i = 0; i < bankSortedList.size(); i++) {

                mTempBankId = mTempBankId + bankSortedList.get(i).getBankName().toString() + "~";
            }

            Log.e("Temp Bank ID  Values_>", mTempBankId);

            for (int i = 0; i < bankSortedList.size(); i++) {
                mBanknames_Array.add(bankSortedList.get(i).getBankName());
                mBanknamesId_Array.add(bankSortedList.get(i).getBankId());
            }

            for (int i = 0; i < bankSortedList.size(); i++) {
                mEngSendtoServerBank_Array.add(bankSortedList.get(i).getBankName());
                mEngSendtoServerBankId_Array.add(bankSortedList.get(i).getBankId());
            }


            final String[] bankNames = new String[bankSortedList.size() + 1];
            bankNames[0] = String.valueOf(AppStrings.S_B_N);
            for (int i = 0; i < bankSortedList.size(); i++) {
                //if (!bankdetails.get(i).getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId()))
                bankNames[i + 1] = bankSortedList.get(i).getBankName().toString();
                System.out.println("-----------------------" + bankNames[i + 1]);
            }

            final String[] bankNames_Id = new String[bankSortedList.size() + 1];
            bankNames_Id[0] = String.valueOf(AppStrings.S_B_N);
            for (int i = 0; i < bankSortedList.size(); i++) {
                //  if (!bankdetails.get(i).getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId()))
                bankNames_Id[i + 1] = bankSortedList.get(i).getBankId().toString();
                System.out.println("-----------------------" + bankNames_Id[i + 1]);
            }

            if (bankSortedList.size() < 1) {
                mSavingsAccRadio.setClickable(false);
            } else {
                mSavingsAccRadio.setClickable(true);
            }

            int size = bankNames.length;
            bankNameItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);// sBankNames.elementAt(i).toString());
                bankNameItems.add(rowItem);
            }
            bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
            mSpinner_tobank.setAdapter(bankNameAdapter);

            mSpinner_tobank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_Id[0];
                        mBankNameValue = "0";

                    } else {
                        selectedItemBank = bankNames_Id[position];
                        selectedSavinAc = bankSortedList.get(position - 1);

                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        String mBankname = null;
                        for (int i = 0; i < mBanknames_Array.size(); i++) {
                            if (selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                                mBankname = mEngSendtoServerBank_Array.get(i);
                            }
                        }
                        mBankNameValue = mBankname;
                        Log.e("Bank N Name--->>>", mBankNameValue);


                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });
            mSubmitButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    System.out.println("------  " + selectedRadio.toString());
                    // TODO Auto-generated method stub

                    mAcctransferAmount = "0";
                    mAcctransferCharge = "0";

                    mAcctransferAmount = mAcctoacctransferamount.getText().toString().trim();
                    mAcctransferCharge = mAcctoacctransfercharges.getText().toString().trim();

                    if (mAcctransferAmount.equals("")) {
                        mAcctransferAmount = "0";
                    }
                    if (mAcctransferCharge.equals("")) {
                        mAcctransferCharge = "0";
                    }

                    int totalAmount = Integer.parseInt(mAcctransferAmount) + Integer.parseInt(mAcctransferCharge);

                    if (!mAcctransferAmount.isEmpty() && !mAcctransferAmount.equals("0")) {

                        if (!selectedRadio.equals("")) {

                            Log.e("Selected Radio Values ", selectedRadio);

                            if (selectedRadio.equals("LOANACCOUNT")) {

                                // String dashBoardDate = DatePickerDialog.sDashboardDate;

                                if (selectedLoan != null && selectedLoan.getLoanId() != null) {
                                    if (totalAmount <= (int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance())) {

                                        onShowConfirmationDialog();
                                    } else {
                                        TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                    }
                                } else {
                                    TastyToast.makeText(getActivity(), AppStrings.mLoanTypeAlert,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }

                            } else {
                                if ((!mBankNameValue.equals("0") && mBankNameValue != null)) {
                                    String result = shgDto.getCashAtBank().replace("\u20B9", "");
                                    if (totalAmount <= (int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance())) {

                                        onShowConfirmationDialog();

                                    } else {
                                        TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                        mAcctransferAmount = "0";
                                        mAcctransferCharge = "0";
                                    }
                                } else {

                                    TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                                }
                            }

                        } else {

                            if (mBankNameValue != null && mBankNameValue.equals("0")) {
                                TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mTransferNullToast,
                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                            }
                            mAcctransferAmount = "0";
                            mAcctransferCharge = "0";
                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.mTransferNullToast, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                        mAcctransferAmount = "";
                        mAcctransferCharge = "";
                    }
                }
            });

            mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // TODO Auto-generated method stub

                    try {
                        int selectedId = group.getCheckedRadioButtonId();
                        mAcctoacctransferamount.setText("");
                        mAcctoacctransfercharges.setText("");
                        if (selectedId == R.id.radioloanaccTransaction_SavingsAcc) {
                            System.out.println("-------Savings_acc----");
                            selectedRadio = "SAVINGSACCOUNT";

                            if (bankdetails.size() <= 1) {
                                TastyToast.makeText(getActivity(), AppStrings.mAccToAccTransferToast, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                                mSubmitButton.setClickable(false);

                            } else {
                                mSavingsAccLayout.setVisibility(View.VISIBLE);
                                mLoanAccLayout.setVisibility(View.GONE);
                                mSubmitButton.setClickable(true);
                            }
                        } else if (selectedId == R.id.radioloanaccTransaction_LoanAcc) {
                            System.out.println("-------Loan_acc----");

                            if (loanDetails.size() >= 1) {
                                selectedRadio = "LOANACCOUNT";
                                mLoanAccLayout.setVisibility(View.VISIBLE);
                                mSavingsAccLayout.setVisibility(View.GONE);

                                mLoanTypeArray = new String[loanDetails.size() + 1];
                                mLoanTypeArray[0] = String.valueOf(AppStrings.mLoanAccType);

                                mLoanIdArray = new String[loanDetails.size() + 1];
                                for (int i = 0; i < loanDetails.size(); i++) {
                                    if (loanDetails.get(i).getLoanTypeName() != null && loanDetails.get(i).getLoanTypeName().length() > 0) {
                                        mLoanTypeArray[i + 1] = loanDetails.get(i).getLoanTypeName().toString();
                                        mLoanIdArray[i] = loanDetails.get(i).getLoanId().toString();
                                    }
                                }

                                loanAccItems = new ArrayList<RowItem>();
                                for (int i = 0; i < loanDetails.size() + 1; i++) {
                                    RowItem rowItem = new RowItem(mLoanTypeArray[i]);
                                    loanAccItems.add(rowItem);
                                }
                                loanAccAdapter = new CustomItemAdapter(getActivity(), loanAccItems);
                                mSpinner_loanAcc.setAdapter(loanAccAdapter);
                                mSpinner_loanAcc.setOnItemSelectedListener(new OnItemSelectedListener() {

                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        // TODO Auto-generated method stub

                                        if (position == 0) {
                                            selectedLoanAcc = mLoanTypeArray[0];
                                            mLoanAccValue = "0";
                                            mLoanAccIdValue = "0";

                                            mLoanOutstanding_value_TextView.setText("");
                                            mBankName_value_TextView.setText("");
                                            mLoanAccNo_value_TextView.setText("");


                                        } else {
                                            selectedLoan = loanDetails.get(position - 1);

                                            if (loanDetails.get(position - 1).getLoanTypeName() != null) {
                                                selectedLoantype = loanDetails.get(position - 1);
                                                mLoanOutstanding_value_TextView.setText(loanDetails.get(position - 1).getLoanOutstanding());

                                                mBankName_value_TextView.setText(loanDetails.get(position - 1).getBankName());
                                                mLoanAccNo_value_TextView.setText(loanDetails.get(position - 1).getAccountNumber());
                                                ToLoanAccNo = loanDetails.get(position - 1).getAccountNumber();
                                            }
                                            // String dashBoardDate = DatePickerDialog.sDashboardDate;

                                        }
                                        selectedLoanAcc = mLoanTypeArray[position];
                                        mLoanAccValue = selectedLoanAcc;

                                        Log.e("Loan Name", mLoanAccValue + "");
                                        Log.e("Loan Id", mLoanAccIdValue + "");
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                        // TODO Auto-generated method stub

                                    }
                                });
                            } else {

                                TastyToast.makeText(getActivity(), AppStrings.noGroupLoan_Alert, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });


        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    private void onShowConfirmationDialog() {
        // TODO Auto-generated method stub
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(
                new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText("" + (AppStrings.confirmation));
        confirmationHeader.setTypeface(LoginActivity.sTypeface);
        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
        String dateStr = simple.format(d);
        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
        transactdate.setText(dateStr);

        TableRow typeRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 5, 10, 5);

        TextView memberName_Text = new TextView(getActivity());
        memberName_Text.setText(
                GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank) + " " + "SB"));
        memberName_Text.setTypeface(LoginActivity.sTypeface);
        memberName_Text.setPadding(5, 5, 5, 5);
        memberName_Text.setLayoutParams(contentParams);
        typeRow.addView(memberName_Text);

        TextView confirm_values = new TextView(getActivity());
        confirm_values.setText(
                GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getBankName())));
        confirm_values.setPadding(5, 5, 5, 5);
        confirm_values.setGravity(Gravity.RIGHT);
        confirm_values.setLayoutParams(contentParams);
        typeRow.addView(confirm_values);

        confirmationTable.addView(typeRow,
                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        // if (ConnectionUtils.isNetworkAvailable(getActivity())) {
        TableRow sbAccNoRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams sbAccParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        sbAccParams.setMargins(10, 5, 10, 5);

        TextView accNo_Text = new TextView(getActivity());
        accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
        accNo_Text.setTypeface(LoginActivity.sTypeface);
        accNo_Text.setPadding(5, 5, 5, 5);
        accNo_Text.setLayoutParams(sbAccParams);
        sbAccNoRow.addView(accNo_Text);

        TextView accNo_values = new TextView(getActivity());
//        accNo_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf((selectedSavinAc != null && selectedSavinAc.getAccountNumber() != null && selectedSavinAc.getAccountNumber().length() > 0) ? selectedSavinAc.getAccountNumber() : "NA")));
        accNo_values.setText(GetSpanText.getSpanString(getActivity(), BankTransaction.sSelectedBank.getAccountNumber()));
        accNo_values.setPadding(5, 5, 5, 5);
        accNo_values.setGravity(Gravity.RIGHT);
        accNo_values.setLayoutParams(sbAccParams);
        sbAccNoRow.addView(accNo_values);

        confirmationTable.addView(sbAccNoRow,
                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        // }

        TableRow bankNameRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        bankNameParams.setMargins(10, 5, 10, 5);

        TextView bankName_Text = new TextView(getActivity());
        if (selectedRadio.equals("SAVINGSACCOUNT")) {
            bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
            bankName_Text.setTypeface(LoginActivity.sTypeface);
        } else {
            bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
            bankName_Text.setTypeface(LoginActivity.sTypeface);
        }
        bankName_Text.setPadding(5, 5, 5, 5);
        bankName_Text.setLayoutParams(bankNameParams);
        bankNameRow.addView(bankName_Text);

        TextView bankName_values = new TextView(getActivity());
        if (selectedRadio.equals("SAVINGSACCOUNT")) {
            bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedSavinAc.getBankName())));
        } else {
            bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mLoanAccValue)));
        }

        bankName_values.setPadding(5, 5, 5, 5);
        bankName_values.setGravity(Gravity.RIGHT);
        bankName_values.setLayoutParams(bankNameParams);
        bankNameRow.addView(bankName_values);

        confirmationTable.addView(bankNameRow,
                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        if (!selectedRadio.equals("SAVINGSACCOUNT")) {
            TableRow sbAccNoRow1 = new TableRow(getActivity());

            @SuppressWarnings("deprecation")
            TableRow.LayoutParams sbAccParams1 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT, 1f);
            sbAccParams1.setMargins(10, 5, 10, 5);

            TextView accNo_Text1 = new TextView(getActivity());
            accNo_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
            accNo_Text1.setTypeface(LoginActivity.sTypeface);
            accNo_Text1.setPadding(5, 5, 5, 5);
            accNo_Text1.setLayoutParams(sbAccParams1);
            sbAccNoRow1.addView(accNo_Text1);

            TextView accNo_values1 = new TextView(getActivity());
            accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(ToLoanAccNo)));
            accNo_values1.setPadding(5, 5, 5, 5);
            accNo_values1.setGravity(Gravity.RIGHT);
            accNo_values1.setLayoutParams(sbAccParams1);
            sbAccNoRow1.addView(accNo_values1);

            confirmationTable.addView(sbAccNoRow1,
                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        }

        TableRow withdrawRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams withdrawParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        withdrawParams.setMargins(10, 5, 10, 5);

        TextView withdraw = new TextView(getActivity());
        withdraw.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferAmount)));
        withdraw.setTypeface(LoginActivity.sTypeface);
        withdraw.setPadding(5, 5, 5, 5);
        withdraw.setLayoutParams(withdrawParams);
        withdrawRow.addView(withdraw);

        TextView withdraw_values = new TextView(getActivity());
        withdraw_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAcctransferAmount)));
        withdraw_values.setPadding(5, 5, 5, 5);
        withdraw_values.setGravity(Gravity.RIGHT);
        withdraw_values.setLayoutParams(withdrawParams);
        withdrawRow.addView(withdraw_values);

        confirmationTable.addView(withdrawRow,
                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        TableRow expenseRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams expensesParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        expensesParams.setMargins(10, 5, 10, 5);

        TextView expense = new TextView(getActivity());
        expense.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferCharges)));
        expense.setTypeface(LoginActivity.sTypeface);
        expense.setPadding(5, 5, 5, 5);
        expense.setLayoutParams(expensesParams);
        expenseRow.addView(expense);

        TextView expense_values = new TextView(getActivity());
        expense_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAcctransferCharge)));
        expense_values.setPadding(5, 5, 5, 5);
        expense_values.setGravity(Gravity.RIGHT);
        expense_values.setLayoutParams(expensesParams);
        expenseRow.addView(expense_values);

        confirmationTable.addView(expenseRow,
                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText("" + (AppStrings.edit));
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText("" + AppStrings.yes);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.frag_Ok:
                if (confirmationDialog != null && confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }

                AddBTDto dto = new AddBTDto();
                if (selectedRadio.equals("SAVINGSACCOUNT")) {
                    dto.setFromSavingsBankDetailsId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                    dto.setToSavingsBankDetailsId(selectedSavinAc.getShgSavingsAccountId());
                    dto.setShgId(shgDto.getShgId());
                    dto.setMobileDate(System.currentTimeMillis() + "");
                    dto.setTransferCharges(mAcctransferCharge);
                    dto.setTransferAmount(mAcctransferAmount);
//                    dto.setTransactionDate(shgDto.getLastTransactionDate());
                    dto.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());

                    try {
                        OfflineDto offline = new OfflineDto();
                        offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                        offline.setShgId(shgDto.getShgId());
//                        offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                        offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                        offline.setModifiedDateTime(System.currentTimeMillis() + "");
                        offline.setModeOCash("2");
                        offline.setTxType(NewDrawerScreen.BANK_TRANSACTION);
                        offline.setTxSubtype(AppStrings.mAccountToAccountTransfer);
                        offline.setMFromBk(BankTransaction.sSelectedBank.getBankName());
                        offline.setMToBk(selectedSavinAc.getBankName());
                        offline.setBtFromSavingAcId(BankTransaction.sSelectedBank.getShgSavingsAccountId());

                        //   offline.setBtToSavingAcId(selectedSavinAc.getShgSavingsAccountId() + "");
                        offline.setTransferCharge(mAcctransferCharge + "");
                        offline.setTransferAmount(mAcctransferAmount + "");
                        offline.setAccountNumber(BankTransaction.sSelectedBank.getShgSavingsAccountId());

                        //offline fundflow
                        int os = 0, cab = 0, cb = 0, cb1 = 0;
                        if (selectedSavinAc != null && selectedSavinAc.getShgSavingsAccountId() != null) {
                            offline.setBtToSavingAcId(selectedSavinAc.getShgSavingsAccountId());
                            offline.setAccountNumber(selectedSavinAc.getShgSavingsAccountId());
                            ShgBankDetails details = BankTable.getBankTransaction(selectedSavinAc.getShgSavingsAccountId());
                            cb1 = ((int) Double.parseDouble(details.getCurrentBalance())) + ((int) Double.parseDouble(mAcctransferAmount));
                        } else {
                            offline.setBtToSavingAcId(null);
                            offline.setAccountNumber(null);
                        }

                        cab = ((int) Double.parseDouble(shgDto.getCashAtBank())) - ((int) Double.parseDouble(mAcctransferCharge));
                        cb = ((int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance())) - ((int) Double.parseDouble(mAcctransferAmount) + (int) Double.parseDouble(mAcctransferCharge));

                        offline.setCashInhand(shgDto.getCashInHand());
                        offline.setCashAtBank(cab + "");
                        offline.setCurr_balance(cb + "");
                        offline.setCurr_balance1(cb1 + "");
                        offline.setOutStanding("");
                        offline.setIs_transaction_tdy("1.0");

                        offlineDBData = offline;


                        //   offline.add(offline);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    String sreqString = new Gson().toJson(dto);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();

                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SAVTOSAV, sreqString, getActivity(), ServiceType.SAV_TO_LOAN);
                    } else {
                        if (TransactionTable.getLoginFlag(AppStrings.mAccountToAccountTransfer).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.mAccountToAccountTransfer).get(TransactionTable.getLoginFlag(AppStrings.mAccountToAccountTransfer).size() - 1).getLoginFlag().equals("1")))
                            insertBT();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                    }
                } else {
                    dto.setShgId(shgDto.getShgId());
                    dto.setMobileDate(System.currentTimeMillis() + "");
//                    dto.setTransactionDate(shgDto.getLastTransactionDate());
                    dto.setTransferCharges(mAcctransferCharge);
                    dto.setTransferAmount(mAcctransferAmount);
                    dto.setSavingsBankDetailsId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                    dto.setLoanBankDetailsId(selectedLoantype.getLoanAccountId());
//                    dto.setTransactionDate(shgDto.getLastTransactionDate());
                    dto.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    dto.setMobileDate(System.currentTimeMillis() + "");

                    try {
                        OfflineDto offline = new OfflineDto();
                        offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                        offline.setShgId(shgDto.getShgId());
//                        offline.setTransactionDate(shgDto.getLastTransactionDate());
                        offline.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                        offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
//                        offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                        offline.setModifiedDateTime(System.currentTimeMillis() + "");
                        offline.setModeOCash("2");
                        offline.setTxType(NewDrawerScreen.BANK_TRANSACTION);
                        offline.setTxSubtype(AppStrings.mAccountToAccountTransfer);
                        offline.setBtFromSavingAcId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                        offline.setBtToLoanId(selectedLoantype.getLoanAccountId() + "");
                        offline.setMLoanName(selectedLoantype.getLoanTypeName() + "");
                        offline.setMFromBk(BankTransaction.sSelectedBank.getBankName() + "");

                        int os = 0, cab = 0, cb = 0;
                        if (selectedLoantype != null && selectedLoantype.getLoanAccountId() != null) {
                            offline.setBtToLoanId(selectedLoan.getLoanAccountId());
                            offline.setAccountNumber(selectedLoan.getLoanAccountId());
                            os = ((int) Double.parseDouble(selectedLoantype.getLoanOutstanding())) - (int) Double.parseDouble(mAcctransferAmount);
                        } else {
                            offline.setBtToLoanId(null);
                            offline.setAccountNumber(null);
                        }


                        offline.setTransferCharge(mAcctransferCharge + "");
                        offline.setTransferAmount(mAcctransferAmount + "");


                        cab = ((int) Double.parseDouble(shgDto.getCashAtBank())) - ((int) Double.parseDouble(mAcctransferAmount) + (int) Double.parseDouble(mAcctransferCharge));
                        //   os = ((int) Double.parseDouble(selectedLoantype.getLoanOutstanding())) - ((int) Double.parseDouble(mAcctransferAmount));
                        cb = ((int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance())) - ((int) Double.parseDouble(mAcctransferAmount) + (int) Double.parseDouble(mAcctransferCharge));

                        offline.setCashInhand(shgDto.getCashInHand());
                        offline.setCashAtBank(cab + "");
                        offline.setOutStanding(os + "");
                        offline.setCurr_balance(cb + "");
                        offline.setIs_transaction_tdy("1.0");
                        offlineDBData = offline;


                        //   offline.add(offline);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    String sreqString = new Gson().toJson(dto);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();

                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SAVTOLOAN, sreqString, getActivity(), ServiceType.SAV_TO_LOAN);
                    } else {
                        if (TransactionTable.getLoginFlag(AppStrings.mAccountToAccountTransfer).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.mAccountToAccountTransfer).get(TransactionTable.getLoginFlag(AppStrings.mAccountToAccountTransfer).size() - 1).getLoginFlag().equals("1")))
                            insertBT();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                    }
                }


                break;
            case R.id.fragment_Edit:
                confirmationDialog.dismiss();
                mAcctransferAmount = "0";
                mAcctransferCharge = "0";

                break;
        }

    }

    private void insertTransaction_tdy()
    {

        offlineDBData.setIs_transaction_tdy("1.0");
        offlineDBData.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offlineDBData);

    }


    private void insertBT() {
        try {

            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.BT_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.BT_COUNT, value);

            offlineDBData.setBtCount(value + "");
            TransactionTable.insertTransBTData(offlineDBData);
            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }

            updateActoAcTxData();
            updateCIH();

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCIH() {
        String cihstr = "", cabstr = "", lastTranstr = "";
        cihstr = offlineDBData.getCashInhand();
        cabstr = offlineDBData.getCashAtBank();
        lastTranstr = offlineDBData.getLastTransactionDateTime();
        CashOfGroup csg = new CashOfGroup();
        csg.setCashInHand(cihstr);
        csg.setCashAtBank(cabstr);
        csg.setLastTransactionDate(lastTranstr);
        SHGTable.updateSHGDetails(csg, shgDto.getId());
    }

    private void updateActoAcTxData() {
        if (offlineDBData != null && offlineDBData.getCurr_balance1() != null && offlineDBData.getCurr_balance1().length() > 0) {
            SHGTable.updateToBankCurrentBalanceDetails(offlineDBData);
        }
        if (offlineDBData != null && offlineDBData.getOutStanding() != null && offlineDBData.getOutStanding().length() > 0) {
            SHGTable.updateLoanOSDetails(offlineDBData);
        }
        if (offlineDBData != null && offlineDBData.getCurr_balance() != null && offlineDBData.getCurr_balance().length() > 0) {
            String value1 = offlineDBData.getBtFromSavingAcId();
            offlineDBData.setAccountNumber(value1);
            offlineDBData.setShgSavingsAccountId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
            SHGTable.updateBankCurrentBalanceDetails(offlineDBData);
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog != null && confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

            }
        }


        switch (serviceType) {


            case LOANTYPE:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        String message = mrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {                       // showMessage(statusCode);
                            Utils.showToast(getActivity(), message);
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        } else if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            for (int i = 0; i < mrDto.getResponseContent().getLoansList().size(); i++) {
                                ExistingLoan loanDto = mrDto.getResponseContent().getLoansList().get(i);
                                loanDto.setShgId(shgDto.getShgId());
                                LoanTable.insertLoanDetails(loanDto);
                            }


                        } else {
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case SAV_TO_LOAN:
                try {
                    if (result != null && result.length() > 0) {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }

                            updateActoAcTxData();
                            updateCIH();
                            insertTransaction_tdy();
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            NewDrawerScreen.showFragment(new MainFragment());

                        } else {
                            Utils.showToast(getActivity(), message);
                            insertBT();
                        }
                    } else {
                        insertBT();
                    }
                } catch (Exception e) {

                }
                break;

        }

    }


}
