package com.oasys.emathi.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.emathi.Adapter.GroupreportAdapter;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.RecyclerItemClickListener;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;

import java.util.ArrayList;
import java.util.Arrays;

public class GroupReportFragment extends Fragment {

    private RecyclerView recyclerViewGroupReport;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, Name;
    private ListOfShg shgDto;
    private LinearLayoutManager linearLayoutManager;
    ArrayList GroupNames = new ArrayList<>(Arrays.asList(AppStrings.groupSavingssummary, AppStrings.groupLoansummary));
    private static FragmentManager fm;


    public GroupReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_group_report, container, false);

        recyclerViewGroupReport = (RecyclerView) view.findViewById(R.id.recyclerViewGroupreport);

        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.ch);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cb);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);


        mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
        mHeader.setText(AppStrings.GroupReports);
        mHeader.setTypeface(LoginActivity.sTypeface);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewGroupReport.setLayoutManager(linearLayoutManager);
        recyclerViewGroupReport.setHasFixedSize(true);


        GroupreportAdapter groupreportAdapter = new GroupreportAdapter(getActivity(), GroupNames);
        recyclerViewGroupReport.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerViewGroupReport.setAdapter(groupreportAdapter);
        fm = getActivity().getSupportFragmentManager();

        recyclerViewGroupReport.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                switch (position) {
                    case 0:
                        NewDrawerScreen.showFragment(new GroupSavingSummaryFragment());
                        break;

                    case 1:
                        NewDrawerScreen.showFragment(new GroupLoanSummaryFragment());
                        break;

                    default:
                        break;
                }

            }
        }));

        return view;
    }

    public static void showFragment(Fragment fragment) {

        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.static_frame, fragment);
        trans.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out);
        trans.show(fragment).commit();
    }

}
