package com.oasys.emathi.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.oasys.emathi.Adapter.Transaction_GroupLoanRepayment_Adapter;
import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.ExpandListItemClickListener;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupLoanRepayment extends Fragment implements ExpandListItemClickListener {
    private View view;
    private ExpandableListView mTrasnsactionloan_repayment;
    public static Transaction_GroupLoanRepayment_Adapter transaction_groupLoanRepayment_adapter;
    private ArrayList<ExistingLoan> listDataHeader;
    private HashMap<String, ArrayList<ExistingLoan>> listDataChild;
    ResponseDto responseDto;
    private ListOfShg shgDto;
    private String shgId;
    private TextView cashinhandgroupreport, cashatbankgroupreport;
    private TextView grouploan_groupname,mcashhandmember,mcashbankmember;
    private List<ExistingLoan> grploanDetails;
    ArrayList<ExistingLoan> mainlistarray;
    private List<MemberList> memList;
    private int previous = -1;


    public GroupLoanRepayment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_group_loan_repayment, container, false);
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        //  networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        grploanDetails = LoanTable.getGrpLoanDetails(shgDto.getShgId());
        listDataChild = new HashMap<String, ArrayList<ExistingLoan>>();
        listDataHeader = new ArrayList<ExistingLoan>();
        initView();
        return view;
    }

    public void initView() {
        mTrasnsactionloan_repayment = (ExpandableListView) view.findViewById(R.id.GrouploanRepayment_ExpandableListView);
        /*cashinhandgroupreport = (TextView) view.findViewById(R.id.cashinhandgroupreport);
        cashinhandgroupreport.setTypeface(LoginActivity.sTypeface);
        cashinhandgroupreport.setText(shgDto.getCashInHand());*/
        /*cashatbankgroupreport = (TextView) view.findViewById(R.id.cashatbankgroupreport);
        cashatbankgroupreport.setTypeface(LoginActivity.sTypeface);
        cashatbankgroupreport.setText(shgDto.getCashAtBank());*/
        grouploan_groupname = (TextView) view.findViewById(R.id.grouploan_groupname);
        grouploan_groupname.setTypeface(LoginActivity.sTypeface);
        grouploan_groupname.setText(shgDto.getName() + " / " + shgDto.getPresidentName());

        mcashhandmember =(TextView)view.findViewById(R.id.cashhandmember);
        mcashhandmember.setTypeface(LoginActivity.sTypeface);
        mcashhandmember.setText(AppStrings.cashinhand + shgDto.getCashInHand());

        mcashbankmember =(TextView)view.findViewById(R.id.cashbankmember);
        mcashbankmember.setTypeface(LoginActivity.sTypeface);
        mcashbankmember.setText(AppStrings.cashatBank + shgDto.getCashAtBank());

        mTrasnsactionloan_repayment.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            @Override
            public void onGroupExpand(int groupPosition) {

                /*if (groupPosition != previous) {
                    mTrasnsactionloan_repayment.collapseGroup(previous);
                    previous = groupPosition;
                }*/

                if (previous != -1 && groupPosition != previous) {
                    mTrasnsactionloan_repayment.collapseGroup(previous);
                }
                previous = groupPosition;

            }
        });

//        mTrasnsactionloan_repayment.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//            @Override
//            public void onGroupCollapse(int i) {
//
//            }
//        });

        mainlistarray = (ArrayList<ExistingLoan>) grploanDetails;

        for (int i = 0; i < grploanDetails.size(); i++) {
            listDataChild.put(grploanDetails.get(0).getLoanId(), mainlistarray);
        }

//                    Log.e("Arraylist..........", "" + responseDto.getResponseContent().getLoansList());
//                    Log.e("Hashmap..........", "" + listDataChild.toString());

        transaction_groupLoanRepayment_adapter = new Transaction_GroupLoanRepayment_Adapter(getActivity(), mainlistarray, listDataChild, this);

        mTrasnsactionloan_repayment.setAdapter(transaction_groupLoanRepayment_adapter);

    }

    private List<ExistingLoan> grpLoanList;
    ArrayList<MemberList> outstandingamt, dummyList;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        /*if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            //   RestClient.getRestClient(GroupLoanRepayment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUP_LOAN_REPAYMENT + "" + shgId, getActivity(), ServiceType.TRANSACTION_GROUP_LOAN_REPAYMENT);

            RestClient.getRestClient(GroupLoanRepayment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUP_LOAN_REPAYMENT + shgId, getActivity(), ServiceType.TRANSACTION_GROUP_LOAN_REPAYMENT);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }*/

        grpLoanList = LoanTable.getGrpLoanDetails(shgDto.getShgId());
      /*  dummyList = new ArrayList<>();
        for (int i = 0; i < memList.size(); i++) {
            for (int j = 0; j < grpLoanList.size(); j++) {
                if (memList.get(i).getMemberId().equals(grpLoanList.get(j).getMemberId())) {
                    MemberList m = new MemberList();
                    m.setMemberId(grpLoanList.get(j).getMemberId());
                    m.setLoanOutstanding(grpLoanList.get(j).getMemberLoanOutstanding());
                    m.setMemberName(memList.get(i).getMemberName());
                    dummyList.add(m);
                }
            }
        }
        outstandingamt = dummyList;
*/
        for (int i = 0; i < grpLoanList.size(); i++) {
            listDataChild.put(grpLoanList.get(0).getLoanId(), (ArrayList<ExistingLoan>) grpLoanList);
        }
        transaction_groupLoanRepayment_adapter = new Transaction_GroupLoanRepayment_Adapter(getActivity(), (ArrayList<ExistingLoan>) grpLoanList, listDataChild, this);

        mTrasnsactionloan_repayment.setAdapter(transaction_groupLoanRepayment_adapter);

    }

   /* @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        try {

            switch (serviceType) {

                case TRANSACTION_GROUP_LOAN_REPAYMENT:

                    Log.d("getDetails", " " + result.toString());
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        responseDto.getResponseContent().getLoansList();
                        Log.d("check", "" + responseDto.getResponseContent().getLoansList());

                        for (int i = 0; i < responseDto.getResponseContent().getLoansList().size(); i++) {
                            listDataChild.put(responseDto.getResponseContent().getLoansList().get(0).getLoanId(), responseDto.getResponseContent().getLoansList());
                        }

//                    Log.e("Arraylist..........", "" + responseDto.getResponseContent().getLoansList());
//                    Log.e("Hashmap..........", "" + listDataChild.toString());

                        transaction_groupLoanRepayment_adapter = new Transaction_GroupLoanRepayment_Adapter(getActivity(), responseDto.getResponseContent().getLoansList(), listDataChild, this);

                        mTrasnsactionloan_repayment.setAdapter(transaction_groupLoanRepayment_adapter);
                    }else {
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {

        Transaction_LoanType_Details fragment = new Transaction_LoanType_Details();
        Bundle bundle = new Bundle();
        bundle.putString("loan_id", (grpLoanList.get(position).getLoanId()));
        bundle.putString("loan_ac_id", (grpLoanList.get(position).getLoanAccountId()));
        bundle.putString("account_number", (grpLoanList.get(position).getAccountNumber()));
        bundle.putString("loan_type", (grpLoanList.get(position).getLoanTypeName()));
        bundle.putString("bank_name", (grpLoanList.get(position).getBankName()));
        bundle.putString("outstanding", (grpLoanList.get(position).getLoanOutstanding()));
        fragment.setArguments(bundle);
        NewDrawerScreen.showFragment(fragment);
/*

        FragmentTransaction transection=getFragmentManager().beginTransaction();
        Transaction_LoanType_Details transaction_loanType_details=new Transaction_LoanType_Details();
        transection.replace(R.id.static_frame, transaction_loanType_details);
        transection.commit();
*/

    }

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {

    }

    @Override
    public void onItemClickAudit(ViewGroup parent, View view, int position) {

    }
}
