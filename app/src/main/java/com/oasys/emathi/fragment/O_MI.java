package com.oasys.emathi.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.RequestDto.SsnNumberRequestDTOList;
import com.oasys.emathi.Dto.RequestDto.SsnNumbersDTOList;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.SssTypeDto;
import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.OasysUtils.AlphaNumericInputFilter;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.views.ButtonFlat;
import com.oasys.emathi.views.CustomHorizontalScrollView;
import com.oasys.emathi.views.Get_EdiText_Filter;
import com.oasys.emathi.views.RaisedButton;
import com.oasys.emathi.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.oasys.emathi.fragment.LD_IL_Entry.TAG;

public class O_MI extends Fragment implements View.OnClickListener, NewTaskListener {
    private  View view;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader,mMemberName;
    LinearLayout mMemberNameLayout;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    private  TextView mPmsby_values;
    private EditText mPmsby_number_values,mEntroll,mBeforeyear;
    public  List<TextView> sPmsby_Fields = new ArrayList<TextView>();
    public  List<EditText> sPmsbyNumber_Fields = new ArrayList<EditText>();
    public  List<EditText> smEntroll_Fields = new ArrayList<EditText>();
    public static List<TextView> sBeforeYear_Fields = new ArrayList<TextView>();
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    ArrayList<SssTypeDto>sssTypeDtos;
    ArrayList<SsnNumbersDTOList>ssnNumbersDTOLists;
    ArrayList<TableData>tableDataArrayList;
    String o_mi_ssstypeid;
    ArrayList <TableData> datavalues;
    TableData tableData;
    String mPOL_Values[]= new String[]{"YES","NO"};
    RadioButton radioButton;
    int selectedId;
    String mSelectedLoanType;
    RaisedButton submit_button;
    private String[] sChoose_Pmsby,sPolicy_Number,sDateofentroll;
    Dialog confirmationDialog;
    private Button  mEdit_RaisedButton, mOk_RaisedButton;
    private CheckBox mAutoFill;
    String sssTypeid;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pmsbylayout, container, false);
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        datavalues = new ArrayList<>();
        Bundle bundle= getArguments();
        if(bundle!=null){
            sssTypeid = bundle.getString("omiid");
            Log.d("typeid",sssTypeid);
        }
        datavalues= SHGTable.getSSNAllData(sssTypeid,shgDto.getShgId());
        Log.d("memberdetails",datavalues+"");
//        sssTypeId();
        inIt(view);
        return view;
    }


    private void inIt(View view1) {

        try {
            memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

            submit_button = (RaisedButton)view1.findViewById(R.id.fragment_Submit_button) ;
            submit_button.setOnClickListener(this);

            mAutoFill = (CheckBox) view1.findViewById(R.id.autoFill);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);

            mGroupName = (TextView) view1.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view1.findViewById(R.id.cashinHand);
            mCashinHand.setText(com.oasys.emathi.OasysUtils.AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view1.findViewById(R.id.cashatBank);
            mCashatBank.setText(com.oasys.emathi.OasysUtils.AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) view1.findViewById(R.id.fragmentHeader);
            mHeader.setText("OTHER MICRO INSURANCE");
            mHeader.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) view1.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) view1.findViewById(R.id.member_name);

            mLeftHeaderTable = (TableLayout) view1.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) view1.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) view1.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) view1.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) view1.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) view1.findViewById(R.id.rightContentHScrollView);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });
            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_Header = new TextView(getActivity());
            mMemberName_Header.setText(AppStrings.memberName);
            mMemberName_Header.setTypeface(LoginActivity.sTypeface);
            mMemberName_Header.setTextColor(Color.WHITE);
            mMemberName_Header.setPadding(10, 5, 10, 5);
            mMemberName_Header.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_Header);

            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView mOutstanding_Header = new TextView(getActivity());
            mOutstanding_Header.setText(AppStrings.mInsurance);
            mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
            mOutstanding_Header.setTextColor(Color.WHITE);
            mOutstanding_Header.setPadding(10, 5, 10, 5);
            mOutstanding_Header.setGravity(Gravity.LEFT);
            mOutstanding_Header.setLayoutParams(contentParams);
            mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mOutstanding_Header);


            TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            POLParams.setMargins(10, 0, 10, 0);

            TextView mPurposeOfLoan_Header = new TextView(getActivity());
            mPurposeOfLoan_Header
                    .setText(AppStrings.mPolicyNumber);
            mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
            mPurposeOfLoan_Header.setTextColor(Color.WHITE);
            mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
            mPurposeOfLoan_Header.setLayoutParams(POLParams);
            mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
            mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPurposeOfLoan_Header);


            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mPL_Header = new TextView(getActivity());
            mPL_Header.setText(AppStrings.mDateenroll);
            mPL_Header.setTypeface(LoginActivity.sTypeface);
            mPL_Header.setTextColor(Color.WHITE);
            mPL_Header.setPadding(10, 5, 10, 5);
            mPL_Header.setGravity(Gravity.CENTER);
            mPL_Header.setLayoutParams(rHeaderParams);
            mPL_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPL_Header);
            mRightHeaderTable.addView(rightHeaderRow);


            for (int i = 0; i < datavalues.size() ; i++) {

                final int pos = i;

                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                leftContentParams.setMargins(5, 25, 5, 0);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(GetSpanText.getSpanString(getActivity(), datavalues.get(i).getMemberName()));
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 10, 5, 0);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(270);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);
                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());

                TableRow.LayoutParams rightContentTextviewParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT, 1f);
                rightContentTextviewParams.setMargins(5, 15, 5, 0);
                mPmsby_values = new TextView(getActivity());
                mPmsby_values.setId(i);
                sPmsby_Fields.add(mPmsby_values);
                mPmsby_values.setTextColor(R.color.black);
                mPmsby_values.setPadding(5, 5, 5, 5);
                mPmsby_values.setWidth(230);
                mPmsby_values.setLayoutParams(rightContentTextviewParams);
                mPmsby_values.setSingleLine(true);
                mPmsby_values.setEllipsize(TextUtils.TruncateAt.END);
                /*if (datavalues.size()==0) {
                    mPmsby_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("CHOOSE INSURANCE")));
                } else {
                    if( datavalues.get(i).getIs_sss().equalsIgnoreCase("true"))
                    {
                        mPmsby_values.setText(GetSpanText.getSpanString(getActivity(),"Yes"));
                    }
                    else
                    {
                        mPmsby_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("CHOOSE INSURANCE")));
                    }
                }*/
                if (datavalues.get(i).getIs_sss() != null) {
                    if(datavalues.get(i).getIs_sss().equals("true")) {
                        mPmsby_values.setText(GetSpanText.getSpanString(getActivity(), "YES"));
                    }else {
                        mPmsby_values.setText(GetSpanText.getSpanString(getActivity(), "NO"));
                    }
                } else {
                    mPmsby_values.setText(GetSpanText.getSpanString(getActivity(), "CHOOSE INSURANCE"));
                }
                mPmsby_values.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        try {
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                            final Dialog ChooseLoanType;
                            final View dialogView;
                            final RadioGroup radioGroup;
                            ChooseLoanType = new Dialog(getActivity());

                            LayoutInflater li = (LayoutInflater) getActivity()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            dialogView = li.inflate(R.layout.dialog_new_loantype, null, false);

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.dialog_ChooseLabel);
                            confirmationHeader.setText(AppStrings.chooseitem);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            int radioColor = getResources().getColor(R.color.pink);
                            radioGroup = (RadioGroup) dialogView.findViewById(R.id.dialog_RadioGroup);


                            for (int j = 0; j < mPOL_Values.length; j++) {

                                radioButton = new RadioButton(getActivity());
                                radioButton.setText(mPOL_Values[j]);
                                radioButton.setId(j);
                                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                                    radioButton.setButtonTintList(ColorStateList.valueOf(radioColor));
                                }

                                radioGroup.addView(radioButton);

                            }

                            ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
                            okButton.setText(AppStrings.dialogOk1);
                            okButton.setTypeface(LoginActivity.sTypeface);
                            okButton.setTag(""+pos);
                            okButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {
                                    // TODO Auto-generated method
                                    // stub
//                                    int itemposotion = Integer.parseInt(view.getTag().toString());
                                    selectedId = radioGroup.getCheckedRadioButtonId();
                                    Log.e(TAG, String.valueOf(selectedId));
                                    if ((selectedId != 100) && (selectedId != (-1))) {

                                        // find the radiobutton by
                                        // returned id
                                        RadioButton radioLoanButton = (RadioButton) dialogView
                                                .findViewById(selectedId);

                                        mSelectedLoanType = radioLoanButton.getText().toString();
                                        Log.v("On Selected LOAN TYPE", mSelectedLoanType);

                                        Log.v("view ID check : ", String.valueOf(v.getId()));
                                        int selectedid = Integer.parseInt(String.valueOf(v.getId()));
                                        TextView selectedTextView = (TextView) v.findViewById(v.getId());

                                        if (selectedId == 0) {
                                            selectedTextView.setText("YES");
                                        } else {
                                            selectedTextView.setText(mSelectedLoanType);
                                        }

                                        if(sPmsby_Fields.get(pos).getText().toString().equalsIgnoreCase("YES")) {
                                            sPmsbyNumber_Fields.get(pos).setEnabled(true);
                                            sPmsbyNumber_Fields.get(pos).setFocusableInTouchMode(true);
                                            sPmsbyNumber_Fields.get(pos).setBackgroundResource(R.drawable.edittext_background);
                                            smEntroll_Fields.get(pos).setEnabled(true);
                                            smEntroll_Fields.get(pos).setFocusable(true);
                                            smEntroll_Fields.get(pos).setBackgroundResource(R.drawable.edittext_background);

                                        }
                                        else
                                        {
                                            sPmsbyNumber_Fields.get(pos).setText("");
                                            sPmsbyNumber_Fields.get(pos).setEnabled(false);
                                            sPmsbyNumber_Fields.get(pos).setFocusable(false);
                                            sPmsbyNumber_Fields.get(pos).setBackgroundResource(R.drawable.edittextbackgoundpmsby);
                                            smEntroll_Fields.get(pos).setText("");
                                            smEntroll_Fields.get(pos).setEnabled(false);
                                            smEntroll_Fields.get(pos).setFocusable(false);
                                            smEntroll_Fields.get(pos).setBackgroundResource(R.drawable.edittextbackgoundpmsby);


                                        }


                                        Log.d("checkvalue",sPmsby_Fields.get(pos).getText().toString());

                                        System.out.println("------idssssssssssss-----" + v.getId());

                                        ChooseLoanType.dismiss();

                                    } else {
                                        TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                                    }

                                }

                            });

                            ChooseLoanType.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            ChooseLoanType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ChooseLoanType.setCanceledOnTouchOutside(false);
                            ChooseLoanType.setContentView(dialogView);
                            ChooseLoanType.setCancelable(true);
                            ChooseLoanType.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


                rightContentRow.addView(mPmsby_values);


                TableRow.LayoutParams rightContentTextviewParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentTextviewParams1.setMargins(20, 5, 20, 5);

                mPmsby_number_values = new EditText(getActivity());
                mPmsby_number_values.setId(i);
                mPmsby_number_values.setMaxLines(1);
                mPmsby_number_values.setSingleLine(true);
                mPmsby_number_values.setEllipsize(TextUtils.TruncateAt.END);
                mPmsby_number_values.setEnabled(false);
                sPmsbyNumber_Fields.add(mPmsby_number_values);
                mPmsby_number_values.setPadding(5, 5, 5, 5);
                mPmsby_number_values.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                ArrayList<InputFilter> curInputFilters = new ArrayList<InputFilter>(Arrays.asList(mPmsby_number_values.getFilters()));
                curInputFilters.add(0, new AlphaNumericInputFilter());
                curInputFilters.add(1, new InputFilter.LengthFilter(20));
                InputFilter[] newInputFilters = curInputFilters.toArray(new InputFilter[curInputFilters.size()]);
                mPmsby_number_values.setFilters(newInputFilters);
//                mPmsby_number_values.setFilters(Get_EdiText_Filter.editText_AccNo_filter());
//                mPmsby_number_values.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                mPmsby_number_values.setBackgroundResource(R.drawable.edittext_background);
                mPmsby_number_values.setLayoutParams(rightContentTextviewParams1);
                mPmsby_number_values.setWidth(300);
               /* if ((datavalues==null)|| (datavalues.size()==0)) {
                    mPmsby_number_values.setText(GetSpanText.getSpanString(getActivity(), ""));
                } else {
                    if((datavalues.get(i).getSss_number()==null)||(datavalues.get(i).getSss_number().equalsIgnoreCase("")))
                    {
                        mPmsby_number_values.setText(GetSpanText.getSpanString(getActivity(),""));
                    }
                    else
                    {
                        mPmsby_number_values.setText(datavalues.get(i).getSss_number());
                    }
                }*/
                if (datavalues.get(i).getSss_number() != null) {
                    mPmsby_number_values.setText(GetSpanText.getSpanString(getActivity(), datavalues.get(i).getSss_number()));
                } else {
                    mPmsby_number_values.setText("");
                }
                mPmsby_number_values.setTextColor(R.color.black);

                rightContentRow.addView(mPmsby_number_values);


                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentParams.setMargins(10, 5, 10, 5);
                mEntroll = new EditText(getActivity());
                mEntroll.setId(i);
                mEntroll.setMaxLines(1);
                mEntroll.setSingleLine(true);
                mEntroll.setEllipsize(TextUtils.TruncateAt.END);
                mEntroll.setEnabled(false);
                smEntroll_Fields.add(mEntroll);
                mEntroll.setTextColor(R.color.black);
                mEntroll.setPadding(5, 5, 5, 5);
                mEntroll.setLayoutParams(rightContentTextviewParams1);
                mEntroll.setFilters(Get_EdiText_Filter.editText_AccNo_filter());
                mEntroll.setBackgroundResource(R.drawable.edittext_background);
                mEntroll.setWidth(350);
                String  sssdate = String.valueOf(datavalues.get(i).getSss_date());
                if (sssdate != null && (!sssdate.equals("0"))) {

                    DateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                    Date d = new Date(datavalues.get(i).getSss_date());
                    String dateStr = dateFormat1.format(d);
                    mEntroll.setText(GetSpanText.getSpanString(getActivity(), dateStr));
                } else {
                    mEntroll.setText("");
                }
               /* if (datavalues.size()==0) {
                    mEntroll.setText(GetSpanText.getSpanString(getActivity(), ""));
                } else {
                    if(datavalues.get(i).getSss_date()==0)
                    {
                        mEntroll.setText(GetSpanText.getSpanString(getActivity(),""));
                    }
                    else
                    {
                        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                        Date d = new Date(datavalues.get(i).getSss_date());
                        String dateStr = simple.format(d);
                        smEntroll_Fields.get(i).setText(dateStr);
                    }
                }*/

                mEntroll.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            Calendar now = Calendar.getInstance();
                            Calendar lastDate = Calendar.getInstance();
                            Log.v("view ID check : ", String.valueOf(view.getId()));

                            final EditText selectedTextView = (EditText) view.findViewById(view.getId());

                            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                                    Calendar selectionCal = Calendar.getInstance();
                                    selectionCal.set(year, month, day);
                                    String formattedDate = df.format(selectionCal.getTime());
                                    selectedTextView.setText(formattedDate);

                                }
                            }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE));
                            datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
                            datePickerDialog.show();
                        }
                        return false;
                    }
                });

                rightContentRow.addView(mEntroll);
                mRightContentTable.addView(rightContentRow);


            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sssTypeId() {

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod1( Constants.BASE_URL + Constants.PROFILE_SSS_TYPE_ID, getActivity(), ServiceType.GETSSSTYPEIDFETCHING);

        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.fragment_Submit_button:

                sChoose_Pmsby = new String[sPmsby_Fields.size()];
                sPolicy_Number = new String[sPmsbyNumber_Fields.size()];
                sDateofentroll = new String[smEntroll_Fields.size()];

                for (int i = 0; i < memList.size() ; i++) {

                    sChoose_Pmsby[i] = sPmsby_Fields.get(i).getText().toString();
                    sPolicy_Number[i] = sPmsbyNumber_Fields.get(i).getText().toString();
                    sDateofentroll[i] = smEntroll_Fields.get(i).getText().toString();

                    if ( sChoose_Pmsby[i].equals(""))
                    {
                        sChoose_Pmsby[i] = "CHOOSE INSURANCE";
                    }
                    if ( sPolicy_Number[i].equals(""))
                    {
                        sPolicy_Number[i] = "No";
                    }
                    if ( sDateofentroll[i].equals(""))
                    {
                        sDateofentroll[i] = "No";
                    }
                }

                showDialog();
                break;

            case R.id.frag_Ok:
                otherMicroInsurancepostApiCall();
                break;

            case R.id.fragment_Edit:
                confirmationDialog.dismiss();
                break;
        }
    }

    private void otherMicroInsurancepostApiCall() {

        confirmationDialog.dismiss();
        ssnNumbersDTOLists =new ArrayList<>();
        tableDataArrayList =new ArrayList<>();

        for (int i = 0; i < memList.size() ; i++) {
            SsnNumbersDTOList ssnNumbersDTOList = new SsnNumbersDTOList();
            tableData =new TableData();
            ssnNumbersDTOList.setShgId(memList.get(i).getShgId());
            tableData.setShgId(memList.get(i).getShgId());

            ssnNumbersDTOList.setUserId(memList.get(i).getMemberUserId());
            ssnNumbersDTOList.setMemberId(memList.get(i).getMemberId());
            tableData.setMemberId(memList.get(i).getMemberId());
            tableData.setMemberName(memList.get(i).getMemberName());

            if(sPmsbyNumber_Fields.get(i).getText().toString().isEmpty())
            {
                ssnNumbersDTOList.setSss_number("0");
                tableData.setSss_number("0");
            }
            else
            {
                ssnNumbersDTOList.setSss_number(sPmsbyNumber_Fields.get(i).getText().toString());
                tableData.setSss_number(sPmsbyNumber_Fields.get(i).getText().toString());
            }
//            ssnNumbersDTOList.setSss_number(sPmsbyNumber_Fields.get(i).getText().toString());
//        ssnNumbersDTOList.setSssDte(smEntroll_Fields.get(i).getText().toString());

            Date date = null;
            long mills=0;
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            try {
                if ((!smEntroll_Fields.get(i).getText().toString().equals("0"))&&(!smEntroll_Fields.get(i).getText().toString().equals("")))
                {
                    date = dateFormat.parse(smEntroll_Fields.get(i).getText().toString());

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(date!=null)
            {
                mills = date.getTime();

            }
            if(mills!=0) {

                tableData.setSss_date(mills);
            }
            else
            {
                tableData.setSss_date(0);
            }

            if(mills!=0) {
                DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(mills);
                String dateStr = dateFormat1.format(d);
                ssnNumbersDTOList.setSssDte(dateStr);
            }
            else
            {
                ssnNumbersDTOList.setSssDte("");

            }

            ssnNumbersDTOList.setSssTypeUuid(sssTypeid);
            tableData.setSssTypeId(sssTypeid);
            if(sPmsby_Fields.get(i).getText().toString().equalsIgnoreCase("YES"))
            {
                ssnNumbersDTOList.setIsSss("1");
                tableData.setIs_sss("true");
            }
            else {
                ssnNumbersDTOList.setIsSss("0");
                tableData.setIs_sss("false");

            }

        ssnNumbersDTOList.setSssTypeUuid(sssTypeid);
        tableData.setSssTypeId(sssTypeid);
           /* if(sBeforeYear_Fields.get(i).getText().toString().equalsIgnoreCase("YES"))
            {
                ssnNumbersDTOList.setIsBefore("1");
                tableData.setIs_before_2019("true");
            }
            else if(sBeforeYear_Fields.get(i).getText().toString().equalsIgnoreCase("NO")) {
                ssnNumbersDTOList.setIsBefore("1");
                tableData.setIs_before_2019("false");
            }
            else {

            }
*/
            ssnNumbersDTOList.setIsBefore("0");
            if(sPmsby_Fields.get(i).getText().toString().equalsIgnoreCase("YES"))
            {
                ssnNumbersDTOList.setIsSss("1");
                tableData.setIs_sss("true");
                ssnNumbersDTOLists.add(ssnNumbersDTOList);
            }
            else if(sPmsby_Fields.get(i).getText().toString().equalsIgnoreCase("NO")) {
                ssnNumbersDTOList.setIsSss("1");
                tableData.setIs_sss("false");
            }
            else {

            }


            tableDataArrayList.add(tableData);

        }

        SsnNumberRequestDTOList ssnNumberRequestDTOList  = new SsnNumberRequestDTOList();
        ssnNumberRequestDTOList.setSsnNumbersDTOList(ssnNumbersDTOLists);

        String checkData = new Gson().toJson(ssnNumberRequestDTOList);
        Log.d("NumbersDTOLists", " " + checkData);

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();

            RestClient.getRestClient(this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_SSN_NUMBER, checkData, getActivity(), ServiceType.OTHER_MICRO_INSURANCE);
        }
    }

//    private void otherMicroInsurancepostApiCall() {
//
//        confirmationDialog.dismiss();
//        ssnNumbersDTOLists =new ArrayList<>();
//        tableDataArrayList =new ArrayList<>();
//        for (int i = 0; i < memList.size() ; i++) {
//            SsnNumbersDTOList ssnNumbersDTOList = new SsnNumbersDTOList();
//            tableData =new TableData();
//            ssnNumbersDTOList.setShgId(memList.get(i).getShgId());
//            tableData.setShgId(memList.get(i).getShgId());
//            ssnNumbersDTOList.setUserId(memList.get(i).getMemberUserId());
//            ssnNumbersDTOList.setMemberId(memList.get(i).getMemberId());
//            tableData.setMemberId(memList.get(i).getMemberId());
//
//            if(sPmsbyNumber_Fields.get(i).getText().toString().isEmpty())
//            {
//                ssnNumbersDTOList.setSss_number("0");
//                tableData.setSss_number("0");
//            }
//            else
//            {
//                ssnNumbersDTOList.setSss_number(sPmsbyNumber_Fields.get(i).getText().toString());
//                tableData.setSss_number(sPmsbyNumber_Fields.get(i).getText().toString());
//            }
//
////            ssnNumbersDTOList.setSssDte(smEntroll_Fields.get(i).getText().toString());
////            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");
//            Date date = null;
//            long mills=0;
//            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//            Log.d("dte",smEntroll_Fields.get(i).getText().toString());
//            try {
//                if ((!smEntroll_Fields.get(i).getText().toString().equals("0"))&&(!smEntroll_Fields.get(i).getText().toString().equals("")))
//                {
//                    date = dateFormat.parse(smEntroll_Fields.get(i).getText().toString());
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            if(date!=null)
//            {
//                 mills = date.getTime();
//
//            }
//
//          if(mills!=0)
//          {
//          tableData.setSss_date(mills);
//           }
//
//            DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
//            Date d = new Date(mills);
//            String dateStr = dateFormat1.format(d);
//            ssnNumbersDTOList.setSssDte(dateStr);
//
//            ssnNumbersDTOList.setSssTypeUuid(o_mi_ssstypeid);
//            tableData.setSssTypeId(o_mi_ssstypeid);
//            if(sPmsby_Fields.get(i).getText().toString().equalsIgnoreCase("YES"))
//            {
//                ssnNumbersDTOList.setIsSss("1");
//                tableData.setIs_sss("true");
//            }
//            else {
//                ssnNumbersDTOList.setIsSss("0");
//                tableData.setIs_sss("false");
//
//            }
//
//            ssnNumbersDTOLists.add(ssnNumbersDTOList);
//            tableDataArrayList.add(tableData);
//
//        }
//
//        SsnNumberRequestDTOList ssnNumberRequestDTOList  = new SsnNumberRequestDTOList();
//        ssnNumberRequestDTOList.setSsnNumbersDTOList(ssnNumbersDTOLists);
//
//        String checkData = new Gson().toJson(ssnNumberRequestDTOList);
//        Log.d("NumbersDTOLists", " " + checkData);
//
//        if (networkConnection.isNetworkAvailable()) {
//            onTaskStarted();
//            RestClient.getRestClient(this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_SSN_NUMBER, checkData, getActivity(), ServiceType.OTHER_MICRO_INSURANCE);
//        }
//    }

    private void showDialog() {

        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 5, 10, 5);


        TableRow header_row = new TableRow(getActivity());

        TableRow.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        headerParams.setMargins(10, 5, 10, 5);

        TextView bankName_header = new TextView(getActivity());
        bankName_header.setText(AppStrings.memberName);
        bankName_header.setTypeface(LoginActivity.sTypeface);
        bankName_header.setTextColor(R.color.black_original);
        bankName_header.setPadding(5, 5, 5, 5);
        bankName_header.setLayoutParams(headerParams);
        header_row.addView(bankName_header);

        TextView branchName_header = new TextView(getActivity());
        branchName_header.setText(AppStrings.mInsurance);
        branchName_header.setTypeface(LoginActivity.sTypeface);
        branchName_header.setTextColor(R.color.black_original);
        branchName_header.setPadding(5, 5, 5, 5);
        branchName_header.setLayoutParams(headerParams);
        header_row.addView(branchName_header);

        TextView accNo_header = new TextView(getActivity());
        accNo_header.setText(AppStrings.mPolicyNumber);
        accNo_header.setTypeface(LoginActivity.sTypeface);
        accNo_header.setTextColor(R.color.black_original);
        accNo_header.setPadding(5, 5, 5, 5);
        accNo_header.setLayoutParams(headerParams);
        header_row.addView(accNo_header);


        TextView mDateenroll = new TextView(getActivity());
        mDateenroll.setText(AppStrings.mDateenroll);
        mDateenroll.setTypeface(LoginActivity.sTypeface);
        mDateenroll.setTextColor(R.color.black_original);
        mDateenroll.setPadding(5, 5, 5, 5);
        mDateenroll.setLayoutParams(headerParams);
        header_row.addView(mDateenroll);

        confirmationTable.addView(header_row,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i < datavalues.size(); i++) {

            TableRow indv_SavingsRow = new TableRow(getActivity());

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(GetSpanText.getSpanString(getActivity(), datavalues.get(i).getMemberName()));
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sChoose_Pmsby[i])));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_SavingsRow.addView(confirm_values);

            TextView POL_values = new TextView(getActivity());
            POL_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPolicy_Number[i])));
            POL_values.setTextColor(R.color.black);
            POL_values.setPadding(5, 5, 5, 5);
            POL_values.setGravity(Gravity.RIGHT);
            POL_values.setLayoutParams(contentParams);
            indv_SavingsRow.addView(POL_values);

            TextView dateofentroll = new TextView(getActivity());
            dateofentroll.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sDateofentroll[i])));
            dateofentroll.setTextColor(R.color.black);
            dateofentroll.setPadding(5, 5, 5, 5);
            dateofentroll.setGravity(Gravity.RIGHT);
            dateofentroll.setLayoutParams(contentParams);
            indv_SavingsRow.addView(dateofentroll);

            confirmationTable.addView(indv_SavingsRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }
        View rullerView = new View(getActivity());
        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
        // 229,
        // 242));
        confirmationTable.addView(rullerView);

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText("" + AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText("" + AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

//        mTotalDisbursement = sPL_total;
//        Log.e("PL Total Valuesssss", mTotalDisbursement + "");

        confirmationDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        try {

            switch (serviceType)
            {
              /*  case GETSSSTYPEIDFETCHING:

                    sssTypeDtos =new ArrayList<>();
                    JSONObject value1 = new JSONObject(result);
                     if((value1.getString("statusCode")).equals("200"))
                {
                    JSONArray jsonArray = value1.getJSONArray("responseContents");
                    for (int x = 0; x < jsonArray.length(); x++) {
                        SssTypeDto sssTypeDto =new SssTypeDto();
                        JSONObject JO = jsonArray.getJSONObject(x);
                        String name = JO.getString("name");
                        sssTypeDto.setName(name);
                        String createdDate = JO.getString("createdDate");
                        sssTypeDto.setCreatedDate(createdDate);
                        String modifiedDate = JO.getString("modifiedDate");
                        sssTypeDto.setModifiedDate(modifiedDate);
                        String status = JO.getString("status");
                        sssTypeDto.setStatus(status);
                        String pflag = JO.getString("pflag");
                        sssTypeDto.setPflag(pflag);
                        String sflag = JO.getString("sflag");
                        sssTypeDto.setSflag(sflag);
                        String id = JO.getString("id");
                        sssTypeDto.setId(id);
                        sssTypeDtos.add(sssTypeDto);
                    }

                    for (int x = 0; x < sssTypeDtos.size() ; x++) {

                        String o_miValue = sssTypeDtos.get(x).getName();

                        if(o_miValue .equalsIgnoreCase("OMI"))
                        {
                            o_mi_ssstypeid = sssTypeDtos.get(x).getId();
                        }

                    }
                    datavalues = SHGTable.getSSNData(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID,""),o_mi_ssstypeid);
                    Log.e("val",datavalues+"");
                    inIt(view);

                }
                else
                {
                    if ((value1.getString("statusCode")).equals("401")) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    }
                    if (confirmationDialog.isShowing()) {
                        confirmationDialog.dismiss();
                    }
                    Utils.showToast(getActivity(),value1.getString("message"));
                }
                break;*/

                case OTHER_MICRO_INSURANCE:

                    try {
                        ResponseDto cdto = new Gson().fromJson(result, ResponseDto.class);

                        if (cdto != null) {
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                         int size = datavalues.size();
                            for (int i = 0; i <datavalues.size() ; i++) {

                                SHGTable.updateSsnDetails1(tableDataArrayList.get(i));

                            }

                                TastyToast.makeText(getActivity(),message,TastyToast.LENGTH_LONG,TastyToast.SUCCESS);
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Profile",MainFragment.Flag_Profile);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);

                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);
                        }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
