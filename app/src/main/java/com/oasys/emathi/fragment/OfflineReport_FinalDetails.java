package com.oasys.emathi.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.emathi.Adapter.CustomItemAdapter;
import com.oasys.emathi.Dto.ExpensesTypeDtoList;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.RegionalConversion;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.ExpenseTable;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.model.RowItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OfflineReport_FinalDetails extends Fragment {

    private View view;
    private String dateTimeStr, txType, subType;


    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
    private TableLayout tableLayout, headerTable;
    private TableRow headerRow;
    ListView mListView;
    List<RowItem> rowItems;
    CustomItemAdapter mAdapter;

    private TextView mCashinHand;
    private TextView mCashatBank;

    String getResponse;
    String amount;
    String interest;
    private static String[] responseArr, response$Arr;
    private static String[] amountsArr;
    private static String[] interestArr;
    public static int[] sumAmount;
    public static int[] sumInterest;
    public static String[] minutesId, minutesName, responseId;
    String[] listItems;
    private String[] dateArr;
    private String transDate;
    private String mLanguagelocale = "";
    private String[] sDepositItems;
    private int otheIncomeAmount = 0;
    String selectedType;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<OfflineDto> reportOffline;
    DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
    private List<ExpensesTypeDtoList> expList;
    private ArrayList<OfflineDto> seedFund, btReport, grpReport;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                dateTimeStr = bundle.getString("date");
                txType = bundle.getString("txtype");
                subType = bundle.getString("subType");
                expList = ExpenseTable.getExpList("Expense Type");
                reportOffline = TransactionTable.getOfflineFinalReport(shgDto.getShgId(), dateTimeStr, txType);
                memList = TransactionTable.getOfflineFinalUniqueReport(shgDto.getShgId(), dateTimeStr, txType);
                memList = TransactionTable.getOfflineFinalUniqueReport(shgDto.getShgId(), dateTimeStr, txType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        view = inflater.inflate(R.layout.offline_new_summary, container, false);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {

        try {
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            mHeadertext = (TextView) view.findViewById(R.id.offline_headertext);
            mHeadertext.setTypeface(LoginActivity.sTypeface);


            headerTable = (TableLayout) view.findViewById(R.id.summaryHeaderTable);
            tableLayout = (TableLayout) view.findViewById(R.id.summaryTable);

            headerRow = new TableRow(getActivity());
            headerRow.setBackgroundResource(R.color.tableHeader);
            TableRow.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            headerRow.setLayoutParams(headerParams);

            mListView = (ListView) view.findViewById(R.id.offlineListView);
            rowItems = new ArrayList<RowItem>();

            mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
                    + RegionalConversion.getRegionalConversion(AppStrings.of)
                    + RegionalConversion.getRegionalConversion(subType)
                    + " ON " + simple.format(new Date(Long.parseLong(dateTimeStr))));

            //Header layout design

            if (txType.equals(NewDrawerScreen.SAVINGS) || txType.equals(NewDrawerScreen.LOAN_DISBURSEMENT) || subType.equals(AppStrings.subscriptioncharges) || subType.equals(AppStrings.penalty) || subType.equals(AppStrings.otherincome)) {
                TextView memName = new TextView(getActivity());


                memName.setText(
                        RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));

                memName.setTypeface(LoginActivity.sTypeface);
                memName.setPadding(60, 5, 10, 5);
                memName.setTextColor(Color.WHITE);
                memName.setLayoutParams(headerParams);
                headerRow.addView(memName);

                TextView amount = new TextView(getActivity());
                amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
                amount.setTypeface(LoginActivity.sTypeface);
                amount.setTextColor(Color.WHITE);
                amount.setPadding(10, 5, 60, 5);
                amount.setGravity(Gravity.RIGHT);
                amount.setLayoutParams(headerParams);
                headerRow.addView(amount);

                headerTable.addView(headerRow);// ,
            } else {
                TextView memName = new TextView(getActivity());
                if (txType.equals(NewDrawerScreen.EXPENCE)) {
                    memName.setText(
                            RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.expenses)));

                    memName.setTypeface(LoginActivity.sTypeface);
                    memName.setPadding(60, 5, 10, 5);
                    memName.setTextColor(Color.WHITE);
                    memName.setLayoutParams(headerParams);
                    headerRow.addView(memName);

                    TextView amount = new TextView(getActivity());
                    amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
                    amount.setTypeface(LoginActivity.sTypeface);
                    amount.setTextColor(Color.WHITE);
                    amount.setPadding(10, 5, 60, 5);
                    amount.setGravity(Gravity.RIGHT);
                    amount.setLayoutParams(headerParams);
                    headerRow.addView(amount);

                    headerTable.addView(headerRow);// ,
                } else if (txType.equals(NewDrawerScreen.MEMBER_LOAN_REPAYMENT)) {
                    memName.setText(
                            RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));

                    memName.setTypeface(LoginActivity.sTypeface);
                    memName.setPadding(20, 5, 10, 5);
                    memName.setTextColor(Color.WHITE);
                    memName.setLayoutParams(headerParams);
                    headerRow.addView(memName);

                    TextView amount = new TextView(getActivity());
                    amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
                    amount.setTypeface(LoginActivity.sTypeface);
                    amount.setTextColor(Color.WHITE);
                    amount.setPadding(10, 5, 60, 5);
                    amount.setGravity(Gravity.RIGHT);
                    amount.setLayoutParams(headerParams);
                    headerRow.addView(amount);
                    TextView intr = new TextView(getActivity());
                    intr.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interest)));
                    intr.setTypeface(LoginActivity.sTypeface);
                    intr.setTextColor(Color.WHITE);
                    intr.setPadding(10, 5, 60, 5);
                    intr.setGravity(Gravity.RIGHT);
                    intr.setLayoutParams(headerParams);
                    headerRow.addView(intr);

                    headerTable.addView(headerRow);// ,
                }


            }


            //Child layout design

            if (txType.equals(NewDrawerScreen.SAVINGS) || txType.equals(NewDrawerScreen.LOAN_DISBURSEMENT)  || subType.equals(AppStrings.subscriptioncharges) || subType.equals(AppStrings.penalty) || subType.equals(AppStrings.otherincome)) {

                for (int i1 = 0; i1 < mSize; i1++) {

                    TableRow indv_Row = new TableRow(getActivity());
                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                            TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    TextView memberName_Text = new TextView(getActivity());
                    memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                            String.valueOf(memList.get(i1).getMemberName())));
                    memberName_Text.setTypeface(LoginActivity.sTypeface);
                    memberName_Text.setTextColor(R.color.black);
                    memberName_Text.setPadding(50, 5, 10, 5);
                    // memberName_Text.setLayoutParams(contentParams);
                    indv_Row.addView(memberName_Text, contentParams);

                    TextView confirm_values = new TextView(getActivity());
                    if (txType.equals("SAVINGS")) {
                        if (subType.equals("SAVINGS")) {
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOfflineFinalUniqueReport(memList.get(i1).getMemberId()).getSavingsAmount() != null && TransactionTable.getOfflineFinalUniqueReport(memList.get(i1).getMemberId()).getSavingsAmount().length() > 0) ? TransactionTable.getOfflineFinalUniqueReport(memList.get(i1).getMemberId()).getSavingsAmount() : "0"));
                        } else if (subType.equals("VOLUNTARY SAVINGS")) {
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOfflineFinalUniqueReport(memList.get(i1).getMemberId()).getVoluntarySavingsAmount() != null && TransactionTable.getOfflineFinalUniqueReport(memList.get(i1).getMemberId()).getVoluntarySavingsAmount().length() > 0) ? TransactionTable.getOfflineFinalUniqueReport(memList.get(i1).getMemberId()).getVoluntarySavingsAmount() : "0"));
                        }
                    } else if (txType.equals(NewDrawerScreen.INCOME)) {
                        if (subType.equals(AppStrings.subscriptioncharges) || subType.equals(AppStrings.penalty) || subType.equals(AppStrings.otherincome)) {
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(), subType).getAmount() != null && TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(), subType).getAmount().length() > 0) ? TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(), subType).getAmount() : "0"));
                        }/* else if (subType.equals(AppStrings.penalty)) {
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(),subType).getAmount() != null && TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(),subType).getAmount().length() > 0) ? TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(),subType).getAmount() : "0"));
                            } else if (subType.equals(AppStrings.otherincome)) {
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(),subType).getAmount() != null && TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(),subType).getAmount().length() > 0) ? TransactionTable.getOffFinalIncomeReport(memList.get(i1).getMemberId(),subType).getAmount() : "0"));
                            }*/
                    }else if(txType.equals(NewDrawerScreen.LOAN_DISBURSEMENT)){
                        confirm_values
                                .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOfflineLoanDisbursement(memList.get(i1).getMemberId()).getAmount() != null && TransactionTable.getOfflineLoanDisbursement(memList.get(i1).getMemberId()).getAmount().length() > 0) ? TransactionTable.getOfflineLoanDisbursement(memList.get(i1).getMemberId()).getAmount() : "0"));
                    }
                    confirm_values.setTextColor(R.color.black);
                    confirm_values.setPadding(10, 5, 60, 5);
                    confirm_values.setGravity(Gravity.RIGHT);
                    // memberName_Text.setLayoutParams(contentParams);
                    indv_Row.addView(confirm_values, contentParams);

                    tableLayout.addView(indv_Row,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    View rullerView = new View(getActivity());
                    rullerView
                            .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                    rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                    tableLayout.addView(rullerView);
                }
                if (subType.equals(AppStrings.otherincome)) {

                    TableRow indv_Row = new TableRow(getActivity());
                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                            TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    TextView memberName_Text = new TextView(getActivity());
                    memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                            String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.mOthers))));


                    memberName_Text.setTextColor(R.color.black);
                    memberName_Text.setPadding(50, 5, 10, 5);
                    // memberName_Text.setLayoutParams(contentParams);
                    indv_Row.addView(memberName_Text, contentParams);

                    TextView confirm_values = new TextView(getActivity());
                    confirm_values
                            .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalIncomeOthersReport(subType).getAmount() != null && TransactionTable.getOffFinalIncomeOthersReport(subType).getAmount().length() > 0) ? TransactionTable.getOffFinalIncomeOthersReport(subType).getAmount() : "0"));//String.valueOf(responseArr[responseArr.length-1])));
                    confirm_values.setTextColor(R.color.black);
                    confirm_values.setPadding(10, 5, 60, 5);
                    confirm_values.setGravity(Gravity.RIGHT);
                    // memberName_Text.setLayoutParams(contentParams);
                    indv_Row.addView(confirm_values, contentParams);

                    tableLayout.addView(indv_Row,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    View rullerView = new View(getActivity());
                    rullerView
                            .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                    rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                    tableLayout.addView(rullerView);

                }
            } else {
                if (txType.equals(NewDrawerScreen.EXPENCE)) {
                    mSize = expList.size();
                    for (int i1 = 0; i1 < mSize; i1++) {

                        TableRow indv_Row = new TableRow(getActivity());
                        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                        contentParams.setMargins(10, 5, 10, 5);

                        TextView memberName_Text = new TextView(getActivity());
                        memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                String.valueOf(expList.get(i1).getName())));
                        memberName_Text.setTypeface(LoginActivity.sTypeface);
                        memberName_Text.setTextColor(R.color.black);
                        memberName_Text.setPadding(50, 5, 10, 5);
                        // memberName_Text.setLayoutParams(contentParams);
                        indv_Row.addView(memberName_Text, contentParams);

                        TextView confirm_values = new TextView(getActivity());
                        confirm_values
                                .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOfflineFinalEXP_UniqueReport(expList.get(i1).getId()).getAmount() != null && TransactionTable.getOfflineFinalEXP_UniqueReport(expList.get(i1).getId()).getAmount().length() > 0) ? TransactionTable.getOfflineFinalEXP_UniqueReport(expList.get(i1).getId()).getAmount() : "0"));
                        confirm_values.setTextColor(R.color.black);
                        confirm_values.setPadding(10, 5, 60, 5);
                        confirm_values.setGravity(Gravity.RIGHT);
                        // memberName_Text.setLayoutParams(contentParams);
                        indv_Row.addView(confirm_values, contentParams);

                        tableLayout.addView(indv_Row,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        View rullerView = new View(getActivity());
                        rullerView
                                .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                        rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                        tableLayout.addView(rullerView);

                    }
                } else if (txType.equals(NewDrawerScreen.INCOME)) {
                    if (subType.equals(AppStrings.mSeedFund)) {

                        seedFund = TransactionTable.getOfflineFinalSF_Report(shgDto.getShgId(), dateTimeStr, txType, subType);
                        for (int i = 0; i < seedFund.size(); i++) {
                            TableRow indv_Row = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView type_Text = new TextView(getActivity());
                            type_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                            type_Text.setTypeface(LoginActivity.sTypeface);
                            type_Text.setPadding(10, 5, 10, 5);
                            type_Text.setTextColor(R.color.black);
                            type_Text.setLayoutParams(contentParams);
                            indv_Row.addView(type_Text);

                            TextView type_Text1 = new TextView(getActivity());

                            if (seedFund.get(i).getModeOCash() != null && seedFund.get(i).getModeOCash().equals("1")) {
                                type_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("BANK")));
                            } else {
                                type_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("CASH")));
                            }
                            type_Text1.setTypeface(LoginActivity.sTypeface);
                            type_Text1.setPadding(10, 5, 10, 5);
                            type_Text1.setTextColor(R.color.black);
                            type_Text1.setLayoutParams(contentParams);
                            indv_Row.addView(type_Text1);

                            tableLayout.addView(indv_Row,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                            TableRow indv_Row1 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView amount_Text = new TextView(getActivity());
                            amount_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.amount)));
                            amount_Text.setTypeface(LoginActivity.sTypeface);
                            amount_Text.setTextColor(R.color.black);
                            amount_Text.setLayoutParams(contentParams1);
                            amount_Text.setPadding(10, 5, 10, 5);
                            indv_Row1.addView(amount_Text);

                            TextView amount_Text1 = new TextView(getActivity());
                            amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(seedFund.get(i).getOtherIncome())));
                            amount_Text1.setTypeface(LoginActivity.sTypeface);
                            amount_Text1.setTextColor(R.color.black);
                            amount_Text1.setPadding(10, 5, 10, 5);
                            amount_Text1.setLayoutParams(contentParams1);
                            indv_Row1.addView(amount_Text1);

                            tableLayout.addView(indv_Row1,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                            //   Log.e("Bank name size", mBankNameArr.length + "");
                            if (seedFund.get(i).getModeOCash().equals("1")) {

                                TableRow indv_Row2 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView bankName_Text = new TextView(getActivity());
                                bankName_Text.setText(
                                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
                                bankName_Text.setTypeface(LoginActivity.sTypeface);
                                bankName_Text.setTextColor(R.color.black);
                                bankName_Text.setPadding(10, 5, 10, 5);
                                bankName_Text.setLayoutParams(contentParams2);
                                indv_Row2.addView(bankName_Text);

                                TextView bankName_Text1 = new TextView(getActivity());
                                bankName_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(seedFund.get(i).getSSelectedBank())));
                                bankName_Text1.setTypeface(LoginActivity.sTypeface);
                                bankName_Text1.setPadding(10, 5, 10, 5);
                                bankName_Text1.setLayoutParams(contentParams2);
                                bankName_Text1.setTextColor(R.color.black);
                                indv_Row2.addView(bankName_Text1);

                                tableLayout.addView(indv_Row2,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }
                            View rullerView2 = new View(getActivity());
                            rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView2);

                        }


                    } else if (subType.equals(AppStrings.donation)) {


                        TableRow indv_row = new TableRow(getActivity());
                        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        contentParams.setMargins(10, 5, 10, 5);

                        TextView donation = new TextView(getActivity());
                        donation.setText(GetSpanText.getSpanString(getActivity(),
                                String.valueOf(AppStrings.donation)));
                        donation.setTypeface(LoginActivity.sTypeface);
                        donation.setTextColor(R.color.black);
                        donation.setPadding(10, 5, 10, 5);
                        donation.setGravity(Gravity.CENTER_HORIZONTAL);
                        donation.setLayoutParams(contentParams);
                        indv_row.addView(donation);

                        TextView confirm_values = new TextView(getActivity());
                        confirm_values.setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOfflineFinalDON_Report(shgDto.getShgId(), dateTimeStr, txType, subType).getAmount() != null && TransactionTable.getOfflineFinalDON_Report(shgDto.getShgId(), dateTimeStr, txType, subType).getAmount().length() > 0) ? TransactionTable.getOfflineFinalDON_Report(shgDto.getShgId(), dateTimeStr, txType, subType).getAmount() : "0"));
                        confirm_values.setTextColor(R.color.black);
                        confirm_values.setPadding(10, 5, 50, 5);
                        confirm_values.setGravity(Gravity.CENTER_HORIZONTAL);
                        donation.setLayoutParams(contentParams);
                        indv_row.addView(confirm_values);

                        tableLayout.addView(indv_row,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        View rullerView = new View(getActivity());
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                        rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                        tableLayout.addView(rullerView);


                    }

                } else if (txType.equals(NewDrawerScreen.BANK_TRANSACTION)) {
                    btReport = TransactionTable.getOfflineFinalBT_Report(shgDto.getShgId(), dateTimeStr, txType, subType);
                    if (subType.equals(AppStrings.bankTransaction)) {
                        for (int i = 0; i < btReport.size(); i++) {

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                            /*    TableRow indv_Row1 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text = new TextView(getActivity());
                                amount_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mCashDeposit)));
                                amount_Text.setTypeface(LoginActivity.sTypeface);
                                amount_Text.setTextColor(R.color.black);
                                amount_Text.setLayoutParams(contentParams1);
                                amount_Text.setPadding(10, 5, 10, 5);
                                indv_Row1.addView(amount_Text);

                                TextView amount_Text1 = new TextView(getActivity());
                                amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getCashDeposit())));
                                amount_Text1.setTypeface(LoginActivity.sTypeface);
                                amount_Text1.setTextColor(R.color.black);
                                amount_Text1.setPadding(10, 5, 10, 5);
                                amount_Text1.setLayoutParams(contentParams1);
                                indv_Row1.addView(amount_Text1);

                                tableLayout.addView(indv_Row1,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    */
                            TableRow indv_Row2 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView d_Text = new TextView(getActivity());
                            d_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankDeposit)));
                            d_Text.setTypeface(LoginActivity.sTypeface);
                            d_Text.setTextColor(R.color.black);
                            d_Text.setLayoutParams(contentParams2);
                            d_Text.setPadding(10, 5, 10, 5);
                            indv_Row2.addView(d_Text);

                            TextView d_Text1 = new TextView(getActivity());
                            d_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtDeposit())));
                            d_Text1.setTypeface(LoginActivity.sTypeface);
                            d_Text1.setTextColor(R.color.black);
                            d_Text1.setPadding(10, 5, 10, 5);
                            d_Text1.setLayoutParams(contentParams2);
                            indv_Row2.addView(d_Text1);

                            tableLayout.addView(indv_Row2,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row3 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView w_Text = new TextView(getActivity());
                            w_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.withdrawl)));
                            w_Text.setTypeface(LoginActivity.sTypeface);
                            w_Text.setTextColor(R.color.black);
                            w_Text.setLayoutParams(contentParams3);
                            w_Text.setPadding(10, 5, 10, 5);
                            indv_Row3.addView(w_Text);

                            TextView w_Text1 = new TextView(getActivity());
                            w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtWithdraw())));
                            w_Text1.setTypeface(LoginActivity.sTypeface);
                            w_Text1.setTextColor(R.color.black);
                            w_Text1.setPadding(10, 5, 10, 5);
                            w_Text1.setLayoutParams(contentParams3);
                            indv_Row3.addView(w_Text1);

                            tableLayout.addView(indv_Row3,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row4 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView bt_charge = new TextView(getActivity());
                            bt_charge
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankCharges)));
                            bt_charge.setTypeface(LoginActivity.sTypeface);
                            bt_charge.setTextColor(R.color.black);
                            bt_charge.setLayoutParams(contentParams4);
                            bt_charge.setPadding(10, 5, 10, 5);
                            indv_Row4.addView(bt_charge);

                            TextView bt_charge1 = new TextView(getActivity());
                            bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtCharge())));
                            bt_charge1.setTypeface(LoginActivity.sTypeface);
                            bt_charge1.setTextColor(R.color.black);
                            bt_charge1.setPadding(10, 5, 10, 5);
                            bt_charge1.setLayoutParams(contentParams4);
                            indv_Row4.addView(bt_charge1);

                            tableLayout.addView(indv_Row4,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row5 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams5 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView int_charge = new TextView(getActivity());
                            int_charge.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.interestSubvention)));
                            int_charge.setTypeface(LoginActivity.sTypeface);
                            int_charge.setTextColor(R.color.black);
                            int_charge.setLayoutParams(contentParams5);
                            int_charge.setPadding(10, 5, 10, 5);
                            indv_Row5.addView(int_charge);

                            TextView int_charge1 = new TextView(getActivity());
                            int_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBt_intSubventionRecieved())));
                            int_charge1.setTypeface(LoginActivity.sTypeface);
                            int_charge1.setTextColor(R.color.black);
                            int_charge1.setPadding(10, 5, 10, 5);
                            int_charge1.setLayoutParams(contentParams5);
                            indv_Row5.addView(int_charge1);

                            tableLayout.addView(indv_Row5,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                            //   Log.e("Bank name size", mBankNameArr.length + "");

                            TableRow indv_Row6 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams6 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView int_bankinterest = new TextView(getActivity());
                            int_bankinterest.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankinterest)));
                            int_bankinterest.setTypeface(LoginActivity.sTypeface);
                            int_bankinterest.setTextColor(R.color.black);
                            int_bankinterest.setLayoutParams(contentParams6);
                            int_bankinterest.setPadding(10, 5, 10, 5);
                            indv_Row6.addView(int_bankinterest);

                            TextView int_bankinterest1 = new TextView(getActivity());
                            int_bankinterest1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtInterest())));
                            int_bankinterest1.setTypeface(LoginActivity.sTypeface);
                            int_bankinterest1.setTextColor(R.color.black);
                            int_bankinterest1.setPadding(10, 5, 10, 5);
                            int_bankinterest1.setLayoutParams(contentParams6);
                            indv_Row6.addView(int_bankinterest1);

                            tableLayout.addView(indv_Row6,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));



                            View rullerView2 = new View(getActivity());
                            rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView2);

                        }
                    } else if (subType.equals(AppStrings.fixedDeposit)) {
                        for (int i = 0; i < btReport.size(); i++) {
                            TableRow indv_Row = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView type_Text = new TextView(getActivity());
                            type_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankDeposit)));
                            type_Text.setTypeface(LoginActivity.sTypeface);
                            type_Text.setPadding(10, 5, 10, 5);
                            type_Text.setTextColor(R.color.black);
                            type_Text.setLayoutParams(contentParams);
                            indv_Row.addView(type_Text);

                            TextView type_Text1 = new TextView(getActivity());
                            type_Text1.setText(btReport.get(i).getBtDeposit());
                            type_Text1.setTextColor(R.color.black);
                            type_Text1.setTypeface(LoginActivity.sTypeface);
                            type_Text1.setPadding(10, 5, 10, 5);
                            type_Text1.setTextColor(R.color.black);
                            type_Text1.setLayoutParams(contentParams);
                            indv_Row.addView(type_Text1);

                            tableLayout.addView(indv_Row,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                            TableRow indv_Row1 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView amount_Text = new TextView(getActivity());
                            amount_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankInterest)));
                            amount_Text.setTypeface(LoginActivity.sTypeface);
                            amount_Text.setTextColor(R.color.black);
                            amount_Text.setLayoutParams(contentParams1);
                            amount_Text.setPadding(10, 5, 10, 5);
                            indv_Row1.addView(amount_Text);

                            TextView amount_Text1 = new TextView(getActivity());
                            amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtInterest())));
                            amount_Text1.setTypeface(LoginActivity.sTypeface);
                            amount_Text1.setTextColor(R.color.black);
                            amount_Text1.setPadding(10, 5, 10, 5);
                            amount_Text1.setLayoutParams(contentParams1);
                            indv_Row1.addView(amount_Text1);

                            tableLayout.addView(indv_Row1,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            TableRow indv_Row3 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView w_Text = new TextView(getActivity());
                            w_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.withdrawl)));
                            w_Text.setTypeface(LoginActivity.sTypeface);
                            w_Text.setTextColor(R.color.black);
                            w_Text.setLayoutParams(contentParams3);
                            w_Text.setPadding(10, 5, 10, 5);
                            indv_Row3.addView(w_Text);

                            TextView w_Text1 = new TextView(getActivity());
                            w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtWithdraw())));
                            w_Text1.setTypeface(LoginActivity.sTypeface);
                            w_Text1.setTextColor(R.color.black);
                            w_Text1.setPadding(10, 5, 10, 5);
                            w_Text1.setLayoutParams(contentParams3);
                            indv_Row3.addView(w_Text1);

                            tableLayout.addView(indv_Row3,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row4 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView bt_charge = new TextView(getActivity());
                            bt_charge
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankExpenses)));
                            bt_charge.setTypeface(LoginActivity.sTypeface);
                            bt_charge.setTextColor(R.color.black);
                            bt_charge.setLayoutParams(contentParams4);
                            bt_charge.setPadding(10, 5, 10, 5);
                            indv_Row4.addView(bt_charge);

                            TextView bt_charge1 = new TextView(getActivity());
                            bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtExpense())));
                            bt_charge1.setTypeface(LoginActivity.sTypeface);
                            bt_charge1.setTextColor(R.color.black);
                            bt_charge1.setPadding(10, 5, 10, 5);
                            bt_charge1.setLayoutParams(contentParams4);
                            indv_Row4.addView(bt_charge1);

                            tableLayout.addView(indv_Row4,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                            //   Log.e("Bank name size", mBankNameArr.length + "");

                            View rullerView2 = new View(getActivity());
                            rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView2);

                        }
                    } else if (subType.equals(AppStrings.recurringDeposit)) {


                        ArrayList<OfflineDto> bankArr, loanArr;
                        bankArr = new ArrayList<>();
                        loanArr = new ArrayList<>();

                        for (int i = 0; i < btReport.size(); i++) {
                            if (btReport.get(i).getBtToLoanId() != null && btReport.get(i).getBtToLoanId().length() > 0) {
                                loanArr.add(btReport.get(i));

                                TableRow indv_Row = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView type_Text = new TextView(getActivity());
                                type_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
                                type_Text.setTypeface(LoginActivity.sTypeface);
                                type_Text.setPadding(10, 5, 10, 5);
                                type_Text.setTextColor(R.color.black);
                                type_Text.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text);

                                TextView type_Text1 = new TextView(getActivity());
                                type_Text1.setText(btReport.get(i).getMFromBk());
                                type_Text1.setTypeface(LoginActivity.sTypeface);
                                type_Text1.setPadding(10, 5, 10, 5);
                                type_Text1.setTextColor(R.color.black);
                                type_Text1.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text1);

                                tableLayout.addView(indv_Row,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                                TableRow indv_Row00 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams01 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView l_Text = new TextView(getActivity());
                                l_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanName)));
                                l_Text.setTypeface(LoginActivity.sTypeface);
                                l_Text.setTextColor(R.color.black);
                                l_Text.setLayoutParams(contentParams01);
                                l_Text.setPadding(10, 5, 10, 5);
                                indv_Row00.addView(l_Text);

                                TextView l_Text1 = new TextView(getActivity());
                                l_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getMLoanName())));
                                l_Text1.setTypeface(LoginActivity.sTypeface);
                                l_Text1.setTextColor(R.color.black);
                                l_Text1.setPadding(10, 5, 10, 5);
                                l_Text1.setLayoutParams(contentParams01);
                                indv_Row00.addView(l_Text1);

                                tableLayout.addView(indv_Row00,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row1 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text = new TextView(getActivity());
                                amount_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanId)));
                                amount_Text.setTypeface(LoginActivity.sTypeface);
                                amount_Text.setTextColor(R.color.black);
                                amount_Text.setLayoutParams(contentParams1);
                                amount_Text.setPadding(10, 5, 10, 5);
                                indv_Row1.addView(amount_Text);

                                TextView amount_Text1 = new TextView(getActivity());
                                amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtToLoanId())));
                                amount_Text1.setTypeface(LoginActivity.sTypeface);
                                amount_Text1.setTextColor(R.color.black);
                                amount_Text1.setPadding(10, 5, 10, 5);
                                amount_Text1.setLayoutParams(contentParams1);
                                indv_Row1.addView(amount_Text1);

                                tableLayout.addView(indv_Row1,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row01 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams00 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView type_Text00 = new TextView(getActivity());
                                type_Text00
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankDeposit)));
                                type_Text00.setTypeface(LoginActivity.sTypeface);
                                type_Text00.setPadding(10, 5, 10, 5);
                                type_Text00.setTextColor(R.color.black);
                                type_Text00.setLayoutParams(contentParams00);
                                indv_Row01.addView(type_Text00);

                                TextView type_Text01 = new TextView(getActivity());
                                type_Text01.setText(btReport.get(i).getBtDeposit());
                                type_Text01.setTypeface(LoginActivity.sTypeface);
                                type_Text01.setPadding(10, 5, 10, 5);
                                type_Text01.setTextColor(R.color.black);
                                type_Text01.setLayoutParams(contentParams00);
                                indv_Row01.addView(type_Text01);

                                tableLayout.addView(indv_Row01,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                                TableRow indv_Row10 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams10 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text0 = new TextView(getActivity());
                                amount_Text0
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankInterest)));
                                amount_Text0.setTypeface(LoginActivity.sTypeface);
                                amount_Text0.setTextColor(R.color.black);
                                amount_Text0.setLayoutParams(contentParams10);
                                amount_Text0.setPadding(10, 5, 10, 5);
                                indv_Row10.addView(amount_Text0);

                                TextView amount_Text11 = new TextView(getActivity());
                                amount_Text11.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtInterest())));
                                amount_Text11.setTypeface(LoginActivity.sTypeface);
                                amount_Text11.setTextColor(R.color.black);
                                amount_Text11.setPadding(10, 5, 10, 5);
                                amount_Text11.setLayoutParams(contentParams10);
                                indv_Row10.addView(amount_Text11);

                                tableLayout.addView(indv_Row10,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                                TableRow indv_Row3 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView w_Text = new TextView(getActivity());
                                w_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.withdrawl)));
                                w_Text.setTypeface(LoginActivity.sTypeface);
                                w_Text.setTextColor(R.color.black);
                                w_Text.setLayoutParams(contentParams3);
                                w_Text.setPadding(10, 5, 10, 5);
                                indv_Row3.addView(w_Text);

                                TextView w_Text1 = new TextView(getActivity());
                                w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtWithdraw())));
                                w_Text1.setTypeface(LoginActivity.sTypeface);
                                w_Text1.setTextColor(R.color.black);
                                w_Text1.setPadding(10, 5, 10, 5);
                                w_Text1.setLayoutParams(contentParams3);
                                indv_Row3.addView(w_Text1);

                                tableLayout.addView(indv_Row3,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row4 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView bt_charge = new TextView(getActivity());
                                bt_charge.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankExpenses)));
                                bt_charge.setTypeface(LoginActivity.sTypeface);
                                bt_charge.setTextColor(R.color.black);
                                bt_charge.setLayoutParams(contentParams4);
                                bt_charge.setPadding(10, 5, 10, 5);
                                indv_Row4.addView(bt_charge);

                                TextView bt_charge1 = new TextView(getActivity());
                                bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtExpense())));
                                bt_charge1.setTypeface(LoginActivity.sTypeface);
                                bt_charge1.setTextColor(R.color.black);
                                bt_charge1.setPadding(10, 5, 10, 5);
                                bt_charge1.setLayoutParams(contentParams4);
                                indv_Row4.addView(bt_charge1);

                                tableLayout.addView(indv_Row4,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                                //   Log.e("Bank name size", mBankNameArr.length + "");

                                View rullerView2 = new View(getActivity());
                                rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView2);


                            } else {
                                bankArr.add(btReport.get(i));

                                TableRow indv_Row = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView type_Text = new TextView(getActivity());
                                type_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
                                type_Text.setTypeface(LoginActivity.sTypeface);
                                type_Text.setPadding(10, 5, 10, 5);
                                type_Text.setTextColor(R.color.black);
                                type_Text.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text);

                                TextView type_Text1 = new TextView(getActivity());
                                type_Text1.setText(btReport.get(i).getSSelectedBank());
                                type_Text1.setTypeface(LoginActivity.sTypeface);
                                type_Text1.setPadding(10, 5, 10, 5);
                                type_Text1.setTextColor(R.color.black);
                                type_Text1.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text1);

                                tableLayout.addView(indv_Row,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                                TableRow indv_Row1 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text = new TextView(getActivity());
                                amount_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
                                amount_Text.setTypeface(LoginActivity.sTypeface);
                                amount_Text.setTextColor(R.color.black);
                                amount_Text.setLayoutParams(contentParams1);
                                amount_Text.setPadding(10, 5, 10, 5);
                                indv_Row1.addView(amount_Text);

                                TextView amount_Text1 = new TextView(getActivity());
                                amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtToSavingAcId())));
                                amount_Text1.setTypeface(LoginActivity.sTypeface);
                                amount_Text1.setTextColor(R.color.black);
                                amount_Text1.setPadding(10, 5, 10, 5);
                                amount_Text1.setLayoutParams(contentParams1);
                                indv_Row1.addView(amount_Text1);

                                tableLayout.addView(indv_Row1,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                                TableRow indv_Row00 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams00 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView type_Text01 = new TextView(getActivity());
                                type_Text01
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankDeposit)));
                                type_Text01.setTypeface(LoginActivity.sTypeface);
                                type_Text01.setPadding(10, 5, 10, 5);
                                type_Text01.setTextColor(R.color.black);
                                type_Text01.setLayoutParams(contentParams00);
                                indv_Row00.addView(type_Text01);

                                TextView type_Text10 = new TextView(getActivity());
                                type_Text10.setText(btReport.get(i).getBtDeposit());
                                type_Text10.setTypeface(LoginActivity.sTypeface);
                                type_Text10.setPadding(10, 5, 10, 5);
                                type_Text10.setTextColor(R.color.black);
                                type_Text10.setLayoutParams(contentParams00);
                                indv_Row00.addView(type_Text10);

                                tableLayout.addView(indv_Row00,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                                TableRow indv_Row11 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams12 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text12 = new TextView(getActivity());
                                amount_Text12
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankInterest)));
                                amount_Text12.setTypeface(LoginActivity.sTypeface);
                                amount_Text12.setTextColor(R.color.black);
                                amount_Text12.setLayoutParams(contentParams12);
                                amount_Text12.setPadding(10, 5, 10, 5);
                                indv_Row11.addView(amount_Text12);

                                TextView amount_Text13 = new TextView(getActivity());
                                amount_Text13.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtInterest())));
                                amount_Text13.setTypeface(LoginActivity.sTypeface);
                                amount_Text13.setTextColor(R.color.black);
                                amount_Text13.setPadding(10, 5, 10, 5);
                                amount_Text13.setLayoutParams(contentParams12);
                                indv_Row11.addView(amount_Text13);

                                tableLayout.addView(indv_Row11,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                                TableRow indv_Row3 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView w_Text = new TextView(getActivity());
                                w_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.withdrawl)));
                                w_Text.setTypeface(LoginActivity.sTypeface);
                                w_Text.setTextColor(R.color.black);
                                w_Text.setLayoutParams(contentParams3);
                                w_Text.setPadding(10, 5, 10, 5);
                                indv_Row3.addView(w_Text);

                                TextView w_Text1 = new TextView(getActivity());
                                w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtWithdraw())));
                                w_Text1.setTypeface(LoginActivity.sTypeface);
                                w_Text1.setTextColor(R.color.black);
                                w_Text1.setPadding(10, 5, 10, 5);
                                w_Text1.setLayoutParams(contentParams3);
                                indv_Row3.addView(w_Text1);

                                tableLayout.addView(indv_Row3,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row4 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView bt_charge = new TextView(getActivity());
                                bt_charge.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankExpenses)));
                                bt_charge.setTypeface(LoginActivity.sTypeface);
                                bt_charge.setTextColor(R.color.black);
                                bt_charge.setLayoutParams(contentParams4);
                                bt_charge.setPadding(10, 5, 10, 5);
                                indv_Row4.addView(bt_charge);

                                TextView bt_charge1 = new TextView(getActivity());
                                bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtExpense())));
                                bt_charge1.setTypeface(LoginActivity.sTypeface);
                                bt_charge1.setTextColor(R.color.black);
                                bt_charge1.setPadding(10, 5, 10, 5);
                                bt_charge1.setLayoutParams(contentParams4);
                                indv_Row4.addView(bt_charge1);

                                tableLayout.addView(indv_Row4,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                                //   Log.e("Bank name size", mBankNameArr.length + "");

                                View rullerView2 = new View(getActivity());
                                rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView2);
                            }
                        }


                    } else if (subType.equals(AppStrings.mAccountToAccountTransfer)) {

                        ArrayList<OfflineDto> bankArr, loanArr;
                        bankArr = new ArrayList<>();
                        loanArr = new ArrayList<>();

                        for (int i = 0; i < btReport.size(); i++) {
                            if (btReport.get(i).getBtToLoanId() != null && btReport.get(i).getBtToLoanId().length() > 0) {
                                loanArr.add(btReport.get(i));

                                TableRow indv_Row = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView type_Text = new TextView(getActivity());
                                type_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
                                type_Text.setTypeface(LoginActivity.sTypeface);
                                type_Text.setPadding(10, 5, 10, 5);
                                type_Text.setTextColor(R.color.black);
                                type_Text.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text);

                                TextView type_Text1 = new TextView(getActivity());
                                type_Text1.setText(btReport.get(i).getMFromBk());
                                type_Text1.setTypeface(LoginActivity.sTypeface);
                                type_Text1.setPadding(10, 5, 10, 5);
                                type_Text1.setTextColor(R.color.black);
                                type_Text1.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text1);

                                tableLayout.addView(indv_Row,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                                TableRow indv_Row00 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams01 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView l_Text = new TextView(getActivity());
                                l_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanName)));
                                l_Text.setTypeface(LoginActivity.sTypeface);
                                l_Text.setTextColor(R.color.black);
                                l_Text.setLayoutParams(contentParams01);
                                l_Text.setPadding(10, 5, 10, 5);
                                indv_Row00.addView(l_Text);

                                TextView l_Text1 = new TextView(getActivity());
                                l_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getMLoanName())));
                                l_Text1.setTypeface(LoginActivity.sTypeface);
                                l_Text1.setTextColor(R.color.black);
                                l_Text1.setPadding(10, 5, 10, 5);
                                l_Text1.setLayoutParams(contentParams01);
                                indv_Row00.addView(l_Text1);

                                tableLayout.addView(indv_Row00,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row1 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text = new TextView(getActivity());
                                amount_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanId)));
                                amount_Text.setTypeface(LoginActivity.sTypeface);
                                amount_Text.setTextColor(R.color.black);
                                amount_Text.setLayoutParams(contentParams1);
                                amount_Text.setPadding(10, 5, 10, 5);
                                indv_Row1.addView(amount_Text);

                                TextView amount_Text1 = new TextView(getActivity());
                                amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtToLoanId())));
                                amount_Text1.setTypeface(LoginActivity.sTypeface);
                                amount_Text1.setTextColor(R.color.black);
                                amount_Text1.setPadding(10, 5, 10, 5);
                                amount_Text1.setLayoutParams(contentParams1);
                                indv_Row1.addView(amount_Text1);

                                tableLayout.addView(indv_Row1,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                                TableRow indv_Row3 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView w_Text = new TextView(getActivity());
                                w_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferAmount)));
                                w_Text.setTypeface(LoginActivity.sTypeface);
                                w_Text.setTextColor(R.color.black);
                                w_Text.setLayoutParams(contentParams3);
                                w_Text.setPadding(10, 5, 10, 5);
                                indv_Row3.addView(w_Text);

                                TextView w_Text1 = new TextView(getActivity());
                                w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getTransferAmount())));
                                w_Text1.setTypeface(LoginActivity.sTypeface);
                                w_Text1.setTextColor(R.color.black);
                                w_Text1.setPadding(10, 5, 10, 5);
                                w_Text1.setLayoutParams(contentParams3);
                                indv_Row3.addView(w_Text1);

                                tableLayout.addView(indv_Row3,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row4 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView bt_charge = new TextView(getActivity());
                                bt_charge
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferCharges)));
                                bt_charge.setTypeface(LoginActivity.sTypeface);
                                bt_charge.setTextColor(R.color.black);
                                bt_charge.setLayoutParams(contentParams4);
                                bt_charge.setPadding(10, 5, 10, 5);
                                indv_Row4.addView(bt_charge);

                                TextView bt_charge1 = new TextView(getActivity());
                                bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getTransferCharge())));
                                bt_charge1.setTypeface(LoginActivity.sTypeface);
                                bt_charge1.setTextColor(R.color.black);
                                bt_charge1.setPadding(10, 5, 10, 5);
                                bt_charge1.setLayoutParams(contentParams4);
                                indv_Row4.addView(bt_charge1);

                                tableLayout.addView(indv_Row4,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                                //   Log.e("Bank name size", mBankNameArr.length + "");

                                View rullerView2 = new View(getActivity());
                                rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView2);


                            } else {
                                bankArr.add(btReport.get(i));

                                TableRow indv_Row = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView type_Text = new TextView(getActivity());
                                type_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
                                type_Text.setTypeface(LoginActivity.sTypeface);
                                type_Text.setPadding(10, 5, 10, 5);
                                type_Text.setTextColor(R.color.black);
                                type_Text.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text);

                                TextView type_Text1 = new TextView(getActivity());
                                type_Text1.setText(btReport.get(i).getSSelectedBank());
                                type_Text1.setTypeface(LoginActivity.sTypeface);
                                type_Text1.setPadding(10, 5, 10, 5);
                                type_Text1.setTextColor(R.color.black);
                                type_Text1.setLayoutParams(contentParams);
                                indv_Row.addView(type_Text1);

                                tableLayout.addView(indv_Row,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                                TableRow indv_Row1 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView amount_Text = new TextView(getActivity());
                                amount_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
                                amount_Text.setTypeface(LoginActivity.sTypeface);
                                amount_Text.setTextColor(R.color.black);
                                amount_Text.setLayoutParams(contentParams1);
                                amount_Text.setPadding(10, 5, 10, 5);
                                indv_Row1.addView(amount_Text);

                                TextView amount_Text1 = new TextView(getActivity());
                                amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getBtToSavingAcId())));
                                amount_Text1.setTypeface(LoginActivity.sTypeface);
                                amount_Text1.setTextColor(R.color.black);
                                amount_Text1.setPadding(10, 5, 10, 5);
                                amount_Text1.setLayoutParams(contentParams1);
                                indv_Row1.addView(amount_Text1);

                                tableLayout.addView(indv_Row1,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                                TableRow indv_Row3 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView w_Text = new TextView(getActivity());
                                w_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferAmount)));
                                w_Text.setTypeface(LoginActivity.sTypeface);
                                w_Text.setTextColor(R.color.black);
                                w_Text.setLayoutParams(contentParams3);
                                w_Text.setPadding(10, 5, 10, 5);
                                indv_Row3.addView(w_Text);

                                TextView w_Text1 = new TextView(getActivity());
                                w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getTransferAmount())));
                                w_Text1.setTypeface(LoginActivity.sTypeface);
                                w_Text1.setTextColor(R.color.black);
                                w_Text1.setPadding(10, 5, 10, 5);
                                w_Text1.setLayoutParams(contentParams3);
                                indv_Row3.addView(w_Text1);

                                tableLayout.addView(indv_Row3,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                TableRow indv_Row4 = new TableRow(getActivity());
                                TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                                TextView bt_charge = new TextView(getActivity());
                                bt_charge
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferCharges)));
                                bt_charge.setTypeface(LoginActivity.sTypeface);
                                bt_charge.setTextColor(R.color.black);
                                bt_charge.setLayoutParams(contentParams4);
                                bt_charge.setPadding(10, 5, 10, 5);
                                indv_Row4.addView(bt_charge);

                                TextView bt_charge1 = new TextView(getActivity());
                                bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(btReport.get(i).getTransferCharge())));
                                bt_charge1.setTypeface(LoginActivity.sTypeface);
                                bt_charge1.setTextColor(R.color.black);
                                bt_charge1.setPadding(10, 5, 10, 5);
                                bt_charge1.setLayoutParams(contentParams4);
                                indv_Row4.addView(bt_charge1);

                                tableLayout.addView(indv_Row4,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                                //   Log.e("Bank name size", mBankNameArr.length + "");

                                View rullerView2 = new View(getActivity());
                                rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView2);

                            }
                        }

                    }

                } else if (txType.equals(NewDrawerScreen.MEMBER_LOAN_REPAYMENT)) {
                    if (subType.equals(AppStrings.InternalLoan)) {
                        for (int i1 = 0; i1 < memList.size(); i1++) {

                            TableRow indv_Row = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    String.valueOf(memList.get(i1).getMemberName())));
                            memberName_Text.setTypeface(LoginActivity.sTypeface);
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(50, 5, 10, 5);
                            // memberName_Text.setLayoutParams(contentParams);
                            indv_Row.addView(memberName_Text, contentParams);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalILReport(memList.get(i1).getMemberId(), AppStrings.InternalLoan).getMemAmount() != null && TransactionTable.getOffFinalILReport(memList.get(i1).getMemberId(), AppStrings.InternalLoan).getMemAmount().length() > 0) ? TransactionTable.getOffFinalILReport(memList.get(i1).getMemberId(), AppStrings.InternalLoan).getMemAmount() : "0"));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(10, 5, 60, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            // memberName_Text.setLayoutParams(contentParams);
                            indv_Row.addView(confirm_values, contentParams);


                            TextView confirm_values1 = new TextView(getActivity());
                            confirm_values1
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalILReport(memList.get(i1).getMemberId(), AppStrings.InternalLoan).getMemInterest() != null && TransactionTable.getOffFinalILReport(memList.get(i1).getMemberId(), AppStrings.InternalLoan).getMemInterest().length() > 0) ? TransactionTable.getOffFinalILReport(memList.get(i1).getMemberId(), AppStrings.InternalLoan).getMemInterest() : "0"));
                            confirm_values1.setTextColor(R.color.black);
                            confirm_values1.setPadding(10, 5, 60, 5);
                            confirm_values1.setGravity(Gravity.RIGHT);
                            // memberName_Text.setLayoutParams(contentParams);
                            indv_Row.addView(confirm_values1, contentParams);

                            tableLayout.addView(indv_Row,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            View rullerView = new View(getActivity());
                            rullerView
                                    .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView);
                        }
                    } else {
                        String acno = subType.substring(subType.lastIndexOf("/") + 1);
                        for (int i1 = 0; i1 < memList.size(); i1++) {

                            TableRow indv_Row = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    String.valueOf(memList.get(i1).getMemberName())));
                            memberName_Text.setTypeface(LoginActivity.sTypeface);
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(50, 5, 10, 5);
                            // memberName_Text.setLayoutParams(contentParams);
                            indv_Row.addView(memberName_Text, contentParams);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalMRReport(memList.get(i1).getMemberId(), AppStrings.memberloanrepayment, acno).getMemAmount() != null && TransactionTable.getOffFinalMRReport(memList.get(i1).getMemberId(), AppStrings.memberloanrepayment, acno).getMemAmount().length() > 0) ? TransactionTable.getOffFinalMRReport(memList.get(i1).getMemberId(), AppStrings.memberloanrepayment, acno).getMemAmount() : "0"));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(10, 5, 60, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            // memberName_Text.setLayoutParams(contentParams);
                            indv_Row.addView(confirm_values, contentParams);
                            /*tableLayout.addView(indv_Row,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));*/

                            TextView confirm_values1 = new TextView(getActivity());
                            confirm_values1
                                    .setText(GetSpanText.getSpanString(getActivity(), (TransactionTable.getOffFinalMRReport(memList.get(i1).getMemberId(), AppStrings.memberloanrepayment, acno).getMemInterest() != null && TransactionTable.getOffFinalMRReport(memList.get(i1).getMemberId(), AppStrings.memberloanrepayment, acno).getMemInterest().length() > 0) ? TransactionTable.getOffFinalMRReport(memList.get(i1).getMemberId(), AppStrings.memberloanrepayment, acno).getMemInterest() : "0"));
                            confirm_values1.setTextColor(R.color.black);
                            confirm_values1.setPadding(10, 5, 60, 5);
                            confirm_values1.setGravity(Gravity.RIGHT);
                            // memberName_Text.setLayoutParams(contentParams);
                            indv_Row.addView(confirm_values1, contentParams);

                          /*  tableLayout.addView(indv_Row,/
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));*/
                            tableLayout.addView(indv_Row,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            View rullerView = new View(getActivity());
                            rullerView
                                    .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView);
                        }

                    }

                } else if (txType.equals(NewDrawerScreen.GROUP_LOAN_REPAYMENT)) {
                    String acno = subType.substring(subType.lastIndexOf("/") + 1);
                    grpReport = TransactionTable.getOfflineFinalGrp_Report(shgDto.getShgId(), dateTimeStr, txType, AppStrings.grouploanrepayment);
                    for (int i = 0; i < grpReport.size(); i++) {

                        if (grpReport.get(i).getModeOCash() != null && grpReport.get(i).getModeOCash().length() > 0 && grpReport.get(i).getModeOCash().equals("1")) {


                            TableRow indv_Row00 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams00 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView type_Text00 = new TextView(getActivity());
                            type_Text00
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                            type_Text00.setTypeface(LoginActivity.sTypeface);
                            type_Text00.setPadding(10, 5, 10, 5);
                            type_Text00.setTextColor(R.color.black);
                            type_Text00.setLayoutParams(contentParams00);
                            indv_Row00.addView(type_Text00);

                            TextView type_Text01 = new TextView(getActivity());
                            type_Text01.setText(AppStrings.mLoanaccBank);
                            type_Text01.setTypeface(LoginActivity.sTypeface);
                            type_Text01.setPadding(10, 5, 10, 5);
                            type_Text01.setTextColor(R.color.black);
                            type_Text01.setLayoutParams(contentParams00);
                            indv_Row00.addView(type_Text01);

                            tableLayout.addView(indv_Row00,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView type_Text = new TextView(getActivity());
                            type_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankName)));
                            type_Text.setTypeface(LoginActivity.sTypeface);
                            type_Text.setPadding(10, 5, 10, 5);
                            type_Text.setTextColor(R.color.black);
                            type_Text.setLayoutParams(contentParams);
                            indv_Row.addView(type_Text);

                            TextView type_Text1 = new TextView(getActivity());
                            type_Text1.setText(grpReport.get(i).getMToBk());
                            type_Text1.setTypeface(LoginActivity.sTypeface);
                            type_Text1.setPadding(10, 5, 10, 5);
                            type_Text1.setTextColor(R.color.black);
                            type_Text1.setLayoutParams(contentParams);
                            indv_Row.addView(type_Text1);

                            tableLayout.addView(indv_Row,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                            TableRow indv_Row01 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams01 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView l_Text = new TextView(getActivity());
                            l_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.interest)));
                            l_Text.setTypeface(LoginActivity.sTypeface);
                            l_Text.setTextColor(R.color.black);
                            l_Text.setLayoutParams(contentParams01);
                            l_Text.setPadding(10, 5, 10, 5);
                            indv_Row01.addView(l_Text);

                            TextView l_Text1 = new TextView(getActivity());
                            l_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_interest())));
                            l_Text1.setTypeface(LoginActivity.sTypeface);
                            l_Text1.setTextColor(R.color.black);
                            l_Text1.setPadding(10, 5, 10, 5);
                            l_Text1.setLayoutParams(contentParams01);
                            indv_Row01.addView(l_Text1);

                            tableLayout.addView(indv_Row00,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row1 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView amount_Text = new TextView(getActivity());
                            amount_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mCharges)));
                            amount_Text.setTypeface(LoginActivity.sTypeface);
                            amount_Text.setTextColor(R.color.black);
                            amount_Text.setLayoutParams(contentParams1);
                            amount_Text.setPadding(10, 5, 10, 5);
                            indv_Row1.addView(amount_Text);

                            TextView amount_Text1 = new TextView(getActivity());
                            amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_charge())));
                            amount_Text1.setTypeface(LoginActivity.sTypeface);
                            amount_Text1.setTextColor(R.color.black);
                            amount_Text1.setPadding(10, 5, 10, 5);
                            amount_Text1.setLayoutParams(contentParams1);
                            indv_Row1.addView(amount_Text1);

                            tableLayout.addView(indv_Row1,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            TableRow indv_Row3 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView w_Text = new TextView(getActivity());
                            w_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mRepaymentWithInterest)));
                            w_Text.setTypeface(LoginActivity.sTypeface);
                            w_Text.setTextColor(R.color.black);
                            w_Text.setLayoutParams(contentParams3);
                            w_Text.setPadding(10, 5, 10, 5);
                            indv_Row3.addView(w_Text);

                            TextView w_Text1 = new TextView(getActivity());
                            w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_repayment())));
                            w_Text1.setTypeface(LoginActivity.sTypeface);
                            w_Text1.setTextColor(R.color.black);
                            w_Text1.setPadding(10, 5, 10, 5);
                            w_Text1.setLayoutParams(contentParams3);
                            indv_Row3.addView(w_Text1);

                            tableLayout.addView(indv_Row3,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row4 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView bt_charge = new TextView(getActivity());
                            bt_charge
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.interestSubvention)));
                            bt_charge.setTypeface(LoginActivity.sTypeface);
                            bt_charge.setTextColor(R.color.black);
                            bt_charge.setLayoutParams(contentParams4);
                            bt_charge.setPadding(10, 5, 10, 5);
                            indv_Row4.addView(bt_charge);

                            TextView bt_charge1 = new TextView(getActivity());
                            bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_intSubventionRecieved())));
                            bt_charge1.setTypeface(LoginActivity.sTypeface);
                            bt_charge1.setTextColor(R.color.black);
                            bt_charge1.setPadding(10, 5, 10, 5);
                            bt_charge1.setLayoutParams(contentParams4);
                            indv_Row4.addView(bt_charge1);

                            tableLayout.addView(indv_Row4,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row14 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams14 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView bt_charge01 = new TextView(getActivity());
                            bt_charge01
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankCharges)));
                            bt_charge01.setTypeface(LoginActivity.sTypeface);
                            bt_charge01.setTextColor(R.color.black);
                            bt_charge01.setLayoutParams(contentParams14);
                            bt_charge01.setPadding(10, 5, 10, 5);
                            indv_Row14.addView(bt_charge01);

                            TextView bt_charge11 = new TextView(getActivity());
                            bt_charge11.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getBankCharges())));
                            bt_charge11.setTypeface(LoginActivity.sTypeface);
                            bt_charge11.setTextColor(R.color.black);
                            bt_charge11.setPadding(10, 5, 10, 5);
                            bt_charge11.setLayoutParams(contentParams14);
                            indv_Row14.addView(bt_charge11);

                            tableLayout.addView(indv_Row14,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                            //   Log.e("Bank name size", mBankNameArr.length + "");

                            View rullerView2 = new View(getActivity());
                            rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView2);


                        } else {

                            TableRow indv_Row00 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams00 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView type_Text00 = new TextView(getActivity());
                            type_Text00
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                            type_Text00.setTypeface(LoginActivity.sTypeface);
                            type_Text00.setPadding(10, 5, 10, 5);
                            type_Text00.setTextColor(R.color.black);
                            type_Text00.setLayoutParams(contentParams00);
                            indv_Row00.addView(type_Text00);

                            TextView type_Text01 = new TextView(getActivity());
                            type_Text01.setText(AppStrings.mLoanaccCash);
                            type_Text01.setTypeface(LoginActivity.sTypeface);
                            type_Text01.setPadding(10, 5, 10, 5);
                            type_Text01.setTextColor(R.color.black);
                            type_Text01.setLayoutParams(contentParams00);
                            indv_Row00.addView(type_Text01);

                            tableLayout.addView(indv_Row00,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    /*							View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView);*/

                            TableRow indv_Row01 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams01 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView l_Text = new TextView(getActivity());
                            l_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.interest)));
                            l_Text.setTypeface(LoginActivity.sTypeface);
                            l_Text.setTextColor(R.color.black);
                            l_Text.setLayoutParams(contentParams01);
                            l_Text.setPadding(10, 5, 10, 5);
                            indv_Row01.addView(l_Text);

                            TextView l_Text1 = new TextView(getActivity());
                            l_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_interest())));
                            l_Text1.setTypeface(LoginActivity.sTypeface);
                            l_Text1.setTextColor(R.color.black);
                            l_Text1.setPadding(10, 5, 10, 5);
                            l_Text1.setLayoutParams(contentParams01);
                            indv_Row01.addView(l_Text1);

                            tableLayout.addView(indv_Row01,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row1 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView amount_Text = new TextView(getActivity());
                            amount_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mCharges)));
                            amount_Text.setTypeface(LoginActivity.sTypeface);
                            amount_Text.setTextColor(R.color.black);
                            amount_Text.setLayoutParams(contentParams1);
                            amount_Text.setPadding(10, 5, 10, 5);
                            indv_Row1.addView(amount_Text);

                            TextView amount_Text1 = new TextView(getActivity());
                            amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_charge())));
                            amount_Text1.setTypeface(LoginActivity.sTypeface);
                            amount_Text1.setTextColor(R.color.black);
                            amount_Text1.setPadding(10, 5, 10, 5);
                            amount_Text1.setLayoutParams(contentParams1);
                            indv_Row1.addView(amount_Text1);

                            tableLayout.addView(indv_Row1,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            TableRow indv_Row3 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView w_Text = new TextView(getActivity());
                            w_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mRepaymentWithInterest)));
                            w_Text.setTypeface(LoginActivity.sTypeface);
                            w_Text.setTextColor(R.color.black);
                            w_Text.setLayoutParams(contentParams3);
                            w_Text.setPadding(10, 5, 10, 5);
                            indv_Row3.addView(w_Text);

                            TextView w_Text1 = new TextView(getActivity());
                            w_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_repayment())));
                            w_Text1.setTypeface(LoginActivity.sTypeface);
                            w_Text1.setTextColor(R.color.black);
                            w_Text1.setPadding(10, 5, 10, 5);
                            w_Text1.setLayoutParams(contentParams3);
                            indv_Row3.addView(w_Text1);

                            tableLayout.addView(indv_Row3,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow indv_Row4 = new TableRow(getActivity());
                            TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

                            TextView bt_charge = new TextView(getActivity());
                            bt_charge
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.interestSubvention)));
                            bt_charge.setTypeface(LoginActivity.sTypeface);
                            bt_charge.setTextColor(R.color.black);
                            bt_charge.setLayoutParams(contentParams4);
                            bt_charge.setPadding(10, 5, 10, 5);
                            indv_Row4.addView(bt_charge);

                            TextView bt_charge1 = new TextView(getActivity());
                            bt_charge1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(grpReport.get(i).getGrp_intSubventionRecieved())));
                            bt_charge1.setTypeface(LoginActivity.sTypeface);
                            bt_charge1.setTextColor(R.color.black);
                            bt_charge1.setPadding(10, 5, 10, 5);
                            bt_charge1.setLayoutParams(contentParams4);
                            indv_Row4.addView(bt_charge1);

                            tableLayout.addView(indv_Row4,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                                        /*View rullerView1 = new View(getActivity());
                                rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
                                tableLayout.addView(rullerView1);*/
                            //   Log.e("Bank name size", mBankNameArr.length + "");

                            View rullerView2 = new View(getActivity());
                            rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                            rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
                            tableLayout.addView(rullerView2);

                        }
                    }


                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
