package com.oasys.emathi.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.oasys.emathi.Adapter.CustomListAdapter;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.RegionalConversion;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.model.ListItem;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class OfflineReport_Menu extends Fragment implements AdapterView.OnItemClickListener {

    private View view;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;

    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, mAutoFilllabel, mMemberName;
    private ArrayList<ListItem> listItems;
    private ListView mListView;
    private int listImage;
    String[] listItem;
    private int size;
    private CustomListAdapter mAdapter;
    public static String selectedItem;
    private String loan_id;
    private String dateTimeStr;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.offline_new_transactionlist, container, false);

        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                dateTimeStr = bundle.getString("date");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        init();
    }

    private void init() {
        try {

            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(com.oasys.emathi.activity.LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(com.oasys.emathi.OasysUtils.AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(com.oasys.emathi.      activity.LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(com.oasys.emathi.OasysUtils.AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(com.oasys.emathi.activity.LoginActivity.sTypeface);
            /** UI Mapping **/

            mHeader = (TextView) view.findViewById(R.id.offline_headertext);
            mHeader.setText(com.oasys.emathi.OasysUtils.AppStrings.offlineReports);
            mHeader.setTypeface(com.oasys.emathi.activity.LoginActivity.sTypeface);


            listItems = new ArrayList<ListItem>();
            mListView = (ListView) view.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;

            ArrayList<OfflineDto> lastdateList = TransactionTable.getOfflineReportMenu(shgDto.getShgId(), dateTimeStr);

            ArrayList<String> txtypeStr = new ArrayList<>();
            for (OfflineDto dt : lastdateList) {
                txtypeStr.add(dt.getTxType());
            }


            Set<String> set = new LinkedHashSet<String>(txtypeStr);
            listItem = new String[set.size()];
            set.toArray(listItem);

            for (int j = 0; j < listItem.length; j++) {
                System.out
                        .println("----------TRANSACTION LIST ITEMS---------"
                                + listItem[j].toString()
                                + " i pos " + j);
            }

            size = listItem.length;

            for (int i = 0; i < size; i++) {
                ListItem rowItem = new ListItem();
                rowItem.setTitle(RegionalConversion.getRegionalConversion(listItem[i]
                                .toString()));
                rowItem.setImageId(listImage);
                listItems.add(rowItem);
            }
            System.out.println("ROW ITEM " + listItems.size());

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);


        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        TextView textColor_Change = (TextView) view
                .findViewById(R.id.dynamicText);
        textColor_Change.setText(listItem[position]);
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

       // selectedItem = listItem[position];
        System.out.println("SELECTED ITEM  :" + selectedItem);

        OfflineReport_Sub_Menu fragment = new OfflineReport_Sub_Menu();
        Bundle bundle = new Bundle();
        bundle.putString("date", dateTimeStr);
        bundle.putString("txType", listItem[position]);
        fragment.setArguments(bundle);
        NewDrawerScreen.showFragment(fragment);

    }
}
