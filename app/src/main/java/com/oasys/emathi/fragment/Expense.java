package com.oasys.emathi.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Dialogue.Dialog_New_TransactionDate;
import com.oasys.emathi.Dto.CashOfGroup;
import com.oasys.emathi.Dto.ExpensesTypeDtoList;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.SavingRequest;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.ExpenseTable;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;
import com.oasys.emathi.views.Get_EdiText_Filter;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.OasysUtils.AppDialogUtils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Expense extends Fragment implements View.OnClickListener, NewTaskListener {

    public static final String TAG = Expense.class.getSimpleName();
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private RaisedButton mRaised_SubmitButton;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    private EditText mExpenses_values;

    public static List<EditText> sExpensesFields = new ArrayList<EditText>();
    public static String sExpensesAmounts[];
    public static String[] sExpensesName;
    public static String sSendToServer_Expenses;
    public static int sExpenses_total;

    private Dialog mProgressDialog;
    Dialog confirmationDialog;

    private int mSize;
    String nullVlaue = "0";
    String mLastTrDate = null, mLastTr_ID = null;
    boolean isGetTrid = false;
    boolean isServiceCall = false;
    String mSqliteDBStoredValues_Expenses = null;
    private View rootView;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ExpensesTypeDtoList> arrExpense;
    private List<ExpensesTypeDtoList> expList;
    private Dialog mProgressDilaog;
    ArrayList<OfflineDto> offlineDBData = new ArrayList<>();
    OfflineDto offline;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_expenses, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        expList = ExpenseTable.getExpList("Expense Type");
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrExpense = new ArrayList<>();
        sExpensesFields = new ArrayList<EditText>();
        mSize = expList.size();
        init();
    }

    private void init() {
        try {
            if (MySharedPreference.readInteger(getActivity(), MySharedPreference.EXPENSE_COUNT, 0) <= 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.EXPENSE_COUNT, 0);

            offlineDBData = new ArrayList<>();

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.expenses);
            mHeader.setTypeface(LoginActivity.sTypeface);

            mRaised_SubmitButton = (RaisedButton) rootView.findViewById(R.id.fragment_Submit_button);
            mRaised_SubmitButton.setText(AppStrings.submit);
            mRaised_SubmitButton.setTypeface(LoginActivity.sTypeface);
            mRaised_SubmitButton.setOnClickListener(this);

            TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.expenses_headerTable);

            TableLayout expensesTable = (TableLayout) rootView.findViewById(R.id.fragment_expensesTable);

            TableRow.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TableRow headRow = new TableRow(getActivity());

            TextView expensesText = new TextView(getActivity());
            expensesText.setText(String.valueOf(AppStrings.expenses));
            expensesText.setTypeface(LoginActivity.sTypeface);
            expensesText.setTextColor(Color.WHITE);
            expensesText.setPadding(35, 5, 20, 5);
            expensesText.setLayoutParams(headerParams);

            expensesText.setBackgroundResource(R.color.tableHeader);
            headRow.addView(expensesText);

            TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView amountText = new TextView(getActivity());
            amountText.setText(String.valueOf(AppStrings.amount));
            amountText.setTypeface(LoginActivity.sTypeface);
            amountText.setTextColor(Color.WHITE);
            amountText.setLayoutParams(amountParams);
            amountText.setGravity(Gravity.CENTER);
            amountText.setPadding(60, 5, 50, 5);
            amountText.setBackgroundResource(R.color.tableHeader);
            headRow.addView(amountText);

            headerTable.addView(headRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


            ExpensesTypeDtoList[] stockArr = new ExpensesTypeDtoList[expList.size()];
            stockArr = expList.toArray(stockArr);
            for (int i = 0; i < mSize; i++) {

                TableRow indv_ExpensesRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                TextView expenses_list = new TextView(getActivity());
                expenses_list.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(stockArr[i].getName())));
                expenses_list.setTextColor(R.color.black);
                expenses_list.setPadding(25, 0, 5, 0);
                expenses_list.setLayoutParams(contentParams);
                indv_ExpensesRow.addView(expenses_list);

                TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentEditParams.setMargins(10, 5, 50, 5);

                mExpenses_values = new EditText(getActivity());
                mExpenses_values.setId(i);
                sExpensesFields.add(mExpenses_values);
                mExpenses_values.setPadding(5, 5, 5, 5);
                mExpenses_values.setBackgroundResource(R.drawable.edittext_background);
                mExpenses_values.setLayoutParams(contentEditParams);// contentParams
                // lParams
                mExpenses_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mExpenses_values.setFilters(Get_EdiText_Filter.editText_filter());
                mExpenses_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mExpenses_values.setTextColor(Color.BLACK);
                mExpenses_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);
                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);
                        }
                    }
                });
                indv_ExpensesRow.addView(mExpenses_values);

                expensesTable.addView(indv_ExpensesRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        sExpensesAmounts = new String[sExpensesFields.size()];
        Log.d(TAG, "sExpensesAmounts size : " + sExpensesAmounts.length);

        switch (view.getId()) {
            case R.id.fragment_Submit_button:

                try {
                    sExpenses_total = 0;
                    sSendToServer_Expenses = "0";

                    if (arrExpense != null && arrExpense.size() > 0) {
                        arrExpense.clear();
                    }

                    for (int i = 0; i < mSize; i++) {

                        sExpensesAmounts[i] = String.valueOf(sExpensesFields.get(i).getText());
                        if ((sExpensesAmounts[i].equals("")) || (sExpensesAmounts[i] == null)) {
                            sExpensesAmounts[i] = nullVlaue;
                        }

                        if (sExpensesAmounts[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sExpensesAmounts[i]));
                            sExpensesAmounts[i] = String.valueOf(amount);
                        }

                        ExpensesTypeDtoList expense = new ExpensesTypeDtoList();
                        expense.setExpensesTypeId(expList.get(i).getId());
                        expense.setAmount(sExpensesAmounts[i]);
                        arrExpense.add(expense);

                        sSendToServer_Expenses = sSendToServer_Expenses + sExpensesAmounts[i] + "~";

                        sExpenses_total = sExpenses_total + Integer.parseInt(sExpensesAmounts[i]);

                        try {
                            offline = new OfflineDto();
                            offline.setExpenseTypeId(expList.get(i).getId());
                            offline.setExpenseTypeName(expList.get(i).getName());
                            offline.setIc_exp_Amount(sExpensesAmounts[i]);
                            offline.setTotalExpenseAmount(sExpenses_total + "");
                            offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                            offline.setShgId(memList.get(i).getShgId());
//                            offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                            offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                            offline.setModifiedDateTime(System.currentTimeMillis() + "");
                            offline.setModeOCash("2");
                            offline.setIs_transaction_tdy("1.0");
                            offline.setTxType(NewDrawerScreen.EXPENCE);
                            offline.setTxSubtype(AppStrings.expenses);

                            //offline fundflow
                            int cih = 0;
                            cih = (int) Double.parseDouble(shgDto.getCashInHand()) - (sExpenses_total);
                            offline.setCashInhand(cih + "");
                            offline.setCashAtBank(shgDto.getCashAtBank());
                            offlineDBData.add(offline);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    Log.d(TAG, "Expenses Total:" + sExpenses_total);
                    Log.d(TAG, "Send to server value:" + sSendToServer_Expenses);

                    String sCashinhand = shgDto.getCashInHand();

                    int cahinhand = Integer.parseInt(sCashinhand);

                    if (cahinhand >= sExpenses_total) {


                        if (sExpenses_total != 0 || sExpenses_total >= 0) {
                            if (sExpenses_total != 0) {

                                confirmationDialog = new Dialog(getActivity());
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                                dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT));

                                TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                                confirmationHeader
                                        .setText(AppStrings.confirmation);
                                confirmationHeader.setTypeface(LoginActivity.sTypeface);

                                TableLayout confirmationTable = (TableLayout) dialogView
                                        .findViewById(R.id.confirmationTable);

                                DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                                Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                                String dateStr = simple.format(d);
                                TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                                transactdate.setText(dateStr);

                                for (int i = 0; i < mSize; i++) {

                                    TableRow indv_DepositEntryRow = new TableRow(getActivity());

                                    @SuppressWarnings("deprecation")
                                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                            ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                    contentParams.setMargins(10, 5, 10, 5);

                                    TextView memberName_Text = new TextView(getActivity());
                                    memberName_Text.setText(
                                            GetSpanText.getSpanString(getActivity(), String.valueOf(expList.get(i).getName())));
                                    memberName_Text.setTypeface(LoginActivity.sTypeface);
                                    memberName_Text.setTextColor(R.color.white);
                                    memberName_Text.setPadding(5, 5, 5, 5);
                                    memberName_Text.setLayoutParams(contentParams);
                                    indv_DepositEntryRow.addView(memberName_Text);

                                    TextView confirm_values = new TextView(getActivity());
                                    confirm_values.setText( GetSpanText.getSpanString(getActivity(), String.valueOf(sExpensesAmounts[i])));
                                    confirm_values.setTextColor(R.color.white);
                                    confirm_values.setPadding(5, 5, 5, 5);
                                    confirm_values.setGravity(Gravity.RIGHT);
                                    confirm_values.setLayoutParams(contentParams);
                                    indv_DepositEntryRow.addView(confirm_values);

                                    confirmationTable.addView(indv_DepositEntryRow, new TableLayout.LayoutParams(
                                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                }

                                View rullerView = new View(getActivity());
                                rullerView
                                        .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                                rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                                // 229,
                                // 242));
                                confirmationTable.addView(rullerView);

                                TableRow totalRow = new TableRow(getActivity());

                                @SuppressWarnings("deprecation")
                                TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                totalParams.setMargins(10, 5, 10, 5);

                                TextView totalText = new TextView(getActivity());
                                totalText.setText(
                                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                                totalText.setTypeface(LoginActivity.sTypeface);
                                totalText.setTextColor(R.color.white);
                                totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                                totalText.setLayoutParams(totalParams);
                                totalRow.addView(totalText);

                                TextView totalAmount = new TextView(getActivity());
                                totalAmount
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sExpenses_total)));
                                totalAmount.setTextColor(R.color.white);
                                totalAmount.setPadding(5, 5, 5, 5);
                                totalAmount.setGravity(Gravity.RIGHT);
                                totalAmount.setLayoutParams(totalParams);
                                totalRow.addView(totalAmount);

                                confirmationTable.addView(totalRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                                mEdit_RaisedButton.setText(AppStrings.edit);
                                mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                                mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                                // 205,
                                // 0));
                                mEdit_RaisedButton.setOnClickListener(this);

                                mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                                mOk_RaisedButton.setText(AppStrings.yes);
                                mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                                mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                                mOk_RaisedButton.setOnClickListener(this);

                                confirmationDialog.getWindow()
                                        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                confirmationDialog.setCanceledOnTouchOutside(false);
                                confirmationDialog.setContentView(dialogView);
                                confirmationDialog.setCancelable(true);
                                confirmationDialog.show();

                                ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                                margin.leftMargin = 10;
                                margin.rightMargin = 10;
                                margin.topMargin = 10;
                                margin.bottomMargin = 10;
                                margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin,
                                        margin.bottomMargin);


                            } else {
                                sSendToServer_Expenses = "0";
                                sExpenses_total = Integer.valueOf("0");
                                TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                            }

                        }
                    } else {
                        TastyToast.makeText(getActivity(), "CHECK THE CASH IN HAND", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:
                sSendToServer_Expenses = "0";
                sExpenses_total = Integer.valueOf("0");

                confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }

                SavingRequest sr = new SavingRequest();
                // sr.set(arrExpense);
                sr.setShgId(shgDto.getShgId());
//                sr.setTransactionDate(shgDto.getLastTransactionDate());
                sr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setExpense(arrExpense);
                sr.setGroupId(shgDto.getShgId());
                sr.setModeOfCash("2");
                String sreqString = new Gson().toJson(sr);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.EXPENSE, sreqString, getActivity(), ServiceType.EXPENSE_SUBMIT);
                } else {
                    if (TransactionTable.getLoginFlag(AppStrings.expenses).size() <= 0 || ( !TransactionTable.getLoginFlag(AppStrings.expenses).get(TransactionTable.getLoginFlag(AppStrings.expenses).size()-1).getLoginFlag().equals("1")))
                        insertSavings();
                    else
                        TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);


                }
                break;
        }
    }

    private void insertTransaction_tdy()
    {
        for(OfflineDto dto:offlineDBData) {
            dto.setIs_transaction_tdy("1.0");
            dto.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(dto);
        }

    }

    private void insertSavings() {
        try {
            //Offline fundflow:
            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.EXPENSE_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.EXPENSE_COUNT, value);



            for(OfflineDto dto:offlineDBData) {
                dto.setECount(value + "");
                TransactionTable.insertTransExpenseData(dto);
            }

            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }

            /*String cihstr = "", cabstr = "", lastTranstr = "";
            cihstr = offlineDBData.get(0).getCashInhand();
            cabstr = offlineDBData.get(0).getCashAtBank();
            lastTranstr = offlineDBData.get(0).getLastTransactionDateTime();*/
            CashOfGroup csg = new CashOfGroup();
            csg.setCashInHand(offline.getCashInhand());
            csg.setCashAtBank(offline.getCashAtBank());
            csg.setLastTransactionDate(offline.getLastTransactionDateTime());
            SHGTable.updateSHGDetails(csg, shgDto.getId());

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case EXPENSE_SUBMIT:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                insertTransaction_tdy();
                                Utils.showToast(getActivity(), message);
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                MainFragment mainFragment = new MainFragment();
                                Bundle bundles = new Bundle();
                                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                mainFragment.setArguments(bundles);
                                NewDrawerScreen.showFragment(mainFragment);

                            } else {
                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                                insertSavings();
                            }
                        } else {
                            insertSavings();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }
}
