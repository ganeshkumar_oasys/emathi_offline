package com.oasys.emathi.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Adapter.DialogBranchAdapter;
import com.oasys.emathi.Adapter.Dialog_jandhanAccountNumberUpdation;
import com.oasys.emathi.Dto.BranchList;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.LoanBankDto;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.RequestDto.ShgSavingsAccountDTOList;
import com.oasys.emathi.Dto.RequestDto.SsnNumberRequestDTOList;
import com.oasys.emathi.Dto.RequestDto.SsnNumbersDTOList;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.SssTypeDto;
import com.oasys.emathi.Dto.TableData;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AlphaNumericInputFilter;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.RegionalConversion;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.BankBranchTable;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.views.ButtonFlat;
import com.oasys.emathi.views.CustomHorizontalScrollView;
import com.oasys.emathi.views.Get_EdiText_Filter;
import com.oasys.emathi.views.RaisedButton;
import com.oasys.emathi.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.oasys.emathi.fragment.LD_IL_Entry.TAG;

public class JDFragment extends Fragment implements View.OnClickListener, NewTaskListener {
    private  View view;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    String width[] = { RegionalConversion.getRegionalConversion(AppStrings.bankName),
            RegionalConversion.getRegionalConversion(AppStrings.mBranchName),
            RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber) };
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    private List<MemberList> memList;
    private TextView mBankName_values, mBranchName_values,mBefore2019;
    private  List<TextView> sBankName_Fields = new ArrayList<TextView>();
    private  List<TextView> sBranch_Fields = new ArrayList<TextView>();
    private  List<EditText> sAccountNumber_Fields = new ArrayList<EditText>();
    private  List<EditText> sDateofopenings_Fields = new ArrayList<EditText>();
    private List<TextView> sBeforeYears_Fields = new ArrayList<TextView>();
    private EditText mAccountNumber_values;
    private EditText mdateofopening_values;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
    private ListOfShg shgDto;
    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    public static HashMap<Integer, List<BranchList>> janadhanselected_bank_branches;
    String selectedradiovalues;
    int selId;
    int selecteditemvalue;
    int selectedId;
    String mSelectedLoanType;
    String mPOL_Values[]= new String[]{"YES","NO"};
    RadioButton radioButton;
    private RaisedButton mSubmit_Raised_Button, mEdit_RaisedButton,mOk_RaisedButton;
    Dialog confirmationDialog;
    private String[]  bankNameArr_value,branchNameArr_value,accNoArr_value,year_value,dateofOpening;
    private String[] accNoArr_server, bankName_server, branchName_server,yearvalue_server,dateofpening_server;
    private NetworkConnection networkConnection;
    ResponseDto cdto;
    private ArrayList<LoanBankDto> loanBankDtoArrayList;
    public List<LoanBankDto> bankList;
    ArrayList<ShgSavingsAccountDTOList> savingsAccountDTOLists;
    ArrayList<SsnNumbersDTOList>ssnNumbersDTOLists;
    ArrayList<TableData>tableDataArrayList;
    TableData tableData;
    String o_mi_ssstypeid;
    private Dialog mProgressDilaog;
    ArrayList<SssTypeDto>sssTypeDtos;
    ArrayList <TableData> datavalues;
    ArrayList<TableData> tableDatacheck;
    private  ArrayList<TableData> shgTable;


    String afterTextChanged = "";
    String beforeTextChanged = "";
    String onTextChanged = "";
    String sssTypeid;
    String ssstypeidjandhan;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.jan_dhan_layout, container, false);
        janadhanselected_bank_branches = new HashMap<>();
        savingsAccountDTOLists = new ArrayList<>();
        datavalues = new ArrayList<>();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgTable = SHGTable.getSssTypeData();
        OnBankandBranchNameValues();

        Bundle bundle= getArguments();
        if(bundle!=null){
            sssTypeid = bundle.getString("jandhanid");
            Log.d("typeid",sssTypeid);
        }

//        datavalues= SHGTable.getSSNAllData(shgDto.getShgId());
        datavalues= SHGTable.getSSNAllData(sssTypeid,shgDto.getShgId());
        Log.d("memberdetails",datavalues+"");

        inIt(view);
        return  view;
    }

    private void OnBankandBranchNameValues() {

        if (networkConnection.isNetworkAvailable()) {

            RestClient.getRestClient(JDFragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION + shgDto.getDistrictId(), getActivity(), ServiceType.PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION_LIST);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void inIt(View view1) {
        try {

            memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


            mGroupName = (TextView) view1.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view1.findViewById(R.id.cashinHand);
            mCashinHand.setText(com.oasys.emathi.OasysUtils.AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view1.findViewById(R.id.cashatBank);
            mCashatBank.setText(com.oasys.emathi.OasysUtils.AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) view1.findViewById(R.id.fragmentHeader);
            mHeader.setText("JAN DHAN");
            mHeader.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) view1.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) view1.findViewById(R.id.member_name);

            mLeftHeaderTable = (TableLayout) view1.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) view1.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) view1.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) view1.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) view1.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) view1.findViewById(R.id.rightContentHScrollView);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });


            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_Header = new TextView(getActivity());
            mMemberName_Header.setText(AppStrings.memberName);
            mMemberName_Header.setTypeface(LoginActivity.sTypeface);
            mMemberName_Header.setTextColor(Color.WHITE);
            mMemberName_Header.setPadding(10, 5, 10, 5);
            mMemberName_Header.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_Header);

            mLeftHeaderTable.addView(leftHeaderRow);


            TableRow rightHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView mOutstanding_Header = new TextView(getActivity());
            mOutstanding_Header.setText(AppStrings.bankName);
            mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
            mOutstanding_Header.setTextColor(Color.WHITE);
            mOutstanding_Header.setPadding(10, 5, 10, 5);
            mOutstanding_Header.setGravity(Gravity.LEFT);
            mOutstanding_Header.setLayoutParams(contentParams);
            mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mOutstanding_Header);


            TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            POLParams.setMargins(10, 0, 10, 0);

            TextView mPurposeOfLoan_Header = new TextView(getActivity());
            mPurposeOfLoan_Header
                    .setText(AppStrings.mBranchName);
            mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
            mPurposeOfLoan_Header.setTextColor(Color.WHITE);
            mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
            mPurposeOfLoan_Header.setLayoutParams(POLParams);
            mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
            mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPurposeOfLoan_Header);


            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mPL_Header = new TextView(getActivity());
            mPL_Header.setText(AppStrings.mAccountNumber);
            mPL_Header.setTypeface(LoginActivity.sTypeface);
            mPL_Header.setTextColor(Color.WHITE);
            mPL_Header.setPadding(10, 5, 10, 5);
            mPL_Header.setGravity(Gravity.CENTER);
            mPL_Header.setLayoutParams(rHeaderParams);
            mPL_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPL_Header);



            TableRow.LayoutParams rBefore = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rBefore.setMargins(10, 0, 10, 0);

            TextView mYear_Header = new TextView(getActivity());
            mYear_Header.setText(AppStrings.mYear);
            mYear_Header.setTypeface(LoginActivity.sTypeface);
            mYear_Header.setTextColor(Color.WHITE);
            mYear_Header.setPadding(10, 5, 10, 5);
            mYear_Header.setGravity(Gravity.CENTER);
            mYear_Header.setLayoutParams(rHeaderParams);
            mYear_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mYear_Header);

            TableRow.LayoutParams rDateOpen = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rDateOpen.setMargins(10, 0, 10, 0);

            TextView mDateopen_Header = new TextView(getActivity());
            mDateopen_Header.setText(AppStrings.mDateOpen);
            mDateopen_Header.setTypeface(LoginActivity.sTypeface);
            mDateopen_Header.setTextColor(Color.WHITE);
            mDateopen_Header.setPadding(10, 5, 10, 5);
            mDateopen_Header.setGravity(Gravity.CENTER);
            mDateopen_Header.setLayoutParams(rHeaderParams);
            mDateopen_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mDateopen_Header);
            mRightHeaderTable.addView(rightHeaderRow);

            for (int i = 0; i < datavalues.size() ; i++) {

                final int pos = i;
                selecteditemvalue = pos;
                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                leftContentParams.setMargins(5, 25, 5, 0);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),datavalues.get(i).getMemberName()));
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5,10, 5, 0);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);
                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());

                TableRow.LayoutParams rightContentTextviewParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT, 1f);
                rightContentTextviewParams.setMargins(5, 15, 5, 0);
                mBankName_values = new TextView(getActivity());
                mBankName_values.setId(i);
                sBankName_Fields.add(mBankName_values);
//                if(datavalues.get(i).getSssTypeId()!=null) {
//                    if (datavalues.get(i).getSssTypeId().equals(sssTypeid)) {
                       if(datavalues.get(i).getBankName()!=null) {
                           mBankName_values.setText(GetSpanText.getSpanString(getActivity(), datavalues.get(i).getBankName()));
                       }
                       else {
                           mBankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BANK")));
                       }
//                    }
//                    else {
//                        mBankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BANK")));
//                    }
//                }
//                else {
//                    mBankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BANK")));
//                }

                mBankName_values.setTextColor(R.color.black);
                mBankName_values.setPadding(5, 5, 5, 5);
                mBankName_values.setWidth(230);
                mBankName_values.setLayoutParams(rightContentTextviewParams);
                mBankName_values.setSingleLine(true);
                mBankName_values.setEllipsize(TextUtils.TruncateAt.END);

                mBankName_values.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selId = mBankName_values.getId();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        Log.d("Banknames",""+BankBranchTable.jandhanbankname);

                        if (BankBranchTable.jandhanbankname != null) {
                            Dialog_jandhanAccountNumberUpdation dialog = new Dialog_jandhanAccountNumberUpdation(getActivity(), BankBranchTable.jandhanbankname, pos, 0, mRightContentTable);
                            dialog.show(fm, "");
                        } else {
                            TastyToast.makeText(getActivity(), "Bank List Empty!", TastyToast.LENGTH_SHORT,
                                    TastyToast.ERROR);
                        }

                    }
                });
                rightContentRow.addView(mBankName_values);


                TableRow.LayoutParams rightContentTextviewParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentTextviewParams1.setMargins(20, 5, 20, 5);

                mBranchName_values = new TextView(getActivity());
                mBranchName_values.setId(i);
                sBranch_Fields.add(mBranchName_values);
//                if(datavalues.get(i).getSssTypeId()!=null) {
//                    if (datavalues.get(i).getSssTypeId().equals(sssTypeid)) {
                        if(datavalues.get(i).getBankName()!=null) {
                            mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), datavalues.get(i).getBranchName()));
                        }
                        else {
                            mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BRANCH")));
                        }
//                    }
//                    else {
//                        mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BRANCH")));
//                    }
//                }else {
//                    mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BRANCH")));
//                }
                mBranchName_values.setTextColor(R.color.black);
                mBranchName_values.setPadding(5, 5, 5, 5);
                mBranchName_values.setLayoutParams(rightContentTextviewParams1);
                mBranchName_values.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            Log.e("position_id", "" + pos);


                            EMathiApplication.setAccountNumberBankName(true);
                            FragmentManager fm = getActivity().getSupportFragmentManager();
//                            Log.e("selected_bank_branches", "" + selected_bank_branches.get(pos).toString());
                            List<String> branchnames = new ArrayList<>();
                            if (janadhanselected_bank_branches.containsKey(pos)) {

                                for (int i = 0; i < janadhanselected_bank_branches.get(pos).size(); i++) {
                                    Log.e("getBranchName", "" + janadhanselected_bank_branches.get(pos).get(i).getBranchName());
                                    branchnames.add(janadhanselected_bank_branches.get(pos).get(i).getBranchName());
                                }
//                                Log.e("branchnames", "" + branchnames.toString());
                                branchnames.add("NONE");
                                DialogBranchAdapter dialog = new DialogBranchAdapter(getActivity(), branchnames, pos, 1, mRightContentTable);
                                dialog.show(fm, "");
                            } else {
//                                List<String> no = new ArrayList<>();
                                branchnames.add("NONE");
                                DialogBranchAdapter dialog = new DialogBranchAdapter(getActivity(), branchnames, pos, 1, mRightContentTable);
                                dialog.show(fm, "");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Log.e("error", "" + e.toString());
                        }
                    }
                });
                rightContentRow.addView(mBranchName_values);


                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentParams.setMargins(10, 5, 10, 5);

                mAccountNumber_values = new EditText(getActivity());
                mAccountNumber_values.setId(i);
                mAccountNumber_values.setMaxLines(1);
                mAccountNumber_values.setSingleLine(true);
                mAccountNumber_values.setEllipsize(TextUtils.TruncateAt.END);
                sAccountNumber_Fields.add(mAccountNumber_values);
                mAccountNumber_values.setPadding(5, 5, 5, 5);
                mAccountNumber_values.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                ArrayList<InputFilter> curInputFilters = new ArrayList<InputFilter>(Arrays.asList(mAccountNumber_values.getFilters()));
                curInputFilters.add(0, new AlphaNumericInputFilter());
                curInputFilters.add(1, new InputFilter.LengthFilter(20));
                InputFilter[] newInputFilters = curInputFilters.toArray(new InputFilter[curInputFilters.size()]);
                mAccountNumber_values.setFilters(newInputFilters);
                mAccountNumber_values.setBackgroundResource(R.drawable.edittextbackgoundpmsby);
                mAccountNumber_values.setLayoutParams(rightContentParams);
                mAccountNumber_values.setWidth(300);
//                if(datavalues.get(i).getSssTypeId()!=null) {
//                    if (datavalues.get(i).getSssTypeId().equals(sssTypeid)) {
                        if(datavalues.get(i).getSss_number()!=null) {
                            mAccountNumber_values.setText(GetSpanText.getSpanString(getActivity(), datavalues.get(i).getSss_number()));
                        }else {
                            mAccountNumber_values.setText("");
                        }
//                    }
//                    else {
//                        mAccountNumber_values.setText("");
//                    }
//                }else {
//                    mAccountNumber_values.setText("");
//                }

                mAccountNumber_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            mMemberName.setTypeface(LoginActivity.sTypeface);
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });

                mAccountNumber_values.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int st, int b, int c)
                    {
                        Log.d("cli","ontextchanged");
                        onTextChanged = mAccountNumber_values.getText().toString();
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int st, int c, int a)
                    {
                        Log.d("cli1","beforeTextChanged");
                        beforeTextChanged = mAccountNumber_values.getText().toString();
                    }

                    @Override
                    public void afterTextChanged(Editable s)
                    {
                        Log.d("cli2","afterTextChanged");

                        afterTextChanged = sAccountNumber_Fields.get(pos).getText().toString().trim();

                        if(afterTextChanged.length()>0)
                        {
                            sBeforeYears_Fields.get(pos).setEnabled(true);
                            sBeforeYears_Fields.get(pos).setFocusable(true);
                        }
                        else {

                            sBeforeYears_Fields.get(pos).setEnabled(false);
                            sBeforeYears_Fields.get(pos).setFocusable(false);
                            sBeforeYears_Fields.get(pos).setText("CHOOSE");
                            sBeforeYears_Fields.get(pos).setTextColor(R.color.black);
                            sDateofopenings_Fields.get(pos).setEnabled(false);
                            sDateofopenings_Fields.get(pos).setFocusable(false);
                            sDateofopenings_Fields.get(pos).setText("");

                        }
                    }
                });
                rightContentRow.addView(mAccountNumber_values);


                TableRow.LayoutParams rightContentTextviewParamsBefore = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentTextviewParamsBefore.setMargins(20, 5, 20, 5);

                mBefore2019 = new TextView(getActivity());
                mBefore2019.setId(i);
                sBeforeYears_Fields.add(mBefore2019);
                mBefore2019.setEnabled(false);
//                if(datavalues.get(i).getSssTypeId()!=null) {
//                    if (datavalues.get(i).getSssTypeId().equals(sssTypeid)) {
                        if (datavalues.get(i).getIs_before_2019() != null) {
                            if (datavalues.get(i).getIs_before_2019().equals("true")) {
                                mBefore2019.setText(GetSpanText.getSpanString(getActivity(), "YES"));
                            } else {
                                mBefore2019.setText(GetSpanText.getSpanString(getActivity(), "NO"));
                            }
                        } else {
                            mBefore2019.setText(GetSpanText.getSpanString(getActivity(), "CHOOSE"));
                        }
//                    }
//                    else {
//                        mBefore2019.setText(GetSpanText.getSpanString(getActivity(), "CHOOSE"));
//
//                    }
//                }
//                else {
//                    mBefore2019.setText(GetSpanText.getSpanString(getActivity(), "CHOOSE"));
//
//                }
                mBefore2019.setTextColor(R.color.black);
                mBefore2019.setPadding(5, 5, 5, 5);
                mBefore2019.setWidth(230);
                mBefore2019.setLayoutParams(rightContentTextviewParamsBefore);
                mBefore2019.setSingleLine(true);
                mBefore2019.setEllipsize(TextUtils.TruncateAt.END);
                mBefore2019.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        try {
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                            final Dialog ChooseLoanType;
                            final View dialogView;
                            final RadioGroup radioGroup;
                            ChooseLoanType = new Dialog(getActivity());

                            LayoutInflater li = (LayoutInflater) getActivity()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            dialogView = li.inflate(R.layout.dialog_new_loantype, null, false);

                            TextView confirmationHeader = (TextView) dialogView
                                    .findViewById(R.id.dialog_ChooseLabel);
                            confirmationHeader.setText(AppStrings.chooseitem);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            int radioColor = getResources().getColor(R.color.pink);
                            radioGroup = (RadioGroup) dialogView.findViewById(R.id.dialog_RadioGroup);


                            for (int j = 0; j < mPOL_Values.length; j++) {

                                radioButton = new RadioButton(getActivity());
                                radioButton.setText(mPOL_Values[j]);
                                radioButton.setId(j);
                                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                                    radioButton.setButtonTintList(ColorStateList.valueOf(radioColor));
                                }

                                radioGroup.addView(radioButton);

                            }

                            ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
                            okButton.setText(AppStrings.dialogOk1);
                            okButton.setTypeface(LoginActivity.sTypeface);
                            okButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {
                                    // TODO Auto-generated method
                                    // stub

                                    selectedId = radioGroup.getCheckedRadioButtonId();
                                    Log.e(TAG, String.valueOf(selectedId));
                                    if ((selectedId != 100) && (selectedId != (-1))) {

                                        // find the radiobutton by
                                        // returned id
                                        RadioButton radioLoanButton = (RadioButton) dialogView
                                                .findViewById(selectedId);

                                        mSelectedLoanType = radioLoanButton.getText().toString();
                                        Log.v("On Selected LOAN TYPE", mSelectedLoanType);

                                        Log.v("view ID check : ", String.valueOf(v.getId()));

                                        TextView selectedTextView = (TextView) v.findViewById(v.getId());

                                        if (selectedId == 0) {
                                            selectedTextView.setText("YES");
                                        } else {
                                            selectedTextView.setText(mSelectedLoanType);
                                        }

                                        if(sBeforeYears_Fields.get(pos).getText().toString().equalsIgnoreCase("YES")) {
                                            sDateofopenings_Fields.get(pos).setEnabled(true);
                                            sDateofopenings_Fields.get(pos).setFocusable(true);

                                        }
                                        else
                                        {
                                            sDateofopenings_Fields.get(pos).setText("");
                                            sDateofopenings_Fields.get(pos).setEnabled(true);
                                            sDateofopenings_Fields.get(pos).setFocusable(true);
                                        }

                                        System.out.println("------idssssssssssss-----" + v.getId());

                                        ChooseLoanType.dismiss();

                                    } else {
                                        TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                                    }

                                }

                            });

                            ChooseLoanType.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            ChooseLoanType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ChooseLoanType.setCanceledOnTouchOutside(false);
                            ChooseLoanType.setContentView(dialogView);
                            ChooseLoanType.setCancelable(true);
                            ChooseLoanType.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                rightContentRow.addView(mBefore2019);


                TableRow.LayoutParams rightContentDateofopenings = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentDateofopenings.setMargins(10, 5, 10, 5);

                mdateofopening_values = new EditText(getActivity());
                mdateofopening_values.setId(i);
                mdateofopening_values.setMaxLines(1);
                mdateofopening_values.setSingleLine(true);
                mdateofopening_values.setEllipsize(TextUtils.TruncateAt.END);
                mdateofopening_values.setEnabled(false);
                sDateofopenings_Fields.add(mdateofopening_values);
                mdateofopening_values.setPadding(5, 5, 5, 5);
                mdateofopening_values.setFilters(Get_EdiText_Filter.editText_AccNo_filter());
                mdateofopening_values.setBackgroundResource(R.drawable.edittext_background);
                mdateofopening_values.setLayoutParams(rightContentDateofopenings);
                mdateofopening_values.setWidth(300);
                String  sssdate = String.valueOf(datavalues.get(i).getSss_date());
//                if(datavalues.get(i).getSssTypeId()!=null) {
//                    if (datavalues.get(i).getSssTypeId().equals(sssTypeid)) {
                        if (sssdate != null && (!sssdate.equals("0"))) {

                            DateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                            Date d = new Date(datavalues.get(i).getSss_date());
                            String dateStr = dateFormat1.format(d);
                            mdateofopening_values.setText(GetSpanText.getSpanString(getActivity(), dateStr));
                        } else {
                            mdateofopening_values.setText("");
                        }
                    /*}
                    else {
                        mdateofopening_values.setText("");
                    }
                }else {
                    mdateofopening_values.setText("");
                }*/
                mdateofopening_values.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                            if(sBeforeYears_Fields.get(pos).getText().toString().equalsIgnoreCase("YES")) {

                                Calendar now = Calendar.getInstance();
                                Calendar lastDate = Calendar.getInstance();
//                            long s =1577800608000l;
                                Log.v("view ID check : ", String.valueOf(view.getId()));

                                final EditText selectedTextView = (EditText) view.findViewById(view.getId());

                                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                                        Calendar selectionCal = Calendar.getInstance();
                                        selectionCal.set(year, month, day);
                                        String formattedDate = df.format(selectionCal.getTime());
                                        selectedTextView.setText(formattedDate);

                                    }
                                }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE));
                                datePickerDialog.getDatePicker().setMaxDate(1546194600000l);
                                datePickerDialog.show();
                            }
                            else
                            {
                                Calendar now = Calendar.getInstance();
                                Calendar lastDate = Calendar.getInstance();
//                            long s =1577800608000l;
                                Log.v("view ID check : ", String.valueOf(view.getId()));

                                final EditText selectedTextView = (EditText) view.findViewById(view.getId());

                                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                                        Calendar selectionCal = Calendar.getInstance();
                                        selectionCal.set(year, month, day);
                                        String formattedDate = df.format(selectionCal.getTime());
                                        selectedTextView.setText(formattedDate);

                                    }
                                }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE));
                                datePickerDialog.getDatePicker().setMinDate(1546281000000l);
                                datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
                                datePickerDialog.show();
                            }

                        }
                        return false;
                    }
                });

                rightContentRow.addView(mdateofopening_values);
                mRightContentTable.addView(rightContentRow);

            }

            mSubmit_Raised_Button = (RaisedButton) view1.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);


            resizeMemberNameWidth();
//            resizeRightSideTable();
            resizeBodyTableRowHeight();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

   /* private void resizeRightSideTable() {
        // TODO Auto-generated method stub
        int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
        for (int i = 0; i < rightHeaderCount; i++) {
            rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
            rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
        }
        for (int i = 0; i < rightHeaderCount; i++) {
            if (rightHeaderWidth[i] < rightContentWidth[i]) {
                ((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightContentWidth[i];
            } else {
                ((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightHeaderWidth[i];
            }
        }
    }*/

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fragment_Submit_button:

                accNoArr_value = new String[sAccountNumber_Fields.size()];
                bankNameArr_value = new String[sAccountNumber_Fields.size()];
                branchNameArr_value = new String[sAccountNumber_Fields.size()];
                year_value = new String[sAccountNumber_Fields.size()];
                dateofOpening = new String[sAccountNumber_Fields.size()];

                accNoArr_server = new String[sAccountNumber_Fields.size()];
                bankName_server = new String[sAccountNumber_Fields.size()];
                branchName_server = new String[sAccountNumber_Fields.size()];
                yearvalue_server = new String[sAccountNumber_Fields.size()];
                dateofpening_server = new String[sAccountNumber_Fields.size()];


                for (int i = 0; i < memList.size(); i++) {

                    accNoArr_value[i] = sAccountNumber_Fields.get(i).getText().toString();
                    accNoArr_server[i] = sAccountNumber_Fields.get(i).getText().toString();

                    bankNameArr_value[i] = sBankName_Fields.get(i).getText().toString();
                    bankName_server[i] = sBankName_Fields.get(i).getText().toString();

                    branchNameArr_value[i] = sBranch_Fields.get(i).getText().toString();
                    branchName_server[i] = sBranch_Fields.get(i).getText().toString();

                    year_value[i]=sBeforeYears_Fields.get(i).getText().toString();
                    yearvalue_server[i]=sBeforeYears_Fields.get(i).getText().toString();

                    dateofOpening[i] = sDateofopenings_Fields.get(i).getText().toString();
                    dateofpening_server[i] = sDateofopenings_Fields.get(i).getText().toString();


                    if (bankNameArr_value[i].equals("SELECT A BANK") || bankNameArr_value[i].equals("NONE") || (BankBranchTable.jandhanbankname_id == null || BankBranchTable.jandhanbankname_id.equals(""))) {
                        bankNameArr_value[i] = "No";
                        // BankBranchTable.bankname_id.add(i, "No");
                    }

                    if (accNoArr_value[i].equals("")) {
                        accNoArr_value[i] = "No";
                    }

//                    if (branchNameArr_value[i].equals("SELECT A BRANCH") || branchNameArr_value[i].equals("NONE") || (BankBranchTable.jandhanbranchnames_id == null || BankBranchTable.jandhanbranchnames_id.equals(""))) {
                    if (branchNameArr_value[i].equals("SELECT A BRANCH") || branchNameArr_value[i].equals("NONE")) {
                        branchNameArr_value[i] = "No";
                        // BankBranchTable.branchnames_id.add(i, "No");
                    }

                    if(year_value[i].equals("CHOOSE"))
                    {
                        year_value[i] = "CHOOSE";
                    }

                    if(dateofOpening[i].equals(""))
                    {
                        dateofOpening[i]="No";
                    }

                }

//                Log.d("emptycheck",bankNameArr_value+""+accNoArr_value+""+branchNameArr_value+""+year_value+""+dateofOpening);
//                if((!bankNameArr_value.equals("No")) && (!accNoArr_value.equals("No")) && (!branchNameArr_value.equals("No")) && (!year_value.equals("CHOOSE")) && (!dateofOpening.equals("No")))
//                {
//                    TastyToast.makeText(getActivity(),"PLEASE ENTER THE DETAILS",TastyToast.LENGTH_LONG,TastyToast.ERROR);
//                }
//                else {
//                    callConfirmationDialog();
//                }

//                for (int j = 0; j <bankNameArr_value.length ; j++) {
//                    if(!bankNameArr_value[j].equals("No")) {
//                        callConfirmationDialog();
//                        break;
//                    } else {
//                        TastyToast.makeText(getActivity(),"PLEASE ENTER THE DETAILS",TastyToast.LENGTH_LONG,TastyToast.ERROR);
//
//                    }
//                }

                callConfirmationDialog();
                break;

            case R.id.fragment_Edit:

                mSubmit_Raised_Button.setClickable(true);
                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:
                otherMicroInsurancepostApiCall();
                break;
        }

    }

    private void otherMicroInsurancepostApiCall() {

        confirmationDialog.dismiss();
        ssnNumbersDTOLists =new ArrayList<>();
        tableDataArrayList =new ArrayList<>();
        for (int i = 0; i < memList.size() ; i++) {
            SsnNumbersDTOList ssnNumbersDTOList = new SsnNumbersDTOList();
            tableData =new TableData();
            ssnNumbersDTOList.setShgId(memList.get(i).getShgId());
            tableData.setShgId(memList.get(i).getShgId());
            ssnNumbersDTOList.setUserId(memList.get(i).getMemberUserId());
            ssnNumbersDTOList.setMemberId(memList.get(i).getMemberId());
            tableData.setMemberId(memList.get(i).getMemberId());
            tableData.setMemberName(memList.get(i).getMemberName());

            if(sAccountNumber_Fields.get(i).getText().toString().isEmpty())
            {
                ssnNumbersDTOList.setSss_number("");
                tableData.setSss_number("");
            }
            else
            {
                ssnNumbersDTOList.setSss_number(sAccountNumber_Fields.get(i).getText().toString());
                tableData.setSss_number(sAccountNumber_Fields.get(i).getText().toString());
            }

            Log.d("bannkvalues",bankNameArr_value[i]+"");
            if (bankNameArr_value[i].equals("No") || bankNameArr_value[i].equals("SELECT A BANK") || bankNameArr_value[i].equals("NONE")) {
                ssnNumbersDTOList.setBankNameId("");
                tableData.setBankNameId("");
                ssnNumbersDTOList.setIsSss("0");
                tableData.setIs_sss("false");
//                tableData.setBankName("");
            } else {
                for (int i1 = 0; i1 < BankBranchTable.jandhanbankList.size(); i1++) {
                    if (bankNameArr_value[i].equals(BankBranchTable.jandhanbankList.get(i1).getName())) {
                        ssnNumbersDTOList.setBankNameId(BankBranchTable.jandhanbankname_id.get(i1));
                        tableData.setBankNameId(BankBranchTable.jandhanbankname_id.get(i1));
                        tableData.setBankName(BankBranchTable.jandhanbankList.get(i1).getName());
                        tableData.setIs_sss("true");
                        ssnNumbersDTOList.setIsSss("1");

                    }
                }

            }

            List<BranchList> branchList = BankBranchTable.getJandhanBranchList(ssnNumbersDTOList.getBankNameId());
            Log.d("bannkvalues",branchNameArr_value[i]+"");
            if (branchNameArr_value[i].equals("No") || branchNameArr_value[i].equals("SELECT A BRANCH") || branchNameArr_value[i].equals("NONE") || BankBranchTable.jandhanbranchnames_id.equals("")) {
                ssnNumbersDTOList.setBranchId("");
                tableData.setBranchId("");
//                tableData.setBranchName("");
            } else {

                for (int i1 = 0; i1 < branchList.size(); i1++) {
                    if (branchNameArr_value[i].equals(branchList.get(i1).getBranchName())) {
                        ssnNumbersDTOList.setBranchId(branchList.get(i1).getBranchId());
                        tableData.setBranchNameId(branchList.get(i1).getBranchId());
                        tableData.setBranchName(branchList.get(i1).getBranchName());
                    }
                }

            }

            Date date = null;
            long mills=0;
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            try {
                if ((!sDateofopenings_Fields.get(i).getText().toString().equals("0"))&&(!sDateofopenings_Fields.get(i).getText().toString().equals("")))
                {
                    date = dateFormat.parse(sDateofopenings_Fields.get(i).getText().toString());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(date!=null)
            {
                mills = date.getTime();

            }
            if(mills!=0) {

                tableData.setSss_date(mills);
            }
            else
            {
                tableData.setSss_date(0);
            }


            if(mills!=0) {
                DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(mills);
                String dateStr = dateFormat1.format(d);
                ssnNumbersDTOList.setSssDte(dateStr);
            }
            else
            {
                ssnNumbersDTOList.setSssDte("");

            }

            ssnNumbersDTOList.setSssTypeUuid(sssTypeid);
            tableData.setSssTypeId(sssTypeid);
            if(sBeforeYears_Fields.get(i).getText().toString().equalsIgnoreCase("YES"))
            {
                ssnNumbersDTOList.setIsBefore("1");
                tableData.setIs_before_2019("true");
            }
            else if(sBeforeYears_Fields.get(i).getText().toString().equalsIgnoreCase("NO")) {
                ssnNumbersDTOList.setIsBefore("1");
                tableData.setIs_before_2019("false");
            }
            else {

            }

            if(!ssnNumbersDTOList.getBankNameId().isEmpty()) {
                ssnNumbersDTOLists.add(ssnNumbersDTOList);
            }
            tableDataArrayList.add(tableData);

        }

        SsnNumberRequestDTOList ssnNumberRequestDTOList  = new SsnNumberRequestDTOList();
        ssnNumberRequestDTOList.setSsnNumbersDTOList(ssnNumbersDTOLists);

        String checkData = new Gson().toJson(ssnNumberRequestDTOList);
        Log.d("NumbersDTOLists", " " + checkData);

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_SSN_NUMBER, checkData, getActivity(), ServiceType.OTHER_MICRO_INSURANCE);
        }
    }


    private void callConfirmationDialog() {

        savingsAccountDTOLists.clear();
        confirmationDialog = new Dialog(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactiontext);
        transactdate.setVisibility(View.GONE);

        TableRow header_row = new TableRow(getActivity());

        TableRow.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        headerParams.setMargins(10, 5, 10, 5);

        TextView bankName_header = new TextView(getActivity());
        bankName_header.setText(AppStrings.bankName);
        bankName_header.setTypeface(LoginActivity.sTypeface);
        bankName_header.setTextColor(R.color.black);
        bankName_header.setPadding(5, 5, 5, 5);
        bankName_header.setLayoutParams(headerParams);
        header_row.addView(bankName_header);


        TextView branchName_header = new TextView(getActivity());
        branchName_header.setText(AppStrings.mBranchName);
        branchName_header.setTypeface(LoginActivity.sTypeface);
        branchName_header.setTextColor(R.color.black);
        branchName_header.setPadding(5, 5, 5, 5);
        branchName_header.setLayoutParams(headerParams);
        header_row.addView(branchName_header);

        TextView accNo_header = new TextView(getActivity());
        accNo_header.setText(AppStrings.mAccountNumber);
        accNo_header.setTypeface(LoginActivity.sTypeface);
        accNo_header.setTextColor(R.color.black);
        accNo_header.setPadding(5, 5, 5, 5);
        accNo_header.setLayoutParams(headerParams);
        header_row.addView(accNo_header);

        TextView year_header = new TextView(getActivity());
        year_header.setText(AppStrings.mYear);
        year_header.setTypeface(LoginActivity.sTypeface);
        year_header.setTextColor(R.color.black);
        year_header.setPadding(5, 5, 5, 5);
        year_header.setLayoutParams(headerParams);
        header_row.addView(year_header);

        TextView dateofopen_header = new TextView(getActivity());
        dateofopen_header.setText(AppStrings.mDateOpen);
        dateofopen_header.setTypeface(LoginActivity.sTypeface);
        dateofopen_header.setTextColor(R.color.black);
        dateofopen_header.setPadding(5, 5, 5, 5);
        dateofopen_header.setLayoutParams(headerParams);
        header_row.addView(dateofopen_header);

        confirmationTable.addView(header_row,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i < datavalues.size(); i++) {

            TableRow indv_SavingsRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView bankName_Text = new TextView(getActivity());
            bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankNameArr_value[i])));
            bankName_Text.setTypeface(LoginActivity.sTypeface);
            bankName_Text.setTextColor(R.color.black);
            bankName_Text.setPadding(5, 5, 5, 5);
            bankName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(bankName_Text);

            TextView branchName_Text = new TextView(getActivity());
            branchName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(branchNameArr_value[i])));
            branchName_Text.setTextColor(R.color.black);
            branchName_Text.setPadding(5, 5, 5, 5);
            branchName_Text.setGravity(Gravity.RIGHT);
            branchName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(branchName_Text);

            TextView accNo_Text = new TextView(getActivity());
            accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(accNoArr_value[i])));
            accNo_Text.setTextColor(R.color.black);
            accNo_Text.setPadding(5, 5, 5, 5);
            accNo_Text.setGravity(Gravity.RIGHT);
            accNo_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(accNo_Text);


            TextView year_Text = new TextView(getActivity());
            year_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(year_value[i])));
            year_Text.setTextColor(R.color.black);
            year_Text.setPadding(5, 5, 5, 5);
            year_Text.setGravity(Gravity.RIGHT);
            year_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(year_Text);

            TextView dateofopen_Text = new TextView(getActivity());
            dateofopen_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(dateofOpening[i])));
            dateofopen_Text.setTextColor(R.color.black);
            dateofopen_Text.setPadding(5, 5, 5, 5);
            dateofopen_Text.setGravity(Gravity.RIGHT);
            dateofopen_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(dateofopen_Text);

            confirmationTable.addView(indv_SavingsRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);


        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType)
        {
            case PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION_LIST:

                try {
                    cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    int statusCode = cdto.getStatusCode();
                    String message = cdto.getMessage();
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                    } else if (statusCode == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    } else if (statusCode == Utils.Success_Code) {

                        loanBankDtoArrayList = cdto.getResponseContent().getBankNamesList();

                        BankBranchTable.insertJandhanBranchBankData(cdto);
                        bankList = BankBranchTable.getJandhanBankname();
                        Log.d("banknames",bankList+"");


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

/*
            case GETSSSTYPEIDFETCHING:

                try {

                    sssTypeDtos = new ArrayList<>();
                    JSONObject value1 = new JSONObject(result);
                    if ((value1.getString("statusCode")).equals("200")) {
                        JSONArray jsonArray = value1.getJSONArray("responseContents");
                        for (int x = 0; x < jsonArray.length(); x++) {
                            SssTypeDto sssTypeDto = new SssTypeDto();
                            JSONObject JO = jsonArray.getJSONObject(x);
                            String name = JO.getString("name");
                            sssTypeDto.setName(name);
                            String createdDate = JO.getString("createdDate");
                            sssTypeDto.setCreatedDate(createdDate);
                            String modifiedDate = JO.getString("modifiedDate");
                            sssTypeDto.setModifiedDate(modifiedDate);
                            String status = JO.getString("status");
                            sssTypeDto.setStatus(status);
                            String pflag = JO.getString("pflag");
                            sssTypeDto.setPflag(pflag);
                            String sflag = JO.getString("sflag");
                            sssTypeDto.setSflag(sflag);
                            String id = JO.getString("id");
                            sssTypeDto.setId(id);
                            sssTypeDtos.add(sssTypeDto);
                        }

                        for (int x = 0; x < sssTypeDtos.size(); x++) {

                            String o_miValue = sssTypeDtos.get(x).getName();

                            if (o_miValue.equalsIgnoreCase("JAN DHAN")) {
                                o_mi_ssstypeid = sssTypeDtos.get(x).getId();
                            }

                        }

//                        tableDatacheck = SHGTable.getCheckSSSTypeIdAvailable(shgDto.getShgId());
//
//                        for (int i = 0; i <tableDatacheck.size() ; i++) {
//
//                            if(tableDatacheck.get(i).getSssTypeId()==null)
//                            {
//                                datavalues= SHGTable.getSSNAllData(shgDto.getShgId());
//                            }
//                            else {
//                                datavalues = SHGTable.getSSNData(shgDto.getShgId(), sssTypeid);
//                            }
//                            Log.e("val", datavalues + "");
//                        }
                        */
/*if(tableDatacheck.getSssTypeId()==null)
                        {
                            datavalues= SHGTable.getSSNAllData(shgDto.getShgId());
                        }
                        else {
                            datavalues = SHGTable.getSSNData(shgDto.getShgId(), o_mi_ssstypeid);
                        }*//*


//                        inIt(view);

                    } else {
                        if ((value1.getString("statusCode")).equals("401")) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        if (confirmationDialog.isShowing()) {
                            confirmationDialog.dismiss();
                        }
                        Utils.showToast(getActivity(), value1.getString("message"));
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
*/

            case OTHER_MICRO_INSURANCE:

                try {
                    ResponseDto cdto = new Gson().fromJson(result, ResponseDto.class);

                    if (cdto != null) {
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {

                            for (int i = 0; i <datavalues.size() ; i++) {
//                                if (datavalues.get(i).getSssTypeId()!= null) {
//                                    SHGTable.updateSsnDetails(tableDataArrayList);
//                                    if(datavalues.get(i).getSssTypeId().equals(sssTypeid)) {
                                        SHGTable.updateSsnDetails1(tableDataArrayList.get(i));
//                                    } else
//                                    {
//                                        DbHelper.getInstance(getActivity()).insertSSNList_Data(tableDataArrayList.get(i));
//                                    }
//
//                                } else {
////                                    DbHelper.getInstance(getActivity()).insertSSNList_Data(tableDataArrayList.get(i));
//                                    SHGTable.updateSsnDetails1(tableDataArrayList.get(i));
//
//                                }
                            }
                            TastyToast.makeText(getActivity(),message,TastyToast.LENGTH_LONG,TastyToast.SUCCESS);
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Profile",MainFragment.Flag_Profile);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);

                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}

