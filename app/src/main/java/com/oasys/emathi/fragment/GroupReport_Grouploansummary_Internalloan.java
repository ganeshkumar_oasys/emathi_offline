package com.oasys.emathi.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.emathi.Adapter.GroupReport_Grouploansummary_Internalloan_Adapter;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.database.SHGTable;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupReport_Grouploansummary_Internalloan extends Fragment implements NewTaskListener {

    private GroupReport_Grouploansummary_Internalloan_Adapter groupReport_grouploansummary_internalloan_adapter;
    private RecyclerView grouploan_summary_internalloan_expandablelistview;
    private LinearLayoutManager linearLayoutManager;
    ResponseDto responseDto;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, Name, gloan, os, loanamount, group_otherLoan_values,date,amt,inrt;
//    ArrayList<GroupInternalLoansList> groupInternalLoansLists;


    public GroupReport_Grouploansummary_Internalloan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_report__grouploansummary__internalloan, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            grouploan_summary_internalloan_expandablelistview = (RecyclerView) view.findViewById(R.id.grouploan_summary_internalloan_expandablelistview);
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.ch);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cb);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

   /*     Name = (TextView) view.findViewById(R.id.Name);
        Name.setText(shgDto.getPresidentName());
        Name.setTypeface(LoginActivity.sTypeface);*/

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText("Group Internal Loan Summary");
            mHeader.setTypeface(LoginActivity.sTypeface);

            date = (TextView) view.findViewById(R.id.date);
            date.setText("NAME");
            date.setTypeface(LoginActivity.sTypeface);
            amt = (TextView) view.findViewById(R.id.amt);
            amt.setText(AppStrings.amount);
            amt.setTypeface(LoginActivity.sTypeface);
            inrt = (TextView) view.findViewById(R.id.inrt);
            inrt.setText(AppStrings.mRepaid);
            inrt.setTypeface(LoginActivity.sTypeface);

            gloan = (TextView) view.findViewById(R.id.gloan);
            gloan.setTypeface(LoginActivity.sTypeface);
            gloan.setText(AppStrings.groupLoan + " :");
            loanamount = (TextView) view.findViewById(R.id.loanamount);
            loanamount.setTypeface(LoginActivity.sTypeface);
            group_otherLoan_values = (TextView) view.findViewById(R.id.group_otherLoan_values);
            group_otherLoan_values.setTypeface(LoginActivity.sTypeface);
            os = (TextView) view.findViewById(R.id.os);
            os.setText(AppStrings.outstanding + " :");
            os.setTypeface(LoginActivity.sTypeface);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            grouploan_summary_internalloan_expandablelistview.setLayoutManager(linearLayoutManager);
            grouploan_summary_internalloan_expandablelistview.setHasFixedSize(true);
            grouploan_summary_internalloan_expandablelistview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            RestClient.getRestClient(GroupReport_Grouploansummary_Internalloan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUPLOANSUMMARY_INTERNALLOAN + shgDto.getShgId(), getActivity(), ServiceType.GROUPLOANSUMMARY_INTERNALLOAN_TYPE);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case GROUPLOANSUMMARY_INTERNALLOAN_TYPE:
                if (result != null && result.length() > 0) {
                    Log.d("getDetails", " " + result);
                    responseDto = new Gson().fromJson(result, ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        responseDto.getResponseContent().getGroupInternalLoansList();
                        Log.d("check", "" + responseDto.getResponseContent().getGroupInternalLoansList());

                        groupReport_grouploansummary_internalloan_adapter = new GroupReport_Grouploansummary_Internalloan_Adapter(getActivity(), responseDto.getResponseContent().getGroupInternalLoansList());
                        grouploan_summary_internalloan_expandablelistview.setAdapter(groupReport_grouploansummary_internalloan_adapter);
                        loanamount.setText(" ₹" +responseDto.getResponseContent().getTotalGrouploan());
                        group_otherLoan_values.setText(" ₹" +responseDto.getResponseContent().getLoanOutstanding());

                    }
                    else
                    {
                        Toast.makeText(getActivity(),responseDto.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }

    }
}
