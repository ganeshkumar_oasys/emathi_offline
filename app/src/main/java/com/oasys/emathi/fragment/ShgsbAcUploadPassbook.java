package com.oasys.emathi.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.RequestDto.ImageDto;
import com.oasys.emathi.Dto.RequestDto.ShgAccountNumbersUpdateDTOList;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.ShgBankDTOList;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.FileUtil;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.zelory.compressor.Compressor;


public class ShgsbAcUploadPassbook extends Fragment implements NewTaskListener, View.OnClickListener {

    private TextView mGroupName,mCashInHand,mCashAtBank,mShgaccountnumberheader,mBankname,mBranchname;
    private EditText mShgau_accountnumber_edittext;
    private View rootView;
    private ListOfShg shgDto;
    LayoutInflater in;
    ArrayList<ShgBankDTOList> shgAccountNumberList;
    String mAccountNumber_GetEdit;
    String _bankNameValue, _BranchNameValue,mBankID;

    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;
    ImageView mImageView;
    private File actualImage,compressedImage;
    private String finalImageStr;
    RaisedButton mTakePhoto, mSubmitPhoto;
    Bitmap bitmap_ = null;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    ResponseDto shgbankupdationDto;
    ArrayList<ShgAccountNumbersUpdateDTOList> shgAccountNumbersUpdateDTOLists;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_shgsb_ac_upload_passbook, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        inIt(rootView);
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

            //   setPic();

            String filePath = mCurrentPhotoPath;
            if (filePath != null) {
                Bitmap selectedImage = decodeFile(new File(filePath));
                //   Bitmap bitmap = (Bitmap) data.getExtras().get("data"); //CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
                mImageView.setImageBitmap(selectedImage);
                finalImageStr = bitmapToString(selectedImage);
            }

            try {
                mTakePhoto.setEnabled(false);
                mSubmitPhoto.setVisibility(View.VISIBLE);
                //   bitmap_ = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void inIt(View view) {

        try {

            shgAccountNumbersUpdateDTOLists = new ArrayList<>();

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mShgaccountnumberheader = (TextView) rootView.findViewById(R.id.shgaccountnumberheader);
            mShgaccountnumberheader.setText(AppStrings.mshgsbacuploadpassbook);
            mShgaccountnumberheader.setTypeface(LoginActivity.sTypeface);

            mShgau_accountnumber_edittext = (EditText)rootView.findViewById(R.id.shgau_accountnumber_edittext);
            mBankname = (TextView) rootView.findViewById(R.id.bankname);
            mBranchname = (TextView) rootView.findViewById(R.id.branchname);

            mTakePhoto =(RaisedButton) rootView.findViewById(R.id.take_photo);
            mTakePhoto.setText(AppStrings.upload_passbook);
            mTakePhoto.setTypeface(LoginActivity.sTypeface);
            mTakePhoto.setOnClickListener(this);

            mSubmitPhoto = (RaisedButton) rootView.findViewById(R.id.submit_photo);
            mSubmitPhoto.setOnClickListener(this);
            mSubmitPhoto.setTypeface(LoginActivity.sTypeface);
            mSubmitPhoto.setText(AppStrings.submit);

            mImageView = (ImageView) rootView.findViewById(R.id.imageview);

            shownValues();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void shownValues() {

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            String url = Constants.BASE_URL + Constants.PROFILE_GETSHGACCOUNTNUMBERUPDATION + shgDto.getShgId().toString();
            RestClient.getRestClient(ShgsbAcUploadPassbook.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.SHGACCOUNTNUMBER);
        } else {
            Utils.showToast(getActivity(), "No network Available");
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        try {

            switch (serviceType){

                case SHGACCOUNTNUMBER:

                    ResponseDto shgbankupdationDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    in = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);//Layout inflater for inflating layout views

                    if (shgbankupdationDto.getStatusCode() == Utils.Success_Code) {

                        shgAccountNumberList = new ArrayList<>();
                        if ((shgbankupdationDto.getResponseContent().getShgBankDTOList() != null)) {

                            for (int i = 0; i < shgbankupdationDto.getResponseContent().getShgBankDTOList().size(); i++) {

                                if (shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).isPrimary() == true) {
                                    mAccountNumber_GetEdit = shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getAccountNumber();
                                    mShgau_accountnumber_edittext.setText(mAccountNumber_GetEdit);
                                    mBankID = shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBankId();
                                    _bankNameValue = String.valueOf(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBankName());
                                    mBankname.setText(_bankNameValue);
                                    _BranchNameValue = String.valueOf(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBranchName());
                                    mBranchname.setText(_BranchNameValue);
                                }
                            }

                        } else {
                            if (shgbankupdationDto.getStatusCode() == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                            Utils.showToast(getActivity(), "Null Value");
                        }

                    }
                    break;

                case SHGACCOUNTNUMBERUPLOADANDPASSBOOK:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                Utils.showToast(getActivity(), message);
                                NewDrawerScreen.showFragment(new MainFragment());
                                TastyToast.makeText(getActivity(), message,
                                        TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);

                            } else {

                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                            }
                        }
                    } catch (Exception e) {

                    }
                    break;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.take_photo:
                takePhoto();
                break;
            case R.id.submit_photo:
                if(finalImageStr!=null) {
                    ImageDto imageDto = new ImageDto();
                    imageDto.setUrl(finalImageStr);
                    ShgAccountNumbersUpdateDTOList shgAccountNumbersUpdateDTOList = new ShgAccountNumbersUpdateDTOList();
                        shgAccountNumbersUpdateDTOList.setBankId(mBankID);
                        shgAccountNumbersUpdateDTOList.setShgId(shgDto.getShgId());
                        shgAccountNumbersUpdateDTOList.setAccountNumber(mAccountNumber_GetEdit);
                        shgAccountNumbersUpdateDTOLists.add(shgAccountNumbersUpdateDTOList);
                        Log.d("shgAccountNumbers", " " + shgAccountNumbersUpdateDTOLists.size());


                   imageDto.setShgAccountNumbersUpdateDTOList(shgAccountNumbersUpdateDTOLists);
                    String sreqString = new Gson().toJson(imageDto);
                    mSubmitPhoto.setClickable(false);
                    if (networkConnection.isNetworkAvailable()) {
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.PROFILE_SHGACCOUNTNUMBERANDUPLOADPASSBOOK + shgDto.getShgId(), sreqString, getActivity(), ServiceType.SHGACCOUNTNUMBERUPLOADANDPASSBOOK);
                    }
                }else {
                    TastyToast.makeText(getActivity(), AppStrings.mPhotoErrorMsg, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                }
                break;
        }
    }

    private void takePhoto() {
        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i("photo path = ", mCurrentPhotoPath);
        return image;
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor << 1;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        try {

            actualImage = FileUtil.from(getActivity(), Uri.fromFile(new File(mCurrentPhotoPath)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            String storageDir = Environment.getExternalStorageDirectory() + "/Files/Compressed";

            compressedImage = new Compressor(getActivity()).setMaxWidth(640).setMaxHeight(480).setQuality(35)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)

                    .setDestinationDirectoryPath(storageDir).compressToFile(actualImage);

        } catch (IOException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }

        Matrix mtx = new Matrix();
        // mtx.postRotate(90);
        // Rotating Bitmap
        Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

        if (rotatedBMP != bitmap)
            bitmap.recycle();

        mImageView.setImageBitmap(rotatedBMP);
        finalImageStr = BitMapToString(rotatedBMP);

        File f = new File(compressedImage.getPath());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        try {
            mTakePhoto.setEnabled(false);
            mSubmitPhoto.setVisibility(View.VISIBLE);
            bitmap_ = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void showError(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private Bitmap decodeFile(File f) {

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Find the correct scale value. It should be the power of 2.
        final int REQUIRED_SIZE = 200;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    String bitmapToString(Bitmap bm) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

}