package com.oasys.emathi.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.oasys.emathi.Adapter.CustomExpandableMenuListAdapter;
import com.oasys.emathi.Adapter.Transaction_GroupLoanRepayment_Adapter;
import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.ShgBankDetails;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.ExpandListItemClickListener;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.R;
import com.oasys.emathi.activity.LoginActivity;
import com.oasys.emathi.activity.NewDrawerScreen;
import com.oasys.emathi.database.BankTable;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.MemberTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.model.ListItem;
import com.oasys.emathi.model.RowItem;
import com.oasys.emathi.views.MyExpandableListview;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_ExternalLoan extends Fragment implements ExpandListItemClickListener {
    public static final String TAG = LD_ExternalLoan.class.getSimpleName();
    private TextView mGroupName, mCashInHand, mCashAtBank;
    private Dialog mProgressDialog;
    public static List<RowItem> rowItems;
    public static String[] mGroupLoanNames;
    public static String mloan_Id;
    public static String mloan_Name;
    public static String mSendTo_ServerLoan_Id;
    public static String[] response;
    public static String mGroupOS_Offlineresponse = null;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    int size;

    // private ExpandableLayoutListView mListView;
    private MyExpandableListview mListView;
    private List<ListItem> listItems = new ArrayList<>();
    // private CustomMenuListAdapter mAdapter;
    private CustomExpandableMenuListAdapter mAdapter;
    int listImage;
    private TextView mHeader;
    private ArrayList<HashMap<String, String>> childList;
    private int lastExpandedPosition = -1;
    Date date_dashboard, date_loanDisb;

    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;
    private List<ExistingLoan> loanDetails;
    private ArrayList<ExistingLoan> listLoan;
    public static ExistingLoan sExistingLoagSelection;

    private List<ExistingLoan> grploanDetails;
    private HashMap<String, ArrayList<ExistingLoan>> listDataChild;
    private ArrayList<ExistingLoan> listDataHeader;
    ArrayList<ExistingLoan> mainlistarray;
    public static Transaction_GroupLoanRepayment_Adapter transaction_groupLoanRepayment_adapter;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_menulist_expandable, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        loanDetails = LoanTable.getGrpLoanDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        grploanDetails = LoanTable.getGrpLoanDetails(shgDto.getShgId());
        listDataChild = new HashMap<String, ArrayList<ExistingLoan>>();
        listDataHeader = new ArrayList<ExistingLoan>();

        init();
        /*if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(LD_ExternalLoan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_PLOAN + shgDto.getShgId(), getActivity(), ServiceType.PURPOSELOAN);
        }*/

    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.expandableSubmenuHeaderTextview);
            mHeader.setVisibility(View.VISIBLE);
            mHeader.setText("" + AppStrings.mExistingLoan);
            mHeader.setTypeface(LoginActivity.sTypeface);
            try {

                /*
                 * mListView = (ExpandableLayoutListView)
                 * rootView.findViewById(R.id.fragment_List_loanrepaid);
                 * mListView.setOnItemClickListener(this);
                 */
                mListView = (MyExpandableListview) rootView.findViewById(R.id.fragment_List_loanrepaid);
                listImage = R.drawable.ic_navigate_next_white_24dp;

                // rowItems = new ArrayList<RowItem>();


                mListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        // TODO Auto-generated method stub
                        if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                            mListView.collapseGroup(lastExpandedPosition);
                        }
                        lastExpandedPosition = groupPosition;
                    }
                });

                mainlistarray = (ArrayList<ExistingLoan>) grploanDetails;

                for (int i = 0; i < grploanDetails.size(); i++) {
                    listDataChild.put(grploanDetails.get(0).getLoanId(), mainlistarray);
                }

                transaction_groupLoanRepayment_adapter = new Transaction_GroupLoanRepayment_Adapter(getActivity(), mainlistarray, listDataChild, this);

                mListView.setAdapter(transaction_groupLoanRepayment_adapter);


//                mAdapter = new CustomExpandableMenuListAdapter(getActivity(), listItems, childList, this);
//                mListView.setAdapter(mAdapter);

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


   /* @Override
    public void onTaskStarted() {
        mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDialog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDialog != null) {
            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }

            switch (serviceType) {

                case PURPOSELOAN:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);

                            listLoan = cdto.getResponseContent().getLoansList();
                            listItems = new ArrayList<ListItem>();

                            childList = new ArrayList<HashMap<String, String>>();
                            if (listLoan.size() > 0) {
                                mGroupLoanNames = new String[listLoan.size()];

                                //TODO::   //Static Data:
                                for (int i = 0; i < mGroupLoanNames.length; i++) {

                                    String loanName = listLoan.get(i).getLoanTypeName();
                                    Log.d(TAG, "Loan Name:" + loanName);
                                    Log.d(TAG, "Loan Name:" + "");
                                    mGroupLoanNames[i] = loanName;
                                    Log.d(TAG, "GroupLoanName:>>>" + mGroupLoanNames[i]);
                                }

                                for (int i = 0; i < mGroupLoanNames.length; i++) {
                                    ListItem rowItem = new ListItem();
                                    rowItem.setTitle(mGroupLoanNames[i]);
                                    rowItem.setImageId(listImage);
                                    listItems.add(rowItem);
                                }


                                for (int i = 0; i < mGroupLoanNames.length; i++) {
                                    HashMap<String, String> temp = new HashMap<String, String>();
                                    if (listLoan.get(i).getAccountNumber() != null && listLoan.get(i).getAccountNumber().length() > 0) {
                                        temp.put("AccNo", listLoan.get(i).getAccountNumber());
                                    } else {
                                        temp.put("AccNo", "NA");
                                    }

                                    if (listLoan.get(i).getBankName() != null && listLoan.get(i).getBankName().length() > 0) {
                                        temp.put("BankName", listLoan.get(i).getBankName());
                                    } else {
                                        temp.put("BankName", "NA");
                                    }

                                    if (listLoan.get(i).getDisbursmentDate() != null && listLoan.get(i).getDisbursmentDate().length() > 0)
                                        temp.put("DisbursementDate", df.format(new Date(Long.parseLong(listLoan.get(i).getDisbursmentDate()))));
                                    else {
                                        temp.put("DisbursementDate", "NA");
                                    }
                                    childList.add(temp);
                                }

                                // mAdapter = new CustomMenuListAdapter(getActivity(), listItems);
                                mAdapter = new CustomExpandableMenuListAdapter(getActivity(), listItems, childList, this);
                                mListView.setAdapter(mAdapter);

                            }

                        } else {

                            if (statusCode == 401) {
                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            }
                            Utils.showToast(getActivity(), message);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }


        }

    }*/

    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {
        try {

//            if (listLoan.size() > 0) {
            if (mainlistarray.size() > 0) {

//                sExistingLoagSelection = listLoan.get(position);
                sExistingLoagSelection = mainlistarray.get(position);
                LD_EL_SBLoanMenu loanMenu = new LD_EL_SBLoanMenu();
                NewDrawerScreen.showFragment(loanMenu);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {
               ///NOTHING
    }

    @Override
    public void onItemClickAudit(ViewGroup parent, View view, int position) {

    }
}
