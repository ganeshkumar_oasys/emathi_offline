package com.oasys.emathi.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


/**
 * Created by user1 on 9/12/16.
 */
public class OfflineTxReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 0;
    private static Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            //JobUtil.scheduleJob(context);
        /*    TransactionService.enqueueWork(context, intent);
            OfflineTxReceiver.setAlarm(context.getApplicationContext(),true);*/
           /* Log.e(this.getClass().getName(), "Sessionrx started");
            context.startService(new Intent(context, TransactionService.class));*/
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this.getClass().getName(), "SessionRx crashed");
        }

    }


    public static void cancelAlarm(Context context) {
        AlarmManager alarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        /* cancel any pending alarm */
        alarm.cancel(getPendingIntent(context));
    }

    public static void setAlarm(Context applicationContext, boolean force) {
        context = applicationContext;
        cancelAlarm(context);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // EVERY X MINUTES
        long delay = (1000 * 120 * 1);
        long when = System.currentTimeMillis();
        if (!force) {
            when += delay;
        }
        /* fire the broadcast */
        alarm.set(AlarmManager.RTC_WAKEUP, when, getPendingIntent(context));
    }
    public static final String CUSTOM_INTENT = "com.test.intent.action.ALARM";
    public static final String CUSTOM_INTENT1 = "com.android.ServiceStopped";
    private static PendingIntent getPendingIntent(Context context) {
          /* get the application context */
        Intent alarmIntent = new Intent(context, OfflineTxReceiver.class);
        alarmIntent.setAction(CUSTOM_INTENT);

        return PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }




}