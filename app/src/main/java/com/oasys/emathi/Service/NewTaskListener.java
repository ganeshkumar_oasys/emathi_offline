package com.oasys.emathi.Service;


import com.oasys.emathi.OasysUtils.ServiceType;

public interface NewTaskListener {
	
	void onTaskStarted();
	
	void onTaskFinished(String result, ServiceType serviceType);

}
