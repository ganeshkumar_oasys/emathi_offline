package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/28/2018.
 */
@Data
public class MembersDetailsDto implements Serializable {
    private String beforeLoanPayAmount;

    private String amount;

    private String surplusAmount;

    private String expenseAmount;

    private String incomeAmount;

    private String memberId;

    private String purposeOfLoanId;

    private String loansAmount;
}
