package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data

public class GroupLoanRepaymentDto implements Serializable {

    private String repaymentAmount;

    private String charges;

    private String shgId;

    private String interest;

    private String modeOfCash;

    private String mobileDate;

    private String sbAccountId;

    private String bankCharges;

    private String interestSubvensionRecevied;

    private String transactionDate;

    private String loanId;
}
