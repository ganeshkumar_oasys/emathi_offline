package com.oasys.emathi.Dto;

import java.util.ArrayList;

import lombok.Data;

@Data
public class InternalLoanEntry {

    String bank;
//    String bankname;
    String shgId;
    String purposeOfLoanId;
    String bankId;
    String branch;
    String branchId;
    int loanType;
    String loanTypeName;
    String loanId;
    String loanAccountNumber;
    String sanctionDate;
    String sanctionAmount;
    String bankCharge;
    String bankCharges;
    String bankName;
    String branchName;
    String type;
    String modeOfCash;
    String disbursmentDate;
    String disbursmentAmount;
    String tenure;
    String installmentTypeName;
    int installmentType;
    String installmentTypeId;
    String subsidyAmount;
    String subsidyReserveAmount;
    String savingsAccountId;
    int purposeOfLoan;
    String purposeOfloans;
    String interestRate;
    String transactionDate;
    String mobileDate;
    //  ShgBankDetails sSelectedSavingbank;
    ArrayList<MemberList> loanDisbursmentDetails;

    private String disbursementAmount;
    private String disbursementDate;
    private String sbAccountId;
    private String loanLedgerId;
    private String increaseLimit;






}
