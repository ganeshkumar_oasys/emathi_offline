package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PresidentRotationDto implements Serializable {
    String presidentRotationDate;
    String presidentName;
}
