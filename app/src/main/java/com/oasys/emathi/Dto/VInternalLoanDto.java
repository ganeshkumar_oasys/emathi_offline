package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class VInternalLoanDto implements Serializable {


    private String amount;

    private String memberName;

    private String memeberId;

    private String loanId;
}
