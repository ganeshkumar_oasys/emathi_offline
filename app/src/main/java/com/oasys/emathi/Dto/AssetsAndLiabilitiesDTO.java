package com.oasys.emathi.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class AssetsAndLiabilitiesDTO  implements Serializable {

    private VTransaction transaction;
}
