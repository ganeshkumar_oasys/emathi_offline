package com.oasys.emathi.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class VLoanTypes implements Serializable{
//        @Expose
        private String memberName;
//        @Expose
        private String memeberId;
//        @Expose
        private String memeberLoanId;
//        @Expose
        private String amount;
//        @Expose
        private String voluntaryAmount;
//        @Expose
        private String groupId;
//        @Expose
        private String loanId;
//        @Expose
        private String groupName;
//        @Expose
        private String subsidyAmount;
//        @Expose
        private String subsidyReserveAmount;
//        @Expose
        private String outStandingAmount;
//        @Expose
        private String transactionId;


}
