package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class FirstSynchReqDto implements Serializable {
    String deviceNum;

    String tableName,userId;

    int totalCount;

    int currentCount;

    int totalSentCount;

    String refNum;


    boolean isEndOfSynch;


    /**
     * last synch time recieved at server
     */

    String lastSyncTime;


}
