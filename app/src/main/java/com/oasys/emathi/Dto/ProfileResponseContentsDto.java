package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProfileResponseContentsDto implements Serializable
{
    private String name;

    private String userId;

    private String mobileNumber;
}
