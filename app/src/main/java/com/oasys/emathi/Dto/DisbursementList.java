package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class DisbursementList {

    private String name;

    private String memberId;

    private String loanOutstanding;

    private String shgId;



}
