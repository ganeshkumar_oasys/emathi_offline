package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 1/21/219.
 */
@Data
public class MinutesofmeetingsRequestDto implements Serializable {
    private String shgId;

    private String mobileDate;

    private ArrayList<MinutesId> minutesId;

    private String venue;

    private String transactionDate;
}
