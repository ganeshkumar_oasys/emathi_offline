package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MeetingAuditingDetailsDto implements Serializable {

    private String fromDate;

    private String shgId;

    private String auditingDate;

    private String toDate;

    private String auditorName;
}
