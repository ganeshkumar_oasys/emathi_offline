package com.oasys.emathi.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class TableData implements Serializable {
    private String is_transaction_tdy;
    private String memberId,userId;
    private String memberName;
    private String shgId;
    private long phoneNumber;
//    private String phoneNumber;
    private String accountNumber;
    private String bankName;
    private String bankId,bankNameId;
    private String branchName,branchNameId;
    private String branchId;
    private String aadharNumber;
    private String pipNumber;
    private String ifsc;
    private String is_trans_audit;


    ;
    private String id;
    private String name;
    private String type,isOffline;
    private long groupFormationDate;
    private String code;
    private long openingDate,disbursementDate;
    private String isVerified;
    private String blockName;
    private String panchayatName;
    private String villageName;
    private String districtId;
    private long lastTransactionDate;
    private double cashInHand;
    private double cashAtBank;
    private String presidentName;
    private String shgSavingsAccountId;
    private String currentBalance;
    private String currentFixedDeposit;
    private String recurringDepositedBalance;

  /*  private double currentBalance;
    private double currentFixedDeposit;
    private double recurringDepositedBalance;
    */

    private String isPrimary;
    private String accountNo,loanAccountId,loanAccountNo;
    private double loanOutstanding;
    private String loanId;
//    private double memberLoanOutstanding;
    private String memberLoanOutstanding;
    private String loanType;
    private String flg_Count;

//    private String phoneNumber;

    private long dateOfAssigning;

    private String totalGroup;

    private String age;

    private String AnimatorName;



    private long shgCreatedDate;

    private long shgVerifiedDate;

    private String gramPachayat;

    private String animatorName;

    private String treasuereName;

    private long mobileNumber;

    private String gramPachayatName;

    private String totalMembers;

    private String secretaryName;

    private String SHGName;

    private String shgType;

//    private String name;

    private String uuid;

    private String language_uuid;


    private long sss_date;

    private long first_update;

//    private String shgId;

//    private String bankNameId;

    private long modified_date;

    private String sssTypeId;

    private String is_before_2019;

    private String is_sss;

    private long last_update;

    private long created_date;

//    private null branchNameId;

    private String sss_number;

//    private String memberName;






    private String installment_amount;

    private String rate_of_interest;

    private String current_balance;

    private String opening_balance;

    private String tenure;

    private String status;












}
