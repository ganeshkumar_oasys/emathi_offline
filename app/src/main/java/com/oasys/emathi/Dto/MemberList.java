package com.oasys.emathi.Dto;


import lombok.Data;

@Data
public class MemberList {
    private String shgId;
    private String memberId;
    private String memberName;
    private String phoneNumber;
    private String aadharNumber;
    private String pipNumber;
    private String bankName;
    private String branchName;
    private String bankId,loanId;
    private String accountNumber;
    private String bankNameId;
    private String branchNameId;
    private String memberUserId,loanAccountId;


    private String savingsAmount;
    private String voluntarySavingsAmount;

    private String loanOutstanding,memAmount,memInterest;
    private String OutStanding;

    private String amount;
    private String loanAmount;




}
