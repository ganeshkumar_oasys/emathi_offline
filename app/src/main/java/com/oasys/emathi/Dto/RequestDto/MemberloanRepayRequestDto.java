package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/20/2018.
 */
@Data
public class MemberloanRepayRequestDto implements Serializable {
    private String shgId;

    private String loanUuid;

    private String mobileDate;

    private String transactionDate;

    private boolean isDigit;

    private ArrayList<RepaymentDetails> repaymentDetails;

}
