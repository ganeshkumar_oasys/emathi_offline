package com.oasys.emathi.Dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;


@Data
public class BaseDTO implements Serializable {

    int statusCode;
    String message;
    Object responseContent;
  //  ResponseContent responseContent;
    List<?> responseContents;
    List<?> TransactionHistoryDto;
    String Username;
    String Password;


}