package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public  class SavingsAccounts implements Serializable {

    private String id;

    private String bankName,currentBalance;
}
