package com.oasys.emathi.Dto.RequestDto;



import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class SsnNumberRequestDTOList implements Serializable {

    ArrayList<SsnNumbersDTOList> ssnNumbersDTOList;

}
