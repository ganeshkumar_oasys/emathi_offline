package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/13/2018.
 */
@Data
public class ChangepasswordDto implements Serializable {
    private String newPassword;

    private String username;

    private String oldPassword;
}
