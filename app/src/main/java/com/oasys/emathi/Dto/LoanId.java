package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class LoanId {

    private String interestRate;

    private LoanType loanType;

//    private null installmentAmount;
//
//    private null bankCharges;

    private ShgId shgId;

    private String subsidyAmount;

    private LoanTransactionTypeId loanTransactionTypeId;
//
//    private PurposeOfLoan purposeOfLoan;

//    private null totalRepaidAsOnLastRepaidDate;

    private String createdDate;

//    private null instalmentType;

//    private null firstInstallmentDate;

    private String isClosed;

    private String createdBy;

    private LoanAccountId loanAccountId;

    private String modifiedDate;

//    private null loanStatus;

    private String modifiedBy;

    private String id;

//    private null tempId;

    private String subsidyReserveAmount;

//    private SanctiontransactionId sanctiontransactionId;

    private String tenure;
}
