package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public  class SavingsBalance implements Serializable {

    private String currentBalance;

    private String currentFixedDeposit;

}
