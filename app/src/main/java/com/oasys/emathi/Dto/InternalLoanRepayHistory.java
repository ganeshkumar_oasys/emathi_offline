package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class InternalLoanRepayHistory {

    private String transaction_date;

    private String interest;

    private String repay_amount;
}
