package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class BankId {

    private String createdDate;

    private String createdBy;

    private String modifiedDate;

    private String bankNameId;

    private String modifiedBy;

    private String id;

    private String branchNameId;

    private String ifsc;

    private String accountNumber;
}
