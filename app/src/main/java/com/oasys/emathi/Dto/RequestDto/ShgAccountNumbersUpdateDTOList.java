package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/12/2018.
 */
@Data
public class ShgAccountNumbersUpdateDTOList implements Serializable {

    private String accountNumber;

    private String bankId,shgId;
}
