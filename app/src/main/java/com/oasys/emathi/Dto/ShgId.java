package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class ShgId {

    private String id;

    private String isOpenDate;

}
