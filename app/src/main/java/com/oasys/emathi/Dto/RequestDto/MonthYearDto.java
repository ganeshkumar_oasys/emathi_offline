package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MonthYearDto implements Serializable {

    private int month;

    private String year;

}
