package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class LoginDto implements Serializable {

    String username,userId;

    String password;
    String imeiNo;

    double Lat;
    double Long;
    String ipAddress;
    String macAddress;

    String Language;

}
