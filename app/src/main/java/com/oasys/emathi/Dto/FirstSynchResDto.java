package com.oasys.emathi.Dto;

import java.util.ArrayList;
import java.util.Map;

import lombok.Data;

@Data
public class FirstSynchResDto {

    int statusCode;

    Map<String, Integer> tableDetails;

    int totalCount;

    ArrayList<TableData> tableData;

    boolean hasMore;

    int currentCount;

    int totalSentCount;

    String tableName;

    String refNum;


    boolean firstFetch;


    /**
     * last synch time recieved at server
     */

    String lastSyncTime;
}
