package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 1/2/2019.
 */
@Data
public class TrainingsList implements Serializable {
    private String id;

    private String name;
}
