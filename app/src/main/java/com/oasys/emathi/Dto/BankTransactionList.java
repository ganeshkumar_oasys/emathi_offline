package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class BankTransactionList {

    private String currentBalance;

    private String shgId;

    private String recurringDepositedBalance;

    private String currentFixedDeposit;

    private String shgSavingsAccountId;
}
