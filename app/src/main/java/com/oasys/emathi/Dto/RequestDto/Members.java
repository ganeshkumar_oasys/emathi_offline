package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 1/8/2019.
 */
@Data
public class Members implements Serializable {
    private String memberId;
}
