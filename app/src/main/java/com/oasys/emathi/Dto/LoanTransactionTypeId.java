package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class LoanTransactionTypeId {

    private String settings;

//    private null createdDate;
//
//    private null createdBy;

    private String pflag;

//    private null modifiedDate;
//
//    private null modifiedBy;

    private String id;

    private String sflag;

//    private null parentId;

    private String status;

}
