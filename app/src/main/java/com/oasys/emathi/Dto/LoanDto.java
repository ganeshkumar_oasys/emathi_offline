package com.oasys.emathi.Dto;

import lombok.Data;

/**
 * Created by Dell on 16 Dec, 2018.
 */

@Data
public class LoanDto {
    String shgId;
    String loanTypeId;
    String loanTypeName;
    String loanOutstanding;
    String bankName;
    String loanId;
    String accountNumber;

    String memberId;
    String internalLoanAmount;
    String ploanTypeId;
    String tenure;


}
