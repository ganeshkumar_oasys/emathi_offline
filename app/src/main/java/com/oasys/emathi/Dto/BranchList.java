package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class BranchList implements Serializable {

    private String branchId;
    private String branchName;
}
