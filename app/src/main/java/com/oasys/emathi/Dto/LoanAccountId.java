package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class LoanAccountId {

    private String createdDate;

    private String createdBy;

    private LoanBankId loanBankId;

    private String currentBalance;

    private String modifiedDate;

    private ShgId shgId;

    private String modifiedBy;

    private String id;

}
