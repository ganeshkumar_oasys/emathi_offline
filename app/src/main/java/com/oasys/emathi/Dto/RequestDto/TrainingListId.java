package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 1/4/2019.
 */
@Data
public class TrainingListId implements Serializable {
    private String id;
}
