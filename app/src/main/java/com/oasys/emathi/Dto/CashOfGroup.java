package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class CashOfGroup {
    public String cashInHand;
    public String lastTransactionDate;
    public String cashAtBank;

    //BT _FD<CB UPDATES
    public String currentBalance;
    public String currentFixedDeposit,recurringDepositedBalance;


}
