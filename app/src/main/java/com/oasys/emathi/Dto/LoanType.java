package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class LoanType {

//    private null createdDate;
//
//    private null createdBy;

    private String pflag;

//    private null modifiedDate;

    private String name;

//    private null modifiedBy;

    private String id;

    private String sflag;

    private String status;
}
