package com.oasys.emathi.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ResponseDto implements Serializable
{
    private String message;

    private int statusCode;

    private ResponseContent responseContent;


//    private ResponseContents[] responseContents;
    private ArrayList<ResponseContents> responseContents;


    private String cookie;


//    String message;

//    Object responseContent;
//    PaginationResponseData paginationResponseData;

//    List<Map<String, Object>> totalListOfDataReport;
//    List<?> responseContents;

}
