package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class GroupLoanSummaryDTOList implements Serializable {

    private String amount;

    private String transactiondate;

    private String interest;
}
