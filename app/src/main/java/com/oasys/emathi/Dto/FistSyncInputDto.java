package com.oasys.emathi.Dto;

import com.oasys.emathi.enums.TableNames;

import lombok.Data;

@Data
public class FistSyncInputDto {
    String tableName;
    int count;
    String textToDisplay;
    String endTextToDisplay;
    boolean dynamic;
    TableNames tableNames;
}
