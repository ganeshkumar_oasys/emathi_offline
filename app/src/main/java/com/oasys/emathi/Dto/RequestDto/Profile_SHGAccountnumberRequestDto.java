package com.oasys.emathi.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/12/2018.
 */
@Data
public class Profile_SHGAccountnumberRequestDto implements Serializable {
    private ArrayList<ShgAccountNumbersUpdateDTOList> shgAccountNumbersUpdateDTOList;
}
