package com.oasys.emathi.Dto;

import java.io.Serializable;

public class BranchNameList implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String mBranchName;

	public BranchNameList(final String branchName) {

		this.mBranchName = branchName;

	}

	public String getBranchName() {
		return mBranchName;
	}

}
