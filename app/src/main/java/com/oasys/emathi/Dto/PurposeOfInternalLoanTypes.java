package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/27/2018.
 */
@Data
public class PurposeOfInternalLoanTypes implements Serializable {
    private String loanTypeName;

    private String loanTypeId;
}
