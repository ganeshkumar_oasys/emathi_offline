package com.oasys.emathi.Dto;


import lombok.Data;

@Data
public class ShgBankDetails {
    private String shgId;
    private String bankId;
    private String bankName;
    private String isPrimary;
    private String branchName;
    private String branchId;
    private String ifsc;
    private String accountNumber;
    private String bankNameId;
    private String branchNameId;
    private String shgSavingsAccountId,currentBalance,currentFixedDepositBalance,recurringDepositedBalance;

}
