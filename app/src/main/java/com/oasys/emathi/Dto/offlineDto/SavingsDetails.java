package com.oasys.emathi.Dto.offlineDto;


import com.oasys.emathi.Dto.AddBTDto;
import com.oasys.emathi.Dto.RequestDto.AttendanceRequestDto;
import com.oasys.emathi.Dto.RequestDto.GroupLoanRepaymentDto;
import com.oasys.emathi.Dto.RequestDto.ImageDto;
import com.oasys.emathi.Dto.RequestDto.MeetingAuditingDetailsDto;
import com.oasys.emathi.Dto.RequestDto.MemberloanRepayRequestDto;
import com.oasys.emathi.Dto.RequestDto.MinutesofmeetingsRequestDto;
import com.oasys.emathi.Dto.RequestDto.TrainingRequestDto;
import com.oasys.emathi.Dto.SavingRequest;

import java.util.ArrayList;

import lombok.Data;

@Data
public class SavingsDetails {

    ArrayList<SavingRequest> savingsDetails;
    ArrayList<SavingRequest> incomeDetails;
    ArrayList<SavingRequest> expenseDetails;
    ArrayList<SavingRequest> internalLoanDetails;
    ArrayList<AddBTDto> bankTransactionDetails;
    ArrayList<AddBTDto> accountToAccountDetails;
    ArrayList<AddBTDto> savingsAccountToLoanAccountDetails;
    ArrayList<AddBTDto> fixedDepositDetails;
    ArrayList<AddBTDto> recurringDepositDetails;
    ArrayList<MemberloanRepayRequestDto> memberLoanRepaymentDetails, internalLoanRepaymentDetails;
    ArrayList<GroupLoanRepaymentDto> groupLoanRepaymentDetails;
    ArrayList<AttendanceRequestDto> shgMeetingAttendanceDetails;
    ArrayList<MinutesofmeetingsRequestDto> minutesOfMeetingDetails;
    ArrayList<ImageDto> imageUpload;
    ArrayList<MeetingAuditingDetailsDto> meetingAuditingDetailsDto;
    ArrayList<TrainingRequestDto> trainingRequestDto;
    private String userId;
}
