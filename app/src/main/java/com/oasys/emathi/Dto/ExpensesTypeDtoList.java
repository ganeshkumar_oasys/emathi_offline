package com.oasys.emathi.Dto;


import lombok.Data;

@Data
public class ExpensesTypeDtoList {

    String expensesTypeId,id,name,incomeType;
    String expensesTypeName;
    String amount;

}
