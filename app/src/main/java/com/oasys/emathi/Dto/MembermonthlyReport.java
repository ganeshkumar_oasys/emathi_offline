package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MembermonthlyReport implements Serializable {

    private String Amount;

    private String id;

    private String Name;

}
