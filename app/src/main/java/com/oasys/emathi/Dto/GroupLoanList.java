package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class GroupLoanList {

    private String loanAccountNo;

    private String branchId;

    private String bankId;

    private String loanOutstanding;

    private String loanType;

    private String loanAccountId;

    private String shgId;

    private String disbursementDate;

    private String bankName;

    private String loanId;



}
