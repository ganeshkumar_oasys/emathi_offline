package com.oasys.emathi.Dto;


import lombok.Data;

@Data
class SavingAccountId {

    String id;
}
