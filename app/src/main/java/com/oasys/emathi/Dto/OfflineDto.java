package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class OfflineDto implements Serializable {
    String is_transaction_tdy;
    String id,shgId,animatorId,sCount,iCount,eCount,btCount,ldCount,mrCount,grpCount,attCount,momCount,momName,uploadCount,auditcount,trainingcount;
    String totalIncomeAmount,savAmount,vSavAmount,cashAtBank,cashInhand,mFromBk,mToBk,mLoanName;
    String totalExpenseAmount;
    String grp_interest;
    String mem_amount,mem_os,grp_os;
    String memInterest;
    String btInterest;
    String btDeposit;
    String btWithdraw;
    String btExpense,btFromSavingAcId;
    String btToSavingAcId,shgSavingsAccountId;
    String btToLoanId;
    String btCharge;
    String transferCharge;
    String transferAmount;
    String memCurrentDue;
    String grp_repayment;
    String grp_intSubventionRecieved;
    String bt_intSubventionRecieved;
    String grp_charge;
    String ic_exp_Amount;
    String otherIncome;
    String totalSavAmount;
    String modeOCash;
    String txType;
    String txSubtype,fd_value,rd_value,curr_balance,curr_balance1,selectedcurrentbalance;
    String grouploan_Outstanding;
    String selected_bank_balance;
    String current_balance;
    String recurringbankBalanceId;
    String is_trans_Audit;




    String memberId,sync_status;
    String memberName;
    String expenseTypeName;
    String expenseTypeId;
    String member_user_id;

    String bankId,loanId,OutStanding,loanTypeName;
    String fromAccountNumber,toAccountNumber,accountNumber,sSelectedBank,branchName;
    String sanctionDateTime,disbursementDateTime,modifiedDateTime,lastTransactionDateTime,groupFormationDate,loginFlag;


    String savingsAmount,voluntarySavingsAmount,transactionDate,mobileDate,incomeTypeId,amount;
    public String cashDeposit;
    public String bankInterest;
    public String interestSubventionReceived;
    public String withdrawal;
    public String bankCharges;
    public String toBankId;

    public String savingsBankId;
    public int transactionFlowType;
    public String savingsBankDetailsId;
    public String loanBankDetailsId;
    public String fromSavingsBankDetailsId;
    public String toSavingsBankDetailsId;
    public String transferCharges;

    private String repaymentAmount;
    private String interest;

    private String loanUuid;

    String internalLoanAmount;
    String ploanTypeId;
    String tenure,polName,status;

    String imageurl;

    private String fromDate;

    private String auditingDate;

    private String toDate;

    private String auditorName;

    private String trainingDate;
    private String bankName,ifsc,branchid,installmentamount,rateofinterest;







}
