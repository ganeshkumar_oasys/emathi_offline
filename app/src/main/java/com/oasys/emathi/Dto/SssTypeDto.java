package com.oasys.emathi.Dto;

import lombok.Data;

@Data
public class SssTypeDto {
    String name;
    String createdDate;
    String modifiedDate;
    String status;
    String pflag;
    String sflag;
    String id;
}
