package com.oasys.emathi.Dto;

import java.util.ArrayList;

import lombok.Data;

@Data
public class SavingRequest {

    private String shgId;
    private String modeOfCash;
    private String shgSavingsAccountId,bankId;
    private ArrayList<MemberList> savingsAmount;
    private String transactionDate;
//    private Date transactionDate;
    private String mobileDate;

    private String incomeTypeId;
    private String amount;
    private ArrayList<MemberList> memberSaving;
    private ArrayList<LoanDto> loanDetails;
    ArrayList<ExpensesTypeDtoList> expense;
    private String groupId;

}
