package com.oasys.emathi.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class Memberreportloansummary implements Serializable {

    private String bulk_outstanding;

    private String CashCredit_outstanding;

    private String cif_outstanding;

    private String BulkLoan;

    private String TermLoan;

    private String MfiLoan;

    private String Mfi_outstanding;

    private String term_outstanding;

    private String id;

    private String CashCredit;

    private String CifLoan;



}
