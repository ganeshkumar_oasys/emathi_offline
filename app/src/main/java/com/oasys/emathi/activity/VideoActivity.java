package com.oasys.emathi.activity;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.oasys.emathi.OasysUtils.Config;
import com.oasys.emathi.R;

import at.huber.youtubeExtractor.YouTubeUriExtractor;
import at.huber.youtubeExtractor.YtFile;

public class VideoActivity extends YouTubeBaseActivity implements  YouTubePlayer.OnInitializedListener {

    View view;
    private YouTubePlayerView youTubeView;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    Bundle extras;
    private Button downloadbutton;
    String downloadLink="";
    String title="";
    String newLink;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.fragment_videofragment);

        youTubeView = (YouTubePlayerView) findViewById(R.id.videoview);
        downloadbutton = (Button) findViewById(R.id.downloadbutton);
        youTubeView.initialize(Config.DEVELOPER_KEY, this);
        extras = getIntent().getExtras();

        downloadbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadMyVideo(view);
            }
        });
    }

    public void DownloadMyVideo(View view){
        YouTubeUriExtractor youTubeUriExtractor=new YouTubeUriExtractor(VideoActivity.this) {
            @Override
            public void onUrisAvailable(String videoId, String videoTitle, SparseArray<YtFile> ytFiles) {

                if(ytFiles!=null){
                    int tag=22;
                    newLink=ytFiles.get(tag).getUrl();
//                    String title="";
                    DownloadManager.Request request=new DownloadManager.Request(Uri.parse(newLink));
                    request.setTitle(title);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,title+".mp4");
                    DownloadManager downloadManager=(DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);
                    request.allowScanningByMediaScanner();
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
                    downloadManager.enqueue(request);

                }

            }
        };

        youTubeUriExtractor.execute(downloadLink);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            String value = null;
            if (extras != null) {
                 value = extras.getString("eshakti");

                 if(value.equals("0"))
                 { player.loadVideo("VmP0NsKE5Xc");
                 downloadLink = "https://youtube.com/watch?v=VmP0NsKE5Xc";
                 title = "Eshakti(English)";
                 }else if(value.equals("1"))
                 { player.loadVideo("PupTJkr5r4w&t=65s");
                 downloadLink = "https://youtube.com/watch?v=PupTJkr5r4w&t=65s";
                 title = "Eshakti(Hindi)";
                 } else if(value.equals("2"))
                 { player.loadVideo("yu6KA2FW--A");
                  downloadLink = "https://youtube.com/watch?v=yu6KA2FW--A";
                  title = "Eshakti_Mobile_Application";
                 }
            }
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }
    }
    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) view.findViewById(R.id.videoview);
    }



}