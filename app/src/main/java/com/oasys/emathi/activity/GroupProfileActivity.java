package com.oasys.emathi.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.emathi.Dto.ListCountLoan;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.ShggroupDetailsDTO;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.GetExit;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.R;
import com.oasys.emathi.R.color;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.views.RaisedButton;


public class GroupProfileActivity extends AppCompatActivity implements NewTaskListener {
    String responseArr[];
    private TextView mGroupName, mHeadertext;
    RaisedButton mNextButton;
    boolean isNavigateEditOpeningBalanceActivity = false;
    private Dialog mProgressDialog;
    private NetworkConnection networkConnection;
    private ListOfShg shgDetails;
    private String shgDetail;
    private TableLayout tableLayout;
    public static ListCountLoan listCountLoanDetails;
    private Toolbar mToolbar;
    private TextView mTitle;
    public static String isOpendate;

    public GroupProfileActivity() {
        // TODO Auto-generated constructor stub
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        MenuItem logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist_edit) {

            item.setTitle(SS);

        }

        if (logOutItem.getItemId() == R.id.menu_logout_edit) {

            logOutItem.setTitle(logOutBuilder);
        }


        return false;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist_edit) {

            try {
                startActivity(new Intent(GroupProfileActivity.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.menu_logout_edit) {
            Log.e(" Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        mTitle.setText("ESHAKTI");
        mTitle.setGravity(Gravity.CENTER);

      /*  Toolbar mToolBar_Target = (Toolbar) findViewById(R.id.toolbar_grouplist_target);
        TextView mTargetValues = (TextView) mToolBar_Target.findViewById(R.id.toolbar_title_target);
        mTargetValues.setText(
                "TARGET : " + 0 + "  COMPLETED : " + 0 + "  PENDING : " + 0);
        mTargetValues.setGravity(Gravity.CENTER);
*/
        shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));
        shgDetail = "b9e4099d-4659-46fe-b145-476ab44e199c";
        networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
         //   RestClient.getRestClient(GroupProfileActivity.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.VERIFICATION + shgDetail, GroupProfileActivity.this, ServiceType.VERIFICATION);
            RestClient.getRestClient(GroupProfileActivity.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.VERIFICATION + shgDetails.getShgId(), GroupProfileActivity.this, ServiceType.VERIFICATION);
        }
        init();
    }

    private void init() {
        try {

            mGroupName = (TextView) findViewById(R.id.groupname);
            mGroupName.setText(shgDetails.getName() + " / " + shgDetails.getPresidentName());

            mHeadertext = (TextView) findViewById(R.id.fragment_agent_group_profile_headertext);
            mHeadertext.setText(AppStrings.groupProfile);

            mNextButton = (RaisedButton) findViewById(R.id.fragment_groupProfile_Submitbutton);
            mNextButton.setText(AppStrings.next);


            mNextButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    //	isNavigateEditOpeningBalanceActivity = true;

                    Intent intent = new Intent(GroupProfileActivity.this, EditOpenBalanceDialogActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                    //	new Get_Edit_OpeningbalanceWebservice(GroupProfileActivity.this).execute();
                }
            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //  finish();

    }

    @Override
    public void onTaskStarted() {
        // TODO Auto-generated method stub
        mProgressDialog = AppDialogUtils.createProgressDialog(this);
        mProgressDialog.show();
    }

    @Override
    public void onTaskFinished(final String result, ServiceType serviceType) {

        if (mProgressDialog != null) {
            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
        switch (serviceType) {
            case VERIFICATION:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(GroupProfileActivity.this, message);
                        ShggroupDetailsDTO shg = cdto.getResponseContent().getShggroupDetailsDTO();
                        isOpendate=cdto.getResponseContent().getShggroupDetailsDTO().getIsOpenDate();
                        responseArr = new String[24];
                        System.out.println("Length of response :" + responseArr.length);

                        Log.d("Group Profile : ", "");

                        responseArr[0] = "SHG TYPE";
                        responseArr[2] = "SHG CODE";
                        responseArr[4] = "SHG NAME";
                        responseArr[6] = "SHG CREATION DATE";
                        responseArr[8] = "CONTACT NUMBER";
                        responseArr[10] = "TOTAL MEMBERS";
                        responseArr[12] = "PRESIDENT NAME";
                        responseArr[14] = "SECRETARY NAME";
                        responseArr[16] = "TREASURER NAME";
                        responseArr[18] = "BLOCK NAME";
                        responseArr[20] = "PANCHAYAT NAME";
                        responseArr[22] = "VILLAGE NAME";
                      /*  responseArr[24] = "transactionDate";
                        responseArr[26] = "groupId";*/
                        responseArr[1] = (cdto.getResponseContent() != null && shg.getShGType() != null && shg.getShGType().length() > 0) ? shg.getShGType() : "NA";
                        responseArr[3] = (cdto.getResponseContent() != null && shg.getShgCode() != null && shg.getShgCode().length() > 0) ? shg.getShgCode() : "NA";
                        responseArr[5] = (cdto.getResponseContent() != null && shg.getShgName() != null && shg.getShgName().length() > 0) ? shg.getShgName() : "NA";
                        responseArr[7] = (cdto.getResponseContent() != null && shg.getShgCreationDate() != null && shg.getShgCreationDate().length() > 0) ? shg.getShgCreationDate() : "NA";
                        responseArr[9] = (cdto.getResponseContent() != null && shg.getContactNumber() != null && shg.getContactNumber().length() > 0) ? shg.getContactNumber() : "NA";
                        responseArr[11] = (cdto.getResponseContent() != null && shg.getTotalNumbers() != null && shg.getTotalNumbers().length() > 0) ? shg.getTotalNumbers() : "NA";
                        responseArr[13] = (cdto.getResponseContent() != null && shg.getPresidentName() != null && shg.getPresidentName().length() > 0) ? shg.getPresidentName() : "NA";
                        responseArr[15] = (cdto.getResponseContent() != null && shg.getSecretaryName() != null && shg.getSecretaryName().length() > 0) ? shg.getSecretaryName() : "NA";
                        responseArr[17] = (cdto.getResponseContent() != null && shg.getTreasureName() != null && shg.getTreasureName().length() > 0) ? shg.getTreasureName() : "NA";
                        responseArr[19] = (cdto.getResponseContent() != null && shg.getBlockName() != null && shg.getBlockName().length() > 0) ? shg.getBlockName() : "NA";
                        responseArr[21] = (cdto.getResponseContent() != null && shg.getPanchayatName() != null && shg.getPanchayatName().length() > 0) ? shg.getPanchayatName() : "NA";
                        responseArr[23] = (cdto.getResponseContent() != null && shg.getVillageName() != null && shg.getVillageName().length() > 0) ? shg.getVillageName() : shg.getPanchayatName();
                   /*     responseArr[25] =(cdto.getResponseContent()!=null && shg.getTransactionDate()!=null && shg.getTransactionDate().length()>0)?shg.getTransactionDate():"NA";
                        responseArr[27] = (cdto.getResponseContent()!=null && shg.getGroupId()!=null && shg.getGroupId().length()>0)?shg.getGroupId():"NA";*/

                        tableLayout = (TableLayout) findViewById(R.id.fragment_agent_group_profile_tableLayout);
                        for (int i = 0; i < responseArr.length; i = i + 2) {

                            TableRow indv_row = new TableRow(this);
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                                    LayoutParams.WRAP_CONTENT);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView agent_left_details = new TextView(this);
                            agent_left_details.setText(GetSpanText.getSpanString(this, String.valueOf(responseArr[i])));
                            agent_left_details.setTextColor(color.black);
                            agent_left_details.setPadding(5, 5, 5, 5);
                            agent_left_details.setLayoutParams(contentParams);
                            indv_row.addView(agent_left_details);

                            TextView agent_right_details = new TextView(this);
                            agent_right_details.setText(GetSpanText.getSpanString(this, String.valueOf(" : " + responseArr[i + 1])));
                            agent_right_details.setTextColor(color.black);
                            agent_right_details.setPadding(5, 5, 5, 5);
                            agent_right_details.setLayoutParams(contentParams);
                            indv_row.addView(agent_right_details);
                            tableLayout.addView(indv_row);
                        }


                        EMathiApplication.vertficationDto = cdto.getResponseContent();
                       listCountLoanDetails = cdto.getResponseContent().getListCountLoan();
                      //  MySharedPreference.writeInteger(this, MySharedPreference.BALANCESHEET_DATE_TRANSACTION, cdto.getResponseContent().getgro);
                        MySharedPreference.writeInteger(this, MySharedPreference.TermLoan, listCountLoanDetails.getTermLoan());
                        MySharedPreference.writeInteger(this, MySharedPreference.MFICount, listCountLoanDetails.getMFILoan());
                        MySharedPreference.writeInteger(this, MySharedPreference.CCcount, listCountLoanDetails.getCashCredit());
                        MySharedPreference.writeInteger(this, MySharedPreference.RFACount, listCountLoanDetails.getRFA());
                        MySharedPreference.writeInteger(this, MySharedPreference.BulkCount, listCountLoanDetails.getBulkLoan());
                        MySharedPreference.writeInteger(this, MySharedPreference.CifCount, listCountLoanDetails.getCIF());


                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(GroupProfileActivity.this);

                        }
                        Utils.showToast(GroupProfileActivity.this, message);
                        Intent intent_ = new Intent(GroupProfileActivity.this, SHGGroupActivity.class);
                        intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent_);
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                        finish();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }


    }


}