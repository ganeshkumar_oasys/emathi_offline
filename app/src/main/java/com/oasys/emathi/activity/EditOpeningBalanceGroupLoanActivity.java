package com.oasys.emathi.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.emathi.Dto.VLoanTypes;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.GetExit;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.R;
import com.oasys.emathi.R.color;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.views.Get_EdiText_Filter;
import com.oasys.emathi.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static com.oasys.emathi.activity.NewDrawerScreen.shgDetails;

public class EditOpeningBalanceGroupLoanActivity extends AppCompatActivity implements OnClickListener {

    public static final String TAG = EditOpeningBalanceGroupLoanActivity.class.getSimpleName();
    private TextView mGroupName, mHeaderTextView, mHeaderText;
    private TableLayout mHeaderTable, mContentTable;
    private RaisedButton mSubmit_RaisedButton;

    List<EditText> sSavingsFields;
    private EditText mSavings_values;
    int mSize;
    private String[] leftContent = {AppStrings.amount, AppStrings.Subsidy_Amount, AppStrings.Subsidy_Reserve_Fund};
    private ArrayList<VLoanTypes> mEditInternalOS_indi;

    String[] mEditMasterValues;
    public static Vector<String> mGroupVectorLoanid, mGroupVectorAmount, mGroupVectorSubsidy_Amount,
            mGroupVectorSubsidy_Reserve_Fund;

    String mLoanId = null, mLoanName = null;

    Dialog confirmationDialog;
    private static String sGLAmounts[];
    public static String sSendToServer_GLOutstanding;
    // public static int sOutstanding_Total;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    String[] confirmArr;
    private String mAmount_Values;
    String nullVlaue = "0";
    String mLanguageLocalae;
    boolean mIsNegativeValues = false;

    private int termCount;
    private int ccCount;
    private int CifCount;
    private int rfaCount;
    private int bulkCount;
    private int mfiCount;
    private Toolbar mToolbar;
    private TextView mTitle;

    public static int subsidytotalamout = 0;
    public static int subsidyreservetotalamout = 0;


    public EditOpeningBalanceGroupLoanActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        mGroupVectorLoanid.clear();
        mGroupVectorAmount.clear();
        mGroupVectorSubsidy_Amount.clear();
        mGroupVectorSubsidy_Reserve_Fund.clear();
        sGLAmounts = null;
        confirmArr = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_edit_ob_internalloan);

        mGroupVectorLoanid = new Vector<String>();
        mGroupVectorAmount = new Vector<String>();
        mGroupVectorSubsidy_Amount = new Vector<String>();
        mGroupVectorSubsidy_Reserve_Fund = new Vector<String>();

        sSavingsFields = new ArrayList<EditText>();
        try {
            shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));

            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
            mTitle.setText("ESHAKTI");
            mTitle.setGravity(Gravity.CENTER);


            mGroupName = (TextView) findViewById(R.id.groupname_edit_Internal);
            mGroupName.setText(String.valueOf(""));
            mGroupName.setText(shgDetails.getName() + " / " + shgDetails.getPresidentName());

            mHeaderTextView = (TextView) findViewById(R.id.fragmentHeader_edit_Internal);
            mHeaderTextView.setText(AppStrings.mGroupLoanOutstanding);

            mHeaderText = (TextView) findViewById(R.id.header_edit_Internal);
            mHeaderText.setText("");

            mHeaderTable = (TableLayout) findViewById(R.id.headerTable_edit_Internal);
            mContentTable = (TableLayout) findViewById(R.id.contentTableLayout_edit_Internal);

            mSubmit_RaisedButton = (RaisedButton) findViewById(R.id.fragment_edit_Internal);
            mSubmit_RaisedButton.setText(AppStrings.mConfirm);
            mSubmit_RaisedButton.setOnClickListener(this);


            termCount = EMathiApplication.vertficationDto.getListCountLoan().getTermLoan();
            mfiCount = EMathiApplication.vertficationDto.getListCountLoan().getMFILoan();
            ccCount = EMathiApplication.vertficationDto.getListCountLoan().getCashCredit();
            rfaCount = EMathiApplication.vertficationDto.getListCountLoan().getRFA();
            bulkCount = EMathiApplication.vertficationDto.getListCountLoan().getBulkLoan();
            CifCount = EMathiApplication.vertficationDto.getListCountLoan().getCIF();  // TODO:; Dynamic


            if (termCount > 0) {
                if (termCount == 1) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan();
                    mHeaderText.setText("TermLoan " + (AppStrings.outstanding));
                } else if (termCount == 2) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan1();
                    mHeaderText.setText("TermLoan1 " + (AppStrings.outstanding));
                } else if (termCount == 3) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan2();
                    mHeaderText.setText("TermLoan2 " + (AppStrings.outstanding));
                } else if (termCount == 4) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan3();
                    mHeaderText.setText("TermLoan3 " + (AppStrings.outstanding));
                } else if (termCount == 5) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan4();
                    mHeaderText.setText("TermLoan4 " + (AppStrings.outstanding));
                } else if (termCount == 6) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan5();
                    mHeaderText.setText("TermLoan5 " + (AppStrings.outstanding));
                } else if (termCount == 7) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan6();
                    mHeaderText.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 8) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan7();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 9) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan8();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 10) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan9();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 11) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan10();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 12) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan11();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 13) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan12();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 14) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan13();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 15) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan14();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                }


            } else if (mfiCount > 0) {

                if (mfiCount == 1) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan();
                    mHeaderText.setText("MFILoan " + (AppStrings.outstanding));
                } else if (mfiCount == 2) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan1();
                    mHeaderText.setText("MFILoan1 " + (AppStrings.outstanding));
                } else if (mfiCount == 3) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan2();
                    mHeaderText.setText("MFILoan2 " + (AppStrings.outstanding));
                } else if (mfiCount == 4) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan3();
                    mHeaderText.setText("MFILoan3 " + (AppStrings.outstanding));
                } else if (mfiCount == 5) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan4();
                    mHeaderText.setText("MFILoan4 " + (AppStrings.outstanding));
                } else if (mfiCount == 6) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan5();
                    mHeaderText.setText("MFILoan5 " + (AppStrings.outstanding));
                } else if (mfiCount == 7) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan6();
                    mHeaderText.setText("MFILoan6 " + (AppStrings.outstanding));
                } else if (mfiCount == 8) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan7();
                    mHeaderText.setText("MFILoan7 " + (AppStrings.outstanding));
                } else if (mfiCount == 9) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan8();
                    mHeaderText.setText("MFILoan8 " + (AppStrings.outstanding));
                } else if (mfiCount == 10) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan9();
                    mHeaderText.setText("MFILoan9 " + (AppStrings.outstanding));
                } else if (mfiCount == 11) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan10();
                    mHeaderText.setText("MFILoan10 " + (AppStrings.outstanding));
                } else if (mfiCount == 12) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan11();
                    mHeaderText.setText("MFILoan11 " + (AppStrings.outstanding));
                } else if (mfiCount == 13) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan12();
                    mHeaderText.setText("MFILoan12 " + (AppStrings.outstanding));
                } else if (mfiCount == 14) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan13();
                    mHeaderText.setText("MFILoan13 " + (AppStrings.outstanding));
                } else if (mfiCount == 15) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan14();
                    mHeaderText.setText("MFILoan14 " + (AppStrings.outstanding));
                }


            } else if (ccCount > 0) {

                if (ccCount == 1) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit();
                    mHeaderText.setText("CashCreditLoan " + (AppStrings.outstanding));
                } else if (ccCount == 2) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit1();
                    mHeaderText.setText("CashCreditLoan1 " + (AppStrings.outstanding));
                } else if (ccCount == 3) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit2();
                    mHeaderText.setText("CashCreditLoan2 " + (AppStrings.outstanding));
                } else if (ccCount == 4) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit3();
                    mHeaderText.setText("CashCreditLoan3 " + (AppStrings.outstanding));
                } else if (ccCount == 5) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit4();
                    mHeaderText.setText("CashCreditLoan4 " + (AppStrings.outstanding));
                } else if (ccCount == 6) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit5();
                    mHeaderText.setText("CashCreditLoan5 " + (AppStrings.outstanding));
                } else if (ccCount == 7) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit6();
                    mHeaderText.setText("CashCreditLoan6 " + (AppStrings.outstanding));
                } else if (ccCount == 8) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit7();
                    mHeaderText.setText("CashCreditLoan7 " + (AppStrings.outstanding));
                } else if (ccCount == 9) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit8();
                    mHeaderText.setText("CashCreditLoan8 " + (AppStrings.outstanding));
                } else if (ccCount == 10) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit9();
                    mHeaderText.setText("CashCreditLoan9 " + (AppStrings.outstanding));
                } else if (ccCount == 11) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit10();
                    mHeaderText.setText("CashCreditLoan10 " + (AppStrings.outstanding));
                } else if (ccCount == 12) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit11();
                    mHeaderText.setText("CashCreditLoan11 " + (AppStrings.outstanding));
                } else if (ccCount == 13) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit12();
                    mHeaderText.setText("CashCreditLoan12 " + (AppStrings.outstanding));
                } else if (ccCount == 14) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit13();
                    mHeaderText.setText("CashCreditLoan13 " + (AppStrings.outstanding));
                } else if (ccCount == 15) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit14();
                    mHeaderText.setText("CashCreditLoan14 " + (AppStrings.outstanding));
                }


            } else if (CifCount > 0) {

                if (CifCount == 1) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF();
                    mHeaderText.setText("CIFLoan " + (AppStrings.outstanding));
                } else if (CifCount == 2) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF1();
                    mHeaderText.setText("CIFLoan1 " + (AppStrings.outstanding));
                } else if (CifCount == 3) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF2();
                    mHeaderText.setText("CIFLoan2 " + (AppStrings.outstanding));
                } else if (CifCount == 4) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF3();
                    mHeaderText.setText("CIFLoan3 " + (AppStrings.outstanding));
                } else if (CifCount == 5) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF4();
                    mHeaderText.setText("CIFLoan4 " + (AppStrings.outstanding));
                } else if (CifCount == 6) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF5();
                    mHeaderText.setText("CIFLoan5 " + (AppStrings.outstanding));
                } else if (CifCount == 7) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF6();
                    mHeaderText.setText("CIFLoan6 " + (AppStrings.outstanding));
                } else if (CifCount == 8) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF7();
                    mHeaderText.setText("CIFLoan7 " + (AppStrings.outstanding));
                } else if (CifCount == 9) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF8();
                    mHeaderText.setText("CIFLoan8 " + (AppStrings.outstanding));
                } else if (CifCount == 10) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF9();
                    mHeaderText.setText("CIFLoan9 " + (AppStrings.outstanding));
                } else if (CifCount == 11) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF10();
                    mHeaderText.setText("CIFLoan10 " + (AppStrings.outstanding));
                } else if (CifCount == 12) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF11();
                    mHeaderText.setText("CIFLoan11 " + (AppStrings.outstanding));
                } else if (CifCount == 13) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF12();
                    mHeaderText.setText("CIFLoan12 " + (AppStrings.outstanding));
                } else if (CifCount == 14) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF13();
                    mHeaderText.setText("CIFLoan13 " + (AppStrings.outstanding));
                } else if (CifCount == 15) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF14();
                    mHeaderText.setText("CIFLoan14 " + (AppStrings.outstanding));
                }


            } else if (rfaCount > 0) {

                if (rfaCount == 1) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA();
                    mHeaderText.setText("RFALoan " + (AppStrings.outstanding));
                } else if (rfaCount == 2) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA1();
                    mHeaderText.setText("RFALoan1 " + (AppStrings.outstanding));
                } else if (rfaCount == 3) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA2();
                    mHeaderText.setText("RFALoan2 " + (AppStrings.outstanding));
                } else if (rfaCount == 4) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA3();
                    mHeaderText.setText("RFALoan3 " + (AppStrings.outstanding));
                } else if (rfaCount == 5) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA4();
                    mHeaderText.setText("RFALoan4 " + (AppStrings.outstanding));
                } else if (rfaCount == 6) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA5();
                    mHeaderText.setText("RFALoan5 " + (AppStrings.outstanding));
                } else if (rfaCount == 7) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA6();
                    mHeaderText.setText("RFALoan6 " + (AppStrings.outstanding));
                } else if (rfaCount == 8) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA7();
                    mHeaderText.setText("RFALoan7 " + (AppStrings.outstanding));
                } else if (rfaCount == 9) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA8();
                    mHeaderText.setText("RFALoan8 " + (AppStrings.outstanding));
                } else if (rfaCount == 10) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA9();
                    mHeaderText.setText("RFALoan9 " + (AppStrings.outstanding));
                } else if (rfaCount == 11) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA10();
                    mHeaderText.setText("RFALoan10 " + (AppStrings.outstanding));
                } else if (rfaCount == 12) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA11();
                    mHeaderText.setText("RFALoan11 " + (AppStrings.outstanding));
                } else if (rfaCount == 13) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA12();
                    mHeaderText.setText("RFALoan12 " + (AppStrings.outstanding));
                } else if (rfaCount == 14) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA13();
                    mHeaderText.setText("RFALoan13 " + (AppStrings.outstanding));
                } else if (rfaCount == 15) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA14();
                    mHeaderText.setText("RFALoan14 " + (AppStrings.outstanding));
                }


            } else if (bulkCount > 0) {

                if (bulkCount == 1) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan();
                    mHeaderText.setText("BulkLoan " + (AppStrings.outstanding));
                } else if (bulkCount == 2) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan1();
                    mHeaderText.setText("BulkLoan1 " + (AppStrings.outstanding));
                } else if (bulkCount == 3) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan2();
                    mHeaderText.setText("BulkLoan2 " + (AppStrings.outstanding));
                } else if (bulkCount == 4) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan3();
                    mHeaderText.setText("BulkLoan3 " + (AppStrings.outstanding));
                } else if (bulkCount == 5) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan4();
                    mHeaderText.setText("BulkLoan4 " + (AppStrings.outstanding));
                } else if (bulkCount == 6) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan5();
                    mHeaderText.setText("BulkLoan5 " + (AppStrings.outstanding));
                } else if (bulkCount == 7) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan6();
                    mHeaderText.setText("BulkLoan6 " + (AppStrings.outstanding));
                } else if (bulkCount == 8) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan7();
                    mHeaderText.setText("BulkLoan7 " + (AppStrings.outstanding));
                } else if (bulkCount == 9) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan8();
                    mHeaderText.setText("BulkLoan8 " + (AppStrings.outstanding));
                } else if (bulkCount == 10) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan9();
                    mHeaderText.setText("BulkLoan9 " + (AppStrings.outstanding));
                } else if (bulkCount == 11) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan10();
                    mHeaderText.setText("BulkLoan10 " + (AppStrings.outstanding));
                } else if (bulkCount == 12) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan11();
                    mHeaderText.setText("BulkLoan11 " + (AppStrings.outstanding));
                } else if (bulkCount == 13) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan12();
                    mHeaderText.setText("BulkLoan12 " + (AppStrings.outstanding));
                } else if (bulkCount == 14) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan13();
                    mHeaderText.setText("BulkLoan13 " + (AppStrings.outstanding));
                } else if (bulkCount == 15) {
                    mEditInternalOS_indi = EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan14();
                    mHeaderText.setText("BulkLoan14 " + (AppStrings.outstanding));
                }


            } else if (EMathiApplication.vertficationDto.getShgBalanceDetailsDTO() != null) {
                EMathiApplication.vertficationDto.getListCountLoan().setTermLoan(MySharedPreference.readInteger(this, MySharedPreference.TermLoan, 0));
                EMathiApplication.vertficationDto.getListCountLoan().setMFILoan(MySharedPreference.readInteger(this, MySharedPreference.MFICount, 0));
                EMathiApplication.vertficationDto.getListCountLoan().setCIF(MySharedPreference.readInteger(this, MySharedPreference.CifCount, 0));
                EMathiApplication.vertficationDto.getListCountLoan().setRFA(MySharedPreference.readInteger(this, MySharedPreference.RFACount, 0));
                EMathiApplication.vertficationDto.getListCountLoan().setCashCredit(MySharedPreference.readInteger(this, MySharedPreference.CCcount, 0));
                EMathiApplication.vertficationDto.getListCountLoan().setBulkLoan(MySharedPreference.readInteger(this, MySharedPreference.BulkCount, 0));

                Log.i("print","after_intent");
                Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                        EditOpeningBalanceBankDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                finish();


            }


            buildTableHeaderLayout();

            buildTableContentLayout();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    private void buildTableHeaderLayout() {
        // TODO Auto-generated method stub

        TableRow leftHeaderRow = new TableRow(this);

        TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);

        TextView mMemberName_headerText = new TextView(this);
        mMemberName_headerText.setText(String.valueOf(AppStrings.details));
        mMemberName_headerText.setTextColor(Color.WHITE);
        mMemberName_headerText.setPadding(20, 5, 10, 5);
        mMemberName_headerText.setLayoutParams(lHeaderParams);
        leftHeaderRow.addView(mMemberName_headerText);

        TextView mSavingsAmount_HeaderText = new TextView(this);
        mSavingsAmount_HeaderText.setText(String.valueOf(AppStrings.amount));
        mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
        mSavingsAmount_HeaderText.setPadding(10, 5, 10, 5);
        mSavingsAmount_HeaderText.setLayoutParams(lHeaderParams);
        mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
        mSavingsAmount_HeaderText.setSingleLine(true);
        leftHeaderRow.addView(mSavingsAmount_HeaderText);

        mHeaderTable.addView(leftHeaderRow);

    }

    @SuppressWarnings("deprecation")
    private void buildTableContentLayout() {
        // TODO Auto-generated method stub

        mSize = leftContent.length;

        for (int i = 0; i < mSize; i++) {
            
            TableRow leftContentRow = new TableRow(this);

            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
            leftContentParams.setMargins(5, 5, 5, 5);

            TextView memberName_Text = new TextView(this);
            memberName_Text.setText(GetSpanText.getSpanString(this, String.valueOf(leftContent[i])));
            memberName_Text.setTextColor(color.black);
            memberName_Text.setPadding(15, 5, 5, 5);
            memberName_Text.setLayoutParams(leftContentParams);
            leftContentRow.addView(memberName_Text);

            TableRow.LayoutParams leftContentParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
            leftContentParams1.setMargins(5, 5, 30, 5);

            mSavings_values = new EditText(this);

            mSavings_values.setId(i);
            sSavingsFields.add(mSavings_values);
            mSavings_values.setPadding(5, 5, 5, 5);
            mSavings_values.setBackgroundResource(R.drawable.edittext_background);
            mSavings_values.setLayoutParams(leftContentParams1);
            mSavings_values.setTextAppearance(this, R.style.MyMaterialTheme);
            mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
            mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
            mSavings_values.setTextColor(color.black);
            mSavings_values.setWidth(150);
            final int finals = i;

            mSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {

                        if(v.getId()==finals) {

                            if (sSavingsFields.get(finals).getText().toString().equals("0.0") || sSavingsFields.get(finals).getText().toString().equals("0")) {
                                ((EditText)v).setText("");
                            }
                        }

                    }

                }
            });
            if (i == 0) {
                mSavings_values.setText(mEditInternalOS_indi.get(0).getOutStandingAmount());
                Log.d("outstand",""+ mEditInternalOS_indi.get(0).getOutStandingAmount());
            }

            else if (i == 1) {

                if(mEditInternalOS_indi.get(0).getSubsidyAmount()!=null)
                {
                    mSavings_values.setText(mEditInternalOS_indi.get(0).getSubsidyAmount());
                    Log.d("outstand",""+ mEditInternalOS_indi.get(0).getSubsidyAmount());
                }




            } else if (i == 2) {

                if(mEditInternalOS_indi.get(0).getSubsidyReserveAmount()!=null)
                {
                    mSavings_values.setText((mEditInternalOS_indi.get(0).getSubsidyReserveAmount()));
                    Log.d("outstand", "" + mEditInternalOS_indi.get(0).getSubsidyReserveAmount());
                }
//                int prereser = (int) Double.parseDouble(sSavingsFields.get(0).getText().toString());
//                subsidyreservetotalamout = subsidyreservetotalamout+prereser;

            }



            mSavings_values.setGravity(Gravity.LEFT);
            leftContentRow.addView(mSavings_values);

            mContentTable.addView(leftContentRow);

        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.fragment_edit_Internal:

                sGLAmounts = new String[leftContent.length];
                try {

                    // sOutstanding_Total = 0;

                    confirmArr = new String[mSize];

                    mAmount_Values = "";
                    sSendToServer_GLOutstanding = "";

                    mAmount_Values = mSavings_values.getText().toString();

                    if (mAmount_Values.equals("") || mAmount_Values == null) {
                        mAmount_Values = nullVlaue;
                    } else {
                        mAmount_Values = mSavings_values.getText().toString();
                    }

                    for (int i = 0; i < sSavingsFields.size(); i++) {
                        sGLAmounts[i] = String.valueOf(sSavingsFields.get(i).getText());
                        double amount = 0;

                        if (sGLAmounts[i].matches("\\d*\\.?\\d+")) { // match a
                            // decimal
                            // number
                            amount = Double.parseDouble(sGLAmounts[i]);
                            int value=(int)Math.round(amount);
                            sGLAmounts[i] = String.valueOf(value);
                        }

                        if (amount < 0) {
                            mIsNegativeValues = true;
                        }


                        if (termCount > 0) {
                            if (termCount == 1) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (termCount == 2) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan1().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan1().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan1().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                            } else if (termCount == 3) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan2().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan2().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan2().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            } else if (termCount == 4) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan3().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan3().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan3().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                            } else if (termCount == 5) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan4().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan4().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan4().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                                ;
                            } else if (termCount == 6) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan5().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan5().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan5().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                                ;
                            } else if (termCount == 7) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan6().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan6().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan6().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                                ;
                            }
                        } else if (mfiCount > 0) {
                            if (mfiCount == 1) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                            } else if (mfiCount == 2) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan1().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan1().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan1().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (mfiCount == 3) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan2().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan2().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan2().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            } else if (mfiCount == 4) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan3().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan3().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan3().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            } else if (mfiCount == 5) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan4().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan4().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan4().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            } else if (mfiCount == 6) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan5().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan5().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan5().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            } else if (mfiCount == 7) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan6().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan6().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getMFILoan6().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            }
                        } else if (ccCount > 0) {
                            if (ccCount == 1) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            } else if (ccCount == 2) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit1().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit1().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit1().get(0).setSubsidyReserveAmount(sGLAmounts[2]);

                            } else if (ccCount == 3) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit2().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit2().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit2().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (ccCount == 4) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit3().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit3().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit3().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (ccCount == 5) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit4().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit4().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit4().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (ccCount == 6) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit5().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit5().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit5().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (ccCount == 7) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit6().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit6().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCashCredit6().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                                ;
                            }
                        } else if (CifCount > 0) {
                            if (CifCount == 1) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (CifCount == 2) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF1().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF1().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF1().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (CifCount == 3) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF2().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF2().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF2().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (CifCount == 4) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF3().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF3().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF3().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (CifCount == 5) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF4().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF4().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF4().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (CifCount == 6) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF5().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF5().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF5().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (CifCount == 7) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF6().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF6().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getCIF6().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            }
                        } else if (rfaCount > 0) {
                            if (rfaCount == 1) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (rfaCount == 2) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA1().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA1().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA1().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (rfaCount == 3) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA2().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA2().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA2().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (rfaCount == 4) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA3().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA3().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA3().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (rfaCount == 5) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA4().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA4().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA4().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (rfaCount == 6) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA5().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA5().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA5().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (rfaCount == 7) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA6().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA6().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getRFA6().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            }
                        } else if (bulkCount > 0) {
                            if (bulkCount == 1) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (bulkCount == 2) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan1().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan1().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan1().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (bulkCount == 3) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan2().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan2().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan2().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (bulkCount == 4) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan3().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan3().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan3().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (bulkCount == 5) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan4().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan4().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan4().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (bulkCount == 6) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan5().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan5().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan5().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            } else if (bulkCount == 7) {
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan6().get(0).setOutStandingAmount(sGLAmounts[0]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan6().get(0).setSubsidyAmount(sGLAmounts[1]);
                                EMathiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan6().get(0).setSubsidyReserveAmount(sGLAmounts[2]);
                            }
                        }


                    }

                    sSendToServer_GLOutstanding = String.valueOf(mLoanId) + "~" + sGLAmounts[0] + ".00" + "~"
                            + sGLAmounts[1] + ".00" + "~" + sGLAmounts[2] + ".00";

                    Log.d(TAG, sSendToServer_GLOutstanding);

                    // Do the SP insertion
                    if (!mIsNegativeValues) {

                        confirmationDialog = new Dialog(this);

                        LayoutInflater inflater = this.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < leftContent.length; i++) {

                            TableRow indv_SavingsRow = new TableRow(this);

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(this);
                            memberName_Text.setText(GetSpanText.getSpanString(this, String.valueOf(leftContent[i])));
                            memberName_Text.setTextColor(color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(this);
                            confirm_values.setText(GetSpanText.getSpanString(this, String.valueOf(sGLAmounts[i])));
                            confirm_values.setTextColor(color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.mVerified);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.mIsNegativeOpeningBalance,
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        sSendToServer_GLOutstanding = "0";

                        mIsNegativeValues = false;
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit:

                mSubmit_RaisedButton.setClickable(true);

                sSendToServer_GLOutstanding = "0";
                // sOutstanding_Total = Integer.valueOf(nullVlaue);

                mAmount_Values = "0";

                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:
                confirmationDialog.dismiss();
                mIsNegativeValues = false;

                for (int i = 0; i < sSavingsFields.size(); i++) {
                    sGLAmounts[i] = String.valueOf(sSavingsFields.get(i).getText());

                    if (i==1) {
                        int pre = (int) Double.parseDouble(String.valueOf(sSavingsFields.get(i).getText()));
                        subsidytotalamout = subsidytotalamout + pre;
                        Log.d("subsidytotalamout", "" + subsidytotalamout);

                    }
                    else if(i==2){

                        int prereser = (int) Double.parseDouble(sSavingsFields.get(i).getText().toString());
                        subsidyreservetotalamout = subsidyreservetotalamout+prereser;

                    }
                }

                EMathiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO().getTransaction().getSubsidy().setSubsidyAmount(String.valueOf(subsidytotalamout));
                EMathiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO().getTransaction().getSubsidy().setSubsidyReserveAmount(String.valueOf(subsidyreservetotalamout));
                if (termCount > 0) {

                    Log.i("print","if");
                    int count = EMathiApplication.vertficationDto.getListCountLoan().getTermLoan() - 1;
                    EMathiApplication.vertficationDto.getListCountLoan().setTermLoan(count);


                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (mfiCount > 0) {
                    Log.i("print","else-if(1)");
                    int count = EMathiApplication.vertficationDto.getListCountLoan().getMFILoan() - 1;
                    EMathiApplication.vertficationDto.getListCountLoan().setMFILoan(count);

                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (ccCount > 0) {
                    Log.i("print","else-if(2)");
                    int count = EMathiApplication.vertficationDto.getListCountLoan().getCashCredit() - 1;
                    EMathiApplication.vertficationDto.getListCountLoan().setCashCredit(count);


                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                } else if (CifCount > 0) {
                    Log.i("print","else-if(3)");
                    int count = EMathiApplication.vertficationDto.getListCountLoan().getCIF() - 1;
                    EMathiApplication.vertficationDto.getListCountLoan().setCIF(count);


                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                } else if (rfaCount > 0) {
                    Log.i("print","else-if(4)");
                    int count = EMathiApplication.vertficationDto.getListCountLoan().getRFA() - 1;
                    EMathiApplication.vertficationDto.getListCountLoan().setRFA(count);

                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (bulkCount > 1) {
                    Log.i("print","else-if(5)");
                    int count = EMathiApplication.vertficationDto.getListCountLoan().getBulkLoan() - 1;
                    EMathiApplication.vertficationDto.getListCountLoan().setBulkLoan(count);

                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                } else if (EMathiApplication.vertficationDto.getShgBalanceDetailsDTO() != null) {
                    Log.i("print","else-if(6)");
                    EMathiApplication.vertficationDto.getListCountLoan().setTermLoan(MySharedPreference.readInteger(this, MySharedPreference.TermLoan, 0));
                    EMathiApplication.vertficationDto.getListCountLoan().setMFILoan(MySharedPreference.readInteger(this, MySharedPreference.MFICount, 0));
                    EMathiApplication.vertficationDto.getListCountLoan().setCIF(MySharedPreference.readInteger(this, MySharedPreference.CifCount, 0));
                    EMathiApplication.vertficationDto.getListCountLoan().setRFA(MySharedPreference.readInteger(this, MySharedPreference.RFACount, 0));
                    EMathiApplication.vertficationDto.getListCountLoan().setCashCredit(MySharedPreference.readInteger(this, MySharedPreference.CCcount, 0));
                    EMathiApplication.vertficationDto.getListCountLoan().setBulkLoan(MySharedPreference.readInteger(this, MySharedPreference.BulkCount, 0));

                    Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
                            EditOpeningBalanceBankDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                }


                break;

        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        MenuItem logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist_edit) {

            item.setTitle(SS);

        }

        if (logOutItem.getItemId() == R.id.menu_logout_edit) {

            logOutItem.setTitle(logOutBuilder);
        }


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist_edit) {

            try {
                startActivity(new Intent(EditOpeningBalanceGroupLoanActivity.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.menu_logout_edit) {
            Log.e(" Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



			/*if (!Get_Edit_OpeningbalanceWebservice.mEdit_OpeningBalance_Values_Check[4].equals("")) {
				if (publicValues.mBankLoanSize_GroupLoan == publicValues.mCurrentBankLoanSize_GroupLoan) {
					publicValues.mCurrentBankLoanSize_GroupLoan = 0;
					publicValues.mBankLoanSize_GroupLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan
							+ sSendToServer_GLOutstanding + "%";
					Log.e("Current Group Loan Det.", publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan);
					Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
							EditOpeningBalanceBankDetailsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else if (publicValues.mBankLoanSize_GroupLoan == 1) {
					publicValues.mCurrentBankLoanSize_GroupLoan = 0;
					publicValues.mBankLoanSize_GroupLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan
							+ sSendToServer_GLOutstanding + "%";
					Log.e("Current Group Loan Det.", publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan);
					Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
							EditOpeningBalanceBankDetailsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else {
					if (publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = "";
					}

					publicValues.mCurrentBankLoanSize_GroupLoan = publicValues.mCurrentBankLoanSize_GroupLoan + 1;

					if (publicValues.mBankLoanSize_GroupLoan == publicValues.mCurrentBankLoanSize_GroupLoan) {

						publicValues.mCurrentBankLoanSize_GroupLoan = 0;
						publicValues.mBankLoanSize_GroupLoan = 0;
						if (publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan == null) {
							publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = "";
						}
						publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan
								+ sSendToServer_GLOutstanding + "%";
						Log.e("Current Group Loan Det.", publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan);
						Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
								EditOpeningBalanceBankDetailsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						finish();

					} else {
						if (publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan == null) {
							publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = "";
						}
						publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan
								+ sSendToServer_GLOutstanding + "%";
						Log.e("On 1st Time", publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan);

						Intent intent = new Intent(EditOpeningBalanceGroupLoanActivity.this,
								EditOpeningBalanceGroupLoanActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();
					}
				}
			}*/

}