     package com.oasys.emathi.activity;

    import android.annotation.SuppressLint;
    import android.app.Activity;
    import android.app.AlertDialog;
    import android.app.Dialog;
    import android.app.SearchManager;
    import android.content.Context;
    import android.content.Intent;
    import android.graphics.Color;
    import android.graphics.drawable.ColorDrawable;
    import android.os.Bundle;

    import androidx.annotation.Nullable;
    import androidx.appcompat.app.AppCompatActivity;
    import androidx.appcompat.widget.Toolbar;
    import android.text.SpannableStringBuilder;
    import android.util.Log;
    import android.view.Gravity;
    import android.view.LayoutInflater;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
    import android.view.ViewGroup;
    import android.view.Window;
    import android.widget.ArrayAdapter;
    import android.widget.ExpandableListView;
    import android.widget.ImageView;
    import android.widget.LinearLayout;
    import android.widget.ListView;
    import android.widget.SearchView;
    import android.widget.TextView;

    import com.amitshekhar.DebugDB;
    import com.google.gson.Gson;
    import com.google.gson.GsonBuilder;
    import com.oasys.emathi.Adapter.CustomExpandableGroupListAdapter;
    import com.oasys.emathi.Dto.BankTransaction;
    import com.oasys.emathi.Dto.Donation;
    import com.oasys.emathi.Dto.FDBankTransaction;
    import com.oasys.emathi.Dto.GroupLoanRepayment;
    import com.oasys.emathi.Dto.IncomeDisbursement;
    import com.oasys.emathi.Dto.InternalLoanDisbursement;
    import com.oasys.emathi.Dto.ListOfShg;
    import com.oasys.emathi.Dto.MeetingExpense;
    import com.oasys.emathi.Dto.OtherExpense;
    import com.oasys.emathi.Dto.OtherIncome;
    import com.oasys.emathi.Dto.OtherIncomeGroup;
    import com.oasys.emathi.Dto.Penalty;
    import com.oasys.emathi.Dto.ResponseContents;
    import com.oasys.emathi.Dto.ResponseDto;
    import com.oasys.emathi.Dto.ResponseContent;
    import com.oasys.emathi.Dto.Savings;
    import com.oasys.emathi.Dto.SavingsDisbursement;
    import com.oasys.emathi.Dto.SeedFund;
    import com.oasys.emathi.Dto.Subscription;
    import com.oasys.emathi.Dto.SubscriptionCharges;
    import com.oasys.emathi.Dto.SubscriptiontoFeds;
    import com.oasys.emathi.Dto.VoluntarySavings;
    import com.oasys.emathi.Dto.VoluntarySavingsDisbursement;
    import com.oasys.emathi.Dto.internalLoanRepayment;
    import com.oasys.emathi.Dto.memberLoanRepayment;
    import com.oasys.emathi.EMathiApplication;
    import com.oasys.emathi.OasysUtils.AppStrings;
    import com.oasys.emathi.OasysUtils.Constants;
    import com.oasys.emathi.OasysUtils.ExpandListItemClickListener;
    import com.oasys.emathi.OasysUtils.GetExit;
    import com.oasys.emathi.OasysUtils.GroupListItem;
    import com.oasys.emathi.OasysUtils.MySharedPreference;
    import com.oasys.emathi.OasysUtils.NetworkConnection;
    import com.oasys.emathi.OasysUtils.ServiceType;
    import com.oasys.emathi.OasysUtils.Utils;
    import com.oasys.emathi.R;
    import com.oasys.emathi.Service.RestClient;
    import com.oasys.emathi.database.DbHelper;
    import com.oasys.emathi.database.SHGTable;
    import com.oasys.emathi.database.TransactionTable;
    import com.oasys.emathi.views.ButtonFlat;
    import com.oasys.emathi.views.MyExpandableListview;
    import com.oasys.emathi.Service.NewTaskListener;
    import com.oasys.emathi.OasysUtils.AppDialogUtils;

    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.List;

    public class SHGGroupActivity extends AppCompatActivity implements NewTaskListener, ExpandListItemClickListener, SearchView.OnCloseListener, SearchView.OnQueryTextListener {

        Toolbar mToolbar;
        Context context;
        List<ResponseContents> dataList = new ArrayList<>();

        ResponseContents responseContents =new ResponseContents();


        private Dialog mProgressDialog;

        private List<GroupListItem> listItems;
        // private GroupListAdapter mAdapter;
        private CustomExpandableGroupListAdapter mAdapter;
        int listImage, leftImage;

        public static ArrayList<String> mGroupIdlist = new ArrayList<String>();
        boolean isNavigateGroupProfile = false;
        public static String mLanguageLocalae;
        // ExpandableLayoutListView expandableLayoutListView;
        MyExpandableListview expandableLayoutListView;

        private ArrayList<HashMap<String, String>> childList;
        private int lastExpandedPosition = -1;
        private String userId;
        private ResponseDto gdto;
        private TextView mTitle;
        private String nameStr, mobStr;
        private String langId;
        private SearchView searchView;
        LinearLayout myLayout;
        private ArrayList<String> filterdatas = new ArrayList<String>();
        private ListOfShg shgDto;
        private NetworkConnection networkConnection;
        ArrayList<ListOfShg> shgDetails;
        String SelectedShgId;
       public static String shgName,presidentname,shg_id;
       public  static int internaloandisbursementsize,seedFundsize,fDBankTransactionsize,incomeDisbursementsize,otherIncomesize,groupLoanRepaymentsize,penaltysize,bankTransactionsize,subscriptiontoFedssize,otherIncomeGroupsize,
               memberLoanRepaymentsize,voluntarySavingsDisbursementsize,otherExpensesize,meetingExpensesize,internalLoanRepaymentsize,
               donationsize,voluntarysavingsize,savingdisbursementsize,savingsize,subscriptionsize,subscriptionChargessize;
       public static  String shg_userId,auditor_id;


        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_group_expandablelistview);
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));
            networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
            shgDetails = SHGTable.getSHGDetails();

           /* ArrayList<TableData> questions = new ArrayList<TableData>();
            questions = (ArrayList<TableData>) getIntent().getSerializableExtra("AuditData");
            Log.d("AuditData",""+questions);*/

            if(shgDetails!=null) {

            for (int i = 0; i <shgDetails.size() ; i++) {

                if (shgDetails.get(i).getIsTransAudit()!=null){
                    if (shgDetails.get(i).getIsTransAudit().equals("1.0")) {
                        filterdatas.add(shgDetails.get(i).getName() + "/" + shgDetails.get(i).getPresidentName());
                    }
                }
                /*if (shgDetails.get(i).getIsTransAudit().equals("1.0")) {

                        filterdatas.add(shgDetails.get(i).getName() + "/" + shgDetails.get(i).getPresidentName());
                    }*/
                }
            }


            Log.d("val",""+filterdatas);
            if((filterdatas.size()!=0) && (filterdatas!=null)){
                    auditedShgGroupDialog();
            }

            nameStr = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "");
            mobStr = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.USERNAME, "");
            userId = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_ID, "");
            langId = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.LANG_ID, "");
            Log.d("db", DebugDB.getAddressLog());

            Log.d("values",LoginActivity.sign_in_differ_value+""+LoginActivity.signin_previous_user+""+LoginActivity.deviceUniqueIdentifier);
            expandableLayoutListView = (MyExpandableListview) findViewById(R.id.listview);
            myLayout = (LinearLayout)findViewById(R.id.my_layout);
            myLayout.requestFocus();
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView)findViewById(R.id.search);
            int searchImgId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
            ImageView ivIcon = (ImageView) searchView.findViewById(searchImgId);
            if(ivIcon!=null)
                ivIcon.setImageResource(R.drawable.search);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(this);
            searchView.setOnCloseListener(this);

            try {
                mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
                mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
                setSupportActionBar(mToolbar);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setTitle("");
                mTitle.setText(nameStr + "    " + mobStr);
                //     mTitle.setTypeface(LoginActivity.sTypeface);
                mTitle.setGravity(Gravity.CENTER);

                Toolbar mToolBar_Target = (Toolbar) findViewById(R.id.toolbar_grouplist_target);
                TextView mTargetValues = (TextView) mToolBar_Target.findViewById(R.id.toolbar_title_target);
                mTargetValues.setText(
                        "TARGET : " + 0 + "  COMPLETED : " + 0 + "  PENDING : " + 0);
                mTargetValues.setGravity(Gravity.CENTER);

                context = this.getApplicationContext();

                listItems = new ArrayList<GroupListItem>();
                listImage = R.drawable.ic_navigate_next_white_24dp;
                leftImage = R.drawable.star;
                expandableLayoutListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                            expandableLayoutListView.collapseGroup(lastExpandedPosition);
                        }
                        lastExpandedPosition = groupPosition;
                        SelectedShgId =  shgDetails.get(groupPosition).getShgId();
                        shgName = shgDetails.get(groupPosition).getName();
                        presidentname = shgDetails.get(groupPosition).getPresidentName();
                        shg_id = shgDetails.get(groupPosition).getShgId();
                    }
                });
                callShgList(userId, langId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getAuditapiCall() {
            if (networkConnection.isNetworkAvailable()) {
                RestClient.getRestClient(SHGGroupActivity.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.TRANSACTION_AUDIT + SelectedShgId , SHGGroupActivity.this, ServiceType.TRANSACTIONAUDIT);
            }
        }

        private void auditedShgGroupDialog() {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(SHGGroupActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.audit_dialog_confirmation, null);
            alertDialog.setView(convertView);
            ListView lv = (ListView) convertView.findViewById(R.id.simpleListView);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,filterdatas);
            lv.setAdapter(adapter);
            alertDialog.show();

        }

        @Override
        public void onBackPressed() {
            return;
        }
        @Override
        protected void onResume() {
            super.onResume();

        }

        private void callShgList(String id, String lang_id) {
//            networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());

            try {

                ResponseDto lrDto = new ResponseDto();
                ResponseContent rCont = new ResponseContent();
                ArrayList<ListOfShg> newArrayList = SHGTable.getSHGDetailList(id);
                rCont.setListOfShg(newArrayList);
                lrDto.setResponseContent(rCont);
                CommonSetRecyclerview(lrDto);
            } catch (Exception e) {
                Log.e("SHG OFFLINE TABLE Error", e.getMessage());
            }

        /*    if (networkConnection.isNetworkAvailable()) {
                EShaktiApplication.setOfflineTrans(false);

                onTaskStarted();
                String url = Constants.BASE_URL + Constants.SHG_URL + userId + "&languageId=" + lang_id;
                RestClient.getRestClient(SHGGroupActivity.this).callWebServiceForGetMethod(url, SHGGroupActivity.this, ServiceType.SHG_LIST);

            } else {

            }*/
        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
            MenuItem item = menu.getItem(0);
            item.setVisible(true);
            SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);
            if (item.getItemId() == R.id.group_logout_edit) {
                item.setTitle(logOutBuilder);
            }
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.

            int id = item.getItemId();
            if (id == R.id.group_logout_edit) {
                Log.e("Group Logout", "Logout Sucessfully");

                //startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                // this.finish();

                    MySharedPreference.writeBoolean(this, MySharedPreference.LOGOUT, true);
                    showConfirmation_LogoutDialog1(SHGGroupActivity.this);

                return true;

            }

            return super.onOptionsItemSelected(item);
        }


        public void showConfirmation_LogoutDialog1(final Activity activity) {

            try {
                final Dialog confirmationDialog = new Dialog(activity);

                LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = li.inflate(R.layout.dialog_new_logout, null, false);
                final TextView mHeaderView, mMessageView;

                mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
                mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);


                final ButtonFlat mConYesButton, mConNoButton;

                mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
                mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);

                mConYesButton.setText(AppStrings.dialogOk);
                mConYesButton.setTypeface(LoginActivity.sTypeface);
                mConNoButton.setTypeface(LoginActivity.sTypeface);
                mConNoButton.setText(AppStrings.dialogNo);

                mHeaderView.setText(AppStrings.logOut);
                mHeaderView.setTypeface(LoginActivity.sTypeface);
                mHeaderView.setVisibility(View.INVISIBLE);

                if (MySharedPreference.readBoolean(activity, MySharedPreference.UNAUTH, false)) {
                    mMessageView.setText(AppStrings.mUnAuth);
                } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) > 0 && !MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                    MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                    if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 2) {
                        mMessageView.setText(AppStrings.mon_mode);
                    } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 1) {
                        mMessageView.setText(AppStrings.moff_mode);
                    }
                } else if (MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                    mMessageView.setText(AppStrings.mAskLogout);
                }

                mMessageView.setTypeface(LoginActivity.sTypeface);
                mConYesButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        confirmationDialog.dismiss();

                        try {
                            if (MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "").length() > 0) {
                                ListOfShg shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                                if (shgDto != null && shgDto.getFFlag() != null && (shgDto.getFFlag().equals("1") || shgDto.getFFlag().equals("0"))) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                TransactionTable.updateLoginflag(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                            }


                            if (mMessageView.getText().equals(AppStrings.mAskLogout))
                                MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                            MySharedPreference.writeBoolean(activity, MySharedPreference.SINGIN_DIFF, false);
                            MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_NAME, "");
                            //   MySharedPreference.writeString(context,MySharedPreference.USERNAME,"");
                            // MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_ID, "");
                            //MySharedPreference.writeString(activity, MySharedPreference.SHG_ID, "");
                            MySharedPreference.writeString(activity, MySharedPreference.CASHINHAND, "");
                            MySharedPreference.writeString(activity, MySharedPreference.CASHATBANK, "");
                            MySharedPreference.writeString(activity, MySharedPreference.LAST_TRANSACTION, "");

//                            activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
//                            activity.finish();

                            try {

                                if (networkConnection.isNetworkAvailable()) {

                                    RestClient.getRestClient(SHGGroupActivity.this).callRestWebServiceForDelete(Constants.BASE_URL + Constants.LOGOUT_TOKENDELETION, SHGGroupActivity.this, ServiceType.LOG_OUT, "");

                                }
                                else {
                                    activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
                                    activity.finish();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

                mConNoButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        confirmationDialog.dismiss();
                        MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, false);
                    }
                });

                confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                confirmationDialog.setCanceledOnTouchOutside(false);
                confirmationDialog.setContentView(customView);
                confirmationDialog.setCancelable(false);
                confirmationDialog.show();

            } catch (
                    Exception E)

            {
                E.printStackTrace();
            }

        }


        @SuppressLint("LongLogTag")
        @Override
        public void onItemClick(ViewGroup parent, View view, int position) {
            // TODO Auto-generated method stub

            if (networkConnection.isNetworkAvailable()) {
                if (MySharedPreference.readBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, false)) {
                    AppDialogUtils.showConfirmationOfflineAvailDialog(this);
                    return;
                }
            }

            if (networkConnection.isNetworkAvailable()) {
                if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                    AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
                    return ;
                }
                // NOTHING TO DO::
            } else {

                if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                    AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
                    return ;
                }

            }

            String Shgid = gdto.getResponseContent().getListOfShg().get(position).getId();
            String ltd = gdto.getResponseContent().getListOfShg().get(position).getLastTransactionDate();
            String cih = gdto.getResponseContent().getListOfShg().get(position).getCashInHand();
            String cth = gdto.getResponseContent().getListOfShg().get(position).getCashAtBank();

            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.SHG_ID, Shgid);
            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.LAST_TRANSACTION, ltd);
            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHATBANK, cih);
            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHINHAND, cth);

            MySharedPreference.writeInteger(SHGGroupActivity.this, MySharedPreference.MENU_SELECTED, 0);
            Intent intent_ = new Intent(SHGGroupActivity.this, NewDrawerScreen.class);
            intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent_);
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
            finish();
        }

        @Override
        public void onItemClickVerification(ViewGroup parent, View view, int position) {

            if (networkConnection.isNetworkAvailable()) {
                if (MySharedPreference.readBoolean(EMathiApplication.getInstance(), MySharedPreference.ON_OFF_FLAG, false)) {
                    AppDialogUtils.showConfirmationOfflineAvailDialog(this);
                    return;
                }
            }

            if (networkConnection.isNetworkAvailable()) {
                if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                    AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
                    return ;
                }
                // NOTHING TO DO::
            } else {

                if (MySharedPreference.readInteger(EMathiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                    AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
                    return ;
                }

            }


            String Shgid = gdto.getResponseContent().getListOfShg().get(position).getId();
            String ltd = gdto.getResponseContent().getListOfShg().get(position).getLastTransactionDate();
            String cih = gdto.getResponseContent().getListOfShg().get(position).getCashInHand();
            String cth = gdto.getResponseContent().getListOfShg().get(position).getCashAtBank();

            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.SHG_ID, Shgid);
            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.LAST_TRANSACTION, ltd);
            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHATBANK, cih);
            MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHINHAND, cth);
            MySharedPreference.writeInteger(SHGGroupActivity.this, MySharedPreference.MENU_SELECTED, 0);
            Intent intent_ = new Intent(SHGGroupActivity.this, GroupProfileActivity.class);
            intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent_);
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
            finish();
        }

        @Override
        public void onItemClickAudit(ViewGroup parent, View view, int position) {
            getAuditapiCall();

        }


        private void CommonSetRecyclerview(ResponseDto lrdto) {

            this.gdto = lrdto;
            // TODO Auto-generated method stub
            ArrayList<ListOfShg> listSHG = lrdto.getResponseContent().getListOfShg();
            for (int i = 0; i < listSHG.size(); i++) {
                GroupListItem gp = new GroupListItem(leftImage, listSHG.get(i).getName() + " / " + listSHG.get(i).getPresidentName(), listImage);
                listItems.add(gp);
            }
            setCustomAdapter(listSHG);
        }

        private void setCustomAdapter(ArrayList<ListOfShg> shgDto) {
            // TODO Auto-generated method stub
            childList = new ArrayList<HashMap<String, String>>();

            for (int i = 0; i < shgDto.size(); i++) {
                HashMap<String, String> temp = new HashMap<String, String>();
                temp.put("SHGCode", (shgDto.get(i).getCode() != null && shgDto.get(i).getCode().length() > 0) ? shgDto.get(i).getCode().toUpperCase() : "NA");
                temp.put("BlockName", (shgDto.get(i).getBlockName() != null && shgDto.get(i).getBlockName().length() > 0) ? shgDto.get(i).getBlockName().toUpperCase() : "NA");
                temp.put("PanchayatName", (shgDto.get(i).getPanchayatName() != null && shgDto.get(i).getPanchayatName().length() > 0) ? shgDto.get(i).getPanchayatName().toUpperCase() : "NA");
                temp.put("VillageName", (shgDto.get(i).getVillageName() != null && shgDto.get(i).getVillageName().length() > 0) ? shgDto.get(i).getVillageName().toUpperCase() : shgDto.get(i).getPanchayatName());
                temp.put("LastTransactionDate", (shgDto.get(i).getLastTransactionDate() != null && shgDto.get(i).getLastTransactionDate().length() > 0) ? shgDto.get(i).getLastTransactionDate().toUpperCase() : "NA");

                temp.put("SHGCode_Label", "SHG Code");
                temp.put("BlockName_Label", "Block Name");
                temp.put("PanchayatName_Label", "Panchayat Name");
                temp.put("VillageName_Label", "Village Name");
                temp.put("LastTransactionDate_Label", "Last Transaction Date");
                childList.add(temp);
            }

            mAdapter = new CustomExpandableGroupListAdapter(SHGGroupActivity.this, shgDto, listItems, childList, this);
            expandableLayoutListView.setAdapter(mAdapter);

        }


        @Override
        public void onTaskStarted() {
            mProgressDialog = AppDialogUtils.createProgressDialog(this);
            mProgressDialog.show();
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onTaskFinished(String result, ServiceType serviceType) {


            switch (serviceType) {
                /*case SHG_LIST:
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = lrDto.getStatusCode();
                        String message = lrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);
                            Utils.showToast(this, message);
                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }


                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }
                        } else if (statusCode == Utils.Success_Code) {
                            Utils.showToast(this, message);
                            if (lrDto.getResponseContent().getListOfShg().size() > 0) {

                                //  MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_ID, lrDto.getResponseContent().getAnimatorId());
                                MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, lrDto.getResponseContent().getAnimatorName());
                                try {
                                    if (MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "") != null && MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "").length() > 0)
                                        mTitle.setText(MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "") + "    " + mobStr);
                                    //  mTitle.setText(Name + "    " + mobile);

                                    if (SHGTable.getSHGDetails() != null && SHGTable.getSHGDetails().size() > 0) {

                                        ArrayList<ListOfShg> shgListArray = SHGTable.getSHGDetails();
                                        ArrayList<ListOfShg> resposneSHGListArray = lrDto.getResponseContent().getListOfShg();

                                        ArrayList<String> shgListArrayString = new ArrayList<>();
                                        ArrayList<String> resposneSHGListString = new ArrayList<>();

                                        for (int i = 0; i < shgListArray.size(); i++) {
                                            shgListArrayString.add(shgListArray.get(i).getId());
                                        }

                                        for (int i = 0; i < resposneSHGListArray.size(); i++) {
                                            resposneSHGListString.add(resposneSHGListArray.get(i).getId());
                                        }

                                        Collections.sort(shgListArrayString);
                                        Collections.sort(resposneSHGListString);

                                        boolean isEqual = shgListArrayString.equals(resposneSHGListString);

                                        if (isEqual) {

                                        } else {
                                            SHGTable.deleteSHG(userId);
                                            for (int i = 0; i < lrDto.getResponseContent().getListOfShg().size(); i++) {
                                                ListOfShg lg = lrDto.getResponseContent().getListOfShg().get(i);

                                                SHGTable.insertSHGData(lg, userId);
                                            }
                                        }
                                    } else {
                                        for (int i = 0; i < lrDto.getResponseContent().getListOfShg().size(); i++) {
                                            ListOfShg lg = lrDto.getResponseContent().getListOfShg().get(i);
                                            //  SHGTable.deleteSHG(userId);
                                            SHGTable.insertSHGData(lg, userId);
                                        }
                                    }


                                    //     for (int i = 0; i < lrDto.getResponseContent().getShgBankDetails().size(); i++) {
                                    if (lrDto.getResponseContent().getShgBankDetails().size() > 0) {
                                        for (ShgBankDetails lgl : lrDto.getResponseContent().getShgBankDetails()) {
                                            BankTable.insertBankData(lgl);
                                        }
                                    }
                                    if (mProgressDialog != null) {
                                        mProgressDialog.dismiss();
                                    }

                                    if (lrDto.getResponseContent().getAnimatorId() != null) {
                                        lrDto.getResponseContent().setUserId(userId);
                                        LoginTable.updateAnimatorLogin(lrDto);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("***SHG ACTIVITY **", e.toString());
                                }
                                CommonSetRecyclerview(lrDto);
                            } else {
                                Toast.makeText(SHGGroupActivity.this, "SHG List gets empty", Toast.LENGTH_SHORT).show();
                            }


                        } else {
                            Utils.showToast(this, message);
                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }

                        }
                    }
                    break;
*/

                case LOG_OUT:

                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = lrDto.getStatusCode();
                        String message = lrDto.getMessage();
                        Log.d("response status", " " + statusCode);

                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(this, message);

                            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                            this.finish();
                        }

                    }

                    break;


                case TRANSACTIONAUDIT:
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = lrDto.getStatusCode();
                        String message = lrDto.getMessage();
                        Log.d("response status", " " + statusCode);

                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(this, message);

                            DbHelper.getInstance(this).deleteLoantable();
                            shg_userId = lrDto.getResponseContent().getAuditorDetails().get(0).getShgUserId();
                            auditor_id = lrDto.getResponseContent().getAuditorDetails().get(0).getAuditorUserId();
                            internaloandisbursementsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().size();
                            Log.d("internaloandisbursementsize",""+internaloandisbursementsize);
                            seedFundsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().size();
                            Log.d("seedFundsize",""+seedFundsize);
                            fDBankTransactionsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().size();
                            Log.d("fDBankTransactionsize",""+fDBankTransactionsize);
                            incomeDisbursementsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().size();
                            Log.d("incomeDisbursementsize",""+incomeDisbursementsize);
                            otherIncomesize =  lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().size();
                            Log.d("otherIncomesize",""+otherIncomesize);
                            groupLoanRepaymentsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().size();
                            Log.d("groupLoanRepaymentsize",""+groupLoanRepaymentsize);
                            penaltysize =  lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().size();
                            Log.d("penaltysize",""+penaltysize);
                            bankTransactionsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().size();
                            Log.d("bankTransactionsize",""+bankTransactionsize);
                            subscriptiontoFedssize =  lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().size();
                            Log.d("subscriptiontoFedssize",""+subscriptiontoFedssize);
                            otherIncomeGroupsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().size();
                            Log.d("otherIncomeGroupsize",""+otherIncomeGroupsize);
                            memberLoanRepaymentsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().size();
                            Log.d("memberLoanRepaymentsize",""+memberLoanRepaymentsize);
                            voluntarySavingsDisbursementsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().size();
                            Log.d("voluntarySavingsDisbursementsize",""+voluntarySavingsDisbursementsize);
                            otherExpensesize =  lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().size();
                            Log.d("otherExpensesize",""+otherExpensesize);
                            meetingExpensesize =  lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().size();
                            Log.d("meetingExpensesize",""+meetingExpensesize);
                            internalLoanRepaymentsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().size();
                            Log.d("internalLoanRepaymentsize",""+internalLoanRepaymentsize);
                            donationsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().size();
                            Log.d("donationsize",""+donationsize);
                            voluntarysavingsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().size();
                            Log.d("voluntarysavingsize",""+voluntarysavingsize);
                            savingdisbursementsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().size();
                            Log.d("savingdisbursementsize",""+savingdisbursementsize);
                            savingsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().size();
                            Log.d("savingsize",""+savingsize);
                            subscriptionsize =  lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().size();
                            Log.d("subscriptionsize",""+subscriptionsize);
                            subscriptionChargessize =  lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().size();
                            Log.d("subscriptionChargessize",""+subscriptionChargessize);


// 1.INTERNALLOAN DISBURSEMENT

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().isEmpty()){

                                ArrayList<InternalLoanDisbursement> internalLoanDisbursementArrayList =new ArrayList<>();
                                for (int i = 0; i < internaloandisbursementsize; i++) {

                                    InternalLoanDisbursement internalLoanDisbursement =new InternalLoanDisbursement();
                                    internalLoanDisbursement.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getAuditor_uuid());
                                    internalLoanDisbursement.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getShg_uuid());
                                    internalLoanDisbursement.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getMember());
                                    internalLoanDisbursement.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getMember_uuid());
                                    internalLoanDisbursement.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getNew_transaction_amount());
                                    internalLoanDisbursement.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getOld_transaction_amount());
                                    internalLoanDisbursement.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getTransaction_date());
                                    internalLoanDisbursement.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getAuditor_uuid());
                                    internalLoanDisbursement.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanDisbursement().get(i).getSettings());
                                    internalLoanDisbursementArrayList.add(internalLoanDisbursement);

                                }
                                Log.d("Savings",""+internalLoanDisbursementArrayList);
                                DbHelper.insertInternalLoanDisbursementAuditData(internalLoanDisbursementArrayList);

                            }

                            //2.SEED FUND
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().isEmpty()){


                                ArrayList<SeedFund> seedFundArrayList =new ArrayList<>();
                                for (int i = 0; i < seedFundsize; i++) {

                                    SeedFund seedFund =new SeedFund();
                                    seedFund.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getAuditor_uuid());
                                    seedFund.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getShg_uuid());
                                    seedFund.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getMember());
                                    seedFund.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getMember_uuid());
                                    seedFund.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getNew_transaction_amount());
                                    seedFund.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getOld_transaction_amount());
                                    seedFund.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getTransaction_date());
                                    seedFund.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getAuditor_uuid());
                                    seedFund.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getSeedFund().get(i).getSettings());
                                    seedFundArrayList.add(seedFund);

                                }
                                DbHelper.insertseedFundArrayListAuditData(seedFundArrayList);
                            }
// 3.FDBANKTRANSACTION

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().isEmpty()){


                                ArrayList<FDBankTransaction> fdBankTransactionArrayList =new ArrayList<>();

                                for (int i = 0; i < fDBankTransactionsize; i++) {

                                    FDBankTransaction fdBankTransaction =new FDBankTransaction();
                                    fdBankTransaction.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getAuditor_uuid());
                                    fdBankTransaction.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getShg_uuid());
                                    fdBankTransaction.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getMember());
                                    fdBankTransaction.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getMember_uuid());
                                    fdBankTransaction.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getNew_transaction_amount());
                                    fdBankTransaction.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getOld_transaction_amount());
                                    fdBankTransaction.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getTransaction_date());
                                    fdBankTransaction.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getAuditor_uuid());
                                    fdBankTransaction.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getFDBankTransaction().get(i).getSettings());
                                    fdBankTransactionArrayList.add(fdBankTransaction);

                                }
                                DbHelper.insertfdBankTransactionArrayListAuditData(fdBankTransactionArrayList);
                            }
// 4.INCOMEDISBURSEMENT

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().isEmpty()){


                                ArrayList<IncomeDisbursement> incomeDisbursementArrayList =new ArrayList<>();

                                for (int i = 0; i < incomeDisbursementsize; i++) {

                                    IncomeDisbursement incomeDisbursement =new IncomeDisbursement();
                                    incomeDisbursement.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getAuditor_uuid());
                                    incomeDisbursement.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getShg_uuid());
                                    incomeDisbursement.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getMember());
                                    incomeDisbursement.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getMember_uuid());
                                    incomeDisbursement.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getNew_transaction_amount());
                                    incomeDisbursement.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getOld_transaction_amount());
                                    incomeDisbursement.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getTransaction_date());
                                    incomeDisbursement.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getAuditor_uuid());
                                    incomeDisbursement.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getIncomeDisbursement().get(i).getSettings());
                                    incomeDisbursementArrayList.add(incomeDisbursement);

                                }
                                DbHelper.insertIncomeDisbursementArrayListAuditData(incomeDisbursementArrayList);
                            }

// 5.OTHERINCOME
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().isEmpty()){


                                ArrayList<OtherIncome> otherIncomeArrayList =new ArrayList<>();

                                for (int i = 0; i < otherIncomesize; i++) {

                                    OtherIncome otherIncome =new OtherIncome();
                                    otherIncome.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getAuditor_uuid());
                                    otherIncome.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getShg_uuid());
                                    otherIncome.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getMember());
                                    otherIncome.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getMember_uuid());
                                    otherIncome.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getNew_transaction_amount());
                                    otherIncome.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getOld_transaction_amount());
                                    otherIncome.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getTransaction_date());
                                    otherIncome.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getAuditor_uuid());
                                    otherIncome.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncome().get(i).getSettings());
                                    otherIncomeArrayList.add(otherIncome);

                                }
                                DbHelper.insertOtherIncomeArrayListAuditData(otherIncomeArrayList);
                            }
// 6.GROUPLOANREPAYMENT
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().isEmpty()){


                                ArrayList<GroupLoanRepayment> groupLoanRepaymentArrayList =new ArrayList<>();

                                for (int i = 0; i < groupLoanRepaymentsize; i++) {

                                    GroupLoanRepayment groupLoanRepayment =new GroupLoanRepayment();
                                    groupLoanRepayment.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getAuditor_uuid());
                                    groupLoanRepayment.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getShg_uuid());
                                    groupLoanRepayment.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getMember());
                                    groupLoanRepayment.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getMember_uuid());
                                    groupLoanRepayment.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getNew_transaction_amount());
                                    groupLoanRepayment.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getOld_transaction_amount());
                                    groupLoanRepayment.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getTransaction_date());
                                    groupLoanRepayment.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getAuditor_uuid());
                                    groupLoanRepayment.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getGroupLoanRepayment().get(i).getSettings());
                                    groupLoanRepaymentArrayList.add(groupLoanRepayment);

                                }
                                DbHelper.insertGroupLoanRepaymentArrayListAuditData(groupLoanRepaymentArrayList);
                            }
// 7.PENALTY

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().isEmpty()){


                                ArrayList<Penalty> penaltyArrayList =new ArrayList<>();

                                for (int i = 0; i < penaltysize; i++) {

                                    Penalty penalty =new Penalty();
                                    penalty.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getAuditor_uuid());
                                    penalty.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getShg_uuid());
                                    penalty.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getMember());
                                    penalty.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getMember_uuid());
                                    penalty.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getNew_transaction_amount());
                                    penalty.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getOld_transaction_amount());
                                    penalty.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getTransaction_date());
                                    penalty.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getAuditor_uuid());
                                    penalty.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getPenalty().get(i).getSettings());
                                    penaltyArrayList.add(penalty);

                                }
                                DbHelper.insertPenaltyArrayListAuditData(penaltyArrayList);
                            }

// 8.BANKTRANSACTION

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().isEmpty()){


                                ArrayList<BankTransaction> bankTransactionArrayList =new ArrayList<>();

                                for (int i = 0; i < bankTransactionsize; i++) {

                                    BankTransaction bankTransaction =new BankTransaction();
                                    bankTransaction.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getAuditor_uuid());
                                    bankTransaction.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getShg_uuid());
                                    bankTransaction.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getMember());
                                    bankTransaction.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getMember_uuid());
                                    bankTransaction.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getNew_transaction_amount());
                                    bankTransaction.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getOld_transaction_amount());
                                    bankTransaction.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getTransaction_date());
                                    bankTransaction.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getAuditor_uuid());
                                    bankTransaction.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getBankTransaction().get(i).getSettings());
                                    bankTransactionArrayList.add(bankTransaction);

                                }
                                DbHelper.insertBankTransactionArrayListAuditData(bankTransactionArrayList);
                            }
// 9.SUBSCRIPTIONFEDS

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().isEmpty()){


                                ArrayList<SubscriptiontoFeds> subscriptiontoFedsArrayList =new ArrayList<>();

                                for (int i = 0; i < subscriptiontoFedssize; i++) {

                                    SubscriptiontoFeds subscriptiontoFeds =new SubscriptiontoFeds();
                                    subscriptiontoFeds.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getAuditor_uuid());
                                    subscriptiontoFeds.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getShg_uuid());
                                    subscriptiontoFeds.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getMember());
                                    subscriptiontoFeds.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getMember_uuid());
                                    subscriptiontoFeds.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getNew_transaction_amount());
                                    subscriptiontoFeds.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getOld_transaction_amount());
                                    subscriptiontoFeds.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getTransaction_date());
                                    subscriptiontoFeds.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getAuditor_uuid());
                                    subscriptiontoFeds.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptiontoFeds().get(i).getSettings());
                                    subscriptiontoFedsArrayList.add(subscriptiontoFeds);

                                }
                                DbHelper.insertSubscriptionFedsArrayListAuditData(subscriptiontoFedsArrayList);
                            }

//10.OTHERINCOMEGROUP

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().isEmpty()){


                                ArrayList<OtherIncomeGroup> otherIncomeGroupArrayList =new ArrayList<>();

                                for (int i = 0; i < otherIncomeGroupsize; i++) {

                                    OtherIncomeGroup otherIncomeGroup =new OtherIncomeGroup();
                                    otherIncomeGroup.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getAuditor_uuid());
                                    otherIncomeGroup.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getShg_uuid());
                                    otherIncomeGroup.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getMember());
                                    otherIncomeGroup.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getMember_uuid());
                                    otherIncomeGroup.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getNew_transaction_amount());
                                    otherIncomeGroup.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getOld_transaction_amount());
                                    otherIncomeGroup.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getTransaction_date());
                                    otherIncomeGroup.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getAuditor_uuid());
                                    otherIncomeGroup.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherIncomeGroup().get(i).getSettings());
                                    otherIncomeGroupArrayList.add(otherIncomeGroup);

                                }
                                DbHelper.insertOtherincomeGroupArrayListAuditData(otherIncomeGroupArrayList);
                            }

//11.MEMBERLOANREPAYMENT

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().isEmpty()){


                                ArrayList<memberLoanRepayment> memberLoanRepaymentArrayList =new ArrayList<>();

                                for (int i = 0; i < memberLoanRepaymentsize; i++) {

                                    memberLoanRepayment memberLoanRepayments =new memberLoanRepayment();
                                    memberLoanRepayments.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getAuditor_uuid());
                                    memberLoanRepayments.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getShg_uuid());
                                    memberLoanRepayments.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getMember());
                                    memberLoanRepayments.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getMember_uuid());
                                    memberLoanRepayments.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getNew_transaction_amount());
                                    memberLoanRepayments.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getOld_transaction_amount());
                                    memberLoanRepayments.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getTransaction_date());
                                    memberLoanRepayments.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getAuditor_uuid());
                                    memberLoanRepayments.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getMemberLoanRepayment().get(i).getSettings());
                                    memberLoanRepaymentArrayList.add(memberLoanRepayments);

                                }
                                DbHelper.insertmemberLoanRepaymentAuditData(memberLoanRepaymentArrayList);
                            }
// 12.VOLUNTARYSAVINGSDISBURSEMENT

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().isEmpty()){


                                ArrayList<VoluntarySavingsDisbursement> voluntarySavingsDisbursementArrayList =new ArrayList<>();

                                for (int i = 0; i < voluntarySavingsDisbursementsize; i++) {

                                    VoluntarySavingsDisbursement voluntarySavingsDisbursement =new VoluntarySavingsDisbursement();
                                    voluntarySavingsDisbursement.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getAuditor_uuid());
                                    voluntarySavingsDisbursement.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getShg_uuid());
                                    voluntarySavingsDisbursement.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getMember());
                                    voluntarySavingsDisbursement.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getMember_uuid());
                                    voluntarySavingsDisbursement.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getNew_transaction_amount());
                                    voluntarySavingsDisbursement.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getOld_transaction_amount());
                                    voluntarySavingsDisbursement.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getTransaction_date());
                                    voluntarySavingsDisbursement.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getAuditor_uuid());
                                    voluntarySavingsDisbursement.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavingsDisbursement().get(i).getSettings());
                                    voluntarySavingsDisbursementArrayList.add(voluntarySavingsDisbursement);

                                }
                                DbHelper.insertvoluntarysavingsdisbursementAuditData(voluntarySavingsDisbursementArrayList);
                            }
//13.OTHEREXPENSE

                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().isEmpty()){


                                ArrayList<OtherExpense> otherExpenseArrayList =new ArrayList<>();

                                for (int i = 0; i < otherExpensesize; i++) {

                                    OtherExpense otherExpense =new OtherExpense();
                                    otherExpense.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getAuditor_uuid());
                                    otherExpense.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getShg_uuid());
                                    otherExpense.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getMember());
                                    otherExpense.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getMember_uuid());
                                    otherExpense.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getNew_transaction_amount());
                                    otherExpense.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getOld_transaction_amount());
                                    otherExpense.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getTransaction_date());
                                    otherExpense.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getAuditor_uuid());
                                    otherExpense.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getOtherExpense().get(i).getSettings());
                                    otherExpenseArrayList.add(otherExpense);

                                }
                                DbHelper.insertOtherExpenseAuditData(otherExpenseArrayList);
                            }
//14.MEETINGEXPENSE
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().isEmpty()){


                                ArrayList<MeetingExpense> meetingExpenseArrayList =new ArrayList<>();

                                for (int i = 0; i < meetingExpensesize; i++) {

                                    MeetingExpense meetingExpense =new MeetingExpense();
                                    meetingExpense.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getAuditor_uuid());
                                    meetingExpense.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getShg_uuid());
                                    meetingExpense.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getMember());
                                    meetingExpense.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getMember_uuid());
                                    meetingExpense.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getNew_transaction_amount());
                                    meetingExpense.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getOld_transaction_amount());
                                    meetingExpense.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getTransaction_date());
                                    meetingExpense.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getAuditor_uuid());
                                    meetingExpense.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getMeetingExpense().get(i).getSettings());
                                    meetingExpenseArrayList.add(meetingExpense);

                                }
                                DbHelper.insertMeetingExpenseAuditData(meetingExpenseArrayList);
                            }

//15.INTERNALLOAN REPAYMENT
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().isEmpty()){


                                ArrayList<internalLoanRepayment> internalLoanRepaymentArrayList =new ArrayList<>();

                                for (int i = 0; i < internalLoanRepaymentsize; i++) {

                                    internalLoanRepayment internalLoanRepayments =new internalLoanRepayment();
                                    internalLoanRepayments.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getAuditor_uuid());
                                    internalLoanRepayments.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getShg_uuid());
                                    internalLoanRepayments.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getMember());
                                    internalLoanRepayments.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getMember_uuid());
                                    internalLoanRepayments.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getNew_transaction_amount());
                                    internalLoanRepayments.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getOld_transaction_amount());
                                    internalLoanRepayments.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getTransaction_date());
                                    internalLoanRepayments.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getAuditor_uuid());
                                    internalLoanRepayments.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getInternalLoanRepayment().get(i).getSettings());
                                    internalLoanRepaymentArrayList.add(internalLoanRepayments);

                                }
                                DbHelper.insertinternalLoanRepaymentAuditData(internalLoanRepaymentArrayList);
                            }
//16.DONATION
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().isEmpty()){


                                ArrayList<Donation> donationArrayList =new ArrayList<>();

                                for (int i = 0; i < donationsize; i++) {

                                    Donation donation =new Donation();
                                    donation.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getAuditor_uuid());
                                    donation.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getShg_uuid());
                                    donation.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getMember());
                                    donation.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getMember_uuid());
                                    donation.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getNew_transaction_amount());
                                    donation.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getOld_transaction_amount());
                                    donation.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getTransaction_date());
                                    donation.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getAuditor_uuid());
                                    donation.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getDonation().get(i).getSettings());
                                    donationArrayList.add(donation);

                                }
                                DbHelper.insertDonationAuditData(donationArrayList);
                            }
//17.VOLUNTARY SAVINGS
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().isEmpty()) {
                                ArrayList<VoluntarySavings> voluntarySavings =new ArrayList<>();
                                for (int i = 0; i < voluntarysavingsize; i++) {

                                    VoluntarySavings voluntarySavings1 =new VoluntarySavings();
                                    voluntarySavings1.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getAuditor_uuid());
                                    voluntarySavings1.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getShg_uuid());
                                    voluntarySavings1.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getMember());
                                    voluntarySavings1.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getMember_uuid());
                                    voluntarySavings1.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getNew_transaction_amount());
                                    voluntarySavings1.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getOld_transaction_amount());
                                    voluntarySavings1.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getTransaction_date());
                                    voluntarySavings1.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getAuditor_uuid());
                                    voluntarySavings1.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getVoluntarySavings().get(i).getSettings());
                                    voluntarySavings.add(voluntarySavings1);

                                }
                                DbHelper.insertVoluntarySavingAuditData(voluntarySavings);
                            }
//18.SAVINGDISBURSEMENT
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().isEmpty()) {


                                ArrayList<SavingsDisbursement> savingsDisbursementArrayList =new ArrayList<>();
                                for (int i = 0; i < savingdisbursementsize; i++) {

                                    SavingsDisbursement savingsDisbursement =new SavingsDisbursement();
                                    savingsDisbursement.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getAuditor_uuid());
                                    savingsDisbursement.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getShg_uuid());
                                    savingsDisbursement.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getMember());
                                    savingsDisbursement.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getMember_uuid());
                                    savingsDisbursement.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getNew_transaction_amount());
                                    savingsDisbursement.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getOld_transaction_amount());
                                    savingsDisbursement.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getTransaction_date());
                                    savingsDisbursement.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getAuditor_uuid());
                                    savingsDisbursement.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getSavingsDisbursement().get(i).getSettings());
                                    savingsDisbursementArrayList.add(savingsDisbursement);

                                }
                                DbHelper.insertSavingDisbursementAuditData(savingsDisbursementArrayList);
                            }
//19.SAVINGS
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().isEmpty()){


                                ArrayList<Savings> savingsArrayList =new ArrayList<>();
                                for (int i = 0; i <savingsize; i++) {

                                    Savings savings =new Savings();
                                    savings.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getAuditor_uuid());
                                    savings.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getShg_uuid());
                                    savings.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getMember());
                                    savings.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getMember_uuid());
                                    savings.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getNew_transaction_amount());
                                    savings.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getOld_transaction_amount());
                                    savings.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getTransaction_date());
                                    savings.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getAuditor_uuid());
                                    savings.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getSavings().get(i).getSettings());
                                    savingsArrayList.add(savings);

                                }
                                Log.d("Savings",""+savingsArrayList);
                                DbHelper.insertSavingAuditData(savingsArrayList);

                            }

//20.SUBSCRIPTION
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().isEmpty()){

                                ArrayList<Subscription> subscriptionArrayList =new ArrayList<>();
                                for (int i = 0; i <subscriptionsize; i++) {

                                    Subscription subscription =new Subscription();
                                    subscription.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getAuditor_uuid());
                                    subscription.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getShg_uuid());
                                    subscription.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getMember());
                                    subscription.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getMember_uuid());
                                    subscription.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getNew_transaction_amount());
                                    subscription.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getOld_transaction_amount());
                                    subscription.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getTransaction_date());
                                    subscription.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getAuditor_uuid());
                                    subscription.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscription().get(i).getSettings());
                                    subscriptionArrayList.add(subscription);

                                }
                                Log.d("Savings",""+subscriptionArrayList);
                                DbHelper.insertSubscriptionAuditData(subscriptionArrayList);

                            }
//21.SUBSCRIPTIONCHARGES
                            if(!lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().isEmpty()){


                                ArrayList<SubscriptionCharges> subscriptionChargesArrayList =new ArrayList<>();
                                for (int i = 0; i <subscriptionChargessize; i++) {

                                    SubscriptionCharges subscriptionCharges =new SubscriptionCharges();
                                    subscriptionCharges.setAuditor_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getAuditor_uuid());
                                    subscriptionCharges.setShg_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getShg_uuid());
                                    subscriptionCharges.setMember(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getMember());
                                    subscriptionCharges.setMember_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getMember_uuid());
                                    subscriptionCharges.setNew_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getNew_transaction_amount());
                                    subscriptionCharges.setOld_transaction_amount(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getOld_transaction_amount());
                                    subscriptionCharges.setTransaction_date(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getTransaction_date());
                                    subscriptionCharges.setTransaction_uuid(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getAuditor_uuid());
                                    subscriptionCharges.setSettings(lrDto.getResponseContent().getAuditorDetails().get(0).getSubscriptionCharges().get(i).getSettings());
                                    subscriptionChargesArrayList.add(subscriptionCharges);

                                }
                                Log.d("Savings",""+subscriptionChargesArrayList);
                                DbHelper.insertSubscriptionChargesAuditData(subscriptionChargesArrayList);

                            }

                            if (internaloandisbursementsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditInternalLoanDisbursement.class));
                            }  if (seedFundsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditSeedFund.class));
                            } else if (fDBankTransactionsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditFdBankTransaction.class));
                            } else if (incomeDisbursementsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditIncomeDisbursement.class));
                            } else if (otherIncomesize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditOtherIncome.class));
                            } else if (groupLoanRepaymentsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditGroupLoanRepayment.class));
                            } else if (penaltysize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditPenalty.class));
                            } else if (bankTransactionsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditBankTransaction.class));
                            } else if (subscriptiontoFedssize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditSubscriptiontoFeds.class));
                            } else if (otherIncomeGroupsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditOtherincomeGroup.class));
                            } else if (memberLoanRepaymentsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditMemberLoanReapyment.class));
                            } else if (voluntarySavingsDisbursementsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditVoluntarySavingsDisbursement.class));
                            } else if (otherExpensesize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditOtherExpense.class));
                            } else if (meetingExpensesize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditMeetingExpense.class));
                            } else if (internalLoanRepaymentsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditInternalLoanRepayment.class));
                            } else if (donationsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditDonation.class));
                            } else if (voluntarysavingsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditVoluntarysavings.class));
                            } else if (savingdisbursementsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditSavingdisbursement.class));
                            } else if (savingsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, Audit_savings.class));
                            } else if (subscriptionsize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditSubscription.class));
                            } else if (subscriptionChargessize != 0) {
                                startActivity(new Intent(SHGGroupActivity.this, AuditSubscriptiontoFeds.class));
                            }

                        }

                    }
                    break;

            }


        }



        @Override
        public boolean onClose() {
//            mAdapter.filterData("");
            mAdapter.getFilter().filter("");
            return false;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            //mAdapter.filterData(query);
            mAdapter.getFilter().filter(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String query) {
          //  mAdapter.filterData(query);
            mAdapter.getFilter().filter(query);
            return false;
        }
    }
