package com.oasys.emathi.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.VLoanTypes;
import com.oasys.emathi.EMathiApplication;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.GetExit;
import com.oasys.emathi.OasysUtils.GetSpanText;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.R;
import com.oasys.emathi.R.color;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.views.CustomHorizontalScrollView;
import com.oasys.emathi.views.Get_EdiText_Filter;
import com.oasys.emathi.views.RaisedButton;
import com.oasys.emathi.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;


import java.util.ArrayList;
import java.util.List;

public class EditOpeningBalanceSavingsActivity extends AppCompatActivity implements OnClickListener {

    public static final String TAG = EditOpeningBalanceSavingsActivity.class.getSimpleName();
    private TextView mGroupName, mHeaderTextView;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    private RaisedButton mSubmit_RaisedButton;

    List<EditText> sSavingsFields;
    List<EditText> sVSavingsFields;
    private EditText mSavings_values, mVSavings_values;
    int mSize;
    String width[] = {AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    private String mLanguagelocale = "";
    String[] mEditMasterSavings, mSavingsIndi, mVSavingsIndi;
    ArrayList<VLoanTypes> mSavingsArrayValues, mVSavingsArrayValues;

    String mSavingsString, mVSavingsString;
    public static ArrayList<String> mSavingsVector = new ArrayList<>(), mVSavingsVector = new ArrayList<>();

    String sEditSavingsAmounts[], sEditVSavingsAmount[];
    public static String sEditSendToServer_Savings = "", sEditSendToServer_VSavings = "";
    public static int sEditSavings_Total, sEditVSavings_Total;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    String[] editConfirmArr;
    String nullVlaue = "0";
    Dialog confirmationDialog;
    String mLanguageLocalae;
    boolean mIsNegativeValues = false;

    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    private ListOfShg shgDetails;
    private Toolbar mToolbar;
    private TextView mTitle;

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        mVSavingsVector.clear();
        mSavingsVector.clear();
        sEditSavingsAmounts = null;
        sEditVSavingsAmount = null;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_edit_ob_savings);
        setTitleColor(color.white_text);

        try {
            shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));
            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
            mTitle.setText("ESHAKTI");
            mTitle.setGravity(Gravity.CENTER);


            sSavingsFields = new ArrayList<EditText>();
            sVSavingsFields = new ArrayList<EditText>();

            mLanguagelocale = "";

            mGroupName = (TextView) findViewById(R.id.groupname_editSavings);
            mGroupName.setText(String.valueOf(""));
            mGroupName.setText(shgDetails.getName() + " / " + shgDetails.getPresidentName());

            mHeaderTextView = (TextView) findViewById(R.id.fragmentHeader_editSavings);
            mHeaderTextView.setText(AppStrings.savings);

            mLeftHeaderTable = (TableLayout) findViewById(R.id.LeftHeaderTable_editSavings);
            mRightHeaderTable = (TableLayout) findViewById(R.id.RightHeaderTable_editSavings);
            mLeftContentTable = (TableLayout) findViewById(R.id.LeftContentTable_editSavings);
            mRightContentTable = (TableLayout) findViewById(R.id.RightContentTable_editSavings);

            mHSRightHeader = (CustomHorizontalScrollView) findViewById(R.id.rightHeaderHScrollView_editSavings);
            mHSRightContent = (CustomHorizontalScrollView) findViewById(R.id.rightContentHScrollView_editSavings);

            mMemberNameLayout = (LinearLayout) findViewById(R.id.member_name_layout);
            mMemberName = (TextView) findViewById(R.id.member_name);

            if (EMathiApplication.vertficationDto != null && EMathiApplication.vertficationDto.getMemberfinancialDetails() != null && EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings() != null) {

                mSavingsArrayValues = EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings();
                mVSavingsArrayValues = EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings();
                String[] mTempValues = new String[mSavingsArrayValues.size()], mTempValues1 = new String[mSavingsArrayValues.size()];
                for (int i = 0; i < mSavingsArrayValues.size(); i++) {
                    mTempValues[i] = mSavingsArrayValues.get(i).getAmount();
                    mSavingsVector.add(mTempValues[i]);
                }

                for (int j = 0; j < mVSavingsArrayValues.size(); j++) {
                    mTempValues1[j] = mVSavingsArrayValues.get(j).getVoluntaryAmount();
                    mVSavingsVector.add(mTempValues1[j]);
                }
            }

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });

            mSubmit_RaisedButton = (RaisedButton) findViewById(R.id.fragment_Submit_button_editSavings);
            mSubmit_RaisedButton.setText(AppStrings.mConfirm);
            mSubmit_RaisedButton.setOnClickListener(this);

            buildTableHeaderLayout();

            buildTableContentLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void buildTableHeaderLayout() {
        // TODO Auto-generated method stub

        TableRow leftHeaderRow = new TableRow(this);

        TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        lHeaderParams.setMargins(5, 5, 5, 5);

        TextView mMemberName_headerText = new TextView(this);
        mMemberName_headerText.setText(String.valueOf(AppStrings.memberName));
        mMemberName_headerText.setTextColor(Color.WHITE);
        mMemberName_headerText.setPadding(15, 5, 10, 5);
        mMemberName_headerText.setLayoutParams(lHeaderParams);
        leftHeaderRow.addView(mMemberName_headerText);

        mLeftHeaderTable.addView(leftHeaderRow);

        TableRow rightHeaderRow = new TableRow(this);
        TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        rHeaderParams.setMargins(10, 0, 20, 0);
        TextView mSavingsAmount_HeaderText = new TextView(this);
        mSavingsAmount_HeaderText
                .setText(String.valueOf(AppStrings.savingsAmount));
        mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
        mSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
        mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
        // mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
        mSavingsAmount_HeaderText.setSingleLine(true);
        rightHeaderRow.addView(mSavingsAmount_HeaderText);

        TextView mVSavingsAmount_HeaderText = new TextView(this);

        mVSavingsAmount_HeaderText
                .setText(String.valueOf(AppStrings.voluntarySavings));

        mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);
        if (mLanguagelocale.equalsIgnoreCase("English")) {
            mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
        } else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
            mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
        } else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
            mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
        } else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
            mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
        } else {
            mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
        }
        mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
        mVSavingsAmount_HeaderText.setSingleLine(true);
        rightHeaderRow.addView(mVSavingsAmount_HeaderText);

        mRightHeaderTable.addView(rightHeaderRow);

        getTableRowHeaderCellWidth();

    }

    @SuppressWarnings("deprecation")
    private void buildTableContentLayout() {
        try {
            // TODO Auto-generated method stub

            mSize = mSavingsVector.size();
            Log.d("Edit", String.valueOf(mSize));

            for (int i = 0; i < mSize; i++) {
                TableRow leftContentRow = new TableRow(this);

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);

                final TextView memberName_Text = new TextView(this);
                memberName_Text.setText(
                        GetSpanText.getSpanString(this, mSavingsArrayValues.get(i).getMemberName()));
                memberName_Text.setTextColor(color.black);
                memberName_Text.setPadding(15, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);

                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(this);

                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
                        LayoutParams.WRAP_CONTENT, 1f);
                rightContentParams.setMargins(20, 5, 20, 5);

                try {
                    mSavings_values = new EditText(this);
                    mSavings_values.setId(i);
                    sSavingsFields.add(mSavings_values);
                    mSavings_values.setPadding(5, 5, 5, 5);
                    mSavings_values.setBackgroundResource(R.drawable.edittext_background);
                    mSavings_values.setLayoutParams(rightContentParams);
                    mSavings_values.setTextAppearance(this, R.style.MyMaterialTheme);
                    mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    mSavings_values.setTextColor(color.black);
                    mSavings_values.setText(mSavingsVector.get(i));


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                final int finals = i;
                // mSavings_values.setWidth(150);
                mSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {

                            if(v.getId()==finals) {

                                if (sSavingsFields.get(finals).getText().toString().equals("0.0") || sSavingsFields.get(finals).getText().toString().equals("0")) {
                                    ((EditText)v).setText("");
                                }
                            }
//                            ((EditText) v).setGravity(Gravity.LEFT);
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, EditOpeningBalanceSavingsActivity.this);
                        } else {
//                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mSavings_values);



                mVSavings_values = new EditText(this);
                mVSavings_values.setId(i);

                sVSavingsFields.add(mVSavings_values);
                mVSavings_values.setPadding(5, 5, 5, 5);

                mVSavings_values.setBackgroundResource(R.drawable.edittext_background);

                // mVSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mVSavings_values.setLayoutParams(rightContentParams);
                mVSavings_values.setTextAppearance(this, R.style.MyMaterialTheme);
                mVSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mVSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
                mVSavings_values.setTextColor(color.black);
                mVSavings_values.setText(mVSavingsVector.get(i));
                // mVSavings_values.setWidth(150);
                final int finalI = i;
                mVSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {


                            if(v.getId()==finalI) {

                                if (sVSavingsFields.get(finalI).getText().toString().equals("0.0") || sVSavingsFields.get(finals).getText().toString().equals("0")) {
                                    ((EditText)v).setText("");
                                }
                            }
//                            ((EditText) v).setGravity(Gravity.LEFT);
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, EditOpeningBalanceSavingsActivity.this);
                        } else {
//                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });
                rightContentRow.addView(mVSavings_values);

                mRightContentTable.addView(rightContentRow);

            }

            resizeMemberNameWidth();

            resizeBodyTableRowHeight();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub


        sEditSavingsAmounts = new String[mSize];
        sEditVSavingsAmount = new String[mSize];

        switch (v.getId()) {
            case R.id.fragment_Submit_button_editSavings:

                try {

                    sEditSavings_Total = 0;
                    sEditVSavings_Total = 0;
                    editConfirmArr = new String[mSize];

                    sEditSendToServer_Savings = "";
                    sEditSendToServer_VSavings = "";
                    // Do edit values here

                    StringBuilder builder = new StringBuilder();

                    for (int i = 0; i < sEditSavingsAmounts.length; i++) {

                        sEditSavingsAmounts[i] = String.valueOf(sSavingsFields.get(i).getText());

                        sEditVSavingsAmount[i] = String.valueOf(sVSavingsFields.get(i).getText());

                        if ((sEditSavingsAmounts[i].equals("")) || (sEditSavingsAmounts[i] == null)) {
                            sEditSavingsAmounts[i] = nullVlaue;
                        }

                        if ((sEditVSavingsAmount[i].equals("")) || (sEditVSavingsAmount[i] == null)) {
                            sEditVSavingsAmount[i] = nullVlaue;
                        }

                        if (sEditSavingsAmounts[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            int savingsAmount = (int) Math.round(Double.parseDouble(sEditSavingsAmounts[i]));
                            sEditSavingsAmounts[i] = String.valueOf(savingsAmount);
                        }

                        if (sEditVSavingsAmount[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            int vSavingsamount = (int) Math.round(Double.parseDouble(sEditVSavingsAmount[i]));
                            sEditVSavingsAmount[i] = String.valueOf(vSavingsamount);
                        }

                        if (Integer.parseInt(sEditSavingsAmounts[i]) < 0) {

                            mIsNegativeValues = true;
                        }
                        if (Integer.parseInt(sEditVSavingsAmount[i]) < 0) {

                            mIsNegativeValues = true;
                        }

                        EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings().get(i).setAmount(sEditSavingsAmounts[i]);
                        EMathiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings().get(i).setVoluntaryAmount(sEditVSavingsAmount[i]);

					/*sEditSendToServer_Savings = sEditSendToServer_Savings
							+ String.valueOf(member_Id.elementAt(i)) + "~" + sEditSavingsAmounts[i]
							+ ".00" + "#";

					sEditSendToServer_VSavings = sEditSendToServer_VSavings
							+ String.valueOf(member_Id.elementAt(i)) + "~" + sEditVSavingsAmount[i]
							+ ".00" + "#";

					editConfirmArr[i] = String.valueOf(member_Name.elementAt(i)) + "           "
							+ sEditSavingsAmounts[i] + "~" + sEditVSavingsAmount[i];*/

                        sEditSavings_Total = sEditSavings_Total + Integer.parseInt(sEditSavingsAmounts[i]);

                        sEditVSavings_Total = sEditVSavings_Total + Integer.parseInt(sEditVSavingsAmount[i]);

                        builder.append(sEditSavingsAmounts[i]).append(",");

                    }

                    Log.d(TAG, sEditSendToServer_Savings);

                    Log.d(TAG, sEditSendToServer_VSavings);

                    Log.d(TAG, "TOTAL " + String.valueOf(sEditSavings_Total));

                    Log.d(TAG, "VS TOTAL " + String.valueOf(sEditVSavings_Total));

                    // Do the SP insertion

                    // if ((sEditSavings_Total != 0) || (sEditVSavings_Total != 0))
                    // {
                    if (!mIsNegativeValues) {

                        confirmationDialog = new Dialog(this);

                        LayoutInflater inflater = this.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                        LayoutParams lParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                        dialogView.setLayoutParams(lParams);

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < editConfirmArr.length; i++) {

                            TableRow indv_SavingsRow = new TableRow(this);

                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(this);
                            memberName_Text.setText(GetSpanText.getSpanString(this,
                                    mSavingsArrayValues.get(i).getMemberName()));
                            memberName_Text.setTextColor(color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setSingleLine(true);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(this);
                            confirm_values.setText(GetSpanText.getSpanString(this, String.valueOf(sEditSavingsAmounts[i])));
                            confirm_values.setTextColor(color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            TextView confirm_VSvalues = new TextView(this);
                            confirm_VSvalues
                                    .setText(GetSpanText.getSpanString(this, String.valueOf(sEditVSavingsAmount[i])));
                            confirm_VSvalues.setTextColor(color.black);
                            confirm_VSvalues.setPadding(5, 5, 5, 5);
                            confirm_VSvalues.setGravity(Gravity.RIGHT);
                            confirm_VSvalues.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_VSvalues);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }
                        View rullerView = new View(this);
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(this);

                        TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT, 1f);
                        totalParams.setMargins(10, 5, 10, 5);

                        TextView totalText = new TextView(this);
                        totalText.setText(GetSpanText.getSpanString(this, String.valueOf(AppStrings.total)));
                        totalText.setTextColor(color.black);
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(totalParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(this);
                        totalAmount.setText(GetSpanText.getSpanString(this, String.valueOf(sEditSavings_Total)));
                        totalAmount.setTextColor(color.black);
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalAmount);

                        TextView totalVSAmount = new TextView(this);
                        totalVSAmount.setText(GetSpanText.getSpanString(this, String.valueOf(sEditVSavings_Total)));
                        totalVSAmount.setTextColor(color.black);
                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalVSAmount.setGravity(Gravity.RIGHT);
                        totalVSAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalVSAmount);

                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.mVerified);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.mIsNegativeOpeningBalance,
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        sEditSendToServer_Savings = "0";
                        sEditSendToServer_VSavings = "0";
                        sEditSavings_Total = 0;
                        sEditVSavings_Total = Integer.valueOf(nullVlaue);
                        mIsNegativeValues = false;
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit:

                sEditSendToServer_Savings = "0";
                sEditSavings_Total = Integer.valueOf(nullVlaue);
                sEditSendToServer_VSavings = "0";
                sEditVSavings_Total = Integer.valueOf(nullVlaue);
                mSubmit_RaisedButton.setClickable(true);

                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:

                confirmationDialog.dismiss();
                mIsNegativeValues = false;
                //publicValues.mEdit_OB_Sendtoserver_Savings = sEditSendToServer_Savings + "%" + sEditSendToServer_VSavings;

                if (EMathiApplication.vertficationDto.getShgInternalLoanListDTO() != null && EMathiApplication.vertficationDto.getShgInternalLoanListDTO().size() > 0) {

                    Log.i("print","if");
                    Intent intent = new Intent(EditOpeningBalanceSavingsActivity.this,
                            EditOpeningBalanceInternalloanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                    sEditSendToServer_Savings = "0";
                    sEditSavings_Total = Integer.valueOf(nullVlaue);
                    sEditSendToServer_VSavings = "0";
                    sEditVSavings_Total = Integer.valueOf(nullVlaue);

                } else if (EMathiApplication.vertficationDto.getMemberfinancialDetails() != null && EMathiApplication.vertficationDto.getMemberfinancialDetails().size() > 0) {

                    Log.i("print","else-if(1)");
                    Intent intent = new Intent(EditOpeningBalanceSavingsActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                    sEditSendToServer_Savings = "0";
                    sEditSavings_Total = Integer.valueOf(nullVlaue);
                    sEditSendToServer_VSavings = "0";
                    sEditVSavings_Total = Integer.valueOf(nullVlaue);

                } else if (EMathiApplication.vertficationDto.getGroupfinancialDetails() != null && EMathiApplication.vertficationDto.getGroupfinancialDetails().size() > 0) {

                    Log.i("print","else-if(2)");
                    Intent intent = new Intent(EditOpeningBalanceSavingsActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                    sEditSendToServer_Savings = "0";
                    sEditSavings_Total = Integer.valueOf(nullVlaue);
                    sEditSendToServer_VSavings = "0";
                    sEditVSavings_Total = Integer.valueOf(nullVlaue);

                } else if (EMathiApplication.vertficationDto.getShgBalanceDetailsDTO() != null ) {

                    Log.i("print","else-if(3)");
                    Intent intent = new Intent(EditOpeningBalanceSavingsActivity.this,
                            EditOpeningBalanceBankDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                    sEditSendToServer_Savings = "0";
                    sEditSavings_Total = Integer.valueOf(nullVlaue);
                    sEditSendToServer_VSavings = "0";
                    sEditVSavings_Total = Integer.valueOf(nullVlaue);

                }

                break;
        }

    }

    private void getTableRowHeaderCellWidth() {

        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {

            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }

        }
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        MenuItem logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist_edit) {

            item.setTitle(SS);

        }

        if (logOutItem.getItemId() == R.id.menu_logout_edit) {

            logOutItem.setTitle(logOutBuilder);
        }


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist_edit) {

            try {
                startActivity(new Intent(EditOpeningBalanceSavingsActivity.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.menu_logout_edit) {
            Log.e(" Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}