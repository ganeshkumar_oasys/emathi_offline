package com.oasys.emathi.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.oasys.emathi.R;

public class MainActivityTransaction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_transaction);
    }
}