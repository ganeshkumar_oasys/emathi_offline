package com.oasys.emathi.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.oasys.emathi.OasysUtils.RootUtil;
import com.oasys.emathi.R;
import com.oasys.emathi.receiver.OfflineTxReceiver;

public class SplashScreenActivity extends BaseActivity {

    public static final int SPLASH_TIME = 1500;

    private Thread mSplashScreenThread;
    ImageView mSplashImage;
    Animation animZoomOut;

    /* Device Admin */
    @SuppressWarnings("unused")
    private static final int ACTIVITY_RESULT_DEVICE_ADMIN = 808;
    ComponentName adminReceiver;
    DevicePolicyManager devicePolicyManager;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        RootUtil.isRooted();
        Log.e("root",""+  RootUtil.isRooted());

        // ** Exit Snippet **//
        if (getIntent().getBooleanExtra("EXIT", false)) {
            Log.v("Yes Logout", "Logout Working!!!!!!!!!!!!!!!!!");
            finish();
            System.exit(0);

        } else {

            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_splashscreen);

            /* Device Admin */

            @SuppressWarnings("unused")
            ImageView imgPoster = (ImageView) findViewById(R.id.logo);

            //	ConfigUtils.initConstants(this.getApplicationContext());

            try {

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        // Inclusion of MediaPlayer to start the Welcome note.
						/*MediaPlayer mediaPlayer = MediaPlayer.create(SplashScreenActivity.this, R.raw.welcome);
						mediaPlayer.start();*/

                    }
                }, 200);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mSplashScreenThread = new Thread() {

                @SuppressWarnings("finally")
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(SPLASH_TIME);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
/*						if (PrefUtils.getAppDeviceAdmin() == null) {
							PrefUtils.setAppDeviceAdmin("1");
							toggleDeviceAdmin();
						} else if (PrefUtils.getAppDeviceAdmin() != null) {
							String mAppDeviceAdmin = PrefUtils.getAppDeviceAdmin();
							if (mAppDeviceAdmin.equals("1")) {
								finish();
								startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
								overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

							}

						} else {*/
                      //  scheduleSessionAlarm();
                        finish();
                        startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                        /*}*/

                        return;
                    }

                }

            };

            mSplashScreenThread.start();
        }

    }

    public void scheduleSessionAlarm() {
        Log.e("TAG", "scheduleSessionAlarm called...");
		/*Intent intent = new Intent(getApplicationContext(), OfflineTxReceiver.class);
		final PendingIntent pIntent = PendingIntent.getBroadcast(this, OfflineTxReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		long firstMillis = System.currentTimeMillis();
		AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis, 30000, pIntent);*/


	/*	AlarmManager	alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(this, OfflineTxReceiver.class);
		final PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

// Set the alarm to start at CurrentTime.
		Calendar calendar = Calendar.getInstance();012.
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY,calendar.get(Calendar.HOUR));
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));

// setRepeating() lets you specify a precise custom interval--in this case,
// 20 minutes.
		alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
				1000 * 15, alarmIntent);*/
        AlarmManager alarmManager= (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, OfflineTxReceiver.class);
        final PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT < Build.VERSION_CODES.KITKAT)
            alarmManager.set(AlarmManager.RTC_WAKEUP, 1000 * 120, alarmIntent);
        else if (Build.VERSION_CODES.KITKAT <= SDK_INT && SDK_INT < Build.VERSION_CODES.M)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, 1000 * 120, alarmIntent);
        else if (SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 1000 * 120, alarmIntent);
        }
      //  OfflineTxReceiver.setAlarm(getApplicationContext(),true);

        //	JobUtil.scheduleJob(getApplicationContext());


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            synchronized (mSplashScreenThread) {
                mSplashScreenThread.notifyAll();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        synchronized (mSplashScreenThread) {
            mSplashScreenThread.notifyAll();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

/*	private void toggleDeviceAdmin() {
		if (devicePolicyManager.isAdminActive(adminReceiver)) {
			int count = 0;

			List<ComponentName> compNames = devicePolicyManager.getActiveAdmins();
			if (compNames != null) {
				for (ComponentName compName : compNames) {
					if (compName.equals(adminReceiver)) {
						devicePolicyManager.removeActiveAdmin(compName);
						count++;
					}
				}
			}

			Toast.makeText(getApplicationContext(), count + " device admin components deactivated", Toast.LENGTH_LONG)
					.show();

		} else {
			Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
			intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminReceiver);
			startActivityForResult(intent, ACTIVITY_RESULT_DEVICE_ADMIN);

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case ACTIVITY_RESULT_DEVICE_ADMIN:
			if (resultCode == Activity.RESULT_OK) {

				Toast.makeText(getApplicationContext(), "Device admin activated", Toast.LENGTH_LONG).show();
				finish();
				startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

			}
		}
	}*/
}
