package com.oasys.emathi.process;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.google.gson.Gson;
import com.oasys.emathi.Dto.AddBTDto;
import com.oasys.emathi.Dto.ExistingLoan;
import com.oasys.emathi.Dto.ExpensesTypeDtoList;
import com.oasys.emathi.Dto.ListOfShg;
import com.oasys.emathi.Dto.LoanDto;
import com.oasys.emathi.Dto.MemberList;
import com.oasys.emathi.Dto.OfflineDto;
import com.oasys.emathi.Dto.RequestDto.AttendanceRequestDto;
import com.oasys.emathi.Dto.RequestDto.GroupLoanRepaymentDto;
import com.oasys.emathi.Dto.RequestDto.ImageDto;
import com.oasys.emathi.Dto.RequestDto.MeetingAuditingDetailsDto;
import com.oasys.emathi.Dto.RequestDto.MemberloanRepayRequestDto;
import com.oasys.emathi.Dto.RequestDto.Members;
import com.oasys.emathi.Dto.RequestDto.MinutesId;
import com.oasys.emathi.Dto.RequestDto.MinutesofmeetingsRequestDto;
import com.oasys.emathi.Dto.RequestDto.RepaymentDetails;
import com.oasys.emathi.Dto.RequestDto.TrainingListId;
import com.oasys.emathi.Dto.RequestDto.TrainingRequestDto;
import com.oasys.emathi.Dto.ResponseDto;
import com.oasys.emathi.Dto.SavingRequest;
import com.oasys.emathi.Dto.offlineDto.SavingsDetails;
import com.oasys.emathi.OasysUtils.AppDialogUtils;
import com.oasys.emathi.OasysUtils.AppStrings;
import com.oasys.emathi.OasysUtils.Constants;
import com.oasys.emathi.OasysUtils.MySharedPreference;
import com.oasys.emathi.OasysUtils.NetworkConnection;
import com.oasys.emathi.OasysUtils.ServiceType;
import com.oasys.emathi.OasysUtils.Utils;
import com.oasys.emathi.Service.BaseSchedulerService;
import com.oasys.emathi.Service.NewTaskListener;
import com.oasys.emathi.Service.RestClient;
import com.oasys.emathi.database.LoanTable;
import com.oasys.emathi.database.SHGTable;
import com.oasys.emathi.database.TransactionTable;

import java.io.BufferedReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.oasys.emathi.database.LoanTable.updateILOSDetails;
import static com.oasys.emathi.database.LoanTable.updateMROSDetails;
import static com.oasys.emathi.database.SHGTable.updateBankCurrentBalanceDetails;
import static com.oasys.emathi.database.SHGTable.updateSHGGrouploanDetails;


public class TransactionProcess implements BaseSchedulerService, Serializable, NewTaskListener {


    SavingsDetails base;
    private int batteryLevel = 0;
    Context globalContext;
    static boolean register = false;
    BufferedReader in = null;
    private static long fpsId;
    String serverUrl = "";
    ArrayList<OfflineDto> savTrans;
    ArrayList<SavingRequest> savReqs, increq, expreq, ld_ilreq;
    private ArrayList<AddBTDto> btReqs, actoacReqs, savtoloanReqs, fdReqs, rdReqs;
    private ArrayList<MemberloanRepayRequestDto> mrReq, mr_il_Req;
    private ArrayList<AttendanceRequestDto> attReq;
    private ArrayList<MinutesofmeetingsRequestDto> momReq;
    private ArrayList<GroupLoanRepaymentDto> grpReq;
    private ArrayList<ImageDto> imageDtos;
    private ArrayList<MeetingAuditingDetailsDto> meetingAuditingDetailsDtos;
    private ArrayList<TrainingRequestDto> trainingRequestDtos;

    private ArrayList<OfflineDto> offlineDBData = new ArrayList<>();
    private List<ExistingLoan> memloanDetails;
    private ListOfShg shgDto;
    private String userId;

    public void process(Context context) {
        globalContext = context;
        startProcess();

    }

    private void startProcess() {
        //    registerBatteryReceiver();
        initializeValues();
        //    getCurrentLocation();
        boolean sessionInvalid = Transactiontask();
    }

    private void initializeValues() {
        base = new SavingsDetails();
    }


    public boolean Transactiontask() {
        boolean unauthorized = false;
        long primaryId = -1;
        try {

            savReqs = new ArrayList<>();
            increq = new ArrayList<>();
            expreq = new ArrayList<>();
            btReqs = new ArrayList<>();
            actoacReqs = new ArrayList<>();
            savtoloanReqs = new ArrayList<>();
            fdReqs = new ArrayList<>();
            rdReqs = new ArrayList<>();
            mrReq = new ArrayList<>();
            attReq = new ArrayList<>();
            ld_ilreq = new ArrayList<>();
            momReq = new ArrayList<>();
            mr_il_Req = new ArrayList<>();
            grpReq = new ArrayList<>();
            imageDtos = new ArrayList<>();
            meetingAuditingDetailsDtos = new ArrayList<>();
            trainingRequestDtos = new ArrayList<>();
            //savTrans = TransactionTable.getSavingTransaction();
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(globalContext, MySharedPreference.SHG_ID, ""));
            memloanDetails = LoanTable.getMemLoanDetails(shgDto.getShgId());
            userId = MySharedPreference.readString(globalContext, MySharedPreference.ANIMATOR_ID, "");

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.SAVING_COUNT, 0) > 0) {
                SavingRequest saveReq = new SavingRequest();
                ArrayList<MemberList> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.SAVING_COUNT, 0); i++) {
                    savTrans = TransactionTable.getSavingTransaction(i);
                    if (savTrans.size() > 0) {
                        for (int j = 0; j < savTrans.size(); j++) {
                            MemberList ml = new MemberList();
                            ml.setMemberId(savTrans.get(j).getMemberId());
                            ml.setSavingsAmount(savTrans.get(j).getSavingsAmount());
                            ml.setVoluntarySavingsAmount(savTrans.get(j).getVoluntarySavingsAmount());
                            sr.add(ml);
                        }
                        saveReq.setSavingsAmount(sr);
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setModeOfCash(savTrans.get(0).getModeOCash());
                        saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        savReqs.add(saveReq);
                    }
                }
            }

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.UPLOADSCHEDULE_COUNT, 0) > 0) {
                ImageDto imageDto =new ImageDto();
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.UPLOADSCHEDULE_COUNT, 0); i++) {
                    savTrans = TransactionTable.getUploadImages(i);
                    if (savTrans.size() > 0) {
                        imageDto.setId(savTrans.get(0).getShgId());
                        imageDto.setUrl(savTrans.get(0).getImageurl());
                        imageDtos.add(imageDto);
                    }
                }
            }



            if (MySharedPreference.readInteger(globalContext, MySharedPreference.AUDIT_COUNT, 0) > 0) {

                MeetingAuditingDetailsDto saveReq = new MeetingAuditingDetailsDto();

                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.AUDIT_COUNT, 0); i++) {
                    savTrans = TransactionTable.getAudit(i);
                    if (savTrans.size() > 0) {
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setFromDate(savTrans.get(0).getFromDate());
                        saveReq.setToDate(savTrans.get(0).getToDate());
                        saveReq.setAuditingDate(savTrans.get(0).getAuditingDate());
                        saveReq.setAuditorName(savTrans.get(0).getAuditorName());
                        meetingAuditingDetailsDtos.add(saveReq);
                    }
                }
            }


            if (MySharedPreference.readInteger(globalContext, MySharedPreference.TRAINING_COUNT, 0) > 0) {

                TrainingRequestDto saveReq = new TrainingRequestDto();
                ArrayList<TrainingListId> sr = new ArrayList<>();

                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.TRAINING_COUNT, 0); i++) {
                    savTrans = TransactionTable.getTraining(i);
                    if (savTrans.size() > 0) {
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setTrainingDate(savTrans.get(0).getTrainingDate());
                        for (int j = 0; j < savTrans.size(); j++) {
                            TrainingListId ml = new TrainingListId();
                            ml.setId(savTrans.get(j).getId());
                            sr.add(ml);
                        }
                        saveReq.setTrainingListId(sr);
                        trainingRequestDtos.add(saveReq);
                    }
                }
            }


            if (MySharedPreference.readInteger(globalContext, MySharedPreference.INCOME_COUNT, 0) > 0) {

                ArrayList<MemberList> sr = new ArrayList<>();
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.INCOME_COUNT, 0); i++) {
                    savTrans = TransactionTable.getIncomeTransaction(i);
                    if (savTrans.size() > 0) {
                        Log.i("print","Size : "+savTrans.size());
                        Log.i("print","Transaction Type : "+savTrans.get(0).getTxSubtype());
                        Log.i("print","Data : "+AppStrings.donation);
                        //if (savTrans.get(i).getTxSubtype().toUpperCase().equals(AppStrings.subscriptioncharges) || savTrans.get(i).getTxSubtype().toUpperCase().equals(AppStrings.otherincome) || savTrans.get(i).getTxSubtype().toUpperCase().equals(AppStrings.penalty)) {
                        if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.subscriptioncharges) || savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.otherincome) || savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.penalty)) {
//                            sr = new ArrayList<>();
                            for (int j = 0; j < savTrans.size(); j++) {
                                MemberList ml = new MemberList();
                                SavingRequest saveReq = new SavingRequest();
                                ml.setMemberId(savTrans.get(j).getMemberId());
                                ml.setAmount(savTrans.get(j).getAmount());
                                sr.add(ml);
                                saveReq.setIncomeTypeId(savTrans.get(j).getIncomeTypeId());
                                saveReq.setMemberSaving(sr);
                                saveReq.setAmount(savTrans.get(j).getOtherIncome());
                                saveReq.setShgId(savTrans.get(j).getShgId());
                                saveReq.setModeOfCash(savTrans.get(j).getModeOCash());
                                saveReq.setMobileDate(savTrans.get(j).getMobileDate());
                                saveReq.setTransactionDate(savTrans.get(j).getTransactionDate());

                                //   ml.setVoluntarySavingsAmount(savTrans.get(j).getVoluntarySavingsAmount());

                                increq.add(saveReq);

                            }


                        } else if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.mSeedFund)) {
                            //  saveReq.setSavingsAmount(sr);
                            SavingRequest saveReq = new SavingRequest();
                            saveReq.setShgId(savTrans.get(0).getShgId());
                            saveReq.setModeOfCash(savTrans.get(0).getModeOCash());
                            saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            saveReq.setModeOfCash(savTrans.get(0).getModeOCash());
                            saveReq.setShgSavingsAccountId((savTrans.get(0).getShgSavingsAccountId() != null && savTrans.get(0).getShgSavingsAccountId().length() > 0) ? savTrans.get(0).getShgSavingsAccountId() : null);
                            saveReq.setBankId((savTrans.get(0).getBankId() != null && savTrans.get(0).getBankId().length() > 0) ? savTrans.get(0).getBankId() : null);
                            saveReq.setIncomeTypeId(savTrans.get(0).getIncomeTypeId());
                            saveReq.setAmount(savTrans.get(0).getTotalIncomeAmount());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            increq.add(saveReq);

                        } else if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.donation)) {
                            SavingRequest saveReq = new SavingRequest();
                            saveReq.setShgId(savTrans.get(0).getShgId());
                            saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            saveReq.setModeOfCash(savTrans.get(0).getModeOCash());
                            saveReq.setShgSavingsAccountId(savTrans.get(0).getShgSavingsAccountId());
                            saveReq.setBankId((savTrans.get(0).getBankId() != null && savTrans.get(0).getBankId().length() > 0) ? savTrans.get(0).getBankId() : null);
                            saveReq.setIncomeTypeId(savTrans.get(0).getIncomeTypeId());
                            saveReq.setAmount(savTrans.get(0).getTotalIncomeAmount());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            increq.add(saveReq);
                        }
                    }
                    //  saveReq.setSavingsAmount(sr);

                }
            }

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.EXPENSE_COUNT, 0) > 0) {
                SavingRequest saveReq = new SavingRequest();
                ArrayList<ExpensesTypeDtoList> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.EXPENSE_COUNT, 0); i++) {
                    savTrans = TransactionTable.getExpenseTransaction(i);
                    if (savTrans.size() > 0) {
                        for (int j = 0; j < savTrans.size(); j++) {
                            ExpensesTypeDtoList ml = new ExpensesTypeDtoList();
                            ml.setExpensesTypeId(savTrans.get(j).getExpenseTypeId());
                            ml.setAmount(savTrans.get(j).getAmount());
                            sr.add(ml);
                        }
                        saveReq.setExpense(sr);
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setGroupId(savTrans.get(0).getShgId());
                        saveReq.setModeOfCash(savTrans.get(0).getModeOCash());
                        saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        expreq.add(saveReq);
                    }
                }
            }


            if (MySharedPreference.readInteger(globalContext, MySharedPreference.BT_COUNT, 0) > 0) {
                //  AddBTDto saveReq = new AddBTDto();
                ArrayList<ExpensesTypeDtoList> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.BT_COUNT, 0); i++) {
                    savTrans = TransactionTable.getBankTransaction(i);
                    AddBTDto saveReq = new AddBTDto();
                   /* for (int j = 0; j < savTrans.size(); j++) {
                        ExpensesTypeDtoList ml = new ExpensesTypeDtoList();
                        ml.setExpensesTypeId(savTrans.get(j).getMemberId());
                        ml.setAmount(savTrans.get(j).getSavingsAmount());
                        sr.add(ml);
                    }*/
                    if (savTrans.size() > 0) {
                        // TODO  :: validation  BT Validation pending
                        if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.recurringDeposit)) {

                            saveReq.setShgId(savTrans.get(0).getShgId());
                            saveReq.setSavingsBankId(savTrans.get(0).getFromSavingsBankDetailsId());
                            saveReq.setBankCharges(savTrans.get(0).getBtExpense());
                            saveReq.setBankInterest(savTrans.get(0).getBankInterest());
                            saveReq.setWithdrawal(savTrans.get(0).getWithdrawal());
                            saveReq.setCashDeposit(savTrans.get(0).getCashDeposit());
                            //  saveReq.setTransferCharges(savTrans.get(i).getTransferCharges());
                            //  saveReq.setTransferAmount(savTrans.get(i).getTransferAmount());
                            saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            saveReq.setInterestSubventionReceived(savTrans.get(0).getInterestSubventionReceived());
                            //    saveReq.setToSavingsBankDetailsId(savTrans.get(i).getToSavingsBankDetailsId());
                            if (savTrans.get(0).getSavingsBankDetailsId() != null) {
                                saveReq.setToBankId((savTrans.get(0).getSavingsBankDetailsId() != null && savTrans.get(0).getSavingsBankDetailsId().length() > 0) ? savTrans.get(0).getSavingsBankDetailsId() : null);
                                saveReq.setTransactionFlowType(0);
                            } else {
                                saveReq.setTransactionFlowType(1);
                                saveReq.setToBankId((savTrans.get(0).getLoanBankDetailsId() != null && savTrans.get(0).getLoanBankDetailsId().length() > 0) ? savTrans.get(0).getLoanBankDetailsId() : null);
                            }
                            rdReqs.add(saveReq);

                        } else if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.fixedDeposit)) {
                            saveReq.setShgId(savTrans.get(0).getShgId());
                            saveReq.setSavingsBankId(savTrans.get(0).getBankId());
                            saveReq.setBankCharges(savTrans.get(0).getBtExpense());
                            saveReq.setBankInterest(savTrans.get(0).getBankInterest());
                            saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            saveReq.setWithdrawal(savTrans.get(0).getWithdrawal());
                            saveReq.setCashDeposit(savTrans.get(0).getCashDeposit());
                            fdReqs.add(saveReq);

                        } else if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.mAccountToAccountTransfer)) {
                            saveReq.setShgId(savTrans.get(0).getShgId());
                            saveReq.setFromSavingsBankDetailsId(savTrans.get(0).getFromSavingsBankDetailsId());
                            saveReq.setTransferCharges(savTrans.get(0).getTransferCharge());
                            saveReq.setTransferAmount(savTrans.get(0).getTransferAmount());
                            saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            saveReq.setToSavingsBankDetailsId((savTrans.get(0).getToSavingsBankDetailsId() != null && savTrans.get(0).getToSavingsBankDetailsId().length() > 0) ? savTrans.get(0).getToSavingsBankDetailsId() : null);
                            saveReq.setSavingsBankDetailsId((savTrans.get(0).getSavingsBankDetailsId() != null && savTrans.get(0).getSavingsBankDetailsId().length() > 0) ? savTrans.get(0).getSavingsBankDetailsId() : null);
                            saveReq.setLoanBankDetailsId((savTrans.get(0).getLoanBankDetailsId() != null && savTrans.get(0).getLoanBankDetailsId().length() > 0) ? savTrans.get(0).getLoanBankDetailsId() : null);

//                            if (savTrans.get(0).getLoanBankDetailsId() == null && savTrans.get(0).getLoanBankDetailsId().length() <= 0) {
                            if (savTrans.get(0).getLoanBankDetailsId() != null && savTrans.get(0).getLoanBankDetailsId().length() <= 0) {
                                actoacReqs.add(saveReq);
                            } else {
                                savtoloanReqs.add(saveReq);
                            }
                        } else if (savTrans.get(0).getTxSubtype().toUpperCase().equals(AppStrings.bankTransaction)) {
                            saveReq.setShgId(savTrans.get(0).getShgId());
                            saveReq.setBankId((savTrans.get(0).getBankId() != null && savTrans.get(0).getBankId().length() > 0) ? savTrans.get(0).getBankId() : null);
                            saveReq.setBankCharges(savTrans.get(0).getBankCharges());
                            saveReq.setBankInterest(savTrans.get(0).getBankInterest());
                            saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                            saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                            saveReq.setWithdrawal(savTrans.get(0).getWithdrawal());
                            saveReq.setCashDeposit(savTrans.get(0).getCashDeposit());
                            saveReq.setInterestSubventionReceived(savTrans.get(0).getInterestSubventionReceived());
                            btReqs.add(saveReq);
                        }
                    }
                  /*  saveReq.setShgId(savTrans.get(i).getShgId());
                    saveReq.setBankId(savTrans.get(i).getBankId());
                    saveReq.setBankCharges(savTrans.get(i).getBankCharges());
                    saveReq.setBankInterest(savTrans.get(i).getBankInterest());
                    saveReq.setMobileDate(savTrans.get(i).getMobileDate());
                    saveReq.setTransactionDate(savTrans.get(i).getTransactionDate());
                    saveReq.setWithdrawal(savTrans.get(i).getWithdrawal());
                    saveReq.setCashDeposit(savTrans.get(i).getCashDeposit());
                    saveReq.setInterestSubventionReceived(savTrans.get(i).getInterestSubventionReceived());*/
                    //  btReqs.add(saveReq);
                }
            }

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.MR_COUNT, 0) > 0) {

                MemberloanRepayRequestDto saveReq = new MemberloanRepayRequestDto();
                ArrayList<RepaymentDetails> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.MR_COUNT, 0); i++) {
                    savTrans = TransactionTable.getMem_LR_Transaction(i);
                    if (savTrans.size() > 0) {
                        for (int j = 0; j < savTrans.size(); j++) {
                            RepaymentDetails ml = new RepaymentDetails();
                            ml.setRepaymentAmount(savTrans.get(j).getRepaymentAmount());
                            ml.setMemberId(savTrans.get(j).getMemberId());
                            ml.setInterest(savTrans.get(j).getInterest());
                            sr.add(ml);

                        }
                        saveReq.setRepaymentDetails(sr);
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setLoanUuid(savTrans.get(0).getLoanUuid());
                        saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        Log.d("date",""+savTrans.get(0).getTransactionDate());
                        mrReq.add(saveReq);


                    }

                }

            }

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.MR_IL_COUNT, 0) > 0) {

                MemberloanRepayRequestDto saveReq = new MemberloanRepayRequestDto();
                ArrayList<RepaymentDetails> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.MR_IL_COUNT, 0); i++) {
                    savTrans = TransactionTable.getMem_LR_IL_Transaction(i);
                    if (savTrans.size() > 0) {
                        for (int j = 0; j < savTrans.size(); j++) {
                            RepaymentDetails ml = new RepaymentDetails();
                            ml.setRepaymentAmount(savTrans.get(j).getRepaymentAmount());
                            ml.setMemberId(savTrans.get(j).getMemberId());
                            ml.setInterest(savTrans.get(j).getInterest());
                            sr.add(ml);
                        }
                        saveReq.setRepaymentDetails(sr);
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setLoanUuid(savTrans.get(0).getLoanUuid());
                        saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        mr_il_Req.add(saveReq);
                    }
                }

            }


            if (MySharedPreference.readInteger(globalContext, MySharedPreference.LD_COUNT, 0) > 0) {

                SavingRequest saveReq = new SavingRequest();
                ArrayList<LoanDto> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.LD_COUNT, 0); i++) {
                    savTrans = TransactionTable.getLD_IL_Transaction(i);
                    if (savTrans.size() > 0) {
                        for (int j = 0; j < savTrans.size(); j++) {
                            LoanDto ml = new LoanDto();
                            ml.setInternalLoanAmount(savTrans.get(j).getInternalLoanAmount());
                            ml.setMemberId(savTrans.get(j).getMemberId());
                            ml.setTenure(savTrans.get(j).getTenure());
                            ml.setPloanTypeId(savTrans.get(j).getPloanTypeId());
                            sr.add(ml);
                        }
                        saveReq.setLoanDetails(sr);
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        ld_ilreq.add(saveReq);
                    }
                }
            }

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.ATTND_COUNT, 0) > 0) {

                AttendanceRequestDto saveReq = new AttendanceRequestDto();
                ArrayList<Members> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.ATTND_COUNT, 0); i++) {
                    savTrans = TransactionTable.getAttendanceData(i);
                    if (savTrans.size() > 0) {
                        for (int j = 0; j < savTrans.size(); j++) {
                            Members ml = new Members();
                            ml.setMemberId(savTrans.get(j).getMemberId());
                            sr.add(ml);
                        }
                        saveReq.setMembers(sr);
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        attReq.add(saveReq);
                    }
                }
            }

            if (MySharedPreference.readInteger(globalContext, MySharedPreference.GRP_COUNT, 0) > 0) {

                GroupLoanRepaymentDto saveReq = new GroupLoanRepaymentDto();
                ArrayList<Members> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.GRP_COUNT, 0); i++) {
                    savTrans = TransactionTable.getGrpRepayment(i);
                    if (savTrans.size() > 0) {
                        saveReq.setShgId(savTrans.get(0).getShgId());
                        saveReq.setLoanId(savTrans.get(0).getLoanId());
                        saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                        saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        saveReq.setInterest(savTrans.get(0).getGrp_interest());
                        saveReq.setCharges(savTrans.get(0).getGrp_charge());
                        saveReq.setRepaymentAmount(savTrans.get(0).getGrp_repayment());
                        saveReq.setInterestSubvensionRecevied(savTrans.get(0).getGrp_intSubventionRecieved());
                        saveReq.setModeOfCash(savTrans.get(0).getModeOCash());
                        saveReq.setSbAccountId((savTrans.get(0).getShgSavingsAccountId() != null && savTrans.get(0).getShgSavingsAccountId().length() > 0) ? savTrans.get(0).getShgSavingsAccountId() : null);
                        saveReq.setBankCharges(savTrans.get(0).getBankCharges());
                        // saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                        grpReq.add(saveReq);
                    }
                }
            }


            if (MySharedPreference.readInteger(globalContext, MySharedPreference.MoM_COUNT, 0) > 0) {

                MinutesofmeetingsRequestDto saveReq = new MinutesofmeetingsRequestDto();
                ArrayList<MinutesId> sr = new ArrayList<>();
                String shgId = "", modeOfCash = "", mDate = "", tDate = "";
                savTrans = new ArrayList<>();
                for (int i = 1; i <= MySharedPreference.readInteger(globalContext, MySharedPreference.MoM_COUNT, 0); i++) {
                    savTrans = TransactionTable.getMoMeData(i);
                    for (int j = 0; j < savTrans.size(); j++) {
                        MinutesId ml = new MinutesId();
                        ml.setId(savTrans.get(j).getId());
                        sr.add(ml);
                    }
                    saveReq.setMinutesId(sr);
                    saveReq.setShgId(savTrans.get(0).getShgId());
                    saveReq.setTransactionDate(savTrans.get(0).getTransactionDate());
                    saveReq.setMobileDate(savTrans.get(0).getMobileDate());
                    momReq.add(saveReq);
                }
            }


            if (NetworkConnection.getNetworkConnection(globalContext).isNetworkAvailable()) {
                String url = Constants.BASE_URL+Constants.OFFLINE_API;
                base.setSavingsDetails(savReqs);
                base.setImageUpload(imageDtos);
                base.setMeetingAuditingDetailsDto(meetingAuditingDetailsDtos);
                base.setTrainingRequestDto(trainingRequestDtos);
                base.setIncomeDetails(increq);
                base.setExpenseDetails(expreq);
                base.setBankTransactionDetails(btReqs);
                base.setRecurringDepositDetails(rdReqs);
                base.setFixedDepositDetails(fdReqs);
                base.setAccountToAccountDetails(actoacReqs);
                base.setSavingsAccountToLoanAccountDetails(savtoloanReqs);
                base.setInternalLoanDetails(ld_ilreq);
                base.setGroupLoanRepaymentDetails(grpReq);
                base.setInternalLoanRepaymentDetails(mr_il_Req);
                base.setMemberLoanRepaymentDetails(mrReq);
                base.setShgMeetingAttendanceDetails(attReq);
                base.setMinutesOfMeetingDetails(momReq);
                base.setUserId(userId);
                String req_str = new Gson().toJson(base);
                Log.e(this.getClass().getName(), req_str);


                RestClient.getRestClient(this).callRestWebService(url, req_str, globalContext, ServiceType.SYNC_STORAGE);
            }
        } catch (Exception e) {
            // update response into local db
            e.printStackTrace();
            Log.e(this.getClass().getName(), e.getMessage());
            //unregisterBatteryReceiver();

        }
        return unauthorized;
    }

    // Broadcast receiver for battery
    private final BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // Unregistering battery receiver
            if (register) {
                try {
                    context.unregisterReceiver(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                register = false;
            }

            int currentLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            if (currentLevel >= 0 && scale > 0) {
                batteryLevel = (currentLevel * 100) / scale;
            }
        }
    };

    private void registerBatteryReceiver() {
        // Registering battery receiver
        try {
            if (!register) {
                Log.e(this.getClass().getName(), "battery registered");
                IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                globalContext.registerReceiver(batteryLevelReceiver, batteryLevelFilter);
                register = true;
            }
        } catch (Exception e) {
        }
    }

    private void unregisterBatteryReceiver() {
        // Unregistering battery receiver
        if (register) {
            try {
                Log.e(this.getClass().getName(), "battery un-registered");
                globalContext.unregisterReceiver(batteryLevelReceiver);
            } catch (Exception e) {
            }
            register = false;
        }
    }

    private void updateMROS() {
        for (OfflineDto ofdto : offlineDBData) {
            updateMROSDetails(ofdto);
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case SYNC_STORAGE:
                try {
                    if (result != null) {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            //    unregisterBatteryReceiver();
                            Log.e(this.getClass().getName(), "response success");

                            if (MySharedPreference.readInteger(globalContext, MySharedPreference.GRP_COUNT, 0) > 0) {
                                for (int i = 0; i < cdto.getResponseContent().getGroupLoanList().size(); i++) {
                                    String loanid = cdto.getResponseContent().getGroupLoanList().get(i).getLoanId();
                                    String outstanding = cdto.getResponseContent().getGroupLoanList().get(i).getLoanOutstanding();
                                    OfflineDto offlineDto = new OfflineDto();
                                    offlineDto.setLoanId(loanid);
                                    offlineDto.setOutStanding(outstanding);
                                    updateSHGGrouploanDetails(offlineDto);
                                }
                                    if(cdto.getResponseContent().getBankTransactionList()!=null) {
                                        for (int j = 0; j < cdto.getResponseContent().getBankTransactionList().size(); j++) {
                                            OfflineDto offlineDto = new OfflineDto();
                                            String savbankid = cdto.getResponseContent().getBankTransactionList().get(j).getShgSavingsAccountId();
                                            String currentbal = cdto.getResponseContent().getBankTransactionList().get(j).getCurrentBalance();
                                            offlineDto.setAccountNumber(savbankid);
                                            offlineDto.setCurr_balance1(currentbal);
                                            SHGTable.updateSelectedBankCurrentBalanceDetailsoffline(offlineDto);

                                        }
                                }
                            }

                            if (MySharedPreference.readInteger(globalContext, MySharedPreference.MR_COUNT, 0) > 0) {
                                if(cdto.getResponseContent().getMemberLoanList()!=null) {
                                    for (int i = 0; i < cdto.getResponseContent().getMemberLoanList().size(); i++) {
                                        String loanid = cdto.getResponseContent().getMemberLoanList().get(i).getLoanId();
                                        String memberid = cdto.getResponseContent().getMemberLoanList().get(i).getMemberId();
                                        String outstanding = cdto.getResponseContent().getMemberLoanList().get(i).getMemberLoanOutstanding();
                                        OfflineDto offlineDto = new OfflineDto();
                                        offlineDto.setLoanId(loanid);
                                        offlineDto.setMemberId(memberid);
                                        offlineDto.setMem_os(outstanding);
                                        updateMROSDetails(offlineDto);
                                    }
                                }
                            }

                            if (MySharedPreference.readInteger(globalContext, MySharedPreference.BT_COUNT, 0) > 0) {
                                if(cdto.getResponseContent().getBankTransactionList()!=null) {
                                    for (int i = 0; i < cdto.getResponseContent().getBankTransactionList().size(); i++) {
                                        String savbankid = cdto.getResponseContent().getBankTransactionList().get(i).getShgSavingsAccountId();
                                        String currentbal = cdto.getResponseContent().getBankTransactionList().get(i).getCurrentBalance();
                                        String fdvalue = cdto.getResponseContent().getBankTransactionList().get(i).getCurrentFixedDeposit();
                                        String rdvalue = cdto.getResponseContent().getBankTransactionList().get(i).getRecurringDepositedBalance();
                                        OfflineDto offlineDto = new OfflineDto();
                                        offlineDto.setAccountNumber(savbankid);
                                        offlineDto.setCurr_balance(currentbal);
                                        offlineDto.setFd_value(fdvalue);
                                        offlineDto.setRd_value(rdvalue);
                                        updateBankCurrentBalanceDetails(offlineDto);
                                        SHGTable.updateFDDetails(offlineDto);
                                        SHGTable.updateRDDetails(offlineDto);
                                    }
                                }
                            }

                            if (MySharedPreference.readInteger(globalContext, MySharedPreference.LD_COUNT, 0) > 0) {
                                if (cdto.getResponseContent().getDisbursementList() != null) {
                                    for (int i = 0; i < cdto.getResponseContent().getDisbursementList().size(); i++) {
                                        OfflineDto offlineDto = new OfflineDto();
                                        offlineDto.setMem_os(cdto.getResponseContent().getDisbursementList().get(i).getLoanOutstanding());
                                        offlineDto.setMemberId(cdto.getResponseContent().getDisbursementList().get(i).getMemberId());
                                        updateILOSDetails(offlineDto);
                                    }
                                }
                            }

                            MySharedPreference.writeInteger(globalContext, MySharedPreference.SAVING_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.INCOME_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.EXPENSE_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.BT_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.MR_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.GRP_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.MR_IL_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.LD_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.ATTND_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.MoM_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.UPLOADSCHEDULE_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.AUDIT_COUNT, 0);
                            MySharedPreference.writeInteger(globalContext, MySharedPreference.TRAINING_COUNT, 0);
                            TransactionTable.deleteTransactionTable();
                            Utils.showToast(globalContext,message);
                            AppDialogUtils.mProgressDialog.dismiss();
                        } else {
                            //    unregisterBatteryReceiver();
                            Log.e(this.getClass().getName(), "response failure");
                            Utils.showToast(globalContext,message);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(this.getClass().getName(), e.getMessage());
                }
                break;
        }

    }
}
