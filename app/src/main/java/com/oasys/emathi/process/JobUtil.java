package com.oasys.emathi.process;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import com.oasys.emathi.Service.TransactionService;

public class JobUtil {
    public static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, TransactionService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(1 * 1000); // wait at least
        builder.setOverrideDeadline(60* 1000); // maximum delay
      //builder.setPeriodic( 30000 );
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
      //  builder.setRequiresDeviceIdle(false); // device should be idle
        builder.setPersisted(true);
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());

        if( jobScheduler.schedule( builder.build() ) <= 0 ) {
            //If something goes wrong
            Log.e(context.getClass().getName(),"Job Scheduler crashed");

        }


    }
}
